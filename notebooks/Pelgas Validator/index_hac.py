"""
Index HAC files and create a metadata file with start and stop time of each file
"""

from pathlib import Path
from typing import List, Tuple, Dict
import os
import sys
from os import path
import numpy as np
import pandas as pd
from tqdm import tqdm
from pyMovies import pyMovies as mv

from pymovies_3d.core.echointegration.ei_gridding_cpp import stdout_redirected

sys.path.append(path.dirname(path.dirname(path.abspath("__file__"))))


def decode_ping_time(movies_ping) -> np.datetime64:
    """Decode ping time from movies ping object"""
    ping_time = np.int64(0)
    ping_time += movies_ping.m_meanTime.m_TimeCpu * 1e9
    ping_time += movies_ping.m_meanTime.m_TimeFraction * 1e5
    ping_time = int(
        ping_time.astype(np.int64)
    )  # numpy does not like np.int64 for datetime64
    return np.datetime64(ping_time, "ns")


def read_ping(index) -> tuple[np.datetime64, str]:
    ping = mv.moGetPingFan(index)
    # hack to print the time and transducer
    time = decode_ping_time(ping)
    trans_name = ping.m_pSounder.GetTransducer(0).m_transName

    return time, trans_name


# create empty dataframe


class Metadata:
    """Metadata container"""

    def __init__(self, file_path: Path) -> None:
        self.file_path = file_path
        if os.path.exists(self.file_path):
            self.dataframe = pd.read_pickle(self.file_path)
        else:
            self.dataframe = pd.DataFrame({"file": [], "start": [], "stop": []})
        self.knownfiles: List[str] = [str(f) for f in self.dataframe["file"].values]

    def save(self):
        """Save metadata to file"""
        self.dataframe.to_pickle(self.file_path)

    def append(self, file_metadata: pd.DataFrame):
        """append metadata to the current dataframe"""
        self.dataframe = pd.concat([self.dataframe, file_metadata])
        self.knownfiles.extend([str(v) for v in file_metadata["file"]])

    def contains(self, file: Path):
        """Check if a file is already indexed.

        This method verifies if the given file path exists in the known files index.

        Args:
            file (Path): Path object representing the file to check

        Returns:
            bool: True if file exists in index, False otherwise

        Example:
            >>> index.contains(Path('my/file.mp4'))
            True
        """
        return str(file) in self.knownfiles


def read_file(file_name: Path) -> pd.DataFrame:
    if not file_name.exists():
        raise FileNotFoundError(f"File {file_name} does not exist")

    try:
        with stdout_redirected(to=os.devnull):  # remove all stack traces
            # update reader parameters to increase speed => NO IMPACT ON SPEED
            #             param = mv.moLoadReaderParameter()
            #             param.m_ChunckDef.m_chunkNumberOfPingFan = 10000
            #             mv.moSaveReaderParameter(param)
            # #            mv.moLoadConfig(file)
            #             print(f"Chunck Size = {mv.moLoadReaderParameter().m_ChunckDef.m_chunkNumberOfPingFan}")
            file = mv.moOpenHac(str(file_name))

            fileStatus = mv.moGetFileStatus()
            # check that FileStatus exist and is not None
            while not fileStatus.m_StreamClosed:
                mv.moReadChunk()
            nbPing = mv.moGetNumberOfPingFan()

        # find first ping with 18kHz transducer
        key_to_find = "18"
        # try 10 ping
        time_start = None
        for i in range(10):
            time_, trans_name = read_ping(i)
            if key_to_find in trans_name:
                time_start, trans_name_start = time_, trans_name
                break
        if time_start is None:
            raise ValueError(f"Transducer {key_to_find} not found in file {file_name}")

        # try 10 ping
        time_stop = None
        for i in range(10):
            time_, trans_name = read_ping(nbPing - 1 - i)
            if key_to_find in trans_name:
                time_stop, trans_name_stop = time_, trans_name
                break
        if not time_stop:
            raise ValueError(f"Transducer {key_to_find} not found in file {file_name}")
        return pd.DataFrame(
            {"file": [file_name], "start": [time_start], "stop": [time_stop]}
        )
    except Exception as e:
        raise ValueError(f"Error reading file {file_name} : {e}") from e

    # save in a dataframe


# recurse input directory and read all files
# use tree walk to avoid recursion limit
def read_directory(input_dir: Path, metadata: Metadata) -> List[Exception]:
    # retrieve all files in directory
    errors: list[Exception] = []

    # recurse subdirectories
    for directory, dirnames, file_names in os.walk(input_dir):
        for file in tqdm(file_names, desc=f"Processing files in {Path(directory)}"):
            file = Path(directory) / file
            if file.suffix == ".hac" and not metadata.contains(
                file
            ):  # do not index if already done
                try:
                    metadata_f = read_file(Path(directory) / file)
                    metadata.append(metadata_f)
                except Exception as e:
                    errors.append(e)
        # save every time a directory is read
        metadata.save()
    return errors


class Overlap:
    def __init__(
        self, file1: Path, file2: Path, stop1: np.datetime64, start2: np.datetime64
    ):
        self.file1 = file1
        self.file2 = file2
        self.stop1 = stop1
        self.start2 = start2
        self.duration = stop1 - start2


def compute_overlaps(
    metadata: Metadata, time_tolerance_s=0.0
):  # Sort dataframe by start time
    sorted_df = metadata.dataframe.sort_values(by="start")

    # Check for overlaps between consecutive rows
    overlaps: List[Overlap] = []
    for i in range(len(sorted_df) - 1):
        current_stop = sorted_df.iloc[i]["stop"]
        next_start = sorted_df.iloc[i + 1]["start"]
        if current_stop - next_start > pd.Timedelta(time_tolerance_s, unit="s"):
            overlaps.append(
                Overlap(
                    file1=sorted_df.iloc[i]["file"],
                    stop1=current_stop,
                    file2=sorted_df.iloc[i + 1]["file"],
                    start2=next_start,
                )
            )
    return overlaps



if __name__ == "__main__":
    pickle_file: Path = Path(__file__).parent / "test2_metadata_hac_timestamps.pkl"

    # for test only
    if pickle_file.exists():
        os.remove(pickle_file)

    input_dir: Path = Path("D:/PELGAS2024/Acoustique/HAC_Cor/TEST")
    metadata = Metadata(file_path=pickle_file)
    # measure time elaspse
    time_start = pd.Timestamp.now()
    errors = read_directory(input_dir, metadata=metadata)

    time_stop = pd.Timestamp.now()
    print(f"Elapsed time: {time_stop - time_start}")

    # Sort dataframe by start time
    sorted_df = metadata.dataframe.sort_values(by="start")

    time_tolerance_s = 0.0

    # Check for overlaps between consecutive rows
    overlaps = compute_overlaps(metadata, time_tolerance_s)
