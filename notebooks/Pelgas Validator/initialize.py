import matplotlib.pyplot as plt
import numpy as np
import math
import matplotlib.pyplot as plt
from os import sys, path
from typing import Dict
from pathlib import Path

#add pymovies_3d to path
sys.path.append(str(Path("__file__").absolute().parent.parent.parent))
from pymovies_3d.studies.data_preprocessing.main_data_preprocessing import PreprocessingParameters
from pymovies_3d.core.survey import man_tot
from pymovies_3d.core.util.report import Report
from pymovies_3d.studies.data_preprocessing.preprocess_parameters import make_as_usual
from pymovies_3d.studies.data_preprocessing.preprocess_parameters import ValidationInputPathParameters
from pymovies_3d.core.util.report import Report

plt.rcParams['figure.dpi'] = 270

year = 2024

input_root_path =Path(r"D:\PELGAS2024")
mantot_file = input_root_path / Path("EvaluationBiomasse/Donnees/Acoustique/mantot38.csv")


validation_path,sequence_path, workdir_path,ei_paths =  make_as_usual(year=year,root_dir=input_root_path.parent)

metadata_database: Path  = workdir_path.data_model_path / Path("metadata_database.pkl")

thresholds: list[int] = [-60,-80]
ei_path_dict:Dict[int,Path]= {}
for th in thresholds:
    ei_path_dict[th]=input_root_path /  Path("Acoustique/echointegration/tmp") / f"{th}"



from pymovies_3d.studies.data_preprocessing.preprocess_parameters import EchoIntegrationPaths

ei_paths = EchoIntegrationPaths(echointegration_files_60=ei_path_dict[-60],echointegration_files_80=ei_path_dict[-80])

preproc_param = PreprocessingParameters(year=year,input_paths=validation_path,
                        sequence_file=sequence_path,workdir_path=workdir_path,ei_path=ei_paths, use_ei_files=True)