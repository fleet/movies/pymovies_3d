# -*- coding: utf-8 -*-
"""
Created on Thu Jun 10 16:25:56 2021

Compare les détections EK80 FM de deux fichiers, de type xml de calibration ou résultat de détection M3d (samplecomputeTS)

    renseigner les noms de fichiers à comparer et le cas échéant les numéros de sondeur/transducteur
           
@author: nlebouff
"""
import pickle
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pymovies_3d.core.calibration.read_cal_EK80 import read_cal_EK80

# file 1 - calibration XML or samplecomputeTS pickle
####################################################
#file1='F:/Campagnes/ESSTECH21_Thalassa/EK80/resultats_etalonnage/CalibrationDataFile-D20210425-T084154_38kHz_CW_replay_v1p12p4.xml'
file1='F:/Movies3D_script/pymovies_3d/pymovies_3d/test/TS_unit_test/Ref_data/CalibrationDataFile-ESSTEC17_CW38kHz.xml'
#file1='F:/Campagnes/ESSTECH21_Thalassa/EK80/resultats_etalonnage/CalibrationDataFile-D20210425-T084154_38kHz_CW_replay_SvAngle.xml'
#file1='F:/Campagnes/ESSTECH21_Thalassa/EK80/resultats_etalonnage/CalibrationDataFile-D20210425-T093602_70khzCW_replay_SvAngle.xml'
#file1='F:/data/PELGAS19/RUN031/ES70_cal/CalibrationDataFile-D20191114-T112416_CW_70kHz_bille38mm_replay.xml'
# sounder and transducer index if samplecomputeTS pickle
isdr1=0
itsd1=1

# file 2 - calibration XML or samplecomputeTS pickle
# ######################################################
# file2='F:/Campagnes/ESSTECH21_Thalassa/EK80/HAC/70kHz_FM_ESSTECH21_b_results_TS.pickle'
# file2='F:/data/PbAngles_HACetRAW_Wbat38_3secteurs/38_FM_c_results_TS.pickle'
# file2='F:/Campagnes/ESSTECH21_Thalassa/EK80/HAC/ES38_CW/ES38_CW_results_TS.pickle'
# file2='F:/Campagnes/ESSTECH21_Thalassa/EK80/HAC/ES38_CW/Replay_SvAngle/ES38_CW_SvAngle_results_TS.pickle'
file2='F:/Movies3D_script/pymovies_3d/pymovies_3d/test/TS_unit_test/Ref_data/Cal_CW_ESSTECH17_results_TS.pickle'
file2='F:/Movies3D_script/pymovies_3d/pymovies_3d/test/TS_unit_test/Ref_data/CalibrationDataFile-ESSTEC17_CW38kHz_LR.xml'
#file2='F:/Campagnes/ESSTECH21_Thalassa/EK80/resultats_etalonnage/CalibrationDataFile-D20210425-T084154_38kHz_CW_replay_v1p12p4.xml'
#file2='F:/Campagnes/ESSTECH21_Thalassa/EK80/HAC/ES70_CW/ES70_CW_SvAngle_results_TS.pickle'
#file2='F:/data/PELGAS19/RUN031/ES70_cal/ES70_CW_SvAngle_results_TS.pickle'
# sounder and transducer index if samplecomputeTS pickle
isdr2=0
itsd2=1

# reading
#########

# detections 1
if file1[-4:]=='.xml':
    [time_det,range_det,freq,ts,tsu,al_det,at_det,sA,tsf,tsuf]=read_cal_EK80(file1)
    t_ms=np.array([round(time_deti.timestamp()*1000) for time_deti in time_det])
    df_det1=pd.DataFrame(t_ms,columns=['time ms'])
    df_det1.insert(len(df_det1.columns),'range',range_det,True)
    df_det1.insert(len(df_det1.columns),'along',al_det,True)
    df_det1.insert(len(df_det1.columns),'athwart',at_det,True)
    df_det1.insert(len(df_det1.columns),'TS',ts,True)
    df_det1.insert(0,'det1 index',range(0,len(df_det1)),True)
    freq1=freq
else:
    with open(file1,'rb') as f:
        [timeTarget,TSrange,TScomp,TSucomp,TSalong,TSathwart,TSposition,TSpositionGPS,TSlabel,TSfreq,name_sdr_tsd]=pickle.load(f)
    #[timeTarget,TSrange,TScomp,TSucomp,TSalong,TSathwart,TSposition,TSpositionGPS,TSlabel,TSfreq,name_sdr_tsd]=pickle.load(open(file1,'rb'))
    t_ms=np.array([round(timeTargeti.timestamp()*1000) for timeTargeti in timeTarget[isdr1][itsd1]])
    df_det1=pd.DataFrame(t_ms,columns=['time ms'])
    df_det1.insert(len(df_det1.columns),'range',TSposition[isdr1][itsd1][:,2],True)
    #df_det1.insert(len(df_det1.columns),'range',TSrange[isdr1][itsd1],True)
    df_det1.insert(len(df_det1.columns),'along',TSalong[isdr1][itsd1]*180/np.pi,True)
    df_det1.insert(len(df_det1.columns),'athwart',TSathwart[isdr1][itsd1]*180/np.pi,True)
    df_det1.insert(len(df_det1.columns),'TS',TScomp[isdr1][itsd1],True)
    df_det1.insert(0,'det1 index',range(0,len(df_det1)),True)
    freq1=TSfreq[isdr1][itsd1][0,:] #on suppose qu'on ne change pas de bande de fréquence sur le fichier

# detections 2
if file2[-4:]=='.xml':
    [time_det,range_det,freq,ts,tsu,al_det,at_det,sA,tsf,tsuf]=read_cal_EK80(file2)
    t_ms=np.array([round(time_deti.timestamp()*1000) for time_deti in time_det])
    df_det2=pd.DataFrame(t_ms,columns=['time ms'])
    df_det2.insert(len(df_det2.columns),'range',range_det,True)
    df_det2.insert(len(df_det2.columns),'along',al_det,True)
    df_det2.insert(len(df_det2.columns),'athwart',at_det,True)
    df_det2.insert(len(df_det2.columns),'TS',ts,True)
    df_det2.insert(0,'det2 index',range(0,len(df_det2)),True)
    freq2=freq
else:
    with open(file2,'rb') as f:
        [timeTarget,TSrange,TScomp,TSucomp,TSalong,TSathwart,TSposition,TSpositionGPS,TSlabel,TSfreq,name_sdr_tsd]=pickle.load(f)
    #[timeTarget,TSrange,TScomp,TSucomp,TSalong,TSathwart,TSposition,TSpositionGPS,TSlabel,TSfreq,name_sdr_tsd]=pickle.load(open(file2,'rb'))
    t_ms=np.array([round(timeTargeti.timestamp()*1000) for timeTargeti in timeTarget[isdr2][itsd2]])
    df_det2=pd.DataFrame(t_ms,columns=['time ms'])
    df_det2.insert(len(df_det2.columns),'range',TSposition[isdr2][itsd2][:,2],True)
    #df_det2.insert(len(df_det2.columns),'range',TSrange[isdr2][itsd2],True)
    df_det2.insert(len(df_det2.columns),'along',TSalong[isdr2][itsd2]*180/np.pi,True)
    df_det2.insert(len(df_det2.columns),'athwart',TSathwart[isdr2][itsd2]*180/np.pi,True)
    df_det2.insert(len(df_det2.columns),'TS',TScomp[isdr2][itsd2],True)
    df_det2.insert(0,'det2 index',range(0,len(df_det2)),True)
    freq2=TSfreq[isdr2][itsd2][0,:] #on suppose qu'on ne change pas de bande de fréquence sur le fichier
    

# processing/display
####################
# merge les détections en fonction du timestamp
df_merge=pd.merge(df_det1,df_det2,on='time ms')

det_com=df_merge
det_com=det_com[abs(det_com['range_x']-det_com['range_y'])<0.5] # garde les détections proches en range
print('pourcentage de détections communes (instant et range) : \n' + str(round(100*len(det_com)/len(df_det1))) +' % file 1 \n' + str(round(100*len(det_com)/len(df_det2))) +'% file 2')
ind1=det_com['det1 index']
ind2=det_com['det2 index']

#delta des TS FM : delta_ts[idet,ifreq] échantillonné à freq1
#delta_ts=np.array([TSf1[ind1.iloc[idet],:]-np.interp(freq1,freq2,TSf2[ind2.iloc[idet]]) for idet in range(len(det_com))])

print('médiane différence de range: ' + str(round(100*np.median(det_com['range_x']-det_com['range_y']))) + 'cm')
print('std différence de range: ' + str(round(100*np.std(det_com['range_x']-det_com['range_y']))) + 'cm')
print('médiane différence along: ' + str(round(100*np.median(det_com['along_x']-det_com['along_y']))/100) + '°')
print('std différence de along: ' + str(round(100*np.std(det_com['along_x']-det_com['along_y']))/100) + '°')
print('médiane différence athwart: ' + str(round(100*np.median(det_com['athwart_x']-det_com['athwart_y']))/100) + '°')
print('std différence de athwart: ' + str(round(100*np.std(det_com['athwart_x']-det_com['athwart_y']))/100) + '°')
print('médiane différence TS: ' + str(np.median(det_com['TS_x']-det_com['TS_y'])) + 'dB')
# plt.figure()
# plt.plot(freq1/1000,np.median(delta_ts,axis=0))
# plt.title('médiane de TS1(f)-TS2(f)')
# plt.xlabel('kHz')
# plt.ylabel('dB')
# plt.grid()

# plt.figure()
# plt.plot(freq1/1000,np.std(delta_ts,axis=0))
# plt.title('std de TS1(f)-TS2(f)')
# plt.xlabel('kHz')
# plt.ylabel('dB')
# plt.grid()
