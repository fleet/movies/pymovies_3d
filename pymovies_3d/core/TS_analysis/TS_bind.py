﻿#!/usr/bin/python
# -*- coding: utf-8 -*-

import os

import pickle
import numpy as np


def TS_bind(fpath, filename=None):

    """
    Concatenate TS results (such as the ones created by samplecomputeTS, that are created by chunk) into one pickle file

       inputs:
               - fpath: path containing pickle files to concatenate
               - filename (optionnal): filename (path + name) of resulting pickle file. If None, pickle file is recorded as fpath.pickle (in fpath up directory)
        output:
               - one pickle file is created, that can be loaded with:
               timeTarget,TSrange,TScomp,TSucomp,TSalong,TSathwart,TSposition,TSpositionGPS,TSlabel,TSfreq,name_sdr_tsd=pickle.load(open(filename,'rb'))
    """

    filelist = []
    for root, subFolders, files in os.walk(fpath):
        for file in files:
            if file.endswith("_TS.pickle"):
                filelist.append(os.path.join(root, file))

    for FileName in filelist:

        with open(FileName, "rb") as f:

            (
                timeTarget,
                TSrange,
                TScomp,
                TSucomp,
                TSalong,
                TSathwart,
                TSposition,
                TSpositionGPS,
                TSlabel,
                TSfreq,
                name_sdr_tsd,
            ) = pickle.load(f)

            if FileName == filelist[0]:
                # intialisation des structures de résultats
                ###########################################
                # chaque structure est une liste de liste de numpy array : TScomp[isdr][itsd][array des TS[idet,ifreq]]
                timeTarget_all = [
                    [np.array([]) for itsd in range(len(name_sdr_tsd[isdr]))]
                    for isdr in range(len(name_sdr_tsd))
                ]
                TSrange_all = [
                    [np.array([]) for itsd in range(len(name_sdr_tsd[isdr]))]
                    for isdr in range(len(name_sdr_tsd))
                ]
                TScomp_all = [
                    [np.zeros((0, np.size(TSfreq[isdr][itsd],1))) for itsd in range(len(name_sdr_tsd[isdr]))]
                    for isdr in range(len(name_sdr_tsd))
                ]
                TSucomp_all = [
                    [np.zeros((0, np.size(TSfreq[isdr][itsd],1))) for itsd in range(len(name_sdr_tsd[isdr]))]
                    for isdr in range(len(name_sdr_tsd))
                ]
                TSalong_all = [
                    [np.array([]) for itsd in range(len(name_sdr_tsd[isdr]))]
                    for isdr in range(len(name_sdr_tsd))
                ]
                TSathwart_all = [
                    [np.array([]) for itsd in range(len(name_sdr_tsd[isdr]))]
                    for isdr in range(len(name_sdr_tsd))
                ]
                TSposition_all = [
                    [np.zeros((0, 3)) for itsd in range(len(name_sdr_tsd[isdr]))]
                    for isdr in range(len(name_sdr_tsd))
                ]
                TSpositionGPS_all = [
                    [np.zeros((0, 3)) for itsd in range(len(name_sdr_tsd[isdr]))]
                    for isdr in range(len(name_sdr_tsd))
                ]
                TSlabel_all = [
                    [np.array([]) for itsd in range(len(name_sdr_tsd[isdr]))]
                    for isdr in range(len(name_sdr_tsd))
                ]
                TSfreq_all = [
                    [np.zeros((0, np.size(TSfreq[isdr][itsd],1))) for itsd in range(len(name_sdr_tsd[isdr]))]
                    for isdr in range(len(name_sdr_tsd))
                ]

            for isdr, v in enumerate(name_sdr_tsd):
                for itsd, _ in enumerate(v):
                    if len(timeTarget[isdr][itsd])>0:
                        timeTarget_all[isdr][itsd] = np.append(
                            timeTarget_all[isdr][itsd], timeTarget[isdr][itsd]
                        )
                        TSrange_all[isdr][itsd] = np.append(
                            TSrange_all[isdr][itsd], TSrange[isdr][itsd]
                        )
                        if len(TScomp_all[isdr][itsd])==0: #réinitialise lse structures à la bonne taille
                            TScomp_all[isdr][itsd] =np.zeros((0, int(np.size(TSfreq[isdr][itsd])/len(TSfreq[isdr][itsd]))))
                            TSucomp_all[isdr][itsd] =np.zeros((0, int(np.size(TSfreq[isdr][itsd])/len(TSfreq[isdr][itsd]))))
                            TSfreq_all[isdr][itsd] =np.zeros((0, int(np.size(TSfreq[isdr][itsd])/len(TSfreq[isdr][itsd]))))
                        TScomp_all[isdr][itsd] = np.append(
                            TScomp_all[isdr][itsd], TScomp[isdr][itsd], axis=0
                        )
                        TSucomp_all[isdr][itsd] = np.append(
                            TSucomp_all[isdr][itsd], TSucomp[isdr][itsd], axis=0
                        )
                        TSalong_all[isdr][itsd] = np.append(
                            TSalong_all[isdr][itsd], TSalong[isdr][itsd]
                        )
                        TSathwart_all[isdr][itsd] = np.append(
                            TSathwart_all[isdr][itsd], TSathwart[isdr][itsd]
                        )
                        TSposition_all[isdr][itsd] = np.append(
                            TSposition_all[isdr][itsd], TSposition[isdr][itsd], axis=0
                        )
                        TSpositionGPS_all[isdr][itsd] = np.append(
                            TSpositionGPS_all[isdr][itsd], TSpositionGPS[isdr][itsd], axis=0
                        )
                        TSlabel_all[isdr][itsd] = np.append(
                            TSlabel_all[isdr][itsd], TSlabel[isdr][itsd]
                        )
                        TSfreq_all[isdr][itsd] = np.append(
                            TSfreq_all[isdr][itsd], TSfreq[isdr][itsd], axis=0
                        )

    if filename is None:
        ind = fpath.rfind("/")
        if ind == -1:
            filename = fpath
        else:
            if ind == len(fpath) - 1:
                filename = fpath[:-1]
            else:
                filename = fpath
        filename = filename + ".pickle"

    with open(filename, "wb") as f:
        pickle.dump(
            [
                timeTarget_all,
                TSrange_all,
                TScomp_all,
                TSucomp_all,
                TSalong_all,
                TSathwart_all,
                TSposition_all,
                TSpositionGPS_all,
                TSlabel_all,
                TSfreq_all,
                name_sdr_tsd,
            ],
            f,
        )
