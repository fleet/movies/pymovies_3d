﻿# -*- coding: utf-8 -*-
"""
Created on Wed Nov 25 09:05:45 2020

@author: lberger

Effectue l'analyse des échos simples du fichier ou du répertoire indiqué, et l'enregistre en .pickle

   entrées:
           - chemin_ini: chemin du répertoire ou du fichier à traiter
           - chemin_config: chemin de la configuration M3D à utiliser (paramètres de détection etc)
           - chemin_save: chemin du répertoire de sauvegarde des résultats
           - nameTransect: nom de sauvegarde (optionnel), qui sera prolongé par _results_TS. Si None, le fichier pickle sera
           nommé results_TS.pickle
           - indexSounder: index du sondeur à traiter. Si None, tous les sondeurs (et transducteurs) sont traités
           - indexTransducer: index du transducteur à traiter, pour le sondeur indiqué. Si None, tous les transducteurs du sondeur indexSounder sont traités
           - dateStart et dateEnd: dates de début et fin des séquences à traiter, au format 'dd/mm/yyyy' ou ['dd/mm/yyyy','dd/mm/yyyy'...]
           Si dateStart est None, tout le répertoire est traité
           - timeStart et timeEnd: heures de début et fin des séquences à traiter, au format 'hh:mm:ss' ou ['hh:mm:ss','hh:mm:ss'...]
    sortie:
           - Un fichier pickle temporaire par chunk lu est créé, puis les résultats sont concaténés dans un seul fichier pickle. La taille du chunck doit être adaptée pour l'efficacité de l'exécution
           - structures sauvegardées: timeTarget,TSrange,TScomp,TSucomp,TSalong,TSathwart,TSposition,TSpositionGPS,TSlabel,TSfreq,name_sdr_tsd
           - les structures sauvegardées sont du type :
               - TScomp[indexSounder][indexTransducer][numpy array de dimensions: nb_detections x nb_freq] où nb_freq=1 en CW et nb_freq_spectre en FM
               - TSposition[indexSounder][indexTransducer][numpy array de dimensions: nb_detections x 3]
               - name_sdr_tsd[indexSounder][indexTransducer] = 'nom du transducteur'
"""

import calendar

import datetime
import os
import pickle
import shutil
import sys
import time
import numpy as np
import pyMovies.pyMovies as mv

import pymovies_3d.core.hac_util.hac_util as util
from pymovies_3d.core.TS_analysis.TS_bind import TS_bind


# pylint:disable=too-many-nested-blocks
def sample_compute_TS(
    chemin_ini,
    chemin_config,
    chemin_save,
    indexSounder=None,
    indexTransducer=None,
    nameTransect=None,
    dateStart=None,
    timeStart=None,
    dateEnd=None,
    timeEnd=None,
):
    # load configuration
    if not os.path.exists(chemin_config):
        print("****** stop: chemin de configuration non valide")
        return

    mv.moLoadConfig(chemin_config)

    num_bloc = 1

    # Save directory
    if not os.path.exists(chemin_save):
        os.makedirs(chemin_save)

    if nameTransect is not None:
        str_Name = nameTransect + "_"
        chemin_save_chunks = chemin_save + "/" + nameTransect + "_results_TS_temp/"
    else:
        str_Name = ""
        chemin_save_chunks = chemin_save + "/results_TS_temp/"

    if os.path.exists(chemin_save_chunks):
        print(
            "remove "
            + chemin_save_chunks
            + " directory to process single target record and avoid binding results with previous ones"
        )
        return
    os.makedirs(chemin_save_chunks)

    if dateStart is None:
        range_date = [-1]
        process_all = True
    else:
        if isinstance(dateStart, str):
            dateStart = [dateStart]
        if isinstance(dateEnd, str):
            dateEnd = [dateEnd]
        if isinstance(timeStart, str):
            timeStart = [timeStart]
        if isinstance(timeEnd, str):
            timeEnd = [timeEnd]

        range_date = range(len(dateStart))
        if (len(dateEnd) + len(timeStart) + len(timeEnd)) / 3 != len(dateStart):
            print(
                "****** stop: les listes des dates/heures de début/fin n ont pas la même taille"
            )
            return
        process_all = False

    for x in range_date:

        slice_end = False

        if not process_all:

            goto = calendar.timegm(
                time.strptime(
                    "%s %s" % ((dateStart[x]), (timeStart[x])), "%d/%m/%Y %H:%M:%S"
                )
            )

            mv.moOpenHac(chemin_ini)

            util.hac_goto(goto)

            timeEndEI = calendar.timegm(
                time.strptime("%s %s" % (dateEnd[x], timeEnd[x]), "%d/%m/%Y %H:%M:%S")
            )
        else:
            # chargement de tous les fichiers HAC contenus dans le repertoire
            # et lecture du premier chunk
            mv.moOpenHac(chemin_ini)
            timeEndEI = calendar.timegm(datetime.datetime.now().timetuple())

        # presentation sondeurs
        #######################
        list_sounder = util.hac_sounder_descr()
        name_sdr_tsd = [
            [
                list_sounder.GetSounder(isdr).GetTransducer(itsd).m_transName
                for itsd in range(list_sounder.GetSounder(isdr).m_numberOfTransducer)
            ]
            for isdr in range(list_sounder.GetNbSounder())
        ]

        # construction des listes de sondeurs/transducteurs à traiter
        ############################################################
        if indexSounder is not None:
            if indexSounder >= list_sounder.GetNbSounder():
                print("index sondeur hors limite")
                sys.exit()
            sounder = list_sounder.GetSounder(indexSounder)
            list_sdr_Id = np.array([sounder.m_SounderId])
            if indexTransducer is not None:
                if indexTransducer >= sounder.m_numberOfTransducer:
                    print("index transducteur hors limite")
                    sys.exit()
                list_tsd = np.array([[indexTransducer]])
            else:
                list_tsd = np.array([np.arange(sounder.m_numberOfTransducer)])
        else:
            list_sdr_Id = np.empty([list_sounder.GetNbSounder()])
            for isd in range(list_sounder.GetNbSounder()):
                sounder = list_sounder.GetSounder(isd)
                list_sdr_Id[isd] = sounder.m_SounderId
                if isd == 0:
                    list_tsd = np.array([np.arange(sounder.m_numberOfTransducer)])
                else:
                    list_tsd = np.array([list_tsd, np.arange(sounder.m_numberOfTransducer)])

        FileStatus = mv.moGetFileStatus()
        while not FileStatus.m_StreamClosed:

            # intialisation des structures de résultats
            ###########################################
            # chaque structure est une liste de liste de numpy array : TScomp[isdr][itsd][array des TS[idet,ifreq]]
            timeTarget = [
                [
                    np.zeros((0, 0))
                    for itsd in range(
                    list_sounder.GetSounder(isdr).m_numberOfTransducer
                )
                ]
                for isdr in range(list_sounder.GetNbSounder())
            ]
            TSrange = [
                [
                    np.zeros((0, 0))
                    for itsd in range(
                    list_sounder.GetSounder(isdr).m_numberOfTransducer
                )
                ]
                for isdr in range(list_sounder.GetNbSounder())
            ]
            TScomp = [
                [
                    np.zeros((0, 0))
                    for itsd in range(
                    list_sounder.GetSounder(isdr).m_numberOfTransducer
                )
                ]
                for isdr in range(list_sounder.GetNbSounder())
            ]
            TSucomp = [
                [
                    np.zeros((0, 0))
                    for itsd in range(
                    list_sounder.GetSounder(isdr).m_numberOfTransducer
                )
                ]
                for isdr in range(list_sounder.GetNbSounder())
            ]
            TSalong = [
                [
                    np.zeros((0, 0))
                    for itsd in range(
                    list_sounder.GetSounder(isdr).m_numberOfTransducer
                )
                ]
                for isdr in range(list_sounder.GetNbSounder())
            ]
            TSathwart = [
                [
                    np.zeros((0, 0))
                    for itsd in range(
                    list_sounder.GetSounder(isdr).m_numberOfTransducer
                )
                ]
                for isdr in range(list_sounder.GetNbSounder())
            ]
            TSposition = [
                [
                    np.zeros((0, 3))
                    for itsd in range(
                    list_sounder.GetSounder(isdr).m_numberOfTransducer
                )
                ]
                for isdr in range(list_sounder.GetNbSounder())
            ]
            TSpositionGPS = [
                [
                    np.zeros((0, 3))
                    for itsd in range(
                    list_sounder.GetSounder(isdr).m_numberOfTransducer
                )
                ]
                for isdr in range(list_sounder.GetNbSounder())
            ]
            TSlabel = [
                [
                    np.zeros((0, 0))
                    for itsd in range(
                    list_sounder.GetSounder(isdr).m_numberOfTransducer
                )
                ]
                for isdr in range(list_sounder.GetNbSounder())
            ]
            TSfreq = [
                [
                    np.zeros((0, 0))
                    for itsd in range(
                    list_sounder.GetSounder(isdr).m_numberOfTransducer
                )
                ]
                for isdr in range(list_sounder.GetNbSounder())
            ]

            mv.moReadChunk()

            nb_pings = mv.moGetNumberOfPingFan()

            for indexPing in range(nb_pings):
                MX = mv.moGetPingFan(indexPing)
                SounderDesc = MX.m_pSounder
                if SounderDesc.m_SounderId in list_sdr_Id:  # traite le sondeur
                    isdr = np.where(
                        SounderDesc.m_SounderId == list_sdr_Id
                    )  # index sondeur dans la liste sdr_list
                    isdr = int(isdr[0])
                    for itsd in list_tsd[isdr][:]:  # traite le transducteur
                        itsd = int(itsd)
                        for beamIndex in range(
                            SounderDesc.GetTransducer(itsd).m_numberOfSoftChannel
                        ):
                            for index, sp_value in enumerate(MX.m_splitBeamData):
                                if (
                                    sp_value.m_parentSTId
                                    == SounderDesc.GetTransducer(itsd)
                                    .getSoftChannelPolarX(beamIndex)
                                    .m_softChannelId
                                ):
                                    if (
                                        SounderDesc.GetTransducer(itsd).m_pulseShape
                                        == 2
                                    ):
                                        targets = MX.m_splitBeamData[
                                            index
                                        ].m_targetListFM
                                    else:
                                        targets = MX.m_splitBeamData[
                                            index
                                        ].m_targetListCW

                                    # faudrait-il déclarer la taille du append avant pour accélérer? (allocation mémoire contigue)
                                    for indexTarget, _ in enumerate(targets):
                                        timeTarget[isdr][itsd] = np.append(
                                            timeTarget[isdr][itsd],
                                            util.hac_read_day_time(
                                                MX.beam_data[itsd].m_time.m_TimeCpu
                                                + MX.beam_data[itsd].m_time.m_TimeFraction / 10000
                                            ),
                                        )
                                        TSrange[isdr][itsd] = np.append(
                                            TSrange[isdr][itsd],
                                            targets[indexTarget].m_targetRange,
                                        )
                                        TSalong[isdr][itsd] = np.append(
                                            TSalong[isdr][itsd],
                                            targets[indexTarget].m_AlongShipAngleRad,
                                        )
                                        TSathwart[isdr][itsd] = np.append(
                                            TSathwart[isdr][itsd],
                                            targets[indexTarget].m_AthwartShipAngleRad,
                                        )
                                        TSlabel[isdr][itsd] = np.append(
                                            TSlabel[isdr][itsd],
                                            targets[indexTarget].m_trackLabel,
                                        )

                                        TSposition[isdr][itsd] = np.append(
                                            TSposition[isdr][itsd],
                                            [
                                                [
                                                    targets[
                                                        indexTarget
                                                    ].m_positionWorldCoord.x,
                                                    targets[
                                                        indexTarget
                                                    ].m_positionWorldCoord.y,
                                                    targets[
                                                        indexTarget
                                                    ].m_positionWorldCoord.z,
                                                ]
                                            ],
                                            axis=0,
                                        )
                                        TSpositionGPS[isdr][itsd] = np.append(
                                            TSpositionGPS[isdr][itsd],
                                            [
                                                [
                                                    targets[
                                                        indexTarget
                                                    ].m_positionGeoCoord.x,
                                                    targets[
                                                        indexTarget
                                                    ].m_positionGeoCoord.y,
                                                    targets[
                                                        indexTarget
                                                    ].m_positionGeoCoord.z,
                                                ]
                                            ],
                                            axis=0,
                                        )

                                        if SounderDesc.GetTransducer(itsd).m_pulseShape== 2:
                                            freqs = targets[indexTarget].m_freq
                                            tscomp = targets[indexTarget].m_compensatedTS
                                            tsucomp = targets[indexTarget].m_unCompensatedTS
                                        else:
                                            freqs = [SounderDesc.GetTransducer(itsd).getSoftChannelPolarX(beamIndex).m_acousticFrequency]
                                            tscomp = [targets[indexTarget].m_compensatedTS]
                                            tsucomp = [targets[indexTarget].m_unCompensatedTS]

                                        nbFreqs = len(freqs)

                                        if np.size(TScomp[isdr][itsd]) == 0:  # première cible, réinitialise les structures avec la bonne taille
                                            TSfreq[isdr][itsd] = np.zeros((0, nbFreqs))
                                            TScomp[isdr][itsd] = np.zeros((0, nbFreqs))
                                            TSucomp[isdr][itsd] = np.zeros((0, nbFreqs))

                                        TSfreq[isdr][itsd] = np.append(TSfreq[isdr][itsd],[freqs],axis=0)
                                        TScomp[isdr][itsd] = np.append(TScomp[isdr][itsd],[tscomp],axis=0)
                                        TSucomp[isdr][itsd] = np.append(TSucomp[isdr][itsd],[tsucomp],axis=0)

                if (
                    (MX.m_meanTime.m_TimeCpu + MX.m_meanTime.m_TimeFraction / 10000)
                    > timeEndEI
                ) & (not process_all):
                    slice_end = True
                    break

            str_bloc = "%04d" % (num_bloc)

            # save results
            # à l'avenir on pourra choisir un format netcdf pour le stockage des résultats
            with open(
                "%s/%s_%s_results_TS.pickle" % (chemin_save_chunks, str_Name, str_bloc),
                "wb",
            ) as f:
                pickle.dump(
                    [
                        timeTarget,
                        TSrange,
                        TScomp,
                        TSucomp,
                        TSalong,
                        TSathwart,
                        TSposition,
                        TSpositionGPS,
                        TSlabel,
                        TSfreq,
                        name_sdr_tsd,
                    ],
                    f,
                )

            # clear memory from previous pings
            for ip in range(nb_pings, 0, -1):
                MX = mv.moGetPingFan(ip - 1)
                mv.moRemovePing(MX.m_computePingFan.m_pingId)

            num_bloc = num_bloc + 1

            if slice_end:
                break

    # bind results
    TS_bind(
        chemin_save_chunks, filename=chemin_save + "/" + str_Name + "results_TS.pickle"
    )
    # remove chunks directory
    shutil.rmtree(chemin_save_chunks)

    return
