# -*- coding: utf-8 -*-
"""
Created on Fri Apr 30 10:18:53 2021

@author: nlebouff
"""
import xml.etree.ElementTree as ET
import datetime
import pandas as pd
from openpyxl import load_workbook
from openpyxl.chart import LineChart,Reference
#from openpyxl.chart.axis import DateAxis

########################################
# paramètres à renseigner         #######
###########################################
xml_cal_file='X:/traitement_Campagnes/ESS-NSE-TL-22/EK80/ESS-NSE-TL-22/Etalonnage/CalibrationDataFile-D20220511-T124005_200khz_hor_CW.xml'  #résultat xml à intégrer
xls_file='X:/traitement_Campagnes/ESS-NSE-TL-22/EK80/ESS-NSE-TL-22/Etalonnage/TL-Etalonnages-EK80-CW-Bilan.xlsx' #nom du fichier à incrémenter
survey='PELGAS22' #nom de la feuille à renseigner


#########################################
# lecture xml                          ##
########################################
tree = ET.parse(xml_cal_file)
root = tree.getroot()

Common=root.find('Calibration').find('Common')
Target=root.find('Calibration').find('TargetReference')
Results=root.find('Calibration').find('CalibrationResults')

date_cal=Common.find('TimeOfFileCreation').text[0:10]
name_tsd=Common.find('Transducer').find('Name').text
sn_tsd=Common.find('Transducer').find('SerialNumber').text
soft_vs=Common.find('Application').find('SoftwareVersion').text

freq_kHz=int(int(Common.find('TransceiverSetting').find('FrequencyStart').text)/1000)
PL_us=int(float(Common.find('TransceiverSetting').find('PulseLength').text)*(10**6))
TxPower=int(float(Common.find('TransceiverSetting').find('TransmitPower').text))

soundspeed=float(Common.find('EnvironmentData').find('SoundVelocity').text)
abs_dBkm=float(Common.find('EnvironmentData').find('AbsorptionCoefficient').text)*1000
temperature=float(Common.find('EnvironmentData').find('Temperature').text)
sal=float(Common.find('EnvironmentData').find('Salinity').text)

minRange=float(Common.find('SingleTargetDetectorSetting').find('RangeStart').text)

sphere=Target.find('Name').text

Gain=float(Results.find('Gain').text)
SaCor=float(Results.find('SaCorrection').text)
BeamWidthAlongship=float(Results.find('BeamWidthAlongship').text)
BeamWidthAthwartship=float(Results.find('BeamWidthAthwartship').text)
AngleOffsetAlongship=float(Results.find('AngleOffsetAlongship').text)
AngleOffsetAthwartship=float(Results.find('AngleOffsetAthwartship').text)
TsRmsError=float(Results.find('TsRmsError').text)

#########################################
# ecriture xls                        ##
########################################
#xls_file='F:/Logiciels_Manuels/EK80/Documents/TL-Etalonnages-EK80-CW-Bilan.xlsx'
#survey='ESSTECH21'

wb = load_workbook(xls_file)
#sauvegarde de l'ancien au cas où
#wb.save(xls_file[:-5]+'_sav.xlsx')

if survey not in wb.sheetnames:
    #ajout feuille de la mission
    ws=wb.copy_worksheet(wb['VIERGE'])
    ws.title=survey #feuille de la mission
    
    #on la place anvant 'VIERGE'
    id_vierge=wb.index(wb['VIERGE'])
    n_sh=len(wb._sheets)
    wb._sheets[id_vierge+1:n_sh]=wb._sheets[id_vierge:n_sh-1]
    wb._sheets[id_vierge]=ws
else:
    ws=wb[survey]

# numero de colonne de la fréquence du fichier de calibration xml
freq_xls = pd.DataFrame([['C','D','E','F','G','H']],columns=[18,38,70,120,200,333])
col_freq=freq_xls[freq_kHz]
col=col_freq[0]
if name_tsd=='ES200-3C':
    col='I'

#renseignement feuille mission
ws['C2'] = survey
dttm = datetime.datetime.strptime(date_cal, "%Y-%m-%d")
ws['C3'] = dttm
ws[col+'5'] = name_tsd
ws[col+'6'] = int(sn_tsd)
ws[col+'7'] = abs_dBkm
ws[col+'8'] = PL_us
ws[col+'10'] = TxPower
ws['C11'] = soft_vs
ws[col+'12'] = minRange
ws['C14'] = str(temperature)+'/'+str(soundspeed)
ws['C15'] = sal
ws[col+'18'] = sphere
ws[col+'22'] = TsRmsError
ws[col+'23'] = Gain
ws[col+'24'] = SaCor
ws[col+'25'] = BeamWidthAthwartship
ws[col+'26'] = BeamWidthAlongship
ws[col+'27'] = AngleOffsetAthwartship
ws[col+'28'] = AngleOffsetAlongship
Allfile_name=xml_cal_file.split('/')
ws[col+'29'] = Allfile_name[-1]

#ajout dans la feuille recap
ws=wb['Recap']
nmissions=len(ws[1])
letter_survey=''
for isurvey in range(nmissions):
    if ws.cell(row=1, column=isurvey+1).value==survey:
        letter_survey=ws.cell(row=1, column=isurvey+1).column_letter
        break
if letter_survey=='':
    nrows=len(ws['A'])
    last_letter=ws.cell(row=1, column=nmissions).column_letter
    letter_survey=ws.cell(row=1, column=nmissions+1).column_letter
    next_letter=ws.cell(row=1, column=nmissions+2).column_letter
    #copie dernière colonne 2 colonnes plus loin
    for i in range(nrows):
        ws[next_letter+str(i+1)]._style=ws[last_letter+str(i+1)]._style
        ws[next_letter+str(i+1)].value=ws[last_letter+str(i+1)].value
        ws[next_letter+str(i+1)].number_format=ws[last_letter+str(i+1)].number_format
    #    ws[next_letter+str(i+1)].font=Font(sz= ws[last_letter+str(i+1)].font.sz)
    #déplace/traduit la dernière colonne à celle du survey
    ws.move_range(last_letter+str(1)+':'+last_letter+str(nrows),rows=0,cols=1,translate=True)
    ws[letter_survey+'1']=survey
    #replace la colonne sauvegardée
    for i in range(nrows):
        ws[last_letter+str(i+1)]._style=ws[next_letter+str(i+1)]._style
        ws[last_letter+str(i+1)].value=ws[next_letter+str(i+1)].value
    #    ws[last_letter+str(i+1)].data_type=ws[next_letter+str(i+1)].data_type
        ws[last_letter+str(i+1)].number_format=ws[next_letter+str(i+1)].number_format
    ws.delete_cols(nmissions+2)

#letter_survey='H'
#ajout dans la feuille series pour affichage #réécrit Series depuis Recap
wseries=wb['Series']

wR=wb['Recap']
#idate
for idate in range(4,len(wR[1])+1):
    if wR.cell(row=1,column=idate) is None:
        break
    nsdr=7
    letter_survey=wR.cell(row=1,column=idate).column_letter
    
    for iparam in range(7):
        wseries.cell(column=iparam*(nsdr+2)+1,row=idate-1)._style=ws[letter_survey+'2']._style
        wseries.cell(column=iparam*(nsdr+2)+1,row=idate-1).value='=Recap!'+letter_survey+'2'
        wseries.cell(column=iparam*(nsdr+2)+1,row=idate-1).number_format=ws[letter_survey+'2'].number_format
    
    for isdr in range(0,nsdr):
        if wb[wR[letter_survey+'1'].value].cell(row=22,column=3+isdr).value is not None: # sondeur renseigné (gain non vide)
            #Gain
            wseries.cell(row=idate-1,column=2+isdr).value='=Recap!'+letter_survey+str(5+isdr*8)
            wseries.cell(row=idate-1,column=2+isdr).number_format='0.00'
            #SaCorr
            wseries.cell(row=idate-1,column=2+(nsdr+2)+isdr).value='=Recap!'+letter_survey+str(6+isdr*8)
            wseries.cell(row=idate-1,column=2+(nsdr+2)+isdr).number_format='0.00'
            #RMS
            wseries.cell(row=idate-1,column=2+2*(nsdr+2)+isdr).value='=Recap!'+letter_survey+str(4+isdr*8)
            wseries.cell(row=idate-1,column=2+2*(nsdr+2)+isdr).number_format='0.00'
            #BW along
            wseries.cell(row=idate-1,column=2+3*(nsdr+2)+isdr).value='=Recap!'+letter_survey+str(7+isdr*8)
            wseries.cell(row=idate-1,column=2+3*(nsdr+2)+isdr).number_format='0.00'
            #BW athwart
            wseries.cell(row=idate-1,column=2+4*(nsdr+2)+isdr).value='=Recap!'+letter_survey+str(8+isdr*8)
            wseries.cell(row=idate-1,column=2+4*(nsdr+2)+isdr).number_format='0.00'
            #offset along
            wseries.cell(row=idate-1,column=2+5*(nsdr+2)+isdr).value='=Recap!'+letter_survey+str(9+isdr*8)
            wseries.cell(row=idate-1,column=2+5*(nsdr+2)+isdr).number_format='0.00'
            #offset athwart
            wseries.cell(row=idate-1,column=2+6*(nsdr+2)+isdr).value='=Recap!'+letter_survey+str(10+isdr*8)
            wseries.cell(row=idate-1,column=2+6*(nsdr+2)+isdr).number_format='0.00'

    
#################
# graphes
###########

wb.remove(wb['Graphes'])
wb.create_sheet('Graphes')
ws=wb['Graphes']

gr_titles=['Gain (dB)','SaCor(dB)','RMS (dB)','BW Along (°)','BW Athwart (°)','Offset Along (°)','Offset Athwart (°)']
delta_gr=15
ymin=[25,-0.3,0,5,5,-0.5,-0.5]#TL
ymax=[28,0.3,0.6,11,11,0.5,0.5]#TL

ymin=[24,-1,0,5,5,-0.5,-0.5]#EU
ymax=[29,0.3,0.6,11,11,0.5,0.5]#EU

for igr in range(7):
    
    # Chart with date axis
    c = LineChart()
    c.x_axis.title = "Date"
    c.title = gr_titles[igr]
    c.x_axis.number_format = 'mmm-yy'
    c.y_axis.scaling.min = ymin[igr]
    c.y_axis.scaling.max = ymax[igr]
    c.x_axis.tickLblPos = "low"

    data = Reference(wseries, min_col=2+igr*(nsdr+2), min_row=2, max_col=8+igr*(nsdr+2), max_row=len(wseries['A']))
    c.add_data(data, titles_from_data=True)
    dates = Reference(wseries, min_col=1, min_row=3, max_row=len(wseries['A']))
    c.set_categories(dates)

    for s in c.series:
        s.marker.symbol = "triangle"
    
    ws.add_chart(c, "A"+str(5+igr*delta_gr))



wb.save(xls_file)

wb.close()
