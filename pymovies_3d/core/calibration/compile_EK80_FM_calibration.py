# -*- coding: utf-8 -*-
"""
Created on Fri Apr 30 10:18:53 2021

@author: nlebouff
"""
import xml.etree.ElementTree as ET
import datetime
from openpyxl import load_workbook
from openpyxl.chart import ScatterChart,Reference, Series
#from openpyxl.chart.axis import DateAxis


########################################
# paramètres à renseigner         #######
###########################################
#xml_cal_file='F:/Logiciels_Manuels/EK80/documents/TrList_calibration_evhoe19.xml'  #résultat xml à intégrer, xml d'étalonnage ou TrList
xml_cal_file='X:/traitement_Campagnes/ESS-NSE-TL-22/EK80/ESS-NSE-TL-22/Etalonnage/CalibrationDataFile-D20220511-T125759_200khz_hor_FM.xml'  #résultat xml à intégrer, xml d'étalonnage ou TrList
xls_file='X:/traitement_Campagnes/ESS-NSE-TL-22/EK80/ESS-NSE-TL-22/Etalonnage/TL-Etalonnages-EK80-FM-Bilan.xlsx' #nom du fichier à incrémenter
survey='PELGAS22' #nom de la feuille à renseigner

name_tsd='ES120-7C'#nom du transducteur à intégrer, à renseigner si on lit un TrList
date_cal='2019-05-01' #date de l'étalonnage à intégrer, à renseigner si on lit un TrList

#########################################
# lecture xml                          ##
########################################
Allfile_name=xml_cal_file.split('/')
nom_fic= Allfile_name[-1]
if nom_fic[0:6]=='TrList':
    tree = ET.parse(xml_cal_file)
    root = tree.getroot()
    
    ntsd=len(root.find('TransducerData'))
    for itsd in range(ntsd):
        if root.find('TransducerData')[itsd].attrib['TransducerName']==name_tsd:
            Transd=root.find('TransducerData')[itsd]
            sn_tsd=Transd.attrib['TransducerSerialNumber']
            
    freq_Hz=[]
    Gain=[]
    SaCorr=[]
    TsRmsError=[]
    BWAl=[]
    BWAt=[]
    OffAl=[]
    OffAt=[]
    for tsd in Transd:
        if tsd.tag=='FrequencyPar':
            freq_Hz.append(int(tsd.attrib['Frequency']))
            Gain.append(float(tsd.attrib['Gain']))
            SaCorr.append(float(tsd.attrib['SaCorrection']))
            TsRmsError.append(float(0))
            BWAl.append(float(tsd.attrib['BeamWidthAlongship']))
            BWAt.append(float(tsd.attrib['BeamWidthAthwartship']))
            OffAl.append(float(tsd.attrib['AngleOffsetAlongship']))
            OffAt.append(float(tsd.attrib['AngleOffsetAthwartship']))
            
    nfreq=len(freq_Hz)
    soft_vs=''
    PL_us=-1
    f_min=min(freq_Hz)
    f_max=max(freq_Hz)

    soundspeed=-1
    abs_dBkm=-1
    Temperature=-1
    sal=-1
    
    sphere=''
    minRange=-1
    
else:
            
    tree = ET.parse(xml_cal_file)
    root = tree.getroot()
    
    Common=root.find('Calibration').find('Common')
    Target=root.find('Calibration').find('TargetReference')
    Results=root.find('Calibration').find('CalibrationResults')
    
    date_cal=Common.find('TimeOfFileCreation').text
    date_cal=date_cal[0:10]
    name_tsd=Common.find('Transducer').find('Name').text
    sn_tsd=Common.find('Transducer').find('SerialNumber').text
    
    soft_vs=Common.find('Application').find('SoftwareVersion').text
    
    PL_us=int(float(Common.find('TransceiverSetting').find('PulseLength').text)*(10**6))
    f_min=int(Common.find('TransceiverSetting').find('FrequencyStart').text)
    f_max=int(Common.find('TransceiverSetting').find('FrequencyEnd').text)
    
    
    soundspeed=float(Common.find('EnvironmentData').find('SoundVelocity').text)
    abs_dBkm=float(Common.find('EnvironmentData').find('AbsorptionCoefficient').text)
    Temperature=float(Common.find('EnvironmentData').find('Temperature').text)
    sal=float(Common.find('EnvironmentData').find('Salinity').text)
    
    sphere=Target.find('Name').text
    minRange=float(Common.find('SingleTargetDetectorSetting').find('RangeStart').text)
    
    freq_Hz=[]
    Gain=[]
    SaCorr=[]
    TsRmsError=[]
    BWAl=[]
    BWAt=[]
    OffAl=[]
    OffAt=[]
    
    fr=Results.find('Frequency').text.split(';')
    g=Results.find('Gain').text.split(';')
    Sc=Results.find('SaCorrection').text.split(';')
    rms=Results.find('TsRmsError').text.split(';')
    BWl=Results.find('BeamWidthAlongship').text.split(';')
    BWt=Results.find('BeamWidthAthwartship').text.split(';')
    Ofl=Results.find('AngleOffsetAlongship').text.split(';')
    Oft=Results.find('AngleOffsetAthwartship').text.split(';')
    
    nfreq=len(fr)
    
    for ifr in range(nfreq):
        freq_Hz.append(int(fr[ifr]))
        Gain.append(float(g[ifr]))
        SaCorr.append(float(Sc[ifr]))
        TsRmsError.append(float(rms[ifr]))
        BWAl.append(float(BWl[ifr]))
        BWAt.append(float(BWt[ifr]))
        OffAl.append(float(Ofl[ifr]))
        OffAt.append(float(Oft[ifr]))


#########################################
# ecriture xls                        ##
########################################

wb = load_workbook(xls_file)
#sauvegarde de l'ancien au cas où
#wb.save(xls_file[:-5]+'_sav.xlsx')

if survey not in wb.sheetnames:
    #ajout feuille de la mission
    ws=wb.copy_worksheet(wb['VIERGE'])
    ws.title=survey #feuille de la mission
    
    #on la place après 'VIERGE'
    id_vierge=wb.index(wb['VIERGE'])
    n_sh=len(wb._sheets)
    wb._sheets[id_vierge+2:n_sh]=wb._sheets[id_vierge+1:n_sh-1]
    wb._sheets[id_vierge+1]=ws
else:
    ws=wb[survey]

#renseignement feuille mission
ws['C2'] = survey
dttm = datetime.datetime.strptime(date_cal, "%Y-%m-%d")
ws['C3'] = dttm


# numero de ligne de début pour le transducteur du fichier de calibration xml
liste_tsd=['ES38-7','ES70-7C','ES120-7C','ES200-7C','ES333-7C','ES200-3C']
n_tsd=len(liste_tsd)
for itsd in range(n_tsd):
    if liste_tsd[itsd]==name_tsd:
        itsd_cal=itsd
        break
ligne_ref=5+itsd_cal*24

ws.cell(row=ligne_ref+1,column=3).value = name_tsd
ws.cell(row=ligne_ref+2,column=3).value = sn_tsd
ws.cell(row=ligne_ref+3,column=3).value = soft_vs
ws.cell(row=ligne_ref+4,column=3).value = str(f_min)+'-'+str(f_max)
ws.cell(row=ligne_ref+5,column=3).value = PL_us
ws.cell(row=ligne_ref+6,column=3).value = soundspeed
ws.cell(row=ligne_ref+7,column=3).value = Temperature
ws.cell(row=ligne_ref+8,column=3).value = sal
ws.cell(row=ligne_ref+9,column=3).value = abs_dBkm
ws.cell(row=ligne_ref+10,column=3).value = sphere
ws.cell(row=ligne_ref+11,column=3).value = minRange

for ifr in range(nfreq):
    ws.cell(row=ligne_ref+12,column=ifr+3).value=freq_Hz[ifr]
    ws.cell(row=ligne_ref+13,column=ifr+3).value=TsRmsError[ifr]
    ws.cell(row=ligne_ref+14,column=ifr+3).value=Gain[ifr]
    ws.cell(row=ligne_ref+15,column=ifr+3).value=SaCorr[ifr]
    ws.cell(row=ligne_ref+16,column=ifr+3).value=BWAl[ifr]
    ws.cell(row=ligne_ref+17,column=ifr+3).value=BWAt[ifr]
    ws.cell(row=ligne_ref+18,column=ifr+3).value=OffAl[ifr]
    ws.cell(row=ligne_ref+19,column=ifr+3).value=OffAt[ifr]
    
Allfile_name=xml_cal_file.split('/')
ws.cell(row=ligne_ref+20,column=3).value = Allfile_name[-1]

#ajout dans la feuille recap
liste_recap=['Recap ES38-7','Recap ES70-7C','Recap ES120-7C','Recap ES200-7C','Recap ES333-7C','Recap ES200-3C']
wsR=wb[liste_recap[itsd_cal]]

#trouve l'index de première colonne à renseigner
for icol in range(3,len(wsR[2])+1):
    if wsR.cell(row=2,column=icol).value==survey:
        col_survey=icol-2
        col_exist=True
        break
    if wsR.cell(row=2,column=icol).value is None:
        col_survey=icol
        col_exist=False
        break
    col_survey=icol+1
    col_exist=False
    
#renseigne freq et beam letter
wsR.cell(row=4,column=col_survey)._style=wsR.cell(row=4,column=1)._style
wsR.cell(row=4,column=col_survey).value=wsR.cell(row=4,column=1).value
wsR.cell(row=4,column=col_survey+1)._style=wsR.cell(row=4,column=2)._style
wsR.cell(row=4,column=col_survey+1).value=wsR.cell(row=4,column=2).value
for ifr in range(nfreq):
    wsR.cell(row=ifr+5,column=col_survey)._style=wsR.cell(row=ifr+5,column=1)._style
    wsR.cell(row=ifr+5,column=col_survey).value=ws.cell(row=ligne_ref+12,column=3+ifr).value
    wsR.cell(row=ifr+5,column=col_survey+1)._style=wsR.cell(row=ifr+5,column=2)._style
    wsR.cell(row=ifr+5,column=col_survey+1).value=ws.cell(row=ligne_ref+12,column=3+ifr).column_letter

        
if not col_exist:
    #crée feuille de copie temporaire
    ws_temp=wb.copy_worksheet(wsR)
    ws_temp.title='temp' 
    
    first_letter=wsR.cell(row=2, column=col_survey-7).column_letter
    last_letter=wsR.cell(row=2, column=col_survey-1).column_letter
        
    #déplace/traduit les dernières colonnes à celles du survey
    wsR.move_range(first_letter+str(1)+':'+last_letter+str(len(wsR['A'])),rows=0,cols=9,translate=True)
    for iparam in range(2,9):
        wsR.cell(row=2,column=col_survey+iparam).value=survey
    
    #recopie les colonnes déplacées
    for irow in range(2,len(ws_temp[first_letter])+1):
        for icol in range(col_survey-9,col_survey):
            wsR.cell(row=irow,column=icol)._style=ws_temp.cell(row=irow,column=icol)._style
            wsR.cell(row=irow,column=icol).value=ws_temp.cell(row=irow,column=icol).value
            wsR.cell(row=irow,column=icol).number_format=ws_temp.cell(row=irow,column=icol).number_format

    wb.remove(ws_temp)

    
#################
# graphes
###########

gr_titles=['Gain (dB)','SaCorAdj(dB)','RMS (dB)','BW Al (°)','BW At (°)','Offset Al (°)','Offset At (°)']
ngr=len(gr_titles)
dec_param=[1,2,3,4,5,6,7] # colonnes de décalage

ymin=[20,-0.3,0,0,0,-1,-1]
ymax=[34,0.3,0.6,10,10,1,1]

for itsd in range(n_tsd):
    wsR=wb[liste_recap[itsd]]
    #supprime les graphiques
    for ich in range(len(wsR._charts)):
        del wsR._charts[-1]
        
    ligne_ref=5+itsd*24
    
    #nombre de surveys dans la récap
    for icol in range(3,len(wsR[5])+1):
        nsurvey=int(icol/9)
        if wsR.cell(row=5,column=icol).value is None:
            nsurvey=int((icol-1)/9)
            break
    
    
    for igr in range(ngr) :
        # Chart 
        c =ScatterChart()
        c.x_axis.title = "Freq (Hz)"
        c.title = gr_titles[igr]
        c.y_axis.scaling.min = ymin[igr]
        c.y_axis.scaling.max = ymax[igr]
        c.x_axis.tickLblPos = "low"
        xmin=10**6
        xmax=0
    
        for isurvey in range(max(0,nsurvey-3),nsurvey):
            survey_letter=wsR.cell(column=1+9*isurvey,row=1).column_letter
            for irow in range(5,len(wsR[survey_letter])):
                if wsR.cell(column=1+9*isurvey,row=irow+1).value is None:
                    nfreq=irow-4
                    break
                nfreq=irow-3
                        
            x_freq = Reference(wsR, min_col=1+9*isurvey,min_row=5, max_row=4+nfreq)
            xmin=min(xmin,wsR.cell(column=1+9*isurvey,row=5).value)
            xmax=max(xmax,wsR.cell(column=1+9*isurvey,row=4+nfreq).value)
            data = Reference(wsR, min_row=4, max_row=4+nfreq, min_col=2+dec_param[igr]+9*isurvey)
            series=Series(data,x_freq, title_from_data=True)
            series.marker.symbol = "triangle"
            c.series.append(series)
            
        c.x_axis.scaling.min = xmin
        c.x_axis.scaling.max = xmax
            
        wsR.add_chart(c, "B"+str(6+16*igr))
            #c.x_axis.number_format='00'
        

wb.save(xls_file)

wb.close()
