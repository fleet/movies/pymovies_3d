# -*- coding: utf-8 -*-
"""
Created on Fri Apr 30 10:18:53 2021

@author: nlebouff
"""
import xml.etree.ElementTree as ET
import datetime
from openpyxl import load_workbook
from openpyxl.chart import LineChart,Reference
#from openpyxl.chart.axis import DateAxis

########################################
# paramètres à renseigner         #######
###########################################
xml_cal_file='D:/ESS-NSE-TL-22/EK80/ESS-NSE-TL-22/Etalonnage/Bertheaume_Hte_Res_Fan21_Ref0_Gpe4_D20220421_Report.XML'  #résultat xml à intégrer
xls_file='D:/ESS-NSE-TL-22/EK80/ESS-NSE-TL-22/Etalonnage/TL-Etalonnages-ME70-21Beams-Bilan.xlsx' #nom du fichier à incrémenter
survey='ESSNSETL22' #nom de la feuille à renseigner

#########################################
# lecture xml                          ##
########################################
tree = ET.parse(xml_cal_file)
root = tree.getroot()

Common=root.find('CommonParameters')
Target=root.find('TargetReference')
Beams=root.find('Beams')

date_cal=Common.find('DateTimeNow').text
name_config=Common.find('BeamConfigurationName').text
nbeams=int(Common.find('BeamCount').text)
PL_us=int(float(Common.find('PulseLength').text)*(10**6))
soft_vs=Common.find('ApplicationVersion').text
soundspeed=float(Common.find('SoundVelocity').text)

sphere=Common.find('ReferenceTargetSphereName').text
minRange=float(Common.find('TSDetectionStartRange').text)

freq_Hz=[]
angle=[]
abs_dBkm=[]
nhits=[]
GainNom=[]
SaCorrSyst=[]
GainSyst=[]
TSRef=[]
GainAdj=[]
SaCorrAdj=[]
TsRmsError=[]
TSMean=[]
TSMedian=[]
for ib in range(nbeams):
    freq_Hz.append(int(float(Beams[ib].find('ParameterData').find('Frequency').text)))
    angle.append(float(Beams[ib].find('ParameterData').find('DirectionY').text))
    abs_dBkm.append(float(Beams[ib].find('ParameterData').find('AbsorptionCoefficient').text)*1000)   
    if Beams[ib].find('ParameterData').find('NominalGain') is not None:
        GainNom.append(float(Beams[ib].find('ParameterData').find('NominalGain').text))
    SaCorrSyst.append(float(Beams[ib].find('ParameterData').find('SaCorrection').text))
    GainSyst.append(float(Beams[ib].find('ParameterData').find('Gain').text))
    
    nhits.append(int(Beams[ib].find('Results').find('Hits').text))
    TSRef.append(float(Beams[ib].find('Results').find('ReferenceTargetSphereValue').text))
    GainAdj.append(float(Beams[ib].find('Results').find('GainAdjustment').text))
    SaCorrAdj.append(float(Beams[ib].find('Results').find('SACorrectionAdjustment').text))
    TsRmsError.append(float(Beams[ib].find('Results').find('StandardDeviation').text))
    TSMean.append(float(Beams[ib].find('Results').find('TSAvarage').text))
    TSMedian.append(float(Beams[ib].find('Results').find('TSMedian').text))


#########################################
# ecriture xls                        ##
########################################

wb = load_workbook(xls_file)
#sauvegarde de l'ancien au cas où
#wb.save(xls_file[:-5]+'_sav.xlsx')

if survey not in wb.sheetnames:
    #ajout feuille de la mission
    ws=wb.copy_worksheet(wb['VIERGE'])
    ws.title=survey #feuille de la mission
    
    #on la place après 'VIERGE'
    id_vierge=wb.index(wb['VIERGE'])
    n_sh=len(wb._sheets)
    wb._sheets[id_vierge+2:n_sh]=wb._sheets[id_vierge+1:n_sh-1]
    wb._sheets[id_vierge+1]=ws
else:
    ws=wb[survey]

#renseignement feuille mission
ws['C2'] = survey
dttm = datetime.datetime.strptime(date_cal, "%d.%m.%Y")
ws['C3'] = dttm
ws['C4'] = name_config
ws['C5'] = soft_vs
ws['C6'] = PL_us
ws['C7'] = soundspeed
for ib in range(nbeams):
    ws.cell(row=8,column=ib+3).value=freq_Hz[ib]
    ws.cell(row=9,column=ib+3).value=angle[ib]
    ws.cell(row=10,column=ib+3).value=abs_dBkm[ib]
    ws.cell(row=11,column=ib+3).value=GainSyst[ib]
    if len(GainNom)>0:
        ws.cell(row=12,column=ib+3).value=GainNom[ib]
        ws.cell(row=18,column=ib+3).value=GainNom[ib]+GainAdj[ib]
    else: # mode bathy
        ws.cell(row=18,column=ib+3).value=GainSyst[ib]+GainAdj[ib]
    ws.cell(row=15,column=ib+3).value=TSRef[ib]
    ws.cell(row=16,column=ib+3).value=nhits[ib]
    ws.cell(row=17,column=ib+3).value=TsRmsError[ib]
    ws.cell(row=19,column=ib+3).value=SaCorrAdj[ib]
    ws.cell(row=20,column=ib+3).value=GainAdj[ib]
    ws.cell(row=21,column=ib+3).value=TSMean[ib]
    ws.cell(row=22,column=ib+3).value=TSMean[ib]-TSMedian[ib]
ws['C13'] = sphere
ws['C14'] = minRange
Allfile_name=xml_cal_file.split('/')
ws['C23'] = Allfile_name[-1]

#ajout dans la feuille recap
liste_recap=['Recap Gain','Recap SaCorrAdj','Recap RMS']
for lrec in liste_recap:
    wsR=wb[lrec]
    #renseigne angle et beam letter
    for ib in range(nbeams):
        wsR.cell(row=ib+4,column=3).value=ws.cell(row=9,column=3+ib).value
        wsR.cell(row=ib+4,column=2).value=ws.cell(row=9,column=3+ib).column_letter
        wsR.cell(row=ib+4,column=1).value=ib+1
    
    for icol in range(4,len(wsR[2])+1):
        if wsR.cell(row=2,column=icol).value==survey:
            col_survey=icol
            col_exist=True
            break
        if wsR.cell(row=2,column=icol).value is None:
            col_survey=icol
            col_exist=False
            break  
        col_survey=icol+1
        col_exist=False
            
    if not col_exist:
        last_letter=wsR.cell(row=2, column=col_survey-1).column_letter
        letter_survey=wsR.cell(row=2, column=col_survey).column_letter
        next_letter=wsR.cell(row=2, column=col_survey+1).column_letter
        
        #copie de la dernière colonne 2 colonnes+loin
        for irow in range(1,len(wsR['A'])):
            wsR.cell(row=irow+1,column=col_survey+1)._style=wsR.cell(row=irow+1,column=col_survey-1)._style
            wsR.cell(row=irow+1,column=col_survey+1).value=wsR.cell(row=irow+1,column=col_survey-1).value
            wsR.cell(row=irow+1,column=col_survey+1).number_format=wsR.cell(row=irow+1,column=col_survey-1).number_format
        
        #déplace/traduit la dernière colonne à celle du survey
        wsR.move_range(last_letter+str(1)+':'+last_letter+str(len(wsR['A'])),rows=0,cols=1,translate=True)
        wsR[letter_survey+'2']=survey
        
        #replace la colonne sauvegardée
        for irow in range(1,len(wsR['A'])):
            wsR.cell(row=irow+1,column=col_survey-1)._style=wsR.cell(row=irow+1,column=col_survey+1)._style
            wsR.cell(row=irow+1,column=col_survey-1).value=wsR.cell(row=irow+1,column=col_survey+1).value
            wsR.cell(row=irow+1,column=col_survey-1).number_format=wsR.cell(row=irow+1,column=col_survey+1).number_format
        wsR.delete_cols(col_survey+1)
    
    for icol in range(4,len(wsR[2])+1):
        if wsR.cell(row=2,column=icol).value is None:
            max_col_cal=icol-1
            break
        max_col_cal=icol
    
    
#################
# graphes
###########

gr_titles=['Gain (dB)','SaCorAdj(dB)','RMS (dB)']
if nbeams<81:
    ymin=[25,-0.3,0]
    ymax=[35,0.3,0.6]
else:
    ymin=[17,-0.5,0]
    ymax=[27,0.5,1]
    
fig_height=[15,10,10]
deb_col=max(4,(max_col_cal-4))

nrec=len(liste_recap)
for irec in range(nrec):
    wsR=wb[liste_recap[irec]]
    #supprime les graphiques
    for ich in range(len(wsR._charts)):
        del wsR._charts[-1]
        
    # Chart 
    c =LineChart()
    c.x_axis.title = "Angle (°)"
    c.title = gr_titles[irec]
    #c.x_axis.number_format = 'mmm-yy'
    c.y_axis.scaling.min = ymin[irec]
    c.y_axis.scaling.max = ymax[irec]
    c.height=fig_height[irec]
    c.x_axis.tickLblPos = "low"

    data = Reference(wsR, min_col=deb_col, min_row=3, max_col=max_col_cal, max_row=len(wsR['A']))
    c.add_data(data, titles_from_data=True)
    x_angles = Reference(wsR, min_col=3, min_row=4, max_row=len(wsR['A']))
    c.set_categories(x_angles)
    c.x_axis.number_format='00.0'
    
    for s in c.series:
        s.marker.symbol = "triangle"
    
    wsR.add_chart(c, "B6")
    


wb.save(xls_file)

wb.close()
