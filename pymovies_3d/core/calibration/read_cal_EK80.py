# -*- coding: utf-8 -*-
"""
Created on Wed Jun  9 11:52:11 2021

@author: nlebouff

    Lit les détections d'un fichier xml d'étalonnage EK80 CW ou FM
    
    entrée: 
        xml_cal_file : nom du fichier xml (path+name)
        
    sorties (numpy arrays de dimension (nhits,) ou (nhits,nfreq) pour tsf et tsuf):
        time_det:   datetime des détections
        range_det:  range des détections
        freq:       fréquences des détections (valeur unique si CW)
        ts:         TS des détections (pulse compressé en FM)
        tsu:        TSuncomp des détections (pulse compressé en FM)
        al_det:     angle along des détections (°)
        at_det:     angle athwart des détections (°)
        sA:         sA des détections
        tsf:        spectre TS des détections (vide si CW)
        tsuf:       spectre TSuncomp des détections (vide si CW)
    
"""
import xml.etree.ElementTree as ET
import datetime
from datetime import timedelta
import pytz
import numpy as np

def read_cal_EK80(xml_cal_file):
                
    tree = ET.parse(xml_cal_file)
    root = tree.getroot()
    
    Common=root.find('Calibration').find('Common')
    Target=root.find('Calibration').find('TargetReference')
    Results=root.find('Calibration').find('CalibrationResults')
    TargetHits=root.find('Calibration').find('TargetHits')

    freq=np.array(TargetHits.find('Frequency').text.split(';'),'int')
    Hits=TargetHits.findall('HitData')
    
    nfreq=len(freq)
    nhits=len(Hits)
    time_det=np.zeros((0,0))
    range_det=np.zeros((nhits,))
    ts=np.zeros((nhits,))
    tsu=np.zeros((nhits,))
    tsf=np.zeros((nhits,nfreq))
    tsuf=np.zeros((nhits,nfreq))
    al_det=np.zeros((nhits,))
    at_det=np.zeros((nhits,))
    sA=np.zeros((nhits,))
    
    utc_tz = pytz.utc
    
    for it in range(nhits):
        tps=Hits[it].find('Time').text.split('+')
        delta_h=int(tps[1].split(':')[0])
        tps=tps[0]
        time_det=np.append(time_det,datetime.datetime.strptime(tps[:-1], "%Y-%m-%dT%H:%M:%S.%f").replace(tzinfo=utc_tz)) # enlève le 10e de microseconde
        time_det[-1]=time_det[-1]-timedelta(hours=delta_h)
        range_det[it]=float(Hits[it].find('Range').text)
        ts[it]=float(Hits[it].find('TsComp').text)
        tsu[it]=float(Hits[it].find('TsUncomp').text)
        al_det[it]=float(Hits[it].find('Along').text)
        at_det[it]=float(Hits[it].find('Athwart').text)
        sA[it]=float(Hits[it].find('SaValue').text)
        if nfreq>1:
            tsf[it,:]=np.array(Hits[it].find('CompensatedFrequencyResponse').text.split(';'),'float')
            tsuf[it,:]=np.array(Hits[it].find('UnCompensatedFrequencyResponse').text.split(';'),'float')
    
    return [time_det,range_det,freq,ts,tsu,al_det,at_det,sA,tsf,tsuf]
