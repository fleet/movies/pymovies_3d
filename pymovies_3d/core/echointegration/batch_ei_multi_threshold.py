﻿# #!/usr/bin/python
# # -*- coding: utf-8 -*-

import os

from pymovies_3d.core.echointegration.sampleechointegration import (
    sample_echo_integration,
)


def batch_ei_multi_threshold(
    path_hac_survey,
    path_config,
    path_save,
    thresholds,
    runs=None,
    nameTransect=None,
    dateStart=None,
    timeStart=None,
    dateEnd=None,
    timeEnd=None,
):
    if runs is not None:
        for ir in runs:
            for t in thresholds:
                # build dedicated string
                str_run = "%03d" % (ir)
                chemin_config = path_config + "/" + str(t)
                if not os.path.exists(chemin_config):
                    raise RuntimeError(
                        chemin_config + " : Invalid path for MOVIES3D configuration"
                    )

                chemin_hac = path_hac_survey + r"/RUN" + str_run
                if not os.path.exists(chemin_hac):
                    raise RuntimeError(chemin_hac + " : Invalid path for HAC files")
                filelist = [f for f in os.listdir(chemin_hac) if f.endswith(".hac")]
                if len(filelist) == 0:
                    raise RuntimeError(chemin_hac + " does not contain HAC files")

                chemin_save = path_save + r"/RUN" + str_run + "/" + str(t)

                # Run EI
                sample_echo_integration(
                    chemin_hac,
                    chemin_config,
                    chemin_save,
                    nameTransect,
                    dateStart,
                    timeStart,
                    dateEnd,
                    timeEnd,
                )

    else:
        for t in thresholds:
            # build dedicated string
            # str_run = "%03d" % (ir)
            chemin_config = path_config + "/" + str(t)
            if not os.path.exists(chemin_config):
                raise RuntimeError(
                    chemin_config + " : Invalid path for MOVIES3D configuration"
                )

            chemin_hac = path_hac_survey
            if not os.path.exists(chemin_hac):
                raise RuntimeError(chemin_hac + " : Invalid path for HAC files")

            chemin_save = path_save + "/" + str(t)

            # Run EI
            sample_echo_integration(
                chemin_hac,
                chemin_config,
                chemin_save,
                nameTransect,
                dateStart,
                timeStart,
                dateEnd,
                timeEnd,
            )


# batch_EI_multiTHR(path_hac_survey,path_config,path_save,thresholds,nameTransect = 'Fish',dateStart=datedebut,timeStart=heuredebut,dateEnd=datefin,timeEnd=heurefin)
