# -*- coding: utf-8 -*-
"""
Created on Thu Apr 16 09:21:55 2020

@author: Guillaume Brosse
Entrées :  -Sv,Sv_moy_transducteur_x,freq_MFR,freqs_moy_transducteur,nombre_de_categories,Sa,Time,
            Depth,Lat,Lon,nb_transduc,indices_radiales ---> données d'echo-integration standardisées (voir ei_standardizing.py)

           -threshold

           -save ---> Par defaut : False. Si True alors les résultats seronts sauvegardés dans
                             des fichiers .pickle

           -filename ---> Par defaut : None. Si Not None alors le fichier .pickle aura un nom défini

Sorties : Si save = False : Les données suivantes sont retournées à la fin de la fonction

                            -kmeans --->données de classification

                            -labels ---> labels de chaque cluster

                            -Q1_ ---> premier quartile de la réponse en fréquence

                            -Q3_ ---> troisième quartile de la réponse en fréquence

                            -med ---> réponse en fréquence médiane

          Si save = true : Les données kmeans,labels,Q1_,Q3_,med ainsi que les
                          données d'écho-integration nombre_de_categories,Sv,
                         Sv_moy_transducteur_x,Sa,Time,freq_MFR,
                         freqs_moy_transducteur,Depth,Lat,Lon,nb_transduc,indices_radiales
                         seront sauvegardées dans un fichier .pickle



"""
import pickle

import numpy as np
from sklearn.cluster import KMeans
from sklearn.manifold import LocallyLinearEmbedding


# pylint:disable=consider-using-enumerate
def clustering(
    Sv,
    Sv_moy_transducteur_x,
    freq_MFR,
    freqs_moy_transducteur,
    nombre_de_categories,
    Sa,
    Time,
    Depth,
    Lat,
    Lon,
    nb_transduc,
    tableau_radiales,
    nom_numero,
    methode_clustering,
    nombre_composantes,
    nombre_voisins,
    save=False,
    filename=None,
):
    Sv_vect = np.zeros([len(Sv) * len(Sv[1]), len(freq_MFR)])
    for i in range(len(freq_MFR)):
        Sv_vect[:, i] = np.reshape((Sv[:, :, i]), (1, len(Sv) * len(Sv[1])))

    # filter bad values from EI where no echo were integrated (mainly after bottom detection)
    ind_suppr = np.nonzero(Sv_vect == -9999999.9)
    vect_ind_suppr = np.unique(ind_suppr[0])
    Sv_vect_clean = np.delete(Sv_vect, vect_ind_suppr, axis=0)

    if len(Sv_vect_clean) == 0:
        Sv_vect_clean = Sv_vect
        print(
            "/!\\ Attention : le résultat du clustering risque d'etre altéré par la présence",
            "d'une grande quantité de valeurs à -100 dB /!\\ ",
        )

    # clustering normalisé par rapport à la moyenne
    # mean_sv = np.matlib.repmat(10*np.log10(np.mean(10.**(Sv_vect_clean/10),axis=1)),len(Sv_vect_clean[0]),1)
    # Sv_vect_norm = Sv_vect_clean-np.transpose(mean_sv)

    # clustering normalisé par rapport à la fréquence 0
    Sv_vect_norm = Sv_vect_clean - np.outer(
        Sv_vect_clean[:, 0], np.ones((len(freq_MFR)))
    )

    # clustering sans normalisation
    Sv_vect_norm = Sv_vect_clean

    if methode_clustering == "KMeans_seul":
        # clustering avec kmeans seul
        kmeans = KMeans(n_clusters=nombre_de_categories, random_state=0).fit(
            Sv_vect_norm
        )

    else:
        # clustering avec réduction de dimension (LLE)
        if nombre_composantes>len(freq_MFR):
            print('Nombre de composantes trop élevé par rapport au nombre de fréquences')
        elif nombre_voisins<nombre_composantes:
            print('Nombre de voisins trop faible par rapport au nombre de composantes')
        else:
            lle = LocallyLinearEmbedding(
                n_components=nombre_composantes, n_neighbors=nombre_voisins, method="modified", n_jobs=4, random_state=0
            )
            lle.fit(Sv_vect_norm[:, :])
            X_lle = lle.transform(Sv_vect_norm)
            # CLUSTERING
            kmeans = KMeans(n_clusters=nombre_de_categories, random_state=0).fit(X_lle)

            # reindexation des résultats sur l'ensemble des données
            j = 0
            vectLabel = np.empty([len(Sv) * len(Sv[1])])
            vectLabel.fill(np.nan)
            for i in range(len(vectLabel)):
                if not i in vect_ind_suppr:
                    vectLabel[i] = kmeans.labels_[j]
                    j = j + 1
        
            labels = np.squeeze(np.reshape(vectLabel, (len(Sv), len(Sv[1]))))
        
            Q1_ = np.zeros([nombre_de_categories, len(freq_MFR)])
            Q3_ = np.zeros([nombre_de_categories, len(freq_MFR)])
            med = np.zeros([nombre_de_categories, len(freq_MFR)])
            for j in range(nombre_de_categories):
        
                liste_temp_q = np.squeeze(
                    Sv_vect_clean[np.where((np.squeeze(kmeans.labels_)) == j), :]
                )
                Q1_[j, :] = np.quantile(liste_temp_q, 0.25, axis=0)
                Q3_[j, :] = np.quantile(liste_temp_q, 0.75, axis=0)
                med[j, :] = np.quantile(liste_temp_q, 0.5, axis=0)
        
            if save:
                if filename is None:
                    filename = "saved_clustering.pickle"
                with open(filename, "wb") as f:
                    pickle.dump(
                        [
                            kmeans,
                            labels,
                            Q1_,
                            Q3_,
                            med,
                            nombre_de_categories,
                            Sv,
                            Sv_moy_transducteur_x,
                            Sa,
                            Time,
                            freq_MFR,
                            freqs_moy_transducteur,
                            Depth,
                            Lat,
                            Lon,
                            nb_transduc,
                            tableau_radiales,
                            nom_numero,
                        ],
                        f,
                    )

    return kmeans, labels, Q1_, Q3_, med


def clustering_netcdf(
    Sv,
    freq_MFR,
    nombre_de_categories,
    Time,
    Depth,
    Lat,
    Lon,
    nb_transduc,
    nom_numero,
    methode_clustering,
    nombre_composantes,
    nombre_voisins,
    save=False,
    filename=None,
):
    Sv_vect = np.zeros([len(Sv) * len(Sv[1]), len(freq_MFR)])
    for i in range(len(freq_MFR)):
        Sv_vect[:, i] = np.reshape((Sv[:, :, i]), (1, len(Sv) * len(Sv[1])))

    # filter bad values from EI where no echo were integrated (mainly after bottom detection)
    test = np.ma.masked_array(Sv_vect, np.isnan(Sv_vect), fill_value=-9999999.9)
    fixed = np.ma.fix_invalid(test)
    # fixed.data
    ind_suppr = np.nonzero(fixed.data < -100)
    vect_ind_suppr = np.unique(ind_suppr[0])
    Sv_vect_clean = np.delete(fixed.data, vect_ind_suppr, axis=0)

    if len(Sv_vect_clean) == 0:
        Sv_vect_clean = Sv_vect
        print(
            "/!\\ Attention : le résultat du clustering risque d'etre altéré par la présence",
            "d'une grande quantité de valeurs à -100 dB /!\\ ",
        )

    # clustering normalisé par rapport à la moyenne
    # mean_sv = np.matlib.repmat(10*np.log10(np.mean(10.**(Sv_vect_clean/10),axis=1)),len(Sv_vect_clean[0]),1)
    # Sv_vect_norm = Sv_vect_clean-np.transpose(mean_sv)

    # clustering normalisé par rapport à la fréquence 0
    Sv_vect_norm = Sv_vect_clean - np.outer(
        Sv_vect_clean[:, 0], np.ones((len(freq_MFR)))
    )

    # clustering sans normalisation
    Sv_vect_norm = Sv_vect_clean

    if methode_clustering == "KMeans_seul":
        # clustering avec kmeans seul
        kmeans = KMeans(n_clusters=nombre_de_categories, random_state=0).fit(
            Sv_vect_norm
        )

    else:
        if nombre_composantes>len(freq_MFR):
            print('Nombre de composantes trop élevé par rapport au nombre de fréquences')
        elif nombre_voisins<nombre_composantes:
            print('Nombre de voisins trop faible par rapport au nombre de composantes')
        else:
            # clustering avec réduction de dimension (LLE)
            lle = LocallyLinearEmbedding(
                n_components=nombre_composantes, n_neighbors=nombre_voisins, method="modified", n_jobs=4, random_state=0
            )
            lle.fit(Sv_vect_norm[:, :])
            X_lle = lle.transform(Sv_vect_norm)
            # CLUSTERING
            kmeans = KMeans(n_clusters=nombre_de_categories, random_state=0).fit(X_lle)

            # reindexation des résultats sur l'ensemble des données
            j = 0
            vectLabel = np.empty([len(Sv) * len(Sv[1])])
            vectLabel.fill(np.nan)
            for i in range(len(vectLabel)):
                if not i in vect_ind_suppr:
                    vectLabel[i] = kmeans.labels_[j]
                    j = j + 1
        
            labels = np.squeeze(np.reshape(vectLabel, (len(Sv), len(Sv[1]))))
        
            Q1_ = np.zeros([nombre_de_categories, len(freq_MFR)])
            Q3_ = np.zeros([nombre_de_categories, len(freq_MFR)])
            med = np.zeros([nombre_de_categories, len(freq_MFR)])
            for j in range(nombre_de_categories):
        
                liste_temp_q = np.squeeze(
                    Sv_vect_clean[np.where((np.squeeze(kmeans.labels_)) == j), :]
                )
                Q1_[j, :] = np.quantile(liste_temp_q, 0.25, axis=0)
                Q3_[j, :] = np.quantile(liste_temp_q, 0.75, axis=0)
                med[j, :] = np.quantile(liste_temp_q, 0.5, axis=0)
        
            if save:
                if filename is None:
                    filename = "saved_clustering.pickle"
                with open(filename, "wb") as f:
                    pickle.dump(
                        [
                            kmeans,
                            labels,
                            Q1_,
                            Q3_,
                            med,
                            nombre_de_categories,
                            Sv,
                            Time,
                            freq_MFR,
                            Depth,
                            Lat,
                            Lon,
                            nb_transduc,
                            nom_numero,
                            methode_clustering,
                        ],
                        f,
                    )
    return kmeans, labels, Q1_, Q3_, med
