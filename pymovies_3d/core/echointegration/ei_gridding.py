import math
import os
import calendar
import time
import datetime

import netCDF4 as nc

import pyMovies.pyMovies as mv
from sonar_netcdf import sonar_groups as xsf

import pymovies_3d.core.hac_util.hac_util as util
import pymovies_3d.core.export.sonarNetCDFutils as sonarNetCDFutils
from pymovies_3d.core.export.sonarNetCDFutils import BeamType


# pylint:disable=too-many-nested-blocks


class eigridding:
    def __init__(self):
        self.savePath = ""
        self.gridGroups = {}

        # 0 : Sv, 1 : Sa
        self.backscatter_type = 0

    def process_sounder(self, ds, sounder, nb_beams, nb_layersByType, nb_freqs, nb_pings):

        rootGroup = xsf.RootGrp()
        rootGroup.create_group(parent_group=ds)

        sonarGroup = xsf.SonarGrp()
        sonarGroup_ds = sonarGroup.create_group(parent_group=ds)
        sonarGroup.sonar_serial_number = "M3D Sonar"

        self.gridGroups[sounder.m_SounderId] = {}

        gridGroupIdx = 1
        for layerType, count in nb_layersByType.items():
            if count > 0:
                print(
                    "Create grip group for type "
                    + str(layerType)
                    + ", with "
                    + str(count)
                    + " layer"
                )
                gridGroup = sonarNetCDFutils.GridGroup(
                    parentGroup=sonarGroup_ds,
                    index=gridGroupIdx,
                    beamDimensions=nb_beams,
                    frequencyDimensions=nb_freqs,
                    pingDimensions=nb_pings,
                    rangeDimensions=count,
                )
                gridGroup.backscatter_type = self.backscatter_type

                # Range = 0, Depth = 1
                gridGroup.range_axis_interval_type = (
                    1 if layerType == mv.LayerType.SurfaceLayer else 0
                )
                self.gridGroups[sounder.m_SounderId][layerType] = gridGroup

                gridGroupIdx += 1

    def run(
        self,
        chemin_ini,
        chemin_config,
        nameTransect=None,
        dateStart=None,
        timeStart=None,
        dateEnd=None,
        timeEnd=None,
        sounderIdent=None
    ):
        """
        Effectue l'écho-intégration du fichier ou du répertoire indiqué, et l'enregistre en NetCdf

        entrées:
                - chemin_ini: chemin du répertoire ou du fichier à traiter
                - chemin_config: chemin de la configuration M3D à utiliser (ESU, couches etc)
                - nameTransect: nom de sauvegarde (optionnel). Si None, les fichiers pickles seront
                nommés results_000X_EI.pickle
                - dateStart et dateEnd: dates de début et fin des séquences à écho-intégrer, au format 'dd/mm/yyyy' ou ['dd/mm/yyyy','dd/mm/yyyy'...]
                Si dateStart est None, tout le répertoire est écho-intégré
                - timeStart et timeEnd: heures de début et fin des séquences à écho-intégrer, au format 'hh:mm:ss' ou ['hh:mm:ss','hh:mm:ss'...]
                - sounderIdent: identifiant sondeur à écho-intégrer
            sortie:
                - Un fichier netcdf pour la période est créé
        """

        # load configuration
        if not os.path.exists(chemin_config):
            print("****** stop: chemin de configuration non valide")
            return

        mv.moLoadConfig(chemin_config)

        if nameTransect is not None:
            str_Name = "_" + nameTransect + "_"
        else:
            str_Name = ""

        if dateStart is None:
            range_date = [-1]
            process_all = True
        else:
            if isinstance(dateStart, str):
                dateStart = [dateStart]
            if isinstance(dateEnd, str):
                dateEnd = [dateEnd]
            if isinstance(timeStart, str):
                timeStart = [timeStart]
            if isinstance(timeEnd, str):
                timeEnd = [timeEnd]

            range_date = range(len(dateStart))
            if (len(dateEnd) + len(timeStart) + len(timeEnd)) / 3 != len(dateStart):
                print(
                    "****** stop: les listes des dates/heures de début/fin n ont pas la même taille"
                )
                return
            process_all = False

        for x in range_date:

            slice_end = False

            if not process_all:
                goto = calendar.timegm(
                    time.strptime(
                        "%s %s" % (
                            (dateStart[x]), (timeStart[x])), "%d/%m/%Y %H:%M:%S"
                    )
                )
                mv.moOpenHac(chemin_ini)
                util.hac_goto(goto)
                timeEndEI = calendar.timegm(
                    time.strptime(
                        "%s %s" % (dateEnd[x], timeEnd[x]), "%d/%m/%Y %H:%M:%S"
                    )
                )
            else:
                # chargement de tous les fichiers HAC contenus dans le repertoire
                # et lecture du premier chunk
                print("chemin_ini : " + str(chemin_ini))
                mv.moOpenHac(chemin_ini)
                timeEndEI = calendar.timegm(
                    datetime.datetime.now().timetuple())

            # Activate 'EchoIntegrModule'
            EchoIntegrModule = mv.moEchoIntegration()
            EchoIntegrModule.setEnable(True)

            # Calibration module
            CalibModule = mv.moCalibration()

            # récupération de la définition des couches
            LayersDef = EchoIntegrModule.GetEchoIntegrationParameter().m_tabLayerDef
            nbLayers = len(LayersDef)
            nbLayersByType = {}
            layerIndexes = {}
            for indexLayer in range(nbLayers):
                layerType = LayersDef[indexLayer].m_layerType

                indexes = layerIndexes.get(layerType, [])
                indexes.append(indexLayer)
                layerIndexes[layerType] = indexes

                nbLayersByType[layerType] = nbLayersByType.get(
                    layerType, 0) + 1

            # recupération du esu manager
            # Time_seconds = 0, Distance_nautical_miles = 1, Distance_meters = 2, Number_of_ping = 3
            esuMgr = mv.moGetEsuManager()
            esuParamater = esuMgr.GetEsuManagerParameter()
            if esuParamater.m_esuCutType == mv.ESUCutType.EsuCutByTime:
                ping_axis_interval_type = 0
                ping_axis_interval_value = esuParamater.m_time
            elif esuParamater.m_esuCutType == mv.ESUCutType.EsuCutByDistance:
                ping_axis_interval_type = 1
                ping_axis_interval_value = esuParamater.m_distance
            elif esuParamater.m_esuCutType == mv.ESUCutType.EsuCutByPingNb:
                ping_axis_interval_type = 3
                ping_axis_interval_value = esuParamater.m_pingNumber

            FileStatus = mv.moGetFileStatus()
            while not FileStatus.m_StreamClosed:

                mv.moReadChunk()

                if not process_all:
                    outputList = EchoIntegrModule.GetOutputList()
                    if len(outputList) > 0:
                        firstEsu = outputList[0]
                        timeESU = (
                            firstEsu.m_timeEnd.m_TimeCpu
                            + firstEsu.m_timeEnd.m_TimeFraction / 10000
                        )
                        if timeESU > timeEndEI:
                            break

            # split result for each sounder
            AllData = EchoIntegrModule.GetOutputList()
            results = {}
            nbresults= len(AllData)
            for dataIdx in range(nbresults):
                ESUResult = AllData[dataIdx]
                sounderResults = results.get(ESUResult.m_SounderId, [])
                sounderResults.append(ESUResult)
                results[ESUResult.m_SounderId] = sounderResults

            #
            for sounder, sounderResults in results.items() :


                #PROVISOIRE on ne récupère pas le premier ESU qui peut être vide suite à un goto
                ESUResult = sounderResults[2]

                if (ESUResult.m_SounderId==sounderIdent) | (sounderIdent is None):
                    nbPings = len(sounderResults)
                    print(f"Process sounder {sounder} : {nbPings}")
                    nbBeams = len(ESUResult.m_tabChannelResult)

                    ds = nc.Dataset(
                                self.savePath
                                + "_"
                                + str(ESUResult.m_SounderId)
                                + ".xsf.nc",
                                "w",
                            )

                    # On récupère le sondeur
                    sounder = self.get_sounder(ESUResult.m_SounderId)

                    # de la même façon créer un tableau de SoftChannels correspondant à l'index du beam (nbeams = Somme des softChan des transducteurs)
                    allChans = []
                    allTrans = []
                    for indexTrans in range(sounder.m_numberOfTransducer):
                        transducer = sounder.GetTransducer(indexTrans)
                        # indexBeam = beam in SonarNetCDF
                        for indexChan in range(transducer.m_numberOfSoftChannel):
                            allChans.append(transducer.getSoftChannelPolarX(indexChan))
                            allTrans.append(transducer)
                    nbChans = len(allChans)

                    # On récupère la liste de toutes les fréquences et les noms des transducteurs associés, pour tous les transducteurs
                    allFreqs = []
                    allGains = []
                    beamRefs = []
                    # pylint:disable=consider-using-enumerate
                    # on ne peut pas utiliser enumerate sur les objets boost Python C++
                    for esuResultIdx in range(len(ESUResult.m_tabChannelResult)):
                        esuResult = ESUResult.m_tabChannelResult[esuResultIdx]
                        for freqIdx in range(len(esuResult.m_tabFrequencies)):
                            allFreqs.append(esuResult.m_tabFrequencies[freqIdx])
                            beamRefs.append(allTrans[esuResultIdx].m_transName)
                        gains = CalibModule.getCalibrationGain(allChans[esuResultIdx], esuResult.m_tabFrequencies)
                        for gainIdx in range(len(gains)):
                            allGains.append(gains[gainIdx])
                    nbFreqs = len(allFreqs)

                    self.process_sounder(ds, sounder, nbBeams, nbLayersByType, nbFreqs, nbPings)

                    gridGroups = self.gridGroups[ESUResult.m_SounderId]
                    for gridType, gridGroup in gridGroups.items():

                        gridGroup.ping_axis_interval_type = ping_axis_interval_type
                        gridGroup.ping_axis_interval_value = ping_axis_interval_value

                        # On remplit les fréquences
                        for indexFreq in range(nbFreqs):
                            gridGroup.frequency[indexFreq] = allFreqs[indexFreq]
                            gridGroup.beam_reference[indexFreq] = beamRefs[indexFreq]

                        for indexChan in range(nbChans):
                            if allChans[indexChan].m_beamType == BeamType.BeamTypeSplit:
                                gridGroup.beam_type[indexChan] = 2 # split_aperture_4_subbeams
                            elif allChans[indexChan].m_beamType == BeamType.BeamTypeSplit3:
                                gridGroup.beam_type[indexChan] = 3 # split_aperture_4_subbeams
                            elif  allChans[indexChan].m_beamType == BeamType.BeamTypeSplit3CN:
                                gridGroup.beam_type[indexChan] = 4 # split_aperture_4_subbeams
                            gridGroup.beam[indexChan] = allTrans[indexChan].m_transName

                    indexPing = -1
                    for ESUResult in sounderResults:

                        timeESU = ESUResult.m_timeEnd.m_TimeCpu + ESUResult.m_timeEnd.m_TimeFraction / 10000

                        indexPing += 1

                        for gridType, gridGroup in gridGroups.items():

                            # index Ping, méthode plus simple
                            #indexPing = len(gridGroup.sound_speed_at_transducer)

                            # Vitesse du son
                            gridGroup.sound_speed_at_transducer[indexPing] = sounder.m_soundVelocity


                            # stabilisation ? dans le contexte M3D => 1 si ME70, 0 sinon
                            gridGroup.beam_stabilisation[indexPing] = (1 if sounder.m_isMultiBeam else 0)

                            # 0 dans le contexte M3D
                            gridGroup.non_quantitative_processing[indexPing] = 0

                            # gridGroup.tx_transducer_depth[
                            #    indexPing
                            # ] = mainBeamData.m_compensateHeave

                            indexAllFreq = 0

                            for indexBeam in range(nbBeams):
                                tabChannelResult = ESUResult.m_tabChannelResult[indexBeam]
                                freqs = tabChannelResult.m_tabFrequencies

                                gridGroup.cell_ping_time[indexPing, indexBeam] = timeESU*1000000

                                # On remplit le Bottom Range
                                gridGroup.detected_bottom_range[indexPing, indexBeam] = tabChannelResult.m_bottomDepth

                                transducer = allTrans[indexBeam]
                                softChan = allChans[indexBeam]

                                gridGroup.sample_interval[indexPing, indexBeam] = transducer.m_timeSampleInterval * 1e-6
                                gridGroup.blanking_interval[indexPing, indexBeam] = 0

                                gridGroup.transmit_power[indexPing, indexBeam] = softChan.m_transmissionPower
                                gridGroup.beamwidth_transmit_major[indexPing, indexBeam] = math.degrees(softChan.m_beam3dBWidthAthwartRad)
                                gridGroup.beamwidth_transmit_minor[indexPing, indexBeam] = math.degrees(softChan.m_beam3dBWidthAlongRad)
                                gridGroup.beamwidth_receive_major[indexPing, indexBeam] = math.degrees(softChan.m_beam3dBWidthAthwartRad)
                                gridGroup.beamwidth_receive_minor[indexPing, indexBeam] = math.degrees(softChan.m_beam3dBWidthAlongRad)
                                gridGroup.gain_correction[indexPing, indexBeam] = softChan.m_beamSACorrection
                                gridGroup.equivalent_beam_angle[indexPing, indexBeam] = math.pow(10, softChan.m_beamEquTwoWayAngle * 0.1)

                                # Ecriture des données dépendante du txBeam

                                # CW => CW(0), FM => LFM(1), pas HFM(2) dans M3D
                                gridGroup.transmit_type[indexPing, indexBeam] = 1 if transducer.m_pulseShape == 2 else 0

                                # => internalDelayTranslation => modify transducteur, on laisse à 0
                                gridGroup.sample_time_offset[indexPing, indexBeam] = 0

                                gridGroup.transmit_bandwidth[indexPing, indexBeam] = softChan.m_bandWidth
                                gridGroup.tx_beam_rotation_phi[indexPing, indexBeam] = math.degrees(softChan.m_mainBeamAthwartSteeringAngleRad)
                                gridGroup.rx_beam_rotation_phi[indexPing, indexBeam] = math.degrees(softChan.m_mainBeamAthwartSteeringAngleRad)
                                gridGroup.tx_beam_rotation_theta[indexPing, indexBeam] = math.degrees(softChan.m_mainBeamAlongSteeringAngleRad)
                                gridGroup.rx_beam_rotation_theta[indexPing, indexBeam] = math.degrees(softChan.m_mainBeamAlongSteeringAngleRad)
                                gridGroup.tx_beam_rotation_psi[indexPing, indexBeam] = 0
                                gridGroup.rx_beam_rotation_psi[indexPing, indexBeam] = 0

                                # M - Nominal duration of the transmit pulse. This is not the effective pulse duration.
                                gridGroup.transmit_duration_nominal[indexPing, indexBeam] = transducer.m_pulseDuration


                                # MA - Effective duration of the received pulse. This is the duration of the square pulse containing the same energy as the actual receive pulse. This parameter is either theoretical or comes from a calibration exercise and adjusts the nominal duration of the transmitted pulse to the measured one. During calibration it is obtained by integrating the energy of the received signal on the calibration target normalised by its maximum energy. Necessary for type 1, 2,3 and 4 conversion equations.
                                if transducer.m_pulseShape == 2:
                                    T = softChan.m_softChannelComputeData.GetTransmitSignalObject(transducer, softChan).m_effectivePulseLength
                                    gridGroup.transmit_frequency_start[indexPing, indexBeam] = softChan.m_startFrequency
                                    gridGroup.transmit_frequency_stop[indexPing, indexBeam] = softChan.m_endFrequency
                                else:
                                    T = transducer.m_pulseDuration / 1000000.0
                                    gridGroup.transmit_frequency_start[indexPing, indexBeam] = softChan.m_acousticFrequency
                                    gridGroup.transmit_frequency_stop[indexPing, indexBeam] = softChan.m_acousticFrequency

                                effectivePulseLength = T * math.pow(10, (2.0 * softChan.m_beamSACorrection) / 10.0)
                                gridGroup.receive_duration_effective[indexPing, indexBeam] = effectivePulseLength


                                # Cell Latitude/Longitude/Depth
                                for idx, indexLayer in enumerate(layerIndexes.get(gridType, [])) :
                                    layerResult = tabChannelResult.m_tabLayerResult[0][indexLayer]
                                    gridGroup.cell_latitude[indexPing, idx, indexBeam] = layerResult.m_latitude
                                    gridGroup.cell_longitude[indexPing, idx, indexBeam] = layerResult.m_longitude
                                    gridGroup.cell_depth[idx, indexBeam] = layerResult.m_Depth


                                for indexFreq in range(len(freqs)):
                                    gridGroup.transducer_gain[indexPing, indexAllFreq] = allGains[indexAllFreq]
                                    freqResult = tabChannelResult.m_tabLayerResult[indexFreq]
                                    for idx, indexLayer in enumerate(layerIndexes.get(gridType, [])) :
                                        # on sauvegarde le MVBS et on déduiré le NASC du MVBS
                                        gridGroup.integrated_backscatter[indexPing, idx, indexAllFreq] = freqResult[indexLayer].m_Sv

                                    indexAllFreq += 1


                    print("Saving results")
                    for gridType, gridGroup in gridGroups.items():
                        gridGroup.save()

                    # Synchronisation du dataset et fermeture du fichier
                    ds.sync()
                    ds.close()

            # Suppression des résultats d'EI consommés
            EchoIntegrModule.ReleaseOutputList()

            EchoIntegrModule.setEnable(False)
        return

    def get_sounder(self, sounder_id):
        sounders = mv.moGetSounderDefinition()
        nb_sounders = sounders.GetNbSounder()
        for i in range(nb_sounders):
            sounder = sounders.GetSounder(i)
            if sounder.m_SounderId == sounder_id:
                return sounder
        return None
