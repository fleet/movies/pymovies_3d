from contextlib import redirect_stdout
import io
import math
import os
import calendar
from pathlib import Path
import time
import datetime

import netCDF4 as nc

import pyMovies.pyMovies as mv
from sonar_netcdf import sonar_groups as xsf
import pymovies_3d.core.hac_util.hac_util as util
import pymovies_3d.core.export.sonarNetCDFutils as sonarNetCDFutils
from pymovies_3d.core.export.sonarNetCDFutils import BeamType


# pylint:disable=too-many-nested-blocks


import os
import sys
from contextlib import contextmanager

console_redirect = None #use this to keep loggin
#console_redirect = os.devnull #use this to remove all log, even from system


@contextmanager
def stdout_redirected(to=os.devnull):
    '''
    import os

    with stdout_redirected(to=filename):
        print("from Python")
        os.system("echo non-Python applications are also supported")
    '''

    def _redirect_stdout(to,fd):
        sys.stdout.close() # + implicit flush()
        os.dup2(to.fileno(), fd) # fd writes to 'to' file
        sys.stdout = os.fdopen(fd, 'w') # Python writes to fd

    if to is None:
        yield
        return
    else:
        try:
            fd = sys.stdout.fileno()
        
            ##### assert that Python and C stdio write using the same file descriptor
            ####assert libc.fileno(ctypes.c_void_p.in_dll(libc, "stdout")) == fd == 1
            with os.fdopen(os.dup(fd), 'w') as old_stdout:
                with open(to, 'w') as file:
                    _redirect_stdout(to=file,fd=fd)
                try:
                    yield # allow code to be run with the redirected stdout
                finally:
                    _redirect_stdout(to=old_stdout,fd=fd) # restore stdout.
                                                    # buffering and flags such as
                                                    # CLOEXEC may be different
        except io.UnsupportedOperation :
             #in jupyter notebook, sys.stdout is not a file, and raise an exception
            yield
class eigridding_cpp:
    def __init__(self):
        self.gridGroups = {}

        # 0 : Sv, 1 : Sa
        self.backscatter_type = 0

    def run(
        self,
        chemin_ini,
        chemin_config,
        chemin_save,
        nameTransect=None,
        dateStart=None,
        timeStart=None,
        dateEnd=None,
        timeEnd=None
    ):
        """
        Effectue l'écho-intégration du fichier ou du répertoire indiqué, et l'enregistre en NetCdf

        entrées:
                - chemin_ini: chemin du répertoire ou du fichier à traiter
                - chemin_config: chemin de la configuration M3D à utiliser (ESU, couches etc)
                - nameTransect: nom de sauvegarde (optionnel). Si None, les fichiers pickles seront
                nommés results_000X_EI.pickle
                - dateStart et dateEnd: dates de début et fin des séquences à écho-intégrer, au format 'dd/mm/yyyy' ou ['dd/mm/yyyy','dd/mm/yyyy'...]
                Si dateStart est None, tout le répertoire est écho-intégré
                - timeStart et timeEnd: heures de début et fin des séquences à écho-intégrer, au format 'hh:mm:ss' ou ['hh:mm:ss','hh:mm:ss'...]
            sortie:
                - Un fichier netcdf pour la période est créé
        """

        # load configuration
        if not os.path.exists(chemin_config):
            print("****** stop: chemin de configuration non valide")
            return

        with stdout_redirected(to=console_redirect):
            mv.moLoadConfig(chemin_config)

        if nameTransect is not None:
            str_Name = "_" + nameTransect + "_"
        else:
            str_Name = ""

        if dateStart is None:
            range_date = [-1]
            process_all = True
        else:
            if isinstance(dateStart, str):
                dateStart = [dateStart]
            if isinstance(dateEnd, str):
                dateEnd = [dateEnd]
            if isinstance(timeStart, str):
                timeStart = [timeStart]
            if isinstance(timeEnd, str):
                timeEnd = [timeEnd]

            range_date = range(len(dateStart))
            if (len(dateEnd) + len(timeStart) + len(timeEnd)) / 3 != len(dateStart):
                print(
                    "****** stop: les listes des dates/heures de début/fin n ont pas la même taille"
                )
                return
            process_all = False
            
            #on met le chunk à 1 pour permettre ensuite de faire l'EI au ping prêt par rapport à la date de fin
            ParameterDef = mv.moLoadReaderParameter()
            ParameterDef.m_ChunckDef.m_chunkNumberOfPingFan = 1
            mv.moSaveReaderParameter(ParameterDef)

        for x in range_date:

            slice_end = False

            if not process_all:
                goto = calendar.timegm(
                    time.strptime(
                        "%s %s" % (
                            (dateStart[x]), (timeStart[x])), "%d/%m/%Y %H:%M:%S"
                    )
                )
                if not Path(chemin_ini).exists():
                    raise FileNotFoundError(f"hac file path {chemin_ini} does not exists")
                print(f"Opening hac files : {chemin_ini}" )
            
                with stdout_redirected(to=console_redirect):
                    
                    mv.moOpenHac(chemin_ini)
                    util.hac_goto(goto)
        
                timeEndEI = calendar.timegm(
                    time.strptime(
                        "%s %s" % (dateEnd[x], timeEnd[x]), "%d/%m/%Y %H:%M:%S"
                    )
                )
            else:
                # chargement de tous les fichiers HAC contenus dans le repertoire
                # et lecture du premier chunk
                print("chemin_ini : " + str(chemin_ini))
                with stdout_redirected(to=console_redirect):
                    mv.moOpenHac(chemin_ini)
                timeEndEI = calendar.timegm(
                    datetime.datetime.now().timetuple())

            with stdout_redirected(to=console_redirect):
                EchoIntegrModule = mv.moEchoIntegration()

                # Set NetCDF file prefix with transect name
                paramEchointegration=EchoIntegrModule.GetEchoIntegrationParameter()
                paramEchointegration.setFilenamePrefix(nameTransect)
                paramEchointegration.setFilePath(chemin_save)

                # Activate 'EchoIntegrModule'
                EchoIntegrModule.setEnable(True)

                FileStatus = mv.moGetFileStatus()
                last_file = FileStatus.m_StreamDescName
                print(f'processing {last_file}')
                while not FileStatus.m_StreamClosed:
                    new_file = FileStatus.m_StreamDescName
                    if new_file!=last_file:
                        print(f'reading {last_file}')
                        last_file = new_file
                    

                    mv.moReadChunk()

                    if not process_all:
                        outputList = EchoIntegrModule.GetOutputList()
                        if len(outputList) > 0:
                            lastEsu = outputList[-1]
                            timeESU = (
                                lastEsu.m_timeEnd.m_TimeCpu
                                + lastEsu.m_timeEnd.m_TimeFraction / 10000
                            )
                            if timeESU > timeEndEI:
                                break

                # Suppression des résultats d'EI consommés
                EchoIntegrModule.ReleaseOutputList()

                EchoIntegrModule.setEnable(False)
                mv.moReadChunk()
        return
