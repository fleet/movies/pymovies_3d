# -*- coding: utf-8 -*-
"""
Created on Wed Apr 15 14:38:54 2020

@author: guillaume
"""
import pickle
import numpy as np


#pylint:disable=consider-using-enumerate
def scaling(
    freqs_XXxx_db,
    Sv_surfXXxx_db,
    Sa_surfXXxx_db,
    depth_surface_XXxx_db,
    depth_bottom_XXxx_db,
    lat_surfXXxx_db,
    lon_surfXXxx_db,
    indices_transduc_choisis,
    save=False,
    filename=None,
):
    nb_transduc_choisis = len(indices_transduc_choisis)
    freq_prec = freqs_XXxx_db[0][indices_transduc_choisis[0]]
    liste_indice_max = []
    for itsd_choisi in indices_transduc_choisis :
        liste = freqs_XXxx_db[:,itsd_choisi]
        print(min(liste))
        liste_indice_max.append(min(liste))
    print(liste_indice_max)
    if len(liste_indice_max) != nb_transduc_choisis:
        print("ERREUR nombre de transducteurs")

    for x in range(nb_transduc_choisis - 1):

        freq_MFR = np.concatenate(
            (freq_prec, freqs_XXxx_db[0][indices_transduc_choisis[x + 1]])
        )
        freq_prec = freq_MFR

    Depth = np.squeeze(depth_surface_XXxx_db)[:, 0, :]
    # detection du fond : indice_fond représente l'indice de fond
    # pylint:disable=nested-min-max
    detec_fond = np.squeeze(max(max(depth_bottom_XXxx_db)))
    indice_fond = min(np.squeeze(np.where(Depth[0] >= detec_fond)))

    Sv = np.zeros([len(Sv_surfXXxx_db), indice_fond, len(freq_MFR)])
    Lat = np.zeros([len(Sv_surfXXxx_db), len(Sv_surfXXxx_db[1][2])])
    Lon = np.zeros([len(Sv_surfXXxx_db), len(Sv_surfXXxx_db[1][2])])
    Sa = np.zeros([len(Sa_surfXXxx_db), indice_fond, len(freq_MFR)])
    Sa[:, :, :] = np.NaN
    # pylint:disable=nested-min-max
    Depth = np.squeeze(depth_surface_XXxx_db)[:, 0, :]
    detec_fond = np.squeeze(max(max(depth_bottom_XXxx_db)))
    indice_fond = min(np.squeeze(np.where(Depth[0] >= detec_fond)))

    if indice_fond >= len(Sv_surfXXxx_db[1][1]):
        indice_fond = len(Sv_surfXXxx_db[1][1])

    for i in range(len(Sv_surfXXxx_db)):
        for j in range(indice_fond):
            Sv_prec = Sv_surfXXxx_db[i][indices_transduc_choisis[0]][j][
                : liste_indice_max[0]
            ]
            Sa_prec = Sa_surfXXxx_db[i][indices_transduc_choisis[0]][j][
                : liste_indice_max[0]
            ]
            # Depth[i,j]=np.squeeze(depth_surface_EK80_db[i][0][j])
            for x in range(nb_transduc_choisis - 1):
                Sv_temp = np.concatenate(
                    (
                        Sv_prec,
                        Sv_surfXXxx_db[i][indices_transduc_choisis[x + 1]][j][
                            : liste_indice_max[x + 1]
                        ],
                    )
                )
                Sa_temp = np.concatenate(
                    (
                        Sa_prec,
                        Sa_surfXXxx_db[i][indices_transduc_choisis[x + 1]][j][
                            : liste_indice_max[x + 1]
                        ],
                    )
                )
                Sv_prec = Sv_temp
                Sa_prec = Sa_temp
            Sv[i, j, :] = Sv_temp
            Sa[i, j, :] = Sa_temp
    Lat = np.squeeze(np.mean(lat_surfXXxx_db, axis=1, dtype=float))
    Lon = np.squeeze(np.mean(lon_surfXXxx_db, axis=1, dtype=float))
    Depth = Depth[:, :indice_fond]

    if save:
        if filename is None:

            filename = "saved_results.pickle"
        with open(filename, "wb") as f:
            pickle.dump([freq_MFR, Sv, Sa, Depth, Lat, Lon], f)

    return freq_MFR, Sv, Sa, Depth, Lat, Lon
