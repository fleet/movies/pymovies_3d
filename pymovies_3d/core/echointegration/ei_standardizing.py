import itertools
import pickle
import time

import numpy as np

import pymovies_3d.core.hac_util.hac_util as hac_util
import pymovies_3d.core.util.utils as ut

#We skip pylint test, the file probably dont work sint ut.Sv_moy is deleted
# pylint: skip-file

def scaling(
    freqs_XXxx_db,
    Sv_surfXXxx_db,
    Sa_surfXXxx_db,
    depth_surface_XXxx_db,
    depth_bottom_XXxx_db,
    lat_surfXXxx_db,
    lon_surfXXxx_db,
    time_XXxx_db,
    indices_transduc_choisis,
    date_time_debut,
    date_time_fin,
    premiere_date,
    name_radiales,
    save=False,
    filename=None,
):

    nb_transduc = len(freqs_XXxx_db[0])

    # Vérification : l'indice ne doit pas dépasser le nombre de sondeurs total sinon il est supprimé
    indices_transduc_choisis = np.delete(
        indices_transduc_choisis, [np.where(indices_transduc_choisis >= nb_transduc)]
    )

    nb_transduc_choisis = len(indices_transduc_choisis)

    freq_prec = freqs_XXxx_db[0][indices_transduc_choisis[0]]
    liste_indice_max = []
    for x in range(nb_transduc_choisis):
        liste = []
        for y in range(len(freqs_XXxx_db)):
            liste.append(len(freqs_XXxx_db[y][indices_transduc_choisis[x]]))
        print(min(liste))
        liste_indice_max.append(min(liste))
    print(liste_indice_max)
    if len(liste_indice_max) != nb_transduc_choisis:
        print("ERREUR nombre de transducteurs")

    for x in range(nb_transduc_choisis - 1):

        freq_MFR = np.concatenate(
            (
                freq_prec,
                freqs_XXxx_db[0][indices_transduc_choisis[x + 1]][
                    : liste_indice_max[x + 1]
                ],
            )
        )
        freq_prec = freq_MFR

    Depth = np.squeeze(depth_surface_XXxx_db[:][0][:])
    # detection du fond : indice_fond représente l'indice de fond
    detec_fond = max(max(max(depth_bottom_XXxx_db)))
    depth_surf_max = max(Depth[0])
    if depth_surf_max > detec_fond:
        indice_fond = min(np.where(Depth[0] >= detec_fond)[0])
    else:
        indice_fond = len(Depth[0])

    Lat = np.zeros([len(Sv_surfXXxx_db), len(Sv_surfXXxx_db[1][2])])
    Lon = np.zeros([len(Sv_surfXXxx_db), len(Sv_surfXXxx_db[1][2])])

    if indice_fond >= len(Sv_surfXXxx_db[1][1]):
        indice_fond = len(Sv_surfXXxx_db[1][1])

    indice_decalage = [0]
    append_indice_decalage = indice_decalage.append

    Sv_temp = []
    Sa_temp = []
    append_sv = Sv_temp.append
    append_sa = Sa_temp.append
    start1 = time.time()
    start2 = time.perf_counter()
    for i, j, x in itertools.product(
        range(len(Sv_surfXXxx_db)), range(indice_fond), range(nb_transduc_choisis)
    ):
        append_sv(
            Sv_surfXXxx_db[i][indices_transduc_choisis[x]][j][: liste_indice_max[x]]
        )
        append_sa(
            Sa_surfXXxx_db[i][indices_transduc_choisis[x]][j][: liste_indice_max[x]]
        )
        Lat[i, j] = np.array(lat_surfXXxx_db[i][indices_transduc_choisis[0]][j])
        Lon[i, j] = np.array(lon_surfXXxx_db[i][indices_transduc_choisis[0]][j])
    end1 = time.time()
    end2 = time.perf_counter()
    print("time1:", end1 - start1, "s")
    print("time2:", end2 - start2, "s")
    Sv = np.array(list(itertools.chain.from_iterable(Sv_temp)))
    Sv = Sv.reshape((len(Sv_surfXXxx_db), indice_fond, len(freq_MFR)))
    Sa = np.array(list(itertools.chain.from_iterable(Sa_temp)))
    Sa = Sa.reshape((len(Sv_surfXXxx_db), indice_fond, len(freq_MFR)))

    Depth = Depth[:, :indice_fond]

    Sv_moy_transducteur_x = [
        np.transpose(
            ut.Sv_moy(
                Sv,
                Sv_surfXXxx_db,
                freqs_XXxx_db,
                indices_transduc_choisis[0],
                indice_decalage[0],
            )
        )
    ]
    append_Sv_moy_transducteur_x = Sv_moy_transducteur_x.append
    freqs_moy_transducteur = [
        (np.median(freqs_XXxx_db[0][indices_transduc_choisis[0]])) / 1000
    ]
    append_freqs_moy_transducteur = freqs_moy_transducteur.append

    for x in range(nb_transduc_choisis - 1):
        append_indice_decalage(
            max(indice_decalage)
            + len(Sv_surfXXxx_db[0][0][indices_transduc_choisis[x]])
        )
        append_Sv_moy_transducteur_x(
            np.transpose(
                ut.Sv_moy(
                    Sv,
                    Sv_surfXXxx_db,
                    freqs_XXxx_db,
                    indices_transduc_choisis[x + 1],
                    indice_decalage[x + 1],
                )
            )
        )
        append_freqs_moy_transducteur(
            (np.median(freqs_XXxx_db[0][indices_transduc_choisis[x + 1]])) / 1000
        )

    name = np.array(name_radiales)
    nameliste = list(set(name))
    nameliste.sort()
    liste_nomrad = []
    liste_numrad = []
    append1 = liste_nomrad.append
    append2 = liste_numrad.append

    datetime_finrad = ut.convert_string_to_datetime(date_time_fin)
    datetime_debutrad = ut.convert_string_to_datetime(date_time_debut)
    indices_debut_rad = []

    Time = hac_util.hac_read_day_time(time_XXxx_db)
    indice = []
    append3 = indice.append
    for i in range(len(datetime_debutrad)):
        for j in range(len(Time)):

            if (Time[j] <= datetime_finrad[i]) and (Time[j] >= datetime_debutrad[i]):
                append3(j)
                break

        if len(indice) > 0:
            append1(nameliste[i + premiere_date])
            append2(i + premiere_date)

    Sv_moy_transducteur_x = np.array(Sv_moy_transducteur_x)

    print(len(indices_debut_rad))
    print(len(liste_nomrad))

    tableau_radiales = np.zeros((len(Sv)))

    for x in range(1, len(liste_nomrad)):
        tableau_radiales[indice[x - 1] : indice[x]] = liste_numrad[x - 1]

    tableau_radiales[indice[-1] :] = liste_numrad[len(liste_nomrad) - 1]

    nom_numero_radiales = (liste_nomrad, liste_numrad)
    if save:
        if filename is None:

            filename = "saved_results.pickle"
        with open(filename, "wb") as f:
            pickle.dump(
                [
                    freq_MFR,
                    freqs_moy_transducteur,
                    Sv,
                    Sv_moy_transducteur_x,
                    Sa,
                    Depth,
                    Lat,
                    Lon,
                    Time,
                    nb_transduc_choisis,
                    tableau_radiales,
                    nom_numero_radiales,
                ],
                f,
            )

    else:
        return (
            freq_MFR,
            freqs_moy_transducteur,
            Sv,
            Sv_moy_transducteur_x,
            Sa,
            Depth,
            Lat,
            Lon,
            Time,
            nb_transduc_choisis,
            tableau_radiales,
            nom_numero_radiales,
        )
