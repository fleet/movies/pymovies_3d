import os
import shutil
import glob
import numpy as np

from pymovies_3d.core.echointegration.batch_ei_multi_threshold import (
    batch_ei_multi_threshold,
)
from pymovies_3d.core.echointegration.sampleechointegration import (
    sample_echo_integration,
)
from pymovies_3d.core.echointegration.ei_bind import ei_bind
from pymovies_3d.core.echointegration.ei_gridding import eigridding
from pymovies_3d.core.echointegration.ei_gridding_cpp import eigridding_cpp



def ei_survey_transects(
    path_hac_survey,
    path_config,
    path_save,
    date_start,
    date_end,
    time_start,
    time_end,
    name_transects=None,
    ME70=False,
    EK80=True,
    EK80h=False,
):

    """
    Process EI over specified transects periods (potentially partitionned in different parts) and specified echosounders (ME70, EK80 and horizontal EK80)

       inputs:
               - path_hac_survey: path with hac files (they can be in subdirectories)
               - path_config: path of M3D configuration (with ESU definition, layers and EI threshold)
               - path_save: path for resulting EI files
               - date_start and date_end: lists of start and end dates of the transects to be echo-integrated, format ['dd/mm/yyyy',['dd/mm/yyyy','dd/mm/yyyy'],...]
               - time_start and time_end: lists of start and end times of the transects to be echo-integrated, format ['hh:mm:ss',['hh:mm:ss','hh:mm:ss'],...]
               - name_transects: list of names of transects (such as ['Line1','Line13',...])
                       If None, saved files names will be prefixed with ddmmyyy_Thhmmss(start)_to_ddmmyyy_Thhmmss(end)
               - ME70/EK80/EK80h: True if results are to be saved (just EK80 is activated as default value)
        output:
               - one EI pickle file per transects is created in path_save directory for each specified echosounder
               they can be loaded with:
               time_XXX,depth_surface_XXX,depth_bottom_XXX,Sv_surfXXX,Sv_botfXXX,Sa_surfXXX,Sa_botXXX,lat_surfXXX,lon_surfXXX,lat_botXXX,lon_botXXX,vol_surfXXX,freqs_XXX=pickle.load(open(filename,'rb'))

    """
    if isinstance(date_start,str):
        date_start = [date_start]
    if isinstance(date_end,str):
        date_end = [date_end]
    if isinstance(time_start,str):
        time_start = [time_start]
    if isinstance(time_end,str):
        time_end = [time_end]
    if isinstance(name_transects,str):
        name_transects = [name_transects]

    range_date = range(len(date_start))
    if (len(date_end) + len(time_start) + len(time_end)) / 3 != len(date_start):
        print("****** stop: length of time/date lists are inconsistent")
        return

    if not os.path.exists(path_save):
        os.makedirs(path_save)

    for x in range_date:

        if name_transects is None:
            if (
                (np.size(date_start[x]) != 1)
                | (np.size(date_end[x]) != 1)
                | (np.size(time_start[x]) != 1)
                | (np.size(time_end[x]) != 1)
            ):
                print("****** stop: transects are multi-sequences. Specify names")
                return
            name_transect = (
                date_start[x]
                + "_T"
                + time_start[x]
                + "_to_"
                + date_end[x]
                + "_T"
                + time_end[x]
            )
            name_transect = name_transect.replace("/", "")
            name_transect = name_transect.replace(":", "")
        else:
            name_transect=name_transects[x]

        chemin_save = path_save + "/" + name_transect + "/EItemp/"
        if not os.path.exists(chemin_save):
            os.makedirs(chemin_save)
        else:
            print(
                "remove "
                + chemin_save
                + " directory to process echo-integration and avoid binding results with previous ones"
            )
            return

        sample_echo_integration(
            path_hac_survey,
            path_config,
            chemin_save,
            nameTransect=name_transect,
            dateStart=date_start[x],
            timeStart=time_start[x],
            dateEnd=date_end[x],
            timeEnd=time_end[x],
        )

        fname_ME70 = None
        fname_EK80 = None
        fname_EK80h = None
        if ME70:
            fname_ME70 = path_save + "/" + name_transect + "_ME70" + ".pickle"
        if EK80:
            fname_EK80 = path_save + "/" + name_transect + "_EK80" + ".pickle"
        if EK80h:
            fname_EK80h = path_save + "/" + name_transect + "_EK80h" + ".pickle"

        ei_bind(
            chemin_save,
            filename_ME70=fname_ME70,
            filename_EK80=fname_EK80,
            filename_EK80h=fname_EK80h,
        )
        shutil.rmtree(chemin_save)


def ei_survey_RUN(
    path_hac_survey,
    path_config,
    path_save,
    runs=None,
    ME70=False,
    EK80=True,
    EK80h=False,
):

    """
    Process EI over specified RUN directories and specified echosounders (ME70, EK80 and horizontal EK80)

       inputs:
               - path_hac_survey: path of RUN directories
               - path_config: path of M3D configuration (with ESU definition, layers and EI threshold)
               - path_save: path for resulting EI files
               - runs: list of RUNs to process (such as ['RUN001','RUN003'])
                       If None, all RUNs directories in path_hac_survey are processed
               - ME70/EK80/EK80h: True if results are to be saved (just EK80 is activated as default value)
        output:
               - one EI pickle file per run is created in path_save directory for each specified echosounder
               they can be loaded with:
               time_XXX,depth_surface_XXX,depth_bottom_XXX,Sv_surfXXX,Sv_botXXX,Sa_surfXXX,Sa_botXXX,lat_surfXXX,lon_surfXXX,lat_botXXX,lon_botXXX,vol_surfXXX,freqs_XXX=pickle.load(open(filename,'rb'))

    """

    if runs is None:  # lists all runs
        runs = glob.glob1(path_hac_survey, "RUN*")
    if isinstance(runs,str):
        runs = [runs]

    for run in runs:

        chemin_ini = path_hac_survey + "/" + run + "/"
        chemin_save = path_save + "/" + run + "_EItemp/"
        if not os.path.exists(chemin_save):
            os.makedirs(chemin_save)
        else:
            print(
                "remove "
                + chemin_save
                + " directory to process echo-integration and avoid binding results with previous ones"
            )
            return

        sample_echo_integration(chemin_ini, path_config, chemin_save, nameTransect=run)

        fname_ME70 = None
        fname_EK80 = None
        fname_EK80h = None
        if ME70:
            fname_ME70 = path_save + "/" + run + "_ME70" + ".pickle"
        if EK80:
            fname_EK80 = path_save + "/" + run + "_EK80" + ".pickle"
        if EK80h:
            fname_EK80h = path_save + "/" + run + "_EK80h" + ".pickle"

        ei_bind(
            chemin_save,
            filename_ME70=fname_ME70,
            filename_EK80=fname_EK80,
            filename_EK80h=fname_EK80h,
        )
        shutil.rmtree(chemin_save)


def ei_hacfile(
    path_hac_survey,
    hacfilename,
    path_config,
    path_save,
    ME70=False,
    EK80=True,
    EK80h=False,
):

    """
    Process EI over specified hac file and specified echosounders (ME70, EK80 and horizontal EK80)

       inputs:
               - path_hac_survey: hac directory
               - hacfilename: name of hac file
               - path_config: path of M3D configuration (with ESU definition, layers and EI threshold)
               - path_save: path for resulting EI file
               - ME70/EK80/EK80h: True if results are to be saved (just EK80 is activated as default value)
        output:
               - one EI pickle file is created in path_save directory for each specified echosounder
               they can be loaded with:
               time_XXX,depth_surface_XXX,depth_bottom_XXX,Sv_surfXXX,Sv_botXXX,Sa_surfXXX,Sa_botXXX,lat_surfXXX,lon_surfXXX,lat_botXXX,lon_botXXX,vol_surfXXX,freqs_XXX=pickle.load(open(filename,'rb'))

    """

    file_ini = path_hac_survey + "/" + hacfilename
    chemin_save = path_save + "/" + hacfilename[:-4] + "_EItemp/"
    if not os.path.exists(chemin_save):
        os.makedirs(chemin_save)
    else:
        print(
            "remove "
            + chemin_save
            + " directory to process echo-integration and avoid binding results with previous ones"
        )
        return

    sample_echo_integration(file_ini, path_config, chemin_save)

    fname_ME70 = None
    fname_EK80 = None
    fname_EK80h = None
    if ME70:
        fname_ME70 = path_save + "/" + hacfilename[:-4] + "_ME70" + ".pickle"
    if EK80:
        fname_EK80 = path_save + "/" + hacfilename[:-4] + "_EK80" + ".pickle"
    if EK80h:
        fname_EK80h = path_save + "/" + hacfilename[:-4] + "_EK80h" + ".pickle"

    ei_bind(
        chemin_save,
        filename_ME70=fname_ME70,
        filename_EK80=fname_EK80,
        filename_EK80h=fname_EK80h,
    )
    shutil.rmtree(chemin_save)


def ei_survey_multi_threshold(
    path_hac_survey,
    path_config,
    path_save,
    thresholds,
    datedebut,
    datefin,
    heuredebut,
    heurefin,
    runs,
    filename_ME70,
    filename_EK80,
    filename_EK80h,
):
    # batch_ei_multi_threshold(path_hac_survey,path_config,path_save,thresholds,runs)
    # batch_ei_multi_threshold(path_hac_survey,path_config,path_save,thresholds,runs,'Fish',dateStart=datedebut,timeStart=heuredebut,dateEnd=datefin,timeEnd=heurefin)
    batch_ei_multi_threshold(
        path_hac_survey,
        path_config,
        path_save,
        thresholds,
        nameTransect="Fish",
        dateStart=datedebut,
        timeStart=heuredebut,
        dateEnd=datefin,
        timeEnd=heurefin,
    )

    if runs is not None:
        for ir in runs:
            for t in thresholds:
                str_run = "%03d" % (ir)
                path_results = "%s/RUN%s/%d/" % (path_save, str_run, t)
                ei_bind(path_results, filename_ME70, filename_EK80, filename_EK80h)

    else:
        for t in thresholds:
            path_results = "%s/%d/" % (path_save, t)
            ei_bind(path_results, filename_ME70, filename_EK80, filename_EK80h)


def ei_survey_transects_netcdf(
    path_hac_survey,
    path_config,
    path_save,
    date_start,
    date_end,
    time_start,
    time_end,
    name_transects=None,
    sounder_ident=None
):

    """
    Process EI over specified transects periods and specified echosounders (ME70, EK80 and horizontal EK80)

       inputs:
               - path_hac_survey: path with hac files (they can be in subdirectories)
               - path_config: path of M3D configuration (with ESU definition, layers and EI threshold)
               - path_save: path for resulting EI files
               - date_start and date_end: lists of start and end dates of the transects to be echo-integrated, format ['dd/mm/yyyy',['dd/mm/yyyy','dd/mm/yyyy'],...]
               - time_start and time_end: lists of start and end times of the transects to be echo-integrated, format ['hh:mm:ss',['hh:mm:ss','hh:mm:ss'],...]
               - name_transects: list of names of transects (such as ['Line1','Line13',...])
                       If None, saved files names will be prefixed with ddmmyyy_Thhmmss(start)_to_ddmmyyy_Thhmmss(end)
               - sounder_ident: sounder identifier for echointegration (if None all sounders are integrated)
        output:
               - one EI NetCDF file per transects is created in path_save directory for each specified echosounder
    """
    if isinstance(date_start,str):
        date_start = [date_start]
    if isinstance(date_end,str):
        date_end = [date_end]
    if isinstance(time_start,str):
        time_start = [time_start]
    if isinstance(time_end,str):
        time_end = [time_end]
    if isinstance(name_transects,str):
        name_transects = [name_transects]

    range_date = range(len(date_start))
    if (len(date_end) + len(time_start) + len(time_end)) / 3 != len(date_start):
        print("****** stop: length of time/date lists are inconsistent")
        return

    if not os.path.exists(path_save):
        os.makedirs(path_save)

    for x in range_date:

        if name_transects is None:
            if (
                (np.size(date_start[x]) != 1)
                | (np.size(date_end[x]) != 1)
                | (np.size(time_start[x]) != 1)
                | (np.size(time_end[x]) != 1)
            ):
                print("****** stop: transects are multi-sequences. Specify names")
                return
            name_transect = (
                date_start[x]
                + "_T"
                + time_start[x]
                + "_to_"
                + date_end[x]
                + "_T"
                + time_end[x]
            )
            name_transect = name_transect.replace("/", "")
            name_transect = name_transect.replace(":", "")
        else:
            name_transect=name_transects[x]

        chemin_save = path_save + "/" + name_transect

        ei = eigridding()
        ei.savePath = chemin_save
        ei.run(path_hac_survey,
            path_config,
            nameTransect=name_transect,
            dateStart=date_start[x],
            timeStart=time_start[x],
            dateEnd=date_end[x],
            timeEnd=time_end[x],
            sounderIdent=sounder_ident)

def ei_survey_transects_netcdf_cpp(
    path_hac_survey,
    path_config,
    path_save,
    date_start,
    date_end,
    time_start,
    time_end,
    name_transects=None, skip_existing=False
):

    """
    Process EI over specified transects periods (potentially partitionned in different parts) and specified echosounders (ME70, EK80 and horizontal EK80)

       inputs:
               - path_hac_survey: path with hac files (they can be in subdirectories)
               - path_config: path of M3D configuration (with ESU definition, layers and EI threshold)
               - path_save: path for resulting EI files
               - date_start and date_end: lists of start and end dates of the transects to be echo-integrated, format ['dd/mm/yyyy',['dd/mm/yyyy','dd/mm/yyyy'],...]
               - time_start and time_end: lists of start and end times of the transects to be echo-integrated, format ['hh:mm:ss',['hh:mm:ss','hh:mm:ss'],...]
               - transect_portion handles subdivisions of transect when the name of the transect is not unique in the CASINO file
               - name_transects: list of names of transects (such as ['Line1','Line13',...])
                       If None, saved files names will be prefixed with ddmmyyy_Thhmmss(start)_to_ddmmyyy_Thhmmss(end)
               - skip_existing: s
        output:
               - one EI NetCDF file per transects is created in directory set in the echointegration configuration for each specified echosounder
    """
    if isinstance(date_start,str):
        date_start = [date_start]
    if isinstance(date_end,str):
        date_end = [date_end]
    if isinstance(time_start,str):
        time_start = [time_start]
    if isinstance(time_end,str):
        time_end = [time_end]
    if isinstance(name_transects,str):
        name_transects = [name_transects]

    range_date = range(len(date_start))
    if (len(date_end) + len(time_start) + len(time_end)) / 3 != len(date_start):
        print("****** stop: length of time/date lists are inconsistent")
        return

    for x in range_date:

        if name_transects is None:
            name_transect="RXX"
        else:
            name_transect = name_transects[x]
            
        if (
            (np.size(date_start[x]) != 1)
            | (np.size(date_end[x]) != 1)
            | (np.size(time_start[x]) != 1)
            | (np.size(time_end[x]) != 1)
        ):
            print("****** stop: transects are multi-sequences. Specify names")
            return
        name_transect = (name_transect + "_" + 
            date_start[x]
            + "_T"
            + time_start[x]
            + "_to_"
            + date_end[x]
            + "_T"
            + time_end[x]
        )
        name_transect = name_transect.replace("/", "")
        name_transect = name_transect.replace(":", "")
        
        #skip file if already exists
        file_list = glob.glob(f"*{name_transect}*", root_dir=path_save)
        
        if len(file_list)>0 and skip_existing:
            print(f"skipping file {file_list}")
        else:
            ei = eigridding_cpp()
            ei.run(path_hac_survey,
                path_config,
                path_save,
                nameTransect=name_transect,
                dateStart=date_start[x],
                timeStart=time_start[x],
                dateEnd=date_end[x],
                timeEnd=time_end[x])
        
def ei_hacfile_netcdf(
    path_hac_survey,
    hacfilename,
    path_config,
    path_save)  :
   
    """
    Process EI over specified hac file and specified echosounders (ME70, EK80 and horizontal EK80)
       
       inputs:
               - path_hac_survey: hac directory
               - hacfilename: name of hac file
               - path_config: path of M3D configuration (with ESU definition, layers and EI threshold)
               - path_save: path for resulting EI file
        output:
               - one SonarNetcdf file is created in path_save directory for each specified echosounde
               
    """

    file_ini=path_hac_survey + "/" + hacfilename
    chemin_save=path_save + "/" + hacfilename[:-4]
    if not os.path.exists(chemin_save):
        os.makedirs(chemin_save)
    
    ei = eigridding()
    ei.savePath = chemin_save
    ei.run(file_ini, path_config)
