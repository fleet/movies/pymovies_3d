﻿#!/usr/bin/python
# -*- coding: utf-8 -*-

import calendar

import datetime
import os
import pickle
import time
import numpy as np
from pyMovies import pyMovies as mv

import pymovies_3d.core.hac_util.hac_util as util

# pylint:disable=too-many-nested-blocks
def sample_echo_integration(
    chemin_ini,
    chemin_config,
    chemin_save,
    nameTransect=None,
    dateStart=None,
    timeStart=None,
    dateEnd=None,
    timeEnd=None,
):

    """
    Effectue l'écho-intégration du fichier ou du répertoire indiqué, et l'enregistre en .pickle

       entrées:
               - chemin_ini: chemin du répertoire ou du fichier à traiter
               - chemin_config: chemin de la configuration M3D à utiliser (ESU, couches etc)
               - chemin_save: chemin du répertoire de sauvegarde des résultats
               - nameTransect: nom de sauvegarde (optionnel). Si None, les fichiers pickles seront
               nommés results_000X_EI.pickle
               - dateStart et dateEnd: dates de début et fin des séquences à écho-intégrer, au format 'dd/mm/yyyy' ou ['dd/mm/yyyy','dd/mm/yyyy'...]
               Si dateStart est None, tout le répertoire est écho-intégré
               - timeStart et timeEnd: heures de début et fin des séquences à écho-intégrer, au format 'hh:mm:ss' ou ['hh:mm:ss','hh:mm:ss'...]
        sortie:
               - Un fichier pickle par chunk lu est créé
    """

    # load configuration
    if not os.path.exists(chemin_config):
        print("****** stop: chemin de configuration non valide")
        return

    mv.moLoadConfig(chemin_config)

    num_bloc = 1

    if nameTransect is not None:
        str_Name = "_" + nameTransect + "_"
    else:
        str_Name = ""

    if dateStart is None:
        range_date = [-1]
        process_all = True
    else:
        if isinstance(dateStart,str):
            dateStart = [dateStart]
        if isinstance(dateEnd,str):
            dateEnd = [dateEnd]
        if isinstance(timeStart,str):
            timeStart = [timeStart]
        if isinstance(timeEnd,str):
            timeEnd = [timeEnd]

        range_date = range(len(dateStart))
        if (len(dateEnd) + len(timeStart) + len(timeEnd)) / 3 != len(dateStart):
            print(
                "****** stop: les listes des dates/heures de début/fin n ont pas la même taille"
            )
            return
        process_all = False

    for x in range_date:

        slice_end = False

        if not process_all:

            goto = calendar.timegm(
                time.strptime(
                    "%s %s" % ((dateStart[x]), (timeStart[x])), "%d/%m/%Y %H:%M:%S"
                )
            )

            mv.moOpenHac(chemin_ini)

            util.hac_goto(goto)

            timeEndEI = calendar.timegm(
                time.strptime("%s %s" % (dateEnd[x], timeEnd[x]), "%d/%m/%Y %H:%M:%S")
            )
        else:
            # chargement de tous les fichiers HAC contenus dans le repertoire
            # et lecture du premier chunk
            mv.moOpenHac(chemin_ini)
            timeEndEI = calendar.timegm(datetime.datetime.now().timetuple())

        # Activate 'EchoIntegrModule'
        EchoIntegrModule = mv.moEchoIntegration()
        EchoIntegrModule.setEnable(True)

        FileStatus = mv.moGetFileStatus()
        while not FileStatus.m_StreamClosed:

            # init matrices
            time_EK80 = []
            time_ME70 = []
            Sa_surfME70 = []
            Sa_botME70 = []
            Sa_surfEK80 = []
            Sa_botEK80 = []
            Sv_surfME70 = []
            Sv_botME70 = []
            Sv_surfEK80 = []
            Sv_botEK80 = []
            Lat_surfME70 = []
            Long_surfME70 = []
            Depth_surfME70 = []
            Lat_surfEK80 = []
            Long_surfEK80 = []
            Depth_surfEK80 = []
            Lat_botME70 = []
            Long_botME70 = []
            Depth_botME70 = []
            Lat_botEK80 = []
            Long_botEK80 = []
            Depth_botEK80 = []
            Volume_surfME70 = []
            Volume_surfEK80 = []
            Volume_botME70 = []
            Volume_botEK80 = []
            Freqs_EK80 = []
            Freqs_EK80h = []
            Freqs_ME70 = []

            time_EK80h = []
            Sa_surfEK80h = []
            Sv_surfEK80h = []
            Lat_surfEK80h = []
            Long_surfEK80h = []
            Depth_surfEK80h = []
            Volume_surfEK80h = []
            AllData = []

            mv.moReadChunk()

            AllData = EchoIntegrModule.GetOutputList()

            # récupération de la définition des couches
            LayersDef = EchoIntegrModule.GetEchoIntegrationParameter().m_tabLayerDef
            nbLayers = len(LayersDef)
            print("nbLayers " + str(nbLayers))
            #        for indexLayer in range(nbLayers):
            #            print 'Layer ' + str(indexLayer)
            #            if type(LayersDef[indexLayer]) is LayerDefSurface:
            #                print ' type : LayerDefSurface'
            #                print ' min depth : ' + str(LayersDef[indexLayer].m_minDepth)
            #                print ' max depth : ' + str(LayersDef[indexLayer].m_maxDepth)
            #            if type(LayersDef[indexLayer]) is LayerDefDistance:
            #                print ' type : LayerDefDistance'
            #                print ' min depth : ' + str(LayersDef[indexLayer].m_minDepth)
            #                print ' max depth : ' + str(LayersDef[indexLayer].m_maxDepth)
            #            if type(LayersDef[indexLayer]) is LayerDefBottom:
            #                print ' type : LayerDefBottom'
            #                print ' min height' + str(LayersDef[indexLayer].m_minHeight)
            #                print ' max height' + str(LayersDef[indexLayer].m_maxHeight)
            #

            # récupération des volumes de chaque couche (pour 1 ping)
            LayersVolumes = EchoIntegrModule.GetLayersVolumes()
            sounderNb = len(LayersVolumes[0])

            # on souhaite récuperer les informations ME70 et EK60 MFR dans le
            # cas où l'on a également un sondeur horizontal
            indexSounderEK80 = 0
            indexSounderEK80h = 0
            indexSounderME70 = 0
            nbchannelsME70 = 0
            nbchannelsEK80 = 1
            nbchannelsEK80h = 0
            for indexSounder in range(sounderNb):
                nbChans = len(LayersVolumes[0][indexSounder])
                if nbChans > 6:  # ME70
                    indexSounderME70 = indexSounder
                    nbchannelsME70 = nbChans
                elif nbChans > 1:  # EK80 MFR
                    indexSounderEK80 = indexSounder
                    nbchannelsEK80 = nbChans
                elif (
                    nbChans == 1 and nbchannelsEK80 > 1
                ):  # cas du sondeur horizontal de Thalassa avec un sondeur MFR avant
                    indexSounderEK80h = indexSounder
                    nbchannelsEK80h = nbChans

            # print 'nbchannelsME70 : ' +  str(nbchannelsME70)
            # print 'nbchannelsEK80 : ' +  str(nbchannelsEK80)
            # print 'nbchannelsEK80h : ' +  str(nbchannelsEK80h)

            # on récupère l'ordre des résultats des fréquences EK80 pour les
            # sauvegarder touojours dans le même ordre
            # on suppose qu'il y a un seul sondeur multi-transducteur !
            list_sounder = mv.moGetSounderDefinition()
            nb_snd = list_sounder.GetNbSounder()
#            indexEK80 = 0
            list_freqEK80 = np.zeros(nbchannelsEK80)
            for isdr in range(nb_snd):
                sounder = list_sounder.GetSounder(isdr)
                nb_transduc = sounder.m_numberOfTransducer
                if nb_transduc > 1:
                    for itr in range(nb_transduc):
                        list_freqEK80[itr] = (
                            sounder.GetTransducer(itr)
                            .getSoftChannelPolarX(0)
                            .m_acousticFrequency
                        )
#                    indexEK80 = np.argsort(list_freqEK80)

            #                print np.version.version
            #                print 'type de list_freqEK80 : ' + str(type(list_freqEK80))
            #                print 'list_freqEK80 : ' + str(list_freqEK80)
            #                print 'type de indexEK80 : ' + str(type(indexEK80))
            #                print 'indexEK80 : ' + str(indexEK80)

            # nb d'ESUs traitees
            nbResults = len(AllData)

            print("nb results  : " + str(nbResults))

            for indexResults in range(nbResults):

                ESUResult = AllData[indexResults]
                nbbeams = len(ESUResult.m_tabChannelResult)
                # print ('nbbeams ' + str(nbbeams))
                timeESU = (
                    ESUResult.m_timeEnd.m_TimeCpu
                    + ESUResult.m_timeEnd.m_TimeFraction / 10000
                )

                if nbbeams == nbchannelsME70:

                    time_ME70.append(timeESU)
                    nbLayer = len(ESUResult.m_tabChannelResult[0].m_tabLayerResult)

                    # Ajout d'un nouveau tableau de résultat pour l'ESU en cours
                    Sa_surfME70.append([])
                    Sv_surfME70.append([])
                    Lat_surfME70.append([])
                    Long_surfME70.append([])
                    Depth_surfME70.append([])
                    Volume_surfME70.append([])
                    Sa_botME70.append([])
                    Sv_botME70.append([])
                    Lat_botME70.append([])
                    Long_botME70.append([])
                    Depth_botME70.append([])
                    Volume_botME70.append([])

                    Freqs_ME70.append([])

                    for iBeam in range(nbbeams):

                        freqs = ESUResult.m_tabChannelResult[iBeam].m_tabFrequencies
                        nbFreqs = len(freqs)
                        freqsRanges = range(nbFreqs)

                        Sa_surfME70[-1].append([])
                        Sv_surfME70[-1].append([])
                        Lat_surfME70[-1].append([])
                        Long_surfME70[-1].append([])
                        Depth_surfME70[-1].append([])
                        Volume_surfME70[-1].append([])

                        Sa_botME70[-1].append([])
                        Sv_botME70[-1].append([])
                        Lat_botME70[-1].append([])
                        Long_botME70[-1].append([])
                        Depth_botME70[-1].append([])
                        Volume_botME70[-1].append([])

                        Freqs_ME70[-1].append(
                            [freqs[indexFreq] for indexFreq in freqsRanges]
                        )

                        for indexLayer in range(nbLayer):
                            if LayersDef[indexLayer].m_layerType == mv.LayerType.SurfaceLayer:

                                Sa_surfME70[-1][-1].append(
                                    [
                                        ESUResult.m_tabChannelResult[iBeam]
                                        .m_tabLayerResult[indexFreq][indexLayer]
                                        .m_Sa
                                        for indexFreq in freqsRanges
                                    ]
                                )
                                Sv_surfME70[-1][-1].append(
                                    [
                                        ESUResult.m_tabChannelResult[iBeam]
                                        .m_tabLayerResult[indexFreq][indexLayer]
                                        .m_Sv
                                        for indexFreq in freqsRanges
                                    ]
                                )
                                Lat_surfME70[-1][-1].append(
                                    [
                                        ESUResult.m_tabChannelResult[iBeam]
                                        .m_tabLayerResult[0][indexLayer]
                                        .m_latitude
                                    ]
                                )
                                Long_surfME70[-1][-1].append(
                                    [
                                        ESUResult.m_tabChannelResult[iBeam]
                                        .m_tabLayerResult[0][indexLayer]
                                        .m_longitude
                                    ]
                                )
                                Depth_surfME70[-1][-1].append(
                                    [
                                        ESUResult.m_tabChannelResult[iBeam]
                                        .m_tabLayerResult[0][indexLayer]
                                        .m_Depth
                                    ]
                                )
                                Volume_surfME70[-1][-1].append(
                                    [LayersVolumes[indexLayer][indexSounderME70][iBeam]]
                                )

                            else:
                                Sa_botME70[-1][-1].append(
                                    [
                                        ESUResult.m_tabChannelResult[iBeam]
                                        .m_tabLayerResult[indexFreq][indexLayer]
                                        .m_Sa
                                        for indexFreq in freqsRanges
                                    ]
                                )
                                Sv_botME70[-1][-1].append(
                                    [
                                        ESUResult.m_tabChannelResult[iBeam]
                                        .m_tabLayerResult[indexFreq][indexLayer]
                                        .m_Sv
                                        for indexFreq in freqsRanges
                                    ]
                                )
                                Lat_botME70[-1][-1].append(
                                    [
                                        ESUResult.m_tabChannelResult[iBeam]
                                        .m_tabLayerResult[0][indexLayer]
                                        .m_latitude
                                    ]
                                )
                                Long_botME70[-1][-1].append(
                                    [
                                        ESUResult.m_tabChannelResult[iBeam]
                                        .m_tabLayerResult[0][indexLayer]
                                        .m_longitude
                                    ]
                                )
                                Depth_botME70[-1][-1].append(
                                    [
                                        ESUResult.m_tabChannelResult[iBeam]
                                        .m_tabLayerResult[0][indexLayer]
                                        .m_Depth
                                    ]
                                )
                                Volume_botME70[-1][-1].append(
                                    [LayersVolumes[indexLayer][indexSounderME70][iBeam]]
                                )

                elif nbbeams == nbchannelsEK80:

                    time_EK80.append(timeESU)

                    # Ajout d'un nouveau tableau de résultat pour l'ESU en cours
                    Sa_surfEK80.append([])
                    Sv_surfEK80.append([])
                    Lat_surfEK80.append([])
                    Long_surfEK80.append([])
                    Depth_surfEK80.append([])
                    Volume_surfEK80.append([])

                    Sa_botEK80.append([])
                    Sv_botEK80.append([])
                    Lat_botEK80.append([])
                    Long_botEK80.append([])
                    Depth_botEK80.append([])
                    Volume_botEK80.append([])

                    Freqs_EK80.append([])

                    for iBeam in range(nbbeams):

                        freqs = ESUResult.m_tabChannelResult[iBeam].m_tabFrequencies
                        nbFreqs = len(freqs)
                        freqsRanges = range(nbFreqs)

                        Sa_surfEK80[-1].append([])
                        Sv_surfEK80[-1].append([])
                        Lat_surfEK80[-1].append([])
                        Long_surfEK80[-1].append([])
                        Depth_surfEK80[-1].append([])
                        Volume_surfEK80[-1].append([])

                        Sa_botEK80[-1].append([])
                        Sv_botEK80[-1].append([])
                        Lat_botEK80[-1].append([])
                        Long_botEK80[-1].append([])
                        Depth_botEK80[-1].append([])
                        Volume_botEK80[-1].append([])

                        Freqs_EK80[-1].append(
                            [freqs[indexFreq] for indexFreq in freqsRanges]
                        )

                        for indexLayer in range(nbLayers):
                            if LayersDef[indexLayer].m_layerType == mv.LayerType.SurfaceLayer:
                                Sa_surfEK80[-1][-1].append(
                                    [
                                        ESUResult.m_tabChannelResult[iBeam]
                                        .m_tabLayerResult[indexFreq][indexLayer]
                                        .m_Sa
                                        for indexFreq in freqsRanges
                                    ]
                                )
                                Sv_surfEK80[-1][-1].append(
                                    [
                                        ESUResult.m_tabChannelResult[iBeam]
                                        .m_tabLayerResult[indexFreq][indexLayer]
                                        .m_Sv
                                        for indexFreq in freqsRanges
                                    ]
                                )
                                Lat_surfEK80[-1][-1].append(
                                    [
                                        ESUResult.m_tabChannelResult[iBeam]
                                        .m_tabLayerResult[0][indexLayer]
                                        .m_latitude
                                    ]
                                )
                                Long_surfEK80[-1][-1].append(
                                    [
                                        ESUResult.m_tabChannelResult[iBeam]
                                        .m_tabLayerResult[0][indexLayer]
                                        .m_longitude
                                    ]
                                )
                                Depth_surfEK80[-1][-1].append(
                                    [
                                        ESUResult.m_tabChannelResult[iBeam]
                                        .m_tabLayerResult[0][indexLayer]
                                        .m_Depth
                                    ]
                                )
                                Volume_surfEK80[-1][-1].append(
                                    [LayersVolumes[indexLayer][indexSounderEK80][iBeam]]
                                )
                            elif LayersDef[indexLayer].m_layerType == mv.LayerType.BottomLayer:
                                Sa_botEK80[-1][-1].append(
                                    [
                                        ESUResult.m_tabChannelResult[iBeam]
                                        .m_tabLayerResult[indexFreq][indexLayer]
                                        .m_Sa
                                        for indexFreq in freqsRanges
                                    ]
                                )
                                Sv_botEK80[-1][-1].append(
                                    [
                                        ESUResult.m_tabChannelResult[iBeam]
                                        .m_tabLayerResult[indexFreq][indexLayer]
                                        .m_Sv
                                        for indexFreq in freqsRanges
                                    ]
                                )
                                Lat_botEK80[-1][-1].append(
                                    [
                                        ESUResult.m_tabChannelResult[iBeam]
                                        .m_tabLayerResult[0][indexLayer]
                                        .m_latitude
                                    ]
                                )
                                Long_botEK80[-1][-1].append(
                                    [
                                        ESUResult.m_tabChannelResult[iBeam]
                                        .m_tabLayerResult[0][indexLayer]
                                        .m_longitude
                                    ]
                                )
                                Depth_botEK80[-1][-1].append(
                                    [
                                        ESUResult.m_tabChannelResult[iBeam]
                                        .m_tabLayerResult[0][indexLayer]
                                        .m_Depth
                                    ]
                                )
                                Volume_botEK80[-1][-1].append(
                                    [LayersVolumes[indexLayer][indexSounderEK80][iBeam]]
                                )

                elif nbbeams == nbchannelsEK80h:

                    time_EK80h.append(timeESU)

                    # Ajout d'un nouveau tableau de résultat pour l'ESU en cours
                    Sa_surfEK80h.append([])
                    Sv_surfEK80h.append([])
                    Lat_surfEK80h.append([])
                    Long_surfEK80h.append([])
                    Depth_surfEK80h.append([])
                    Volume_surfEK80h.append([])

                    Freqs_EK80h.append([])

                    for iBeam in range(nbbeams):

                        freqs = ESUResult.m_tabChannelResult[iBeam].m_tabFrequencies
                        nbFreqs = len(freqs)
                        freqsRanges = range(nbFreqs)

                        Sa_surfEK80h[-1].append([])
                        Sv_surfEK80h[-1].append([])
                        Lat_surfEK80h[-1].append([])
                        Long_surfEK80h[-1].append([])
                        Depth_surfEK80h[-1].append([])
                        Volume_surfEK80h[-1].append([])

                        Freqs_EK80h[-1].append(
                            [freqs[indexFreq] for indexFreq in freqsRanges]
                        )

                        for indexLayer in range(nbLayers):
                            if LayersDef[indexLayer].m_layerType == mv.LayerType.DistanceLayer:
                                Sa_surfEK80h[-1][-1].append(
                                    [
                                        ESUResult.m_tabChannelResult[iBeam]
                                        .m_tabLayerResult[indexFreq][indexLayer]
                                        .m_Sa
                                        for indexFreq in freqsRanges
                                    ]
                                )
                                Sv_surfEK80h[-1][-1].append(
                                    [
                                        ESUResult.m_tabChannelResult[iBeam]
                                        .m_tabLayerResult[indexFreq][indexLayer]
                                        .m_Sv
                                        for indexFreq in freqsRanges
                                    ]
                                )
                                Lat_surfEK80h[-1][-1].append(
                                    [
                                        ESUResult.m_tabChannelResult[iBeam]
                                        .m_tabLayerResult[0][indexLayer]
                                        .m_latitude
                                    ]
                                )
                                Long_surfEK80h[-1][-1].append(
                                    [
                                        ESUResult.m_tabChannelResult[iBeam]
                                        .m_tabLayerResult[0][indexLayer]
                                        .m_longitude
                                    ]
                                )
                                Depth_surfEK80h[-1][-1].append(
                                    [
                                        ESUResult.m_tabChannelResult[iBeam]
                                        .m_tabLayerResult[0][indexLayer]
                                        .m_Depth
                                    ]
                                )
                                Volume_surfEK80h[-1][-1].append(
                                    [
                                        LayersVolumes[indexLayer][indexSounderEK80h][
                                            iBeam
                                        ]
                                    ]
                                )

                if (timeESU > timeEndEI) & (not process_all):
                    slice_end = True
                    break

            # Save results
            if not os.path.exists(chemin_save):
                os.makedirs(chemin_save)

            str_bloc = "%04d" % (num_bloc)

            # à l'avenir on pourra choisir un format netcdf pour le stockage des résultats
            with open(
                "%s/results_%s%s_EI.pickle" % (chemin_save, str_Name, str_bloc), "wb"
            ) as f:
                # pickle.dump([time_EK80,time_EK80h,time_ME70,Sa_surfME70, Sa_botME70, Sa_surfEK80, Sa_botEK80, Sa_surfEK80h,Sv_surfME70, Sv_botME70, Sv_surfEK80, Sv_botEK80, Sv_surfEK80h,Lat_surfME70, Long_surfME70, Depth_surfME70,Lat_surfEK80,Long_surfEK80,Depth_surfEK80,Lat_surfEK80h,Long_surfEK80h,Depth_surfEK80h,Lat_botME70, Long_botME70, Depth_botME70,Lat_botEK80,Long_botEK80,Depth_botEK80,Volume_surfEK80,Volume_botEK80,Volume_surfEK80h,Volume_surfME70,Volume_botME70], f)
                # pickle.dump([time_EK80, time_EK80h, time_ME70, Sv_surfME70, Sv_botME70, Sv_surfEK80, Sv_botEK80, Sv_surfEK80h, Lat_surfME70, Long_surfME70, Depth_surfME70, Lat_surfEK80, Long_surfEK80, Depth_surfEK80, Lat_surfEK80h, Long_surfEK80h, Depth_surfEK80h, Lat_botME70, Long_botME70, Depth_botME70, Lat_botEK80, Long_botEK80, Depth_botEK80, Volume_surfEK80, Volume_botEK80, Volume_surfEK80h, Volume_surfME70, Volume_botME70, Freqs_EK80, Freqs_EK80h, Freqs_ME70], f)
                pickle.dump(
                    [
                        time_EK80,
                        time_EK80h,
                        time_ME70,
                        Sa_surfME70,
                        Sa_botME70,
                        Sa_surfEK80,
                        Sa_botEK80,
                        Sa_surfEK80h,
                        Sv_surfME70,
                        Sv_botME70,
                        Sv_surfEK80,
                        Sv_botEK80,
                        Sv_surfEK80h,
                        Lat_surfME70,
                        Long_surfME70,
                        Depth_surfME70,
                        Lat_surfEK80,
                        Long_surfEK80,
                        Depth_surfEK80,
                        Lat_surfEK80h,
                        Long_surfEK80h,
                        Depth_surfEK80h,
                        Lat_botME70,
                        Long_botME70,
                        Depth_botME70,
                        Lat_botEK80,
                        Long_botEK80,
                        Depth_botEK80,
                        Volume_surfEK80,
                        Volume_surfEK80h,
                        Volume_surfME70,
                        Freqs_EK80,
                        Freqs_EK80h,
                        Freqs_ME70,
                    ],
                    f,
                )

            # Suppression des résultats d'EI consommés
            EchoIntegrModule.ReleaseOutputList()

            num_bloc = num_bloc + 1

            if slice_end:
                break

        EchoIntegrModule.setEnable(False)
    return
