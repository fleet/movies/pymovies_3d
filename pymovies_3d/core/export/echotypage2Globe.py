# -*- coding: utf-8 -*-
"""
Created on Tue Apr 11 12:01:34 2023

@author: lberger

"""
import os

import numpy as np

import pyg3d.g3d_driver as driver
from sonar_netcdf.utils.nc_reader import NcReader
from sonar_netcdf.utils.nc_reader_presenter import (
    NcReaderPresenter,
)

repository_gridded = "C:/Users/lberger/Desktop/DELMOGES/echointegration/80db/Parcours_1"
repository_g3d = "C:/Users/lberger/Desktop/DELMOGES/echointegration/echotypage"


netcdf_files = sorted(
    [
        os.path.join(subdir, file)
        for subdir, dirs, files in os.walk(repository_gridded)
        for file in files
        if file.endswith(".nc")
    ]
)
for file in netcdf_files:
    reader = NcReader(file, quiet=True)
    presenter = NcReaderPresenter(reader)
    latitude = presenter.dump_content(root="Sonar/Grid_group1/cell_latitude")
    longitude = presenter.dump_content(root="Sonar/Grid_group1/cell_longitude")
    depth = presenter.dump_content(root="Sonar/Grid_group1/cell_depth")
    echotypage_auto = presenter.dump_content("echotypage_auto")
    latitude_g3d = latitude[0, 0, :]
    longitude_3d = longitude[0, 0, :]
    elevation_start = -depth[0, 0] * np.ones(latitude.shape[2])
    elevation_end = -depth[0, -1] * np.ones(latitude.shape[2])
    classification = [np.ma.getdata(echotypage_auto)]
    filename = os.path.splitext(os.path.basename(file))[0]
    driver.create_g3d_from_texture(
        os.path.join(repository_g3d, "RAD" + filename + ".g3d.nc"),
        latitude_g3d,
        longitude_3d,
        elevation_start,
        elevation_end,
        classification,
        ["Classification"],
        ["Class"],
    )
