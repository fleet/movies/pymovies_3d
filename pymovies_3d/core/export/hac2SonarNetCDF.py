import sys
import math
import os
import enum

import numpy as np

import netCDF4 as nc

from pyMovies import pyMovies as mv

from sonar_netcdf import sonar_groups as xsf
from sonar_netcdf.nc_tools import find_type
from pymovies_3d.core.export.sonarNetCDFutils import EnvironmentGroup
#from pymovies_3d.core.export.sonarNetCDFutils import SoundSpeedProfileGroup
from pymovies_3d.core.export.sonarNetCDFutils import PlatformGroup
from pymovies_3d.core.export.sonarNetCDFutils import AttitudeGroup
from pymovies_3d.core.export.sonarNetCDFutils import BeamGroup
from pymovies_3d.core.export.sonarNetCDFutils import BeamType


me70TransmissionPower = [
    15.8437,
    17.3916,
    17.1748,
    15.9917,
    15.1948,
    14.9090,
    15.3041,
    15.5477,
    13.9020,
    10.9221,
    8.4910,
    9.9890,
    12.0286,
    14.8537,
    15.7367,
    15.0501,
    14.9077,
    15.5394,
    16.5049,
    17.5460,
    16.8998,
]

def sv2power(amplitudes, sounder, transducer, softChan):

    frequencyCenter = (softChan.m_startFrequency + softChan.m_endFrequency) / 2
    frequency = softChan.m_acousticFrequency
    if frequencyCenter == 0:
        frequencyCenter = frequency

    accousticAbsorption = softChan.m_absorptionCoef * 0.0001 * 0.001
    accousticWaveLength = sounder.m_soundVelocity / frequencyCenter
    soundSpeed = sounder.m_soundVelocity

    effectivePulseLength = 1
    if transducer.m_pulseShape == 2:
        transmitSignal = softChan.m_softChannelComputeData.GetTransmitSignalObject(
            transducer, softChan
        )
        effectivePulseLength = transmitSignal.GetEffectivePulseLength()
        effectivePulseLength = effectivePulseLength * math.pow(
            10, (2.0 * softChan.m_beamSACorrection) / 10.0
        )
    else:
        T = (
            transducer.m_pulseDuration / 1000000.0
        )  # longueur d'impulsion du signal acoustique
        effectivePulseLength = T * math.pow(
            10, (2.0 * softChan.m_beamSACorrection) / 10.0
        )

    transmitPower = softChan.m_transmissionPower  # En Watt
    if transmitPower == 0:
        # TODO ME70
        channelIndex = 10
        if channelIndex < len(me70TransmissionPower):
            transmitPower = me70TransmissionPower[channelIndex]

    # c'est en DB , on transforme en steradians
    equivalentBeamAngle = math.pow(10, softChan.m_beamEquTwoWayAngle * 0.1)
    gain = softChan.m_beamGain

    pr2pc_factor = 256 / (10 * math.log10(2))

    nbAmplitudes = len(amplitudes)
    power = np.empty(nbAmplitudes, dtype=np.int16)
    power[:] = -32768

    # rangeCWOffset pour décaler globalement d'une demi-longueur d'impulsion.
    pulseLength = transducer.m_pulseDuration / 1000000
    sampleRange = sounder.m_soundVelocity * pulseLength * 0.25
    # sampleRange = 0

    t = (
        transmitPower
        * accousticWaveLength
        * accousticWaveLength
        * soundSpeed
        * equivalentBeamAngle
        * effectivePulseLength
        / (32 * math.pi * math.pi)
    )
    svOffset = 10 * math.log10(t) + 2 * gain

    idx = 0
    for sv in amplitudes:
        sampleRange += transducer.m_beamsSamplesSpacing

        if sv != -32768:
            pr = sv * 0.01
            pr -= 20 * math.log10(sampleRange)
            pr -= 2 * accousticAbsorption * sampleRange
            pr += svOffset

            pr = pr * pr2pc_factor

            power[idx] = pr

        idx += 1

    return power

class hac2SonarNetCDF:
    def __init__(self):
        self.savePath = ""
        self.datasets = {}
        self.environmentGroups = {}
        # self.soundSpeedProfileGroups = {}
        self.platformGroups = {}
        self.beamGroups = {}
        self.debug = True
        self.keepSv = False  # true: pas de conversion Sv vers Power, sinon conversion
        self.ignoreSubBeam = False  # true: on force la sortie en Power/ angle ou Sv/Angle, sinon on utiliser le type de données (quadrants)

    def __del__(self):
        for ds in self.datasets.values():
            ds.close()

    def process_sounder(self, ds, sounder, nbQuadrantsPerChannel):
        print("Process sounder : " + str(sounder.m_SounderId))
        nbTransducers = sounder.m_numberOfTransducer

        rootGroup = xsf.RootGrp()
        rootGroup.create_group(parent_group=ds)

        # TODO ?
        # annotationGroup = xsf.AnnotationGrp()
        # annotationGroup.create_group(group=ds)


        platformGroup = PlatformGroup(parentGroup=ds, sounder=sounder)
        self.platformGroups[sounder.m_SounderId] = platformGroup
        attitudeIdent = 0
        for t in range(0, nbTransducers):
            transducer = sounder.GetTransducer(t)
            if transducer.m_platformId not in platformGroup.attitudes:
                attitudeGroup = xsf.AttitudeGrp().create_group(parent_group=ds)
                attitudeSubGroup = AttitudeGroup(
                    parentGroup=attitudeGroup, ident=str(attitudeIdent)
                )
                platformGroup.attitudes[transducer.m_platformId] = attitudeSubGroup
                attitudeIdent += 1

        provenanceGroup = xsf.ProvenanceGrp()
        provenanceGroupDs = provenanceGroup.create_group(parent_group=ds)
        provenanceGroupDs.conversion_software_name="MOVIES3D"

        # vendorSpecificGroup = xsf.VendorSpecificGrp()
        # vendorSpecificGroup.create_group(group=ds)

        sonarGroup = xsf.SonarGrp()
        sonarGroupDs = sonarGroup.create_group(parent_group=ds)
        sonarGroupDs.sonar_manufacturer = "Simrad"
        sonarGroupDs.sonar_serial_number = str(sounder.m_SounderId)
        if sounder.m_isMultiBeam:
            sonarGroupDs.sonar_model = "ME70"
        else:
            sonarGroupDs.sonar_model = "EK80"

        beamGroups = {}
        channelfrequencies=[]
        channelabsorptions=[]

        print(" Transducer count : " + str(nbTransducers))
        for t in range(0, nbTransducers):
            transducer = sounder.GetTransducer(t)
            print(" Transducer :" + transducer.m_transName)

            numberOfSubBeams = 1
            softChan = transducer.getSoftChannelPolarX(0)
            beam_type = 1
            if transducer.m_numberOfSoftChannel == 1:
                if self.ignoreSubBeam:
                    beam_type = 1
                else:
                    if softChan.m_dataType == 2:
                        numberOfSubBeams = 1
                        beam_type = 1
                    else:
                        if softChan.m_beamType == BeamType.BeamTypeSplit:
                            numberOfSubBeams = 4
                            beam_type = 2 # split_aperture_4_subbeams
                        elif softChan.m_beamType == BeamType.BeamTypeSplit3:
                            numberOfSubBeams = 3
                            beam_type = 3 # split_aperture_3_subbeams
                        elif softChan.m_beamType == BeamType.BeamTypeSplit3CN:
                            numberOfSubBeams = 4
                            beam_type = 4 # split_aperture_3_1_subbeams
                        else:
                            print(" Unknown beam type : " + str(softChan.m_beamType))
                            numberOfSubBeams = 1

                        # Check if numberOfSubBeams is equal to the number of quadrants in first ping data
                        if beam_type != 1:
                            nbQuadrants = nbQuadrantsPerChannel.get(softChan.m_softChannelId, 0)
                            if nbQuadrants != numberOfSubBeams:
                                print("Unexpected number of quadrants for channel " + str(softChan.m_softChannelId)
                                    + ", expected " + str(numberOfSubBeams) + ", got " + str(nbQuadrants)
                                    + ", forcing to beam_type = 1")
                                numberOfSubBeams = 1
                                beam_type = 1
            else:
                numberOfSubBeams = 1

            print("  dataType :" + str(softChan.m_dataType))
            print("  numberOfSubBeams :" + str(numberOfSubBeams))
            print("  beamType :" + str(beam_type))

            beamGroup = BeamGroup(
                parentGroup=sonarGroupDs,
                index=t + 1,
                numberOfSoftChannel=transducer.m_numberOfSoftChannel,
                numberOfSubBeams=numberOfSubBeams,
                isSv=self.keepSv,
            )
            beamGroup.beam.long_name = transducer.m_transName
            beamGroup.beam_type = beam_type

            for channelIdx in range(0, transducer.m_numberOfSoftChannel):
                softChan = transducer.getSoftChannelPolarX(channelIdx)
                # les angles sont convertis en angles mécaniques par M3D on met donc les sensibilités à 1
                if beam_type == 1:
                    beamGroup.echoangle_major_sensitivity[channelIdx] = 1
                    beamGroup.echoangle_minor_sensitivity[channelIdx] = 1
                else:
                    beamGroup.echoangle_major_sensitivity[channelIdx] = softChan.m_beamAthwartAngleSensitivity
                    beamGroup.echoangle_minor_sensitivity[channelIdx] = softChan.m_beamAlongAngleSensitivity

                beamGroup.beam[channelIdx] = softChan.m_channelName
                channelfrequencies.append(softChan.m_acousticFrequency)
                channelabsorptions.append(softChan.m_absorptionCoef * 0.0001 * 0.001)


            beamGroups[t] = beamGroup

        self.beamGroups[sounder.m_SounderId] = beamGroups

        environmentGroup = EnvironmentGroup(parentGroup=ds, frequencies=channelfrequencies, absorptions=channelabsorptions, soundspeed=sounder.m_soundVelocity)
        self.environmentGroups[sounder.m_SounderId] = environmentGroup


    def process_ping(self, ping):
        print("Process ping : " + str(ping.m_computePingFan.m_pingId))
        sounder = ping.m_pSounder

        pingTime = np.int64(0)
        pingTime += ping.m_meanTime.m_TimeCpu * 1e9
        pingTime += ping.m_meanTime.m_TimeFraction * 1e5

        nbTransducers = sounder.m_numberOfTransducer
        quadrants = ping.quadrant_data
        if sounder.m_SounderId in self.datasets:
            ds = self.datasets[sounder.m_SounderId]
        else:
            ds = nc.Dataset(
                self.savePath + "_" + str(sounder.m_SounderId) + ".nc", "w"
            )
            self.datasets[sounder.m_SounderId] = ds

            # compute nb quadrants for each channel using first pring
            nbQuadrantsPerChannel = {}
            channelDataIdx = 0
            for t in range(0, nbTransducers):
                transducer = sounder.GetTransducer(t)
                softChan = transducer.getSoftChannelPolarX(0)
                if len(quadrants) == 0:
                    nbQuadrantsPerChannel[softChan.m_softChannelId] = 0
                else:
                    print("quadrants : " + str(len(quadrants)))
                    nbQuadrantsPerChannel[softChan.m_softChannelId] = len(quadrants[t])

            print("nbQuadrantsPerChannel : " + str(nbQuadrantsPerChannel))


            self.process_sounder(ds, sounder, nbQuadrantsPerChannel)

        # M3D beam datas are stored in a single array for transducers and channel
        beamDatas = ping.beam_data
        beamDataIdx = 0

        for t in range(0, nbTransducers):
            transducer = sounder.GetTransducer(t)
            platform = transducer.GetPlatform()

            mainBeamData = beamDatas[beamDataIdx]

            beamTime = np.int64(0)
            beamTime += mainBeamData.m_time.m_TimeCpu * 1e9
            beamTime += mainBeamData.m_time.m_TimeFraction * 1e5

            beamGroup = self.beamGroups[sounder.m_SounderId][t]

            sample_t_type = find_type(beamGroup.g, "sample_t")
            angle_t_type = find_type(beamGroup.g, "angle_t")

            nbPing = len(beamGroup.ping_time)

            beamGroup.ping_time[nbPing] = beamTime

            navAttribute = mainBeamData.navigationAttribute

            # si premier ping et cap vaut 0, on met un nan
            if nbPing != 0 or navAttribute.m_headingRad != 0:
                beamGroup.platform_heading[nbPing] = math.degrees(
                    navAttribute.m_headingRad
                )

            navPosition = mainBeamData.navigationPosition
            beamGroup.platform_latitude[nbPing] = navPosition.m_lattitudeDeg
            beamGroup.platform_longitude[nbPing] = navPosition.m_longitudeDeg

            navAttitude = mainBeamData.navigationAttitude
            beamGroup.platform_pitch[nbPing] = math.degrees(navAttitude.pitchRad)
            beamGroup.platform_roll[nbPing] = math.degrees(navAttitude.rollRad)

            beamGroup.platform_vertical_offset[nbPing] = navAttitude.sensorHeaveMeter

            # beamGroup.active_MRU[nbPing] = 0
            beamGroup.active_position_sensor[nbPing] = platform.sensor_ident
            beamGroup.beam_stabilisation[nbPing] = (
                1 if sounder.m_isMultiBeam else 0
            )  # stabilisation ? dans le contexte M3D => 1 si ME70, 0 sinon
            beamGroup.non_quantitative_processing[nbPing] = 0  # 0 dans le contexte M3D
            beamGroup.sample_interval[nbPing] = transducer.m_timeSampleInterval * 1e-6
            beamGroup.sound_speed_at_transducer[nbPing] = sounder.m_soundVelocity
            # beamGroup.time_varied_gain[nbPing] # Uniquement si equation type 2, ce qui est a priori pas notre cas, du coup rien
            beamGroup.tx_transducer_depth[nbPing] = mainBeamData.m_compensateHeave+transducer.m_transDepthMeter
            # beamGroup.waterline_to_chart_datum[nbPing] = 0 # optionnel, a priori pas dispo dans M3D

            # Ecriture des données dépendante du beam et txBeam

            # Reshape des amplitudes pour correspondre au structure NetCDF
            polarMatrix = ping.GetPolarMatrix(t)

            amplitudes = np.array(polarMatrix.m_Amplitude)
            amplitudes = np.transpose(amplitudes)

            athwartAngles = np.array(polarMatrix.m_AthwartAngle)
            athwartAngles = np.transpose(athwartAngles)

            alongAngles = np.array(polarMatrix.m_AlongAngle)
            alongAngles = np.transpose(alongAngles)


            # channelIdx = beam in SonarNetCDF
            for channelIdx in range(0, transducer.m_numberOfSoftChannel):

                softChan = transducer.getSoftChannelPolarX(channelIdx)
                channelDataIdx = beamDataIdx + channelIdx

                channelBeamData = beamDatas[channelDataIdx]

                if self.ignoreSubBeam or softChan.m_dataType == 2:
                    # amplitudes[channelIdx][0] = -32768
                    if self.keepSv:
                        beamGroup.backscatter_r[nbPing, channelIdx, 0] = amplitudes[
                            channelIdx
                        ].astype(sample_t_type)
                    else:
                        power = sv2power(
                            amplitudes[channelIdx], sounder, transducer, softChan
                        )
                        beamGroup.backscatter_r[nbPing, channelIdx, 0] = power.astype(
                            sample_t_type
                        )

                    # beamGroup.backscatter_i[nbPing,channelIdx,0] = amplitudes[channelIdx].astype(np.float32)

                    beamGroup.echoangle_major[nbPing, channelIdx] = athwartAngles[
                        channelIdx
                    ].astype(angle_t_type)
                    beamGroup.echoangle_minor[nbPing, channelIdx] = alongAngles[
                        channelIdx
                    ].astype(angle_t_type)

                if beamGroup.beam_type == 1:
                    if beamGroup.conversion_equation_type == 5:
                        beamGroup.backscatter_r[nbPing, channelIdx, 0] = amplitudes[
                            channelIdx
                        ].astype(sample_t_type)
                    elif beamGroup.conversion_equation_type == 1:
                        power = sv2power(
                            amplitudes[channelIdx], sounder, transducer, softChan
                        )
                        beamGroup.backscatter_r[nbPing, channelIdx, 0] = power.astype(
                            sample_t_type
                        )
                    else:
                        print("Unexpected equation type for single, got " + str(beamGroup.conversion_equation_type) )

                    beamGroup.echoangle_major[nbPing, channelIdx] = athwartAngles[
                        channelIdx
                    ].astype(angle_t_type)
                    beamGroup.echoangle_minor[nbPing, channelIdx] = alongAngles[
                        channelIdx
                    ].astype(angle_t_type)

                elif beamGroup.beam_type in (2, 3, 4):
                    if beamGroup.conversion_equation_type == 4:
                        if len(quadrants) == 0:
                            print("No quadrant data....")
                        elif len(quadrants[channelDataIdx]) != beamGroup.numberOfSubBeams:
                            print(
                                "Unexpected quadrant count for channel "
                                + str(softChan.m_softChannelId)
                                + ", got "
                                + str(len(quadrants[channelDataIdx]))
                                + ", expected "
                                + str(beamGroup.numberOfSubBeams)
                            )
                        else:
                            for q in range(0, beamGroup.numberOfSubBeams):  # aka subbeam in SonarNetCDF
                                beamGroup.backscatter_r[nbPing, channelIdx, q] = np.array(
                                    quadrants[channelDataIdx][q][0]
                                ).astype(sample_t_type)
                                beamGroup.backscatter_i[nbPing, channelIdx, q] = np.array(
                                    quadrants[channelDataIdx][q][1]
                                ).astype(sample_t_type)
                    else:
                        print("Unexpected equation type for complex, got " + str(beamGroup.conversion_equation_type) )

                else:
                    print("Unsupported beam_type : " + str(beamGroup.beam_type))

                beamGroup.beamwidth_transmit_major[nbPing, channelIdx] = math.degrees(
                    softChan.m_beam3dBWidthAthwartRad
                )
                beamGroup.beamwidth_transmit_minor[nbPing, channelIdx] = math.degrees(
                    softChan.m_beam3dBWidthAlongRad
                )
                beamGroup.beamwidth_receive_major[nbPing, channelIdx] = math.degrees(
                    softChan.m_beam3dBWidthAthwartRad
                )
                beamGroup.beamwidth_receive_minor[nbPing, channelIdx] = math.degrees(
                    softChan.m_beam3dBWidthAlongRad
                )
                beamGroup.gain_correction[
                    nbPing, channelIdx
                ] = softChan.m_beamSACorrection
                beamGroup.equivalent_beam_angle[nbPing, channelIdx] = math.pow(
                    10, softChan.m_beamEquTwoWayAngle * 0.1
                )

                if channelBeamData.m_bottomWasFound:
                    beamGroup.detected_bottom_range[
                        nbPing, channelIdx
                    ] = channelBeamData.m_bottomRange

                beamGroup.blanking_interval[nbPing, channelIdx] = 0
                # softChan.m_startSample * transducer.m_timeSampleInterval * 1e-6

                # MA - Sensitivity of the sonar receiver for the current ping. Necessary for type 2 conversion equation.
                # beamGroup.receiver_sensitivity[nbPing, channelIdx] = 0
                # => NA

                # MA - Receiving or monostatic transducer index associated with the given beam
                # beamGroup.receive_transducer_index[nbPing, channelIdx] = 0
                # => NA

                # Ecriture des données dépendante du txBeam
                beamGroup.transmit_type[nbPing, channelIdx] = (
                    1 if transducer.m_pulseShape == 2 else 0
                )  # CW => CW(0), FM => LFM(1), pas HFM(2) dans M3D

                beamGroup.sample_time_offset[
                    nbPing, channelIdx
                ] = 0  # => internalDelayTranslation => modify transducteur, on laisse à 0

                beamGroup.transmit_power[
                    nbPing, channelIdx
                ] = softChan.m_transmissionPower
                beamGroup.transmit_frequency_start[nbPing, channelIdx] = (
                    softChan.m_startFrequency
                    if transducer.m_pulseShape == 2
                    else softChan.m_acousticFrequency
                )
                beamGroup.transmit_frequency_stop[nbPing, channelIdx] = (
                    softChan.m_endFrequency
                    if transducer.m_pulseShape == 2
                    else softChan.m_acousticFrequency
                )
                beamGroup.transducer_gain[nbPing, channelIdx] = softChan.m_beamGain
                beamGroup.transmit_bandwidth[nbPing, channelIdx] = softChan.m_bandWidth
                beamGroup.tx_beam_rotation_phi[nbPing, channelIdx] = math.degrees(
                    softChan.m_mainBeamAthwartSteeringAngleRad
                )
                beamGroup.rx_beam_rotation_phi[nbPing, channelIdx] = math.degrees(
                    softChan.m_mainBeamAthwartSteeringAngleRad
                )
                beamGroup.tx_beam_rotation_theta[nbPing, channelIdx] = math.degrees(
                    softChan.m_mainBeamAlongSteeringAngleRad
                )
                beamGroup.rx_beam_rotation_theta[nbPing, channelIdx] = math.degrees(
                    softChan.m_mainBeamAlongSteeringAngleRad
                )
                beamGroup.tx_beam_rotation_psi[nbPing, channelIdx] = 0
                beamGroup.rx_beam_rotation_psi[nbPing, channelIdx] = 0

                # M - Nominal duration of the transmit pulse. This is not the effective pulse duration.
                beamGroup.transmit_duration_nominal[
                    nbPing, channelIdx
                ] = transducer.m_pulseDuration/ 1000000.0

                # MA - Effective duration of the received pulse. This is the duration of the square pulse containing the same energy as the actual receive pulse. This parameter is either theoretical or comes from a calibration exercise and adjusts the nominal duration of the transmitted pulse to the measured one. During calibration it is obtained by integrating the energy of the received signal on the calibration target normalised by its maximum energy. Necessary for type 1, 2,3 and 4 conversion equations.
                if transducer.m_pulseShape == 2:
                    T = softChan.m_softChannelComputeData.GetTransmitSignalObject(
                        transducer, softChan
                    ).m_effectivePulseLength
                else:
                    T = transducer.m_pulseDuration / 1000000.0
                effectivePulseLength = T * math.pow(
                    10, (2.0 * softChan.m_beamSACorrection) / 10.0
                )
                beamGroup.receive_duration_effective[
                    nbPing, channelIdx
                ] = effectivePulseLength

                # beamGroup.transmit_source_level[nbPing, txBeam] = 0 # ça ne nous concerne pas

            beamDataIdx += transducer.m_numberOfSoftChannel

    # def process_environnement_profile(self, e):

    #     objectManager = mv.moGetObjectManager()
    #     navigationContainer = objectManager.GetNavPositionContainer()
    #     navPosition = navigationContainer.findDatedObject(e.objectTime)

    #     for sounderId, soundSpeedProfileGroup in self.soundSpeedProfileGroups.items():

    #         measTime = np.int64(0)
    #         measTime += e.objectTime.m_TimeCpu * 1e9
    #         measTime += e.objectTime.m_TimeFraction * 1e5

    #         for m in e.measures:
    #             nbProfile = len(soundSpeedProfileGroup.profile)
    #             soundSpeedProfileGroup.profile[nbProfile] = nbProfile + 1
    #             soundSpeedProfileGroup.time[nbProfile] = measTime

    #             soundSpeedProfileGroup.lat[nbProfile] = (
    #                 navPosition.m_lattitudeDeg if navPosition else None
    #             )
    #             soundSpeedProfileGroup.lon[nbProfile] = (
    #                 navPosition.m_longitudeDeg if navPosition else None
    #             )

    #             soundSpeedProfileGroup.depth[nbProfile] = m.depthFromSurface
    #             # soundSpeedProfileGroup.sample_count ????
    #             soundSpeedProfileGroup.sound_speed[nbProfile] = m.speedOfSound
    #             soundSpeedProfileGroup.temperature[nbProfile] = m.temperature
    #             soundSpeedProfileGroup.salinity[nbProfile] = m.salinity
    #             soundSpeedProfileGroup.absorption[nbProfile] = m.soundAbsorption

    #             # TODO ?
    #             # soundSpeedProfileGroup.pressure = m.pressure
    #             # soundSpeedProfileGroup.conductivity = m.conductivity

    def process_navPosition(self, navPosition):
        posTime = np.uint64(0)
        posTime += navPosition.objectTime.m_TimeCpu * 1e9
        posTime += navPosition.objectTime.m_TimeFraction * 1e5

        objectManager = mv.moGetObjectManager()
        navAttributesContainer = objectManager.GetNavAttributesContainer()
        navAttributes = navAttributesContainer.findClosestDatedObject(navPosition.objectTime)

        for sounderId, platformGroup in self.platformGroups.items():
            positionSubGroup = platformGroup.position
            nbPos = len(positionSubGroup.time)
            positionSubGroup.time[nbPos] = posTime
            positionSubGroup.latitude[nbPos] = navPosition.m_lattitudeDeg
            positionSubGroup.longitude[nbPos] = navPosition.m_longitudeDeg
            if navAttributes is not None:
                positionSubGroup.heading[nbPos] = math.degrees(
                    navAttributes.m_headingRad
                )
                positionSubGroup.speed_relative[nbPos] = navAttributes.m_speedMeter

    def process_navAttitude(self, navAttitude):

        if navAttitude is None:
            print("None nav attitude !")
            return

        attitudeTime = np.uint64(0)
        attitudeTime += navAttitude.objectTime.m_TimeCpu * 1e9
        attitudeTime += navAttitude.objectTime.m_TimeFraction * 1e5

        objectManager = mv.moGetObjectManager()
        navAttributesContainer = objectManager.GetNavAttributesContainer()
        navAttributes = navAttributesContainer.findDatedObject(navAttitude.objectTime)

        for sounderId, platformGroup in self.platformGroups.items():
            if navAttitude.sensorId in platformGroup.attitudes:
                attitudeSubGroup = platformGroup.attitudes[navAttitude.sensorId]
                nbAttitudes = len(attitudeSubGroup.time)
                attitudeSubGroup.time[nbAttitudes] = attitudeTime
                attitudeSubGroup.vertical_offset[nbAttitudes] = navAttitude.sensorHeaveMeter
                attitudeSubGroup.pitch[nbAttitudes] = math.degrees(navAttitude.pitchRad)
                attitudeSubGroup.roll[nbAttitudes] = math.degrees(navAttitude.rollRad)
                attitudeSubGroup.heading[nbAttitudes] = math.degrees(navAttitude.yawRad)


def main(args):

    if len(args) == 0:
        root = "C:/perso/M3D/datas/hac2xsf/"
        # fileName = 'PELGAS19_594_20190526_080626'
        # fileName = "PELGAS17_130_20170829_095720"  # Donnée EK80 FM
        # fileName = 'PELGAS17_016_20170507_050342'  # Donnée EK80 CW
        fileName = 'Sardine_schools'
        # fileName = 'SEEKLEAK_009_20170913_145231'
        # fileName = 'SEEKLEAK_009_20170913_150231'
        filePath = root + fileName + ".hac"
    else:
        filePath = args[0]

    converter = hac2SonarNetCDF()
    converter.savePath = os.path.splitext(filePath)[0]
    converter.keepSv = True
    converter.ignoreSubBeam = False

    # Chargement de la config
    if len(args) > 1:
        mv.moLoadConfig(args[1])

    kernelParameter = mv.moLoadKernelParameter()
    kernelParameter.m_bKeepQuadrantsInMemory = True
    mv.moSaveKernelParameter(kernelParameter)

    mv.moOpenHac(filePath)

    print("start read...")

    envIdx = 0
    navPositionIdx = 0
    navAttitudeIdx = {}

    nbProcessedPings = 0
    fileStatus = mv.moGetFileStatus()

    while not fileStatus.m_StreamClosed:
        mv.moReadChunk()

        # Process pings
        nbPing = mv.moGetNumberOfPingFan()
        processedPings = []
        for p in range(0, nbPing):
            ping = mv.moGetPingFan(p)
            processedPings.append(ping.m_computePingFan.m_pingId)
            converter.process_ping(ping)
        nbProcessedPings += nbPing
        for p in processedPings:
            mv.moRemovePing(p)

        objectManager = mv.moGetObjectManager()

        # # Process environementProfile
        # envContainer = objectManager.GetEnvironnementContainer()
        # nbEnv = envContainer.count()
        # for idx in range(envIdx, nbEnv):
        #     envData = envContainer.datedObject(idx)
        #     converter.process_environnement_profile(envData)
        # envIdx = nbEnv

        # Process navigation position
        navPositionContainer = objectManager.GetNavPositionContainer()
        nbPosition = navPositionContainer.count()
        for idx in range(navPositionIdx, nbPosition):
            navPosition = navPositionContainer.datedObject(idx)
            converter.process_navPosition(navPosition)
        navPositionIdx = nbPosition

        # Process navigation attitudes
        for i in range(0, objectManager.GetNavAttitudeContainerCount()):
            navId = objectManager.GetNavAttitudeSensorId(i)
            navAttitudeContainer = objectManager.GetNavAttitudeContainer(navId)
            nbAttitude = navPositionContainer.count()
            for idx in range(navAttitudeIdx.get(navId, 0), nbAttitude):
                navAttitude = navAttitudeContainer.datedObject(idx)
                converter.process_navAttitude(navAttitude)
            navAttitudeIdx[navId] = nbAttitude

        fileStatus = mv.moGetFileStatus()

    nbPing = mv.moGetNumberOfPingFan()
    print("Pings processed : " + str(nbProcessedPings))
    # print("EnvironnementProfiles processed : " + str(envIdx))
    print("Nav positions processed : " + str(navPositionIdx))
    print("Nav attitudes processed : " + str(navAttitudeIdx))


if __name__ == "__main__":
    main(sys.argv[1:])
