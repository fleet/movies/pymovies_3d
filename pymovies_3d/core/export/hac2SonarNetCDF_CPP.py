# -*- coding: utf-8 -*-
"""
Created on Tue Sep 20 13:26:26 2022

@author: lberger
"""
from pyMovies import pyMovies as mv

def convert( pathHAC, pathNetCDF):

    # assure de tout réécrire
    ParameterKernel = mv.moLoadKernelParameter()
    ParameterKernel.m_bAutoLengthEnable = True
    ParameterKernel.m_MaxRange = 250
    ParameterKernel.m_bAutoDepthEnable=1
    ParameterKernel.m_bIgnorePhaseData=0
    mv.moSaveKernelParameter(ParameterKernel)

    #lecture par bloc
    ParameterReader = mv.moLoadReaderParameter()
    ParameterReader.m_chunkNumberOfPingFan=2000
    mv.moSaveReaderParameter(ParameterReader)

    mv.moOpenHac(pathHAC)


    # lecture et traitement
    ######################
    FileStatus = mv.moGetFileStatus()

    ilec = 0

    # while not FileStatus.m_StreamClosed and ilec<1:
    while not FileStatus.m_StreamClosed:
        mv.moReadChunk()
        ilec = ilec + 1
        # print(ilec)

    # écriture HAC
    ##############
    mv.moStartWrite(pathNetCDF, "",mv.WriteOutputType.NetCDF) #WriteOutputType.[Hac, NetCDF, Both]
    mv.moStopWrite()
