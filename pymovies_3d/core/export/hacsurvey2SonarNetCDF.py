# -*- coding: utf-8 -*-
"""
Created on Thu Jan 21 14:56:54 2021

@author: lberger
"""
import os
import pymovies_3d.core.export.hac2SonarNetCDF as hac2SonarNetCDF

pathHAC = "C:/Users/lberger/Desktop/M3D_example_dataset/PELGAS13/RUN003"

filelist = []
for root, subFolders, files in os.walk(pathHAC):
    for file in files:
        if file.endswith(".hac"):
            filelist.append(os.path.join(root, file))

for FileName in filelist:
    hac2SonarNetCDF.main([FileName, pathHAC])
