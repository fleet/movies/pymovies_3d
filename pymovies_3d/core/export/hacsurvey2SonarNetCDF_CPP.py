# -*- coding: utf-8 -*-
"""
Created on Thu Jan 21 14:56:54 2021

@author: lberger
"""
import os
import pymovies_3d.core.export.hac2SonarNetCDF_CPP as hac2SonarNetCDF_CPP

pathHAC = "C:/Users/lberger//Desktop/EVHOE2021/RUN002/"
pathNetCDF = "C:/Users/lberger//Desktop/EVHOE2021/NetCDF/"  # dossier de réécriture

filelist = []
for root, subFolders, files in os.walk(pathHAC):
    for file in files:
        if file.endswith(".hac"):
            filelist.append(os.path.join(root, file))

for FileName in filelist:
    hac2SonarNetCDF_CPP.convert(  FileName, pathNetCDF)
