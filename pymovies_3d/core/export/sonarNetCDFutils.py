import enum
import math
import numpy as np
import netCDF4 as nc

from sonar_netcdf import sonar_groups as xsf


class BeamType(enum.IntEnum):
    BeamTypeSplit = 1
    BeamTypeSplit3 = 17
    BeamTypeSplit3CN = 65


class NcGroup:

    attributes = []
    attributes_proxy = []

    def create_attr(self, attr):
        """
        Create property for a given netCDF variable
        """
        fname = "create_" + attr
        #pylint:disable = no-member
        f = getattr(self.xsfGroup, fname, None)
        if f is not None:
            setattr(self, attr, f(self.g))
            self.attributes.append(attr)
        else:
            print(fname + " not found !")

    def create_attr_proxy_array(self, attr, dimensions):
        """
        Create a numpy array as a proxy for a netCDF variable, save must be called to save numpy array to netCDF dataset
        """
        a = getattr(self, attr, None)
        if a is None:
            print(
                "can't create proxy for attribute " + attr + ", attribute not found !"
            )
            return False

        if len(a.dimensions) == 0:
            print(
                "can't create proxy for attribute "
                + attr
                + ", no dimensions provided !"
            )
            return False

        if isinstance(a.datatype, nc._netCDF4.VLType):
            print(
                "can't create proxy for attribute "
                + attr
                + ", inner datatype is VLType !"
            )
            return False

        dimValues = ()
        for dim in a.dimensions:
            dimValues += (dimensions[dim],)

        proxyArray = None

        if hasattr(a, "_FillValue"):
            fillValue = getattr(a, "_FillValue")
            proxyArray = np.full(dimValues, fillValue, dtype=a.datatype)
        else:
            # print("create empty array for " + attr)
            # print("  a : " + str(a))
            # print(" type a : " + str(type(a)))
            proxyArray = np.empty(dimValues, dtype=a.datatype)
            proxyArray[:] = a[:]

        if proxyArray is not None:
            setattr(self, attr, proxyArray)
            self.attributes_proxy.append(attr)
        else:
            print("Proxy array creation failed for " + attr)
            return False

        return True

    def create_attributes(self, ncType, exclude=None):
        """
        Create property for netCDF variables
        """
        if exclude is None:
            exclude = []
        attributeList = [
            func
            for func in dir(ncType)
            if callable(getattr(ncType, func)) and func.startswith("create_")
        ]
        attributeList.remove("create_group")
        attributeList.remove("create_dimension")

        for a in exclude:
            fname = "create_" + a
            if fname in attributeList:
                attributeList.remove(fname)

        for fname in attributeList:
            attr = fname[7:]
            self.create_attr(attr)

    def create_attributes_proxy_arrays(self, dimensions, attributes=None, exclude=None):
        """
        Create numpy arrays proxies for netCDF variables
        """
        if exclude is None:
            exclude = []
        if attributes is None:
            attributes = []
        if len(attributes) == 0:
            attributes = self.attributes

        for attr in attributes:
            if attr not in exclude:
                a = getattr(self, attr, None)
                if not isinstance(a,np.ndarray):
                    self.create_attr_proxy_array(attr, dimensions)

    def save(self):
        """
        Save numpy arrays proxies to real netCDF variables
        """
        for attr in self.attributes_proxy:
            a = getattr(self, attr, None)
            # pylint:disable = no-member
            self.g[attr][:] = a


class EnvironmentGroup(NcGroup):

    # Create attribute for pylint...
    sound_speed_indicative  = None
    frequency  = None
    absorption_indicative  = None

    def __init__(self, parentGroup, frequencies, absorptions, soundspeed):
        self.xsfGroup = xsf.EnvironmentGrp()
        self.g = self.xsfGroup.create_group(parent_group=parentGroup)

        nbFrequencies=len(frequencies)
        EnvironmentDimensions = {}
        EnvironmentDimensions[xsf.EnvironmentGrp.FREQUENCY_DIM_NAME] = nbFrequencies
        self.xsfGroup.create_dimension(group=self.g, values=EnvironmentDimensions)

        self.create_attributes(ncType=xsf.EnvironmentGrp)

        self.sound_speed_indicative[:]=soundspeed
        for freq in range(0, nbFrequencies):
            self.frequency[freq]=frequencies[freq]
            self.absorption_indicative[freq]=absorptions[freq]

class SoundSpeedProfileGroup(NcGroup):
    def __init__(self, parentGroup):
        self.xsfGroup = xsf.SoundSpeedProfileGrp()
        self.g = self.xsfGroup.create_group(parent_group=parentGroup)

        soundSpeedProfileDimensions = {}
        soundSpeedProfileDimensions[xsf.SoundSpeedProfileGrp.PROFILE_TIME_DIM_NAME] = None
        soundSpeedProfileDimensions[
            xsf.SoundSpeedProfileGrp.SAMPLE_COUNT_VNAME
        ] = None
        self.xsfGroup.create_dimension(group=self.g, values=soundSpeedProfileDimensions)

        self.create_attributes(ncType=xsf.SoundSpeedProfileGrp)

class AttitudeGroup(NcGroup):
    def __init__(self, parentGroup, ident):
        self.xsfGroup = xsf.AttitudeSubGroup()
        self.g = self.xsfGroup.create_group(parent_group=parentGroup, ident=ident)

        dimensions = {}
        dimensions[xsf.AttitudeSubGroup.TIME_DIM_NAME] = None
        self.xsfGroup.create_dimension(group=self.g, values=dimensions)

        self.create_attributes(ncType=xsf.AttitudeSubGroup)


class PositionGroup(NcGroup):
    def __init__(self, parentGroup):
        self.xsfGroup = xsf.PositionSubGroup()
        self.g = self.xsfGroup.create_group(parent_group=parentGroup, ident="1")

        dimensions = {}
        dimensions[xsf.PositionSubGroup.TIME_DIM_NAME] = None
        self.xsfGroup.create_dimension(group=self.g, values=dimensions)

        self.create_attributes(ncType=xsf.PositionSubGroup)


class PlatformGroup(NcGroup):

    # Create attribute for pylint...
    mru_offset_x = None
    mru_offset_y = None
    mru_offset_z = None
    mru_rotation_x = None
    mru_rotation_y = None
    mru_rotation_z = None
    position_offset_x = None
    position_offset_y = None
    position_offset_z = None
    transducer_function = None
    transducer_ids = None
    transducer_offset_x = None
    transducer_offset_y = None
    transducer_offset_z = None
    transducer_rotation_x = None
    transducer_rotation_y = None
    transducer_rotation_z = None

    def __init__(self, parentGroup, sounder):
        self.xsfGroup = xsf.PlatformGrp()
        self.g = self.xsfGroup.create_group(parent_group=parentGroup)

        nbTransducers = sounder.m_numberOfTransducer
        dimensions = {}
        dimensions[xsf.PlatformGrp.TRANSDUCER_DIM_NAME] = nbTransducers
        dimensions[xsf.PlatformGrp.POSITION_DIM_NAME] = 1
        dimensions[xsf.PlatformGrp.MRU_DIM_NAME] = nbTransducers
        self.xsfGroup.create_dimension(group=self.g, values=dimensions)

        self.create_attributes(ncType=xsf.PlatformGrp)

        # create attitude and position sub groups
        # attitudeGroup = xsf.AttitudeGrp().create_group(parent_group=parentGroup)
        self.attitudes = {}

        positionGroup = xsf.PositionGrp().create_group(parent_group=parentGroup)
        self.position = PositionGroup(parentGroup=positionGroup)

        # self.mru_ids[0] = 0
        for t in range(0, nbTransducers):
            transducer = sounder.GetTransducer(t)
            platform = transducer.GetPlatform()

            self.mru_offset_x[t] = 0
            self.mru_offset_y[t] = 0
            self.mru_offset_z[t] = platform.depth_offset - transducer.m_transDepthMeter

            self.mru_rotation_x[t] = 0
            self.mru_rotation_y[t] = 0
            self.mru_rotation_z[t] = 0

        # self.position_ids[0] = 0
        self.position_offset_x[0] = 0
        self.position_offset_y[0] = 0
        self.position_offset_z[0] = 0

        # position des transducteurs
        for transducerIdx in range(0, sounder.m_numberOfTransducer):
            transducer = sounder.GetTransducer(transducerIdx)
            self.transducer_function[transducerIdx] = 3
            self.transducer_ids[transducerIdx] = transducer.m_transName

            platform = transducer.GetPlatform()
            self.transducer_offset_x[transducerIdx] = platform.along_ship_offset
            self.transducer_offset_y[transducerIdx] = platform.athwart_ship_offset
            self.transducer_offset_z[transducerIdx] = platform.depth_offset

            self.transducer_rotation_x[transducerIdx] = math.degrees(
                transducer.m_transFaceAlongAngleOffsetRad
            )
            self.transducer_rotation_y[transducerIdx] = math.degrees(
                transducer.m_transFaceAthwarAngleOffsetRad
            )
            self.transducer_rotation_z[transducerIdx] = math.degrees(
                transducer.m_transRotationAngleRad
            )


class BeamGroup(NcGroup):

    # Create attribute for pylint...
    beam = None
    backscatter_r = None
    backscatter_i = None
    echoangle_major = None
    echoangle_minor = None
    echoangle_major_sensitivity = None
    echoangle_minor_sensitivity = None

    def __init__(
        self, parentGroup, index, numberOfSoftChannel, numberOfSubBeams, isSv
    ):

        sample_t_type = np.int16 if numberOfSubBeams == 1 else np.float32
        angle_t_type = np.int16
        vlen_type_dict = {"sample_t": sample_t_type, "angle_t": angle_t_type}

        if numberOfSubBeams==1:
            if isSv:
                conversion_equation_t=5
            else:
                conversion_equation_t=3
        else:
            conversion_equation_t=4


        self.xsfGroup = xsf.BeamGroup1Grp()
        self.g = self.xsfGroup.create_group(
            parent_group=parentGroup,
            vlen_type_dict=vlen_type_dict,
            ident="Beam_group" + str(index),
            beam_mode="vertical",
            conversion_equation_type=np.int8(conversion_equation_t)
        )
        self.g.preferred_MRU = index - 1

        beamDimensions = {}
        beamDimensions[xsf.BeamGroup1Grp.BEAM_DIM_NAME] = numberOfSoftChannel
        beamDimensions[xsf.BeamGroup1Grp.SUBBEAM_DIM_NAME] = numberOfSubBeams
        beamDimensions[xsf.BeamGroup1Grp.PING_TIME_DIM_NAME] = None
        beamDimensions[xsf.BeamGroup1Grp.TX_BEAM_DIM_NAME] = numberOfSoftChannel
        beamDimensions[xsf.BeamGroup1Grp.FREQUENCY_DIM_NAME] = numberOfSoftChannel
        self.xsfGroup.create_dimension(group=self.g, values=beamDimensions)

        self.backscatter_r = self.xsfGroup.create_backscatter_r(group=self.g)
        self.echoangle_major = self.xsfGroup.create_echoangle_major(group=self.g)
        self.echoangle_minor = self.xsfGroup.create_echoangle_minor(group=self.g)
        self.xsfGroup.create_beam_type(group=self.g)

        if numberOfSubBeams == 1:  # Sv of power
            if sample_t_type == np.int16:
                self.backscatter_r.missing_value = -32768
                if isSv:
                    self.backscatter_r.scale_factor = np.float32(0.01)

            if angle_t_type == np.int16:
                self.echoangle_major.missing_value = -32768
                self.echoangle_major.scale_factor = np.float32(0.01)
                self.echoangle_major.valid_range = np.int16([-18000, 18000])

                self.echoangle_minor.missing_value = -32768
                self.echoangle_minor.scale_factor = np.float32(0.01)
                self.echoangle_minor.valid_range = np.int16([-18000, 18000])

        self.create_attributes(
            ncType=xsf.BeamGroup1Grp,
            exclude=["backscatter_r", "echoangle_major", "echoangle_minor", "beam_type"],
        )

        # Disable automatic conversion
        self.backscatter_r.set_auto_maskandscale(False)
        self.backscatter_i.set_auto_maskandscale(False)
        self.echoangle_major.set_auto_maskandscale(False)
        self.echoangle_minor.set_auto_maskandscale(False)

    @property
    def numberOfSoftChannel(self):
        return self.g.dimensions[xsf.BeamGroup1Grp.BEAM_DIM_NAME].size

    @property
    def numberOfSubBeams(self):
        return self.g.dimensions[xsf.BeamGroup1Grp.SUBBEAM_DIM_NAME].size

    @property
    def conversion_equation_type(self):
        return self.g.conversion_equation_type

    @property
    def beam_type(self):
        return self.g['beam_type'][:]

    @beam_type.setter
    def beam_type(self, beam_type):
        self.g['beam_type'][:] = beam_type


class GridGroup(NcGroup):
    def __init__(
        self,
        parentGroup,
        index,
        beamDimensions=None,
        frequencyDimensions=None,
        pingDimensions=None,
        rangeDimensions=None,
    ):

        # sample_t_type = np.int16 if numberOfSubBeams == 1 else np.float32
        sample_t_type = np.int16
        angle_t_type = np.int16
        vlen_type_dict = {"sample_t": sample_t_type, "angle_t": angle_t_type}

        self.xsfGroup = xsf.GridGroup1Grp()
        self.g = self.xsfGroup.create_group(
            parent_group=parentGroup,
            vlen_type_dict=vlen_type_dict,
            ident="Gridded_" + str(index),
        )

        dimensions = {}

        # Somme du nombre de sofChannels de tous les transducers
        dimensions[xsf.GridGroup1Grp.BEAM_DIM_NAME] = beamDimensions
        dimensions[xsf.GridGroup1Grp.TX_BEAM_DIM_NAME] = beamDimensions
        dimensions[xsf.GridGroup1Grp.FREQUENCY_DIM_NAME] = frequencyDimensions
        dimensions[xsf.GridGroup1Grp.PING_AXIS_DIM_NAME] = pingDimensions
        dimensions[xsf.GridGroup1Grp.RANGE_AXIS_DIM_NAME] = rangeDimensions
        self.xsfGroup.create_dimension(group=self.g, values=dimensions)

        self.create_attributes(
            ncType=xsf.GridGroup1Grp, exclude=["integrated_backscatter"]
        )
        self.integrated_backscatter = self.xsfGroup.create_integrated_backscatter(
            group=self.g, missing_value=-9999999.9
        )
        self.attributes.append("integrated_backscatter")

        self.create_attributes_proxy_arrays(
            dimensions,
            exclude=[
                "beam",
                "beam_reference",
                "time_varied_gain",
                "ping_axis_interval_type",
                "ping_axis_interval_value",
                "range_axis_interval_type",
                "range_axis_interval_value",
            ],
        )
