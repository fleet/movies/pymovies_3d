# -*- coding: utf-8 -*-
"""
Created on Fri Dec 16 12:04:52 2022

@author: nlebouff
"""

# imports
#########
import pyMovies.pyMovies as mv

import pymovies_3d.core.hac_util.hac_util as util

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pytz
import os
import sys
import datetime
import csv
#from scipy.signal import medfilt
from scipy.ndimage import binary_dilation, binary_erosion

def P2P_interference_cleaning(FileName,CheminNewHacNetCDF,chemin_config,seuil_rsb,seuil_sv,l_parasite,l_erase,goto):

    """
        Lit un hac ou un dossier de RUN, effectue la detection et suppression de signaux parasites et enregistre un hac filtré
        entree:
            FileName: nom du repertoire ou fichier a traiter
            CheminNewHacNetCDF: repertoire où enregistrer les fichiers traites
            Chemin config: chemin du répertoire de config M3D. Un fichier hac par taille de chunk est enregistré
            seuil_rsb: (dB) seuil de détection d'un parasite par rapport au niveau environnant
            seuil_sv: (dB) seuil de détection d'un parasite en Sv
            l_parasite: (nb echantillons) longueur de parasite recherché
            l_erase: (nb echantillons) longueur d'effacement du parasite
    
    """

    if (0):
        # fichier de config et fichier hac + paramètres
        ###############################################
        chemin_config = "C:/Users/nlebouff/Desktop/Config_M3D/"  # dossier de config M3D avec config  Kernel Auto Length pour conserver en mémoire l'ensemble du fichier
        FileName = "G:/EVHOE2021/RUN004/"
        CheminNewHacNetCDF = "G:/EVHOE2021/filtre/RUN004/"  # dossier de réécriture
        
        # paramètres du filtre
        seuil_rsb=5    #dB seuil de détection de parasite (rsb)
        seuil_sv=-150; #dB seuil de détection de parasite (sv)
        l_parasite=4; #ech longueur minimum de parasite recherché
        l_erase=13; #ech longueur d'effacement du parasite détecté
        
        # goto éventuel
        goto=-1
        #goto = datetime.datetime(2022, 10, 9,8,42,4,tzinfo=datetime.timezone.utc).timestamp()
    
    if ((not os.path.isfile(FileName)) & (not os.path.isdir(FileName))):
            print("le fichier n existe pas")
            sys.exit()
    
    if not os.path.isdir(CheminNewHacNetCDF):
        os.makedirs(CheminNewHacNetCDF, exist_ok=True)
        
    mv.moLoadConfig(chemin_config)
    
    ParameterDef = mv.moLoadReaderParameter()
    #ParameterDef.m_ChunckDef.m_chunkNumberOfPingFan = 100
    taille_chunk = ParameterDef.m_ChunckDef.m_chunkNumberOfPingFan
    mv.moSaveReaderParameter(ParameterDef)
    
    # impose taille mémoire = taille chunk pour enregistrement du hac
    ParameterKernel = mv.moLoadKernelParameter()
    ParameterKernel.m_nbPingFanMax = taille_chunk+2
    ParameterKernel.m_bAutoLengthEnable = False
    #ParameterKernel.m_bIgnorePhaseData= True
    mv.moSaveKernelParameter(ParameterKernel)
    
    utc_tz = pytz.utc  # define UTC timezone
    
    mv.moOpenHac(FileName)
    
    # goto éventuel
    #################
    if goto > -1:
        util.hac_goto(goto)
    
    # presentation sondeurs
    #######################
    list_sounder = util.hac_sounder_descr()
    # choix sondeur à traiter
    index_sondeur = 0
    index_trans = 1
    
    if index_sondeur >= list_sounder.GetNbSounder():
        print("index sondeur hors limite")
        sys.exit()
    sounder = list_sounder.GetSounder(index_sondeur)
    if index_trans >= sounder.m_numberOfTransducer:
        print("index transducteur hors limite")
        sys.exit()
    transducer = sounder.GetTransducer(index_trans)
    
    dec_tir = int(4 * np.ceil(transducer.m_pulseDuration / transducer.m_timeSampleInterval))
    delta = transducer.m_beamsSamplesSpacing
    
    
    # lecture et traitement
    ######################
    FileStatus = mv.moGetFileStatus()
    
    ilec = 0
    pingHacId = -1
    iping=0
    
    #while not FileStatus.m_StreamClosed and ilec<3:
    while not FileStatus.m_StreamClosed:
        mv.moReadChunk()
        
        # force one more ping reading
        ParameterDef.m_ChunckDef.m_chunkNumberOfPingFan = 1
        mv.moSaveReaderParameter(ParameterDef)
        mv.moReadChunk()
        ParameterDef.m_ChunckDef.m_chunkNumberOfPingFan = taille_chunk
        mv.moSaveReaderParameter(ParameterDef)
        
        #check it was not void chunk reading (change of file)
        nb_pings = mv.moGetNumberOfPingFan()
        MX = mv.moGetPingFan(nb_pings-1)
        if MX.m_computePingFan.m_pingHacId>pingHacId:
            
            if iping>0:
                # remove pings from memory that were read in previous chunk (end of file), or strange goto
                MX = mv.moGetPingFan(0)
                while MX.m_computePingFan.m_pingHacId != pingHacId:
                    mv.moRemovePing(MX.m_computePingFan.m_pingId)
                    MX = mv.moGetPingFan(0)
                    
            MX_next = mv.moGetPingFan(0)
            print(pd.to_datetime(datetime.datetime.fromtimestamp(MX_next.m_meanTime.m_TimeCpu+MX_next.m_meanTime.m_TimeFraction/10000, utc_tz)))
        
            nb_pings = mv.moGetNumberOfPingFan()
        
            for index in range(min(iping,1), nb_pings): #first ping has been read previously
                MX = mv.moGetPingFan(index)
                SounderDesc = MX.m_pSounder
                if (SounderDesc.m_SounderId == sounder.m_SounderId) & (MX.m_computePingFan.m_pingHacId > pingHacId):
                    pingHacId = MX.m_computePingFan.m_pingHacId
                    polarMat = MX.GetPolarMatrix(index_trans)
                    sv_data = (np.array(polarMat.m_Amplitude) / 100.0).reshape(-1)
                    iping = iping + 1
                    print('ping ' + str(iping))
                    
                    #recalage en profondeur en fonction du pilonnement
                    heave=MX.beam_data[index_trans].navigationAttitude.sensorHeaveMeter; 
                    dec_heave=np.round(heave/delta)
                    if dec_heave<0:
                        svHeaveComp=np.append(np.zeros((int(-dec_heave),)),sv_data)
                    else:
                        svHeaveComp=sv_data[int(dec_heave):]
                        
            
                    #range mini (sinon déclenche sur l'écho de fond)
                    if MX.beam_data[index_trans].m_bottomWasFound:
                        nrange=np.floor(MX.beam_data[index_trans].m_bottomRange/delta-max(0,dec_heave));
                    else:
                        nrange=len(svHeaveComp);
                        
                    # #stockage des données ping précédent/courant/suivant
                    if (iping==1):
                        svHeaveComp_prev=svHeaveComp;  #données ping précédent
                        svHeaveComp_cur=svHeaveComp;  # données ping traité
                        svHeaveComp_next=svHeaveComp; #données ping suivant
                        MX_cur= MX;
                        MX_next= MX;
                        sv_data_cur=sv_data;
                        sv_data_next=sv_data;
                        dec_heave_cur=dec_heave;
                        dec_heave_prev=dec_heave;
                        dec_heave_next=dec_heave;
                        heave_cur=heave;
                        heave_next=heave;
                        nrange_cur=nrange;
                        nrange_prev=nrange;
                        nrange_next=nrange;
                    elif (iping==2): #cas particulier pour filtrer le premier ping (comparé deux fois au 2e)
                        svHeaveComp_prev=svHeaveComp;  #données ping précédent
                        svHeaveComp_cur=svHeaveComp_next;  # données ping traité
                        svHeaveComp_next=svHeaveComp; #données ping suivant
                        MX_cur= MX_next;
                        MX_next= MX;
                        sv_data_cur=sv_data_next;
                        sv_data_next=sv_data;
                        dec_heave_cur=dec_heave_next;
                        dec_heave_next=dec_heave;
                        dec_heave_prev=dec_heave;
                        heave_cur=heave_next;
                        heave_next=heave;
                        nrange_cur=nrange_next;
                        nrange_next=nrange;
                        nrange_prev=nrange;
                    else:
                        svHeaveComp_prev=svHeaveComp_cur;  #données ping précédent
                        svHeaveComp_cur=svHeaveComp_next;  # données ping traité
                        svHeaveComp_next=svHeaveComp; #données ping suivant
                        MX_cur= MX_next;
                        MX_next= MX;
                        sv_data_cur=sv_data_next;
                        sv_data_next=sv_data;
                        dec_heave_prev=dec_heave_cur;
                        dec_heave_cur=dec_heave_next;
                        dec_heave_next=dec_heave;
                        heave_cur=heave_next;
                        heave_next=heave;
                        nrange_prev=nrange_cur;
                        nrange_cur=nrange_next;
                        nrange_next=nrange;
        
                    #on met tout le monde à la même taille
                    nech_min=int(min([nrange_prev,nrange_cur,nrange_next]));
                    svCom_prev=svHeaveComp_prev[:nech_min];    
                    svCom_cur=svHeaveComp_cur[:nech_min];  # données de travail
                    svCom_next=svHeaveComp_next[:nech_min];    
                           
                    #filtrage lui-meme
                    dif_prev=svCom_cur-svCom_prev;
                    dif_next=svCom_cur-svCom_next;
                    dif_max=np.where(dif_prev<dif_next,dif_prev,dif_next)
                    is_det=((dif_max>seuil_rsb) & (svCom_cur>seuil_sv)); #matrice des detections potentielles (echantillons supérieurs de seuil_rsb à leurs voisins)
                    is_det2=binary_erosion(is_det,structure=np.ones((l_parasite,))); # on garde les detections de longueur au moins egale à l_parasite
                    is_det3=binary_dilation(is_det2,structure=np.ones((l_erase,))); # on efface le parasite sur une longueur l_erase
                    ind_det=np.where(is_det3);
                    svCom_cur[ind_det]=10*np.log10((10**(svCom_prev[ind_det]/10)+10**(svCom_next[ind_det]/10))/2); # on remplace les parasites par la moyenne des ech avant/apres
                    
                    #décompensation du pilonnement
                    if dec_heave<0:
                        sv_new=svCom_cur[int(-dec_heave):]
                    else:
                        sv_new=np.append(np.zeros((int(dec_heave),)),svCom_cur)
        
                    polarMat = MX_cur.GetPolarMatrix(index_trans)
                    sv_data_new=(np.array(polarMat.m_Amplitude) / 100.0).reshape(-1) #données finales, relecture car pfs chgt taille
                    nd=min(len(sv_data_new),len(sv_new))
                    sv_data_new[0:nd]=sv_new[0:nd]
                    
                    # changement données mémoire
                    for i,svi in enumerate(sv_data_new):
                        polarMat.m_Amplitude[i][0]=int(svi*100)
                    MX_cur.SetPolarMatrix(index_trans,polarMat);
                
            
            goto_next=MX_next.m_meanTime.m_TimeCpu+MX_next.m_meanTime.m_TimeFraction/10000
            pingHacId = MX_next.m_computePingFan.m_pingHacId
            # retire le dernier ping pour ne pas l'écrire (pas filtré)    
            mv.moRemovePing(MX_next.m_computePingFan.m_pingId);
            
            # écriture HAC
            ##############
            num='0000'
            strilec=str(ilec)
            if mv.moGetNumberOfPingFan()>1:
                mv.moStartWrite(CheminNewHacNetCDF, num[0:-len(strilec)]+strilec + "_filtre_"  ,mv.WriteOutputType.Hac) #WriteOutputType.[Hac, NetCDF, Both]
                mv.moStopWrite();
            else:
                print('perte d un ping')
            #va au dernier ping (qui a été enlevé)
            mv.moGoTo(goto_next);
            
            
            ilec = ilec + 1
            
