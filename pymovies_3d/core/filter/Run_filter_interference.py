# -*- coding: utf-8 -*-
"""
Created on Fri Dec 16 12:04:52 2022

@author: nlebouff
"""

# imports
#########
import pyMovies as mv

import pymovies_3d.core.hac_util.hac_util as util
import pymovies_3d.core.filter.P2P_interference_cleaning as hac_filter

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pytz
import os
import sys
import datetime

"""
    Lit un hac ou un dossier de RUN, effectue la detection et suppression de signaux parasites et enregistre un hac filtré
    entree:
        FileName: nom du repertoire ou fichier a traiter
        CheminNewHacNetCDF: repertoire où enregistrer les fichiers traites
        Chemin config: chemin du répertoire de config M3D. Un fichier hac par taille de chunk est enregistré
        seuil_rsb: (dB) seuil de détection d'un parasite par rapport au niveau environnant
        seuil_sv: (dB) seuil de détection d'un parasite en Sv
        l_parasite: (nb echantillons) longueur de parasite recherché
        l_erase: (nb echantillons) longueur d'effacement du parasite
"""

####################################################################################
# fichier de config et fichier hac + paramètres
####################################################################################
chemin_config = "./Config_M3D_Filtre_MESO/"  # dossier de config M3D 
CheminHac = "G:/EVHOE2021/RUN005/"
CheminNewHac = "G:/EVHOE2021/filtre/RUN005/"  # dossier de réécriture

# paramètres du filtre
seuil_rsb=5    #dB seuil de détection de parasite (rsb)
seuil_sv=-150; #dB seuil de détection de parasite (sv)
l_parasite=4; #ech longueur minimum de parasite recherché
l_erase=13; #ech longueur d'effacement du parasite détecté

# choix transducteur à traiter
index_sondeur = 0
index_trans = 1

# goto éventuel
goto=-1
#goto = datetime.datetime(2022, 10, 9,8,42,4,tzinfo=datetime.timezone.utc).timestamp()

######################################################################################
######################################################################################


# verification fichiers
#########################
if ((not os.path.isfile(CheminHac)) & (not os.path.isdir(CheminHac))):
        print("le fichier n existe pas")
        sys.exit()

if not os.path.isdir(CheminNewHac):
    os.makedirs(CheminNewHac, exist_ok=True)
    
#execution
hac_filter.P2P_interference_cleaning(CheminHac,CheminNewHac,chemin_config,seuil_rsb,seuil_sv,l_parasite,l_erase,goto)
