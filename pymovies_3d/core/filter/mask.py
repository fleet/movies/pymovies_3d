# -*- coding: utf-8 -*-
"""
Created on Fri May 21 17:03:55 2021

@author: lberger

 Lit un hac ou un dossier de RUN, masque la fréquence X par les échos au dessus d'un certain seuil sur la fréquence Y
    Enregistre des fichiers hac avec les échos corrigés
    
    Renseigner en début de code :
        le numéro de run ou le nom du fichier à traiter (choisir le bloc à activer L31)
        les chemins des dossiers des fichiers et de sauvegarde
        le chemin de la configuration M3D 
"""

# imports
#########
from pymovies import pyMovies as mv

import pymovies_3d.core.hac_util.hac_util as util

import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import datetime
import csv

# fichier de config et fichier hac + paramètres
###############################################
if 0:  # traitement d'un RUN
    chemin_ini = "C:/Users/lberger/Desktop/PELGAS21"  # dossier contenant les dossiers de RUN
    run = str(1)  # numéro de run à traiter
    chemin_config = chemin_ini  # dossier de config M3D

    num_run = "000" + run
    num_run = num_run[-3:]
    name_sav = "RUN" + num_run
    FileName = chemin_ini + "/RUN" + num_run + "\\"
    CheminNewHac = chemin_ini + "/MASK/RUN" + num_run + "\\"

    if not os.path.isdir(FileName):
        print("le dossier n existe pas")
        sys.exit()
        
    display=0

else:  # traitement d'un hac
    chemin_ini = "C:/Users/lberger/Desktop/PELGAS21"  # dossier contenant le hac
    chemin_config = chemin_ini  # dossier de config M3D
    Hac_name = "PELGAS2021_025_20210520_155631.hac"

    name_sav = Hac_name[-4:]
    FileName = chemin_ini + "/" + Hac_name
    CheminNewHac = chemin_ini + "/MASK/"

    if not os.path.isfile(FileName):
        print("le fichier n existe pas")
        sys.exit()
        
    display=1


# goto = datetime.datetime(2019,9,11,13,11,33,tzinfo=datetime.timezone.utc).timestamp()
goto = -1


# ouverture hac et config
#########################


mv.moLoadConfig(chemin_config)
mv.moOpenHac(FileName)

# presentation sondeurs
#######################
list_sounder = util.hac_sounder_descr()
# choix sondeur à traiter
index_sondeur = 0
index_transMak = 0
index_transMaked = 1

if index_sondeur >= list_sounder.GetNbSounder():
    print("index sondeur hors limite")
    sys.exit()
sounder = list_sounder.GetSounder(index_sondeur)
if index_transMak >= sounder.m_numberOfTransducer:
    print("index transducteur hors limite")
    sys.exit()
if index_transMaked >= sounder.m_numberOfTransducer:
    print("index transducteur hors limite")
    sys.exit()


ParameterDef = mv.moLoadReaderParameter()
ParameterDef.m_ChunckDef.m_chunkNumberOfPingFan = 2000
taille_chunk = ParameterDef.m_ChunckDef.m_chunkNumberOfPingFan
mv.moSaveReaderParameter(ParameterDef)

# impose taille mémoire = taille chunk pour enregistrement du hac
ParameterKernel = mv.moLoadKernelParameter()
ParameterKernel.m_nbPingFanMax = taille_chunk
ParameterKernel.m_bAutoLengthEnable = False
mv.moSaveKernelParameter(ParameterKernel)


# goto éventuel
#################
if goto > -1:
    mv.hac_goto(goto)

# lecture et traitement
######################
FileStatus = mv.moGetFileStatus()

pingId = -1
if display:
    sv_unmasked=[]
    sv_masked=[]
    sv_unmasked.append([])
    sv_masked.append([])
    

while not FileStatus.m_StreamClosed:
    mv.moReadChunk()
    
    nb_pings = mv.moGetNumberOfPingFan()

    for index in range(nb_pings):
        MX = mv.moGetPingFan(index)
        SounderDesc = MX.m_pSounder
        if (SounderDesc.m_SounderId == sounder.m_SounderId) & (MX.m_computePingFan.m_pingId > pingId):
            pingId = MX.m_computePingFan.m_pingId
            polarMatMask = MX.GetPolarMatrix(index_transMak)
            sv_data_mask = (np.array(polarMatMask.m_Amplitude) / 100.0).reshape(-1)
            # # lissage sur 4 échantillons et 2 pings
            # sv_data_smooth = np.correlate(sv_data_mask, np.ones(4) / 4, "same")
            # sv_data_smooth=sv_data_mask #temp
            # iping = iping + 1
            # index_pg[iping - iping0] = index
            # if iping == 0:
            #     sv_data_prec = sv_data_smooth
            # sv_data = (sv_data_smooth + sv_data_prec) / 2
            # sv_data_prec = sv_data_smooth
            
            u = np.asarray(np.where(sv_data_mask < -60))  # indices des éléments <-60
            
            polarMatMasked = MX.GetPolarMatrix(index_transMaked)
            
            if display:
                sv_data = (np.array(polarMatMasked.m_Amplitude) / 100.0).reshape(-1)
                sv_data_masked = sv_data.copy()
                sv_data_masked[u]=-32768
                
             
            nb_ech = np.size(sv_data_mask, axis=0)
            for i in range(nb_ech):
                if i in u:
                    polarMatMasked.m_Amplitude[i]=[-15000]

            MX.SetPolarMatrix(index_transMaked,polarMatMasked)           

            if display:
                sv_unmasked[-1].append(sv_data)
                sv_masked[-1].append(sv_data_masked)

    
    
    # écriture HAC
    ##############
    mv.moStartWrite(
        CheminNewHac,"MASK"
    )
    mv.moStopWrite()

    # Affichage éventuel
    ####################
    if display:
        sv_unmasked=np.squeeze(np.asarray(sv_unmasked))
        sv_masked=np.squeeze(np.asarray(sv_masked))
        plt.figure(1, figsize=(30, 15))
        plt.clf()
        plt.tight_layout()
        ax1 = plt.subplot(2, 1, 1)
        ax1.imshow(sv_unmasked.T, vmin=-80, vmax=10, aspect="auto", cmap="jet")
        ax2 = plt.subplot(2, 1, 2)
        ax2.imshow(sv_masked.T, vmin=-80, vmax=10, aspect="auto", cmap="jet")