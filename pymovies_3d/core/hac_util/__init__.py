# -*- coding: utf-8 -*-

"""Package for hac standard tools."""

__author__ = """Naig Le Bouffant"""
__email__ = "naig.le.bouffant@ifremer.fr"
__version__ = "0.1.0"
