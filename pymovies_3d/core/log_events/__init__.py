# -*- coding: utf-8 -*-

"""Package for log and events standard tools."""

__author__ = """Guillaume Brosse"""
__email__ = "guillaume.brosse@ifremer.fr"
__version__ = "0.1.0"
