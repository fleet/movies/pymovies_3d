# -*- coding: utf-8 -*-
"""
Created on Thu Mar  5 14:00:44 2020

@author: Guillaume Brosse
"""

import numpy as np

# import plotly.express as px
import pandas as pd


####################################################################################################
#
# import_casino pour importer les données des fichiers casinos
# La fonction appelle le chemin ou est stocké le fichier Casino et renvoie :
#     - C (tableau de chaines de caractères): l'ensemble des données dans le fichier Casino
#     - Entete (tableau de chaines de caractères) : l'ensemble des entêtes de chaque colonnes
#     - index_events (int): les index de tous les évenements autres que ACQAUTO
#     - date_heure : La date et l'heure au format np.datetime64
#
####################################################################################################


def import_casino(path_evt):
    C = np.loadtxt(path_evt,dtype='U',delimiter = '\t' ,encoding='latin1')
    # C0 = pd.read_table(path_evt)
    # C = C0.to_numpy(dtype="str")
    Entete = C[0, :]
    C = C[1 : (len(C)), :]
    index_events = np.squeeze(np.where(C[:, 6] != "ACQAUTO"))
    date_heure = np.array(
        (pd.to_datetime((np.char.add(C[:, 0], C[:, 1])), format="%d/%m/%Y%H:%M:%S")),
        dtype=np.datetime64,
    )

    return C, Entete, index_events, date_heure

def import_casino_echosonde_bis(path_evt):
    C = np.loadtxt(path_evt,dtype='U',delimiter = '\t' ,encoding='latin1')
    # C0 = pd.read_table(path_evt)
    # C = C0.to_numpy(dtype="str")
    Entete = C[0, :]
    C = C[1 : (len(C)), :]
    index_events = np.squeeze(np.where(C[:, 2] != "ACQAUTO"))
    date_heure = np.array(
        (pd.to_datetime((np.char.add(C[:, 0], C[:, 1])), format="%d/%m/%Y%H:%M:%S")),
        dtype=np.datetime64,
    )

    return C, Entete, index_events, date_heure

def import_jackpot(path_evt):
    C = np.loadtxt(path_evt,dtype='U',delimiter = ';' ,encoding='latin1')
    # C0 = pd.read_table(path_evt)
    # C = C0.to_numpy(dtype="str")
    Entete = C[0, :]
    C = C[1 : (len(C)), :]
    date_heure = np.array(
        (pd.to_datetime((np.char.add(C[:, 0], C[:, 2])), format="%d/%m/%Y%H:%M:%S")),
        dtype=np.datetime64,
    )

    return C, Entete, date_heure
    
def import_casino_bio(path_evt): #sert a lire le fichier casino: PHOENIX2018_casino_evts_V2_bis.txt
    
    #C = np.loadtxt(path_evt,dtype='U',delimiter = ';')
    C = np.loadtxt(path_evt,dtype='U',delimiter = ',' ,encoding='latin1')
    #C = np.loadtxt(path_evt,dtype='U',delimiter = '\t' ,encoding='latin1')
    #C0=pd.read_table(path_evt)
    #C=C0.to_numpy(dtype='str')

    Entete =C[0,:]    
    C = C[1:(len(C)),:]
    index_events =  np.squeeze(np.where(C[:,6]!='OPE'))
    date_heure  = np.array((pd.to_datetime((np.char.add(C[:,0],C[:,1])), format="%d/%m/%Y%H:%M:%S")),dtype = np.datetime64)
  
    return C,Entete,index_events,date_heure  

    
def import_casino_TUBE(path_evt): #sert a lire le fichier casino: PHOENIX2018_casino_plongees_tube.txt
    
    #C = np.loadtxt(path_evt,dtype='U',delimiter = ';')
    C = np.loadtxt(path_evt,dtype='U',delimiter = ',' ,encoding='utf-8')
    #C = np.loadtxt(path_evt,dtype='U',delimiter = '\t' ,encoding='latin1')
    #C0=pd.read_table(path_evt)
    #C=C0.to_numpy(dtype='str')

    Entete =C[0,:]    
    C = C[1:(len(C)),:]
    index_events =  np.squeeze(np.where(C[:,8]!='OPE'))
    date_heure  = np.array((pd.to_datetime((np.char.add(C[:,0],C[:,1])), format='%d/%m/%Y%H:%M:%S')),dtype = np.datetime64)
  
    return C,Entete,index_events,date_heure


####################################################################################################
#
# Fonctions usuelles deux_de_suite_inferieur et deux_de_suite_superieur renvoyant repectivement
# une liste en enlevant les indices lorsqu'ils sont à la suite (fonctions utiles pour
# heures_dates_radiales_str )
#
####################################################################################################


def deux_de_suite_inferieur(liste):
    b_liste = []
    if np.size(liste)>1:
        range_liste = range(len(liste))
        a_prec = liste[0]
        for i in range_liste:
            a_courant = liste[i]
            if a_prec == a_courant:
                b_liste.append(a_courant)
            elif a_prec + 1 < a_courant:
                b_liste.append(a_courant)
            a_prec = a_courant
    else:
        b_liste=liste
                
    return b_liste


def deux_de_suite_superieur(liste):
    b_liste = []
    if np.size(liste)>1:
        range_liste = range(len(liste))
        a_prec = liste[len(liste) - 1]
        for i in range_liste:
            a_courant = liste[len(liste) - 1 - i]
            if a_prec == a_courant:
                b_liste.append(a_courant)
            elif a_prec - 1 > a_courant:
                b_liste.append(a_courant)
            a_prec = a_courant
        b_liste.reverse()
    else:
        b_liste=liste
            
    return b_liste


####################################################################################################
#
# heures_dates_radiales_str est une fonction permettant de trouver les heures de début et de fin
# de radiale dans unfifichier casino avec les codes DEBURAD,REPRAD,FINRAD,STOPRAD
# La fonction retourne quatre tableaux de chaines de caractères contenant respectivement les dates de
# début et de fin ainsi que les heures de début et de fin de chaques radiales
#
####################################################################################################

#pylint:disable=consider-using-enumerate
def infos_radiales(C):
    index_heure = np.squeeze(
        np.where(
            (
                (C[:, 10] == "STOPRAD")
                | (C[:, 10] == "FINRAD")
                | (C[:, 10] == "REPRAD")
                | (C[:, 10] == "DEBURAD")
            )
        )
    )
    C_hd = C[index_heure, :]

    index_heure_fin = np.squeeze(
        np.where(((C_hd[:, 10] == "STOPRAD") | (C_hd[:, 10] == "FINRAD")))
    )
    index_heure_debut = np.squeeze(
        np.where(((C_hd[:, 10] == "REPRAD") | (C_hd[:, 10] == "DEBURAD")))
    )
    index_heure_fin = deux_de_suite_inferieur(index_heure_fin)
    index_heure_debut = deux_de_suite_superieur(index_heure_debut)
    dates_debut = (C_hd[index_heure_debut, 0])
    dates_fin = (C_hd[index_heure_fin, 0])
    heure_debut = (C_hd[index_heure_debut, 1])
    heure_fin = (C_hd[index_heure_fin, 1])

    if len(dates_debut) > len(dates_fin):
        dates_fin.append(C[(len(C) - 1), 0])
        heure_fin.append(C[(len(C) - 1), 1])
        index_heure_fin.append((len(C) - 1))
        
    index_heure_debut_d = np.squeeze(
        np.where((C_hd[:, 10] == "DEBURAD")))
    index_heure_debut_r = np.squeeze(
        np.where((C_hd[:, 10] == "REPRAD")))
 
    name_rad=[]
    for i in range(len(index_heure_debut)):
        if np.isin(index_heure_debut[i],index_heure_debut_d):
            name_rad.append(C_hd[index_heure_debut[i], 11])
        else:
            name_rad.append(C_hd[index_heure_debut[i], 11])
    
    for i in range(len(name_rad)):
        name_rad[i] = name_rad[i].replace(" ", "")
        
    vit_vent_vrai = []
    dir_vent_vrai = []
    for x in range(len(index_heure_debut)):
        vit_str = (C_hd[index_heure_debut[x] : index_heure_fin[x], 65])
        dir_str = (C_hd[index_heure_debut[x] : index_heure_fin[x], 66])
        for i in range(len(vit_str)):
            vit_str[i] = float(vit_str[i].replace(",", "."))
            dir_str[i] = float(dir_str[i].replace(",", "."))
        vit_vent_vrai.append(np.mean(vit_str))
        dir_vent_vrai.append(np.mean(dir_str))
    # vit_vent_vrai = (C_hd[index_heure_debut,65])
    # dir_vent_vrai = (C_hd[index_heure_debut,66])
   

    return (
        dates_debut,
        dates_fin,
        heure_debut,
        heure_fin,
        name_rad,
        vit_vent_vrai,
        dir_vent_vrai,
    )

def infos_radiales_jackpot(C):
    index_heure = np.squeeze(
        np.where(
            (
                (C[:, 6] == "STOPRAD")
                | (C[:, 6] == "FINRAD")
                | (C[:, 6] == "REPRAD")
                | (C[:, 6] == "DEBURAD")
            )
        )
    )
    C_hd = C[index_heure, :]

    index_heure_fin = np.squeeze(
        np.where(((C_hd[:, 6] == "STOPRAD") | (C_hd[:, 6] == "FINRAD")))
    )
    index_heure_debut = np.squeeze(
        np.where(((C_hd[:, 6] == "REPRAD") | (C_hd[:, 6] == "DEBURAD")))
    )
    index_heure_fin = deux_de_suite_inferieur(index_heure_fin)
    index_heure_debut = deux_de_suite_superieur(index_heure_debut)
    dates_debut = (C_hd[index_heure_debut, 0])
    dates_fin = (C_hd[index_heure_fin, 0])
    heure_debut = (C_hd[index_heure_debut, 2])
    heure_fin = (C_hd[index_heure_fin, 2])

    if len(dates_debut) > len(dates_fin):
        dates_fin.append(C[(len(C) - 1), 0])
        heure_fin.append(C[(len(C) - 1), 2])
        index_heure_fin.append((len(C) - 1))

    index_heure_debut_d = np.squeeze(
        np.where((C_hd[:, 6] == "DEBURAD")))
    index_heure_debut_r = np.squeeze(
        np.where((C_hd[:, 6] == "REPRAD")))
    
    name_rad=[]
    for i in range(len(index_heure_debut)):
        if np.isin(index_heure_debut[i],index_heure_debut_d):
            name_rad.append(C_hd[index_heure_debut[i], 12])
        else:
            name_rad.append(C_hd[index_heure_debut[i], 12])
    
    for i in range(len(name_rad)):
        name_rad[i] = name_rad[i].replace(" ", "")
        # vit_vent_vrai[i] = float(vit_vent_vrai[i].replace(',','.'))
        # dir_vent_vrai[i] = float(dir_vent_vrai[i].replace(',','.'))

    return (
        dates_debut,
        dates_fin,
        heure_debut,
        heure_fin,
        name_rad,
    )


def infos_radiales_echosonde(C): #fct infos radiales pour les campagnes echosonde
    index_heure  = np.squeeze(np.where(((C[:,3] == 'STOPRAD')| (C[:,3] == 'FINRAD')|(C[:,3] == 'REPRAD')| (C[:,3] == 'DEBRAD'))))
    C_hd = C[index_heure,:]
    
    
    index_heure_fin  = np.squeeze(np.where(((C_hd[:,3] == 'STOPRAD')| (C_hd[:,3] == 'FINRAD'))))
    index_heure_debut  = np.squeeze(np.where(((C_hd[:,3] == 'REPRAD')| (C_hd[:,3] == 'DEBRAD'))))
    index_heure_fin = deux_de_suite_inferieur(index_heure_fin)
    index_heure_debut = deux_de_suite_superieur(index_heure_debut)
    dates_debut = (C_hd[index_heure_debut,0])
    dates_fin = (C_hd[index_heure_fin,0])
    heure_debut = (C_hd[index_heure_debut,1]) 
    heure_fin  = (C_hd[index_heure_fin,1]) 
    
    
    if len(dates_debut)>len(dates_fin):
        dates_fin.append(C[(len(C)-1),0])
        heure_fin.append(C[(len(C)-1),1])
        index_heure_fin.append((len(C)-1))
        
    name_rad = (C_hd[index_heure_debut,2]) 
    vit_vent_vrai = []
    dir_vent_vrai = []
    for x in  range(len(index_heure_debut)):
        #correspond pas car casino manuel et pas automatique
        if C.shape[1]>7:
            vit_str = (C_hd[index_heure_debut[x]:index_heure_fin[x],7])
            if len(vit_str)>1:
                for i in range (len(vit_str)):
                    vit_str[i] = float(vit_str[i].replace(',','.'))
                vit_vent_vrai.append(np.mean(vit_str))
            else:
                pass
        else:
            pass
        if C.shape[1]>8:
            dir_str =(C_hd[index_heure_debut[x]:index_heure_fin[x],8])
            if len(dir_str)>1:
                for i in range (len(dir_str)):
                    dir_str[i] = float(dir_str[i].replace(',','.'))
                dir_vent_vrai.append(np.mean(dir_str))
            else:
                pass
    # if len(vit_str)>1:
    #     for i in range (len(vit_str)):
    #         vit_str[i] = float(vit_str[i].replace(',','.'))
    #         dir_str[i] = float(dir_str[i].replace(',','.'))
    #     vit_vent_vrai.append(np.mean(vit_str))
    #     dir_vent_vrai.append(np.mean(dir_str))
        else:
            pass
    # vit_vent_vrai = (C_hd[index_heure_debut,65]) 
    # dir_vent_vrai = (C_hd[index_heure_debut,66]) 
    for i in range (len(name_rad)):
        name_rad[i] = name_rad[i]+str(i)
        name_rad[i] = name_rad[i].replace(' ','')
        # vit_vent_vrai[i] = float(vit_vent_vrai[i].replace(',','.'))
        # dir_vent_vrai[i] = float(dir_vent_vrai[i].replace(',','.'))
                                          
    return dates_debut,dates_fin,heure_debut,heure_fin,name_rad,vit_vent_vrai,dir_vent_vrai


def infos_radiales_echosonde_bis(C): #fct infos radiales pour les campagnes echosonde bis
    index_heure  = np.squeeze(np.where(((C[:,3] == 'STOPRAD')| (C[:,3] == 'FINRAD')|(C[:,3] == 'REPRAD')| (C[:,3] == 'DEBRAD'))))
    C_hd = C[index_heure,:]
    
    
    index_heure_fin  = np.squeeze(np.where(((C_hd[:,3] == 'STOPRAD')| (C_hd[:,3] == 'FINRAD'))))
    index_heure_debut  = np.squeeze(np.where(((C_hd[:,3] == 'REPRAD')| (C_hd[:,3] == 'DEBRAD'))))
    index_heure_fin = deux_de_suite_inferieur(index_heure_fin)
    index_heure_debut = deux_de_suite_superieur(index_heure_debut)
    dates_debut = (C_hd[index_heure_debut,0])
    dates_fin = (C_hd[index_heure_fin,0])
    heure_debut = (C_hd[index_heure_debut,1]) 
    heure_fin  = (C_hd[index_heure_fin,1]) 
    
    
    if len(dates_debut)>len(dates_fin):
        dates_fin.append(C[(len(C)-1),0])
        heure_fin.append(C[(len(C)-1),1])
        index_heure_fin.append((len(C)-1))
        
    name_rad = (C_hd[index_heure_debut,4]) 
                                          
    return dates_debut,dates_fin,heure_debut,heure_fin,name_rad
    
    
def infos_radiales_bio(C):  #fct infos radiales pour le fichier casino: PHOENIX2018_casino_evts_V2_bis.txt
    index_heure  = np.squeeze(np.where(((C[:,12] == 'FVIR')| (C[:,12] == 'FINVIR')| (C[:,12] == 'Finvir')| (C[:,12] == 'CULAB')|(C[:,12] == 'DFIL')| (C[:,12] == 'DEBFIL')| (C[:,12] == 'Debvir')| (C[:,12] == 'HQ')| (C[:,12] == 'SURF'))))
    C_hd = C[index_heure,:]
    
    
    index_heure_fin  = np.squeeze(np.where(((C_hd[:,12] == 'FVIR')| (C_hd[:,12] == 'FINVIR')| (C_hd[:,12] == 'Finvir')| (C_hd[:,12] == 'CULAB'))))
    index_heure_debut  = np.squeeze(np.where(((C_hd[:,12] == 'DFIL')| (C_hd[:,12] == 'DEBFIL')| (C_hd[:,12] == 'Debvir')| (C_hd[:,12] == 'HQ')| (C_hd[:,12] == 'SURF'))))
    # index_heure_fin = deux_de_suite_inferieur(index_heure_fin)
    # index_heure_debut = deux_de_suite_superieur(index_heure_debut)
    dates_debut = (C_hd[index_heure_debut,0])
    dates_fin = (C_hd[index_heure_fin,0])
    heure_debut = (C_hd[index_heure_debut,1]) 
    heure_fin  = (C_hd[index_heure_fin,1]) 
    
    
    # if (len(dates_debut)>len(dates_fin)):
    #     dates_fin.append(C[(len(C)-1),0])
    #     heure_fin.append(C[(len(C)-1),1])
    #     index_heure_fin.append((len(C)-1))
        
    name_station = (C_hd[index_heure_debut,13]) 
    vit_vent_vrai = []
    dir_vent_vrai = []
    for x in  range(len(index_heure_debut)):
        vit_str = (C_hd[index_heure_debut[x]:index_heure_fin[x],65])
        dir_str =(C_hd[index_heure_debut[x]:index_heure_fin[x],66])
        for i in range (len(vit_str)):
            vit_str[i] = float(vit_str[i].replace(',','.'))
            dir_str[i] = float(dir_str[i].replace(',','.'))
        vit_vent_vrai.append(np.mean(vit_str))
        dir_vent_vrai.append(np.mean(dir_str))
    # vit_vent_vrai = (C_hd[index_heure_debut,65]) 
    # dir_vent_vrai = (C_hd[index_heure_debut,66]) 
    for i in range (len(name_station)):
        name_station[i] = name_station[i].replace(' ','')
        # vit_vent_vrai[i] = float(vit_vent_vrai[i].replace(',','.'))
        # dir_vent_vrai[i] = float(dir_vent_vrai[i].replace(',','.'))
                                          
    return dates_debut,dates_fin,heure_debut,heure_fin,name_station,vit_vent_vrai,dir_vent_vrai

def infos_radiales_TUBE(C):  #fct infos radiales pour le fichier casino: PHOENIX2018_casino_evts_V2_bis.txt
    index_heure  = np.squeeze((np.where((C[:,16] == 'FINPLONG') | (C[:,16]== 'DEBPLONG'))))
    C_hd = C[index_heure,:]
    
    
    index_heure_fin  = np.squeeze(np.where(((C_hd[:,16] == 'FINPLONG'))))
    index_heure_debut  = np.squeeze(np.where(((C_hd[:,16] == 'DEBPLONG'))))
    # index_heure_fin = deux_de_suite_inferieur(index_heure_fin)
    # index_heure_debut = deux_de_suite_superieur(index_heure_debut)
    dates_debut = (C_hd[index_heure_debut,0])
    dates_fin = (C_hd[index_heure_fin,0])
    heure_debut = (C_hd[index_heure_debut,1]) 
    heure_fin  = (C_hd[index_heure_fin,1]) 
    
    
    # if (len(dates_debut)>len(dates_fin)):
    #     dates_fin.append(C[(len(C)-1),0])
    #     heure_fin.append(C[(len(C)-1),1])
    #     index_heure_fin.append((len(C)-1))
        
    name_station = (C_hd[index_heure_debut,13]) 
    vit_vent_vrai = []
    dir_vent_vrai = []
    for x in  range(len(index_heure_debut)):
        vit_str = (C_hd[index_heure_debut[x]:index_heure_fin[x],66])
        dir_str =(C_hd[index_heure_debut[x]:index_heure_fin[x],67])
        for i in range (len(vit_str)):
            vit_str[i] = float(vit_str[i].replace(',','.'))
            dir_str[i] = float(dir_str[i].replace(',','.'))
        vit_vent_vrai.append(np.mean(vit_str))
        dir_vent_vrai.append(np.mean(dir_str))
    # vit_vent_vrai = (C_hd[index_heure_debut,65]) 
    # dir_vent_vrai = (C_hd[index_heure_debut,66]) 
        # vit_vent_vrai[i] = float(vit_vent_vrai[i].replace(',','.'))
        # dir_vent_vrai[i] = float(dir_vent_vrai[i].replace(',','.'))
                                          
    return dates_debut,dates_fin,heure_debut,heure_fin,name_station,vit_vent_vrai,dir_vent_vrai
