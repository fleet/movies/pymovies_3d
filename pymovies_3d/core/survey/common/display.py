from pathlib import Path
from typing import Iterable, List
import numpy as np
import os
import matplotlib 


import matplotlib.colors as mpl_colors
import matplotlib.pyplot as plt
import xarray as xr

from pymovies_3d.core.survey.common.labels import ClassesPelgas2023
from pymovies_3d.core.survey.preprocessing.ei_file import XrStructure as x

matplotlib.use('Agg')


def save_figure(
    dataset: xr.Dataset, filename: Path, frequencies=None, show_classes=False
):
    """
    Plot and save figure for single threshold ei files
    """

    if "integrated_bs" in dataset.variables:
        fig = _plot_ei_single_threshold(
            dataset=dataset,
            show=False,
            frequencies=frequencies,
            show_classes=show_classes,
        )
    else:
        fig = _plot_ei_multi_threshold(
            dataset=dataset,
            show=False,
            frequencies=frequencies,
            show_classes=show_classes,
        )

    plt.savefig(str(filename), dpi=220)
    plt.close(fig)


def plot_ei( dataset: xr.Dataset, show=False, block=False, frequencies=None, show_classes=False):
    if "integrated_bs" in dataset.variables:
        fig = _plot_ei_single_threshold(
            dataset=dataset,
            show=show,
            block=block,
            frequencies=frequencies,
            show_classes=show_classes,
        )
    else:
        fig = _plot_ei_multi_threshold(
            dataset=dataset,
            show=show,
            block=block,
            frequencies=frequencies,
            show_classes=show_classes,
        )
    return fig

def _plot_ei_single_threshold(
    dataset: xr.Dataset, show=False, block=False, frequencies=None, show_classes=False
):
    return _plot_ei(
        dataset, show, block, frequencies, show_classes, variables=["integrated_bs"]
    )


def _plot_ei_multi_threshold(
    dataset: xr.Dataset, show=False, block=False, frequencies=None, show_classes=False
):
    return _plot_ei(
        dataset, show, block, frequencies, show_classes, variables=["bs_60", "bs_80"]
    )


def _plot_ei(
    dataset: xr.Dataset, show, block, frequencies, show_classes, variables: List[str]
):
    """
    Display dataset content for single threshold EI files
    """
    if frequencies is None:
        subplot_count = dataset.dims[x.frequency]
        frequencies = [float(fq) for fq in dataset.frequency]
    else:
        subplot_count = len(frequencies)

    subplot_count *= len(
        variables
    )  # take into account for  all the threshold we are displaying
    if show_classes:
        subplot_count += 1  # add 1 for category labels

    fig, axes = plt.subplots(nrows=subplot_count, ncols=1)
    if subplot_count == 1:
        axes = [axes]
    axes_iterator = iter(axes)
    for v in variables:
        for freq in frequencies:
            ax = next(axes_iterator)
            dataset[v].sel(frequency=freq).plot(
                ax=ax, cmap=get_ei_cmap(), add_colorbar=True
            )
            ax.invert_yaxis()
            ax.set_title(f"{v} : Frequency {float(freq/1000)} kHz")
            ax.get_xaxis().set_visible(False)

    # plot classes
    if show_classes:
        ax = axes.flatten()[-1]
        cmin, cmax = ClassesPelgas2023.get_minmax()
        obj = dataset.classes.plot(ax=ax, cmap=get_class_cmap(), add_colorbar=True)
        obj.set_clim(vmin=cmin, vmax=cmax)
        # ax.legend(labels=labels,loc='lower left')
        # display legend
        ax.invert_yaxis()

    if show:
        plt.show(block=block)
    return fig


def get_class_cmap():
    cmap = mpl_colors.ListedColormap(
        list(
            [
                "#FF7F00",
                "#FFFF00",
                "#00FFA9",
                "#00D4FF",
                "#B2EBFF",
                "#2A00FF",
                "#AA00FF",
                "#FF00D4",
                "#FF007F",
                "#B8FF8C",
                "#339966",
                "#FF7C80",
                "#CC6600",
                "#33CCCC",
                "#C4C4C4",
                "#000000",
            ]
        )
    )
    minc,maxc =  ClassesPelgas2023.get_minmax()
    if maxc > cmap.N:
        raise Exception("Mismatch between class colors values and classes enumerate")
    return cmap


def get_ei_cmap():
    """Return a movies like colormap"""
    # Define the ListedColormap with the desired color pattern
    cmap = mpl_colors.ListedColormap(
        list(
            reversed(
                [
                    "#000000",
                    "#800000",
                    "#ac0000",
                    "#d00000",
                    "#f00000",
                    "#ff0000",
                    "#ff7979",
                    "#ff8040",
                    "#ff8000",
                    "#ffff00",
                    "#ffff80",
                    "#00ff00",
                    "#80ff80",
                    "#0080c0",
                    "#0080ff",
                    "#80ffff",
                    "#C0C0C0",
                    "#FFFFFF",
                ]
            )
        )
    )
    return cmap


def get_cmap():
    # Define the ListedColormap with the desired color pattern
    cmap = mpl_colors.ListedColormap(
        list(
            reversed(
                [
                    "#000000",
                    "#800000",
                    "#ac0000",
                    "#d00000",
                    "#f00000",
                    "#ff0000",
                    "#ff7979",
                    "#ff8040",
                    "#ff8000",
                    "#ffff00",
                    "#ffff80",
                    "#00ff00",
                    "#80ff80",
                    "#0080c0",
                    "#0080ff",
                    "#80ffff",
                    "#C0C0C0",
                    "#FFFFFF",
                ]
            )
        )
    )
    return cmap