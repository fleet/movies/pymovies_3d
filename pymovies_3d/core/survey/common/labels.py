"""
Mapping of the classes available in echo integrated classes
"""
from enum import Enum, auto, IntFlag


class ClassesPelgas2023(Enum):
    """Type of the cell defined in mantot file"""

    D1 = auto()
    D2 = auto()
    D3 = auto()
    D4 = auto()
    D5 = auto()
    D6 = auto()
    D7 = auto()
    D8 = auto()
    D9 = auto()
    D10 = auto()
    D11 = auto()
    D12 = auto()
    D13 = auto()
    D14 = auto()

    UNKNOWN = auto()
    SEAFLOOR = auto()

    @classmethod
    def description(cls, value):
        descriptions = {
            cls.D1: "BancsNonClupPresFond",
            cls.D2: "BancsClupDecolleFond",
            cls.D3: "BancsNonClupDecoleleFond",
            cls.D4: "BancSurfaceToutesExpeces",
            cls.D8: "BancsSardindePresFond",
            cls.D10: "Poubelle",
            cls.D11: "CouchesDiffusantesProcheSurface",
            cls.D12: "CouchesDiffusantesProcheFond",
            cls.D9: "BancsAnchoisProcheFond",
            cls.D5: "CaprosProcheFond",
            cls.D13: "BancsClupSurface",
            cls.D14: "BancNonClupSurface",
            cls.D7: "Unused",
            cls.D6: "Unused",
            cls.UNKNOWN:"Unknown",
            cls.SEAFLOOR:"Sea floor"
        }
        return descriptions.get(value, "Unknown")

    @classmethod
    def from_name(cls, value):
        # pylint:disable=no-member
        return cls._member_map_.get(value,None)

    @classmethod
    def get_minmax(cls):
        return cls.D1.value,cls.SEAFLOOR.value