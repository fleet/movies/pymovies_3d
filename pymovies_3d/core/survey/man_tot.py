"""Script utilities to process man tot files, man tot are a csv files containing the result of classification pixel per pixel
where
 """
import re
from enum import Enum
from pathlib import Path

import pandas as pd
from pandas import DataFrame
import numpy as np

from pymovies_3d.core.util.report import Report

max_range = 150
class BadAssumption(BaseException):
    """Basic exception when hypothesis are not respected"""

filtered_columns_list = [
    "cellnum",
    "celltype",
    "depthstart",
    "depthend",
    "indexstart",
    "indexend",
    "datestart",
    "dateend",
    "diststart",
    "distend",
    "thresholdlow",
    "lat",
    "long",
    "sa",
    "class",
    "ID"
]


class CellType(Enum):
    """Type of the cell defined in mantot file"""

    REF_SURFACE = 0
    REF_SEABED = 1
    TOTSURF = 2
    TOTSEAB = 3
    TOTAL = 4

    @classmethod
    def description(cls, value):
        descriptions = {
            cls.REF_SURFACE: "surface_reference",
            cls.REF_SEABED: "seabed_reference",
            cls.TOTSURF: "totsurf",
            cls.TOTSEAB: "totseab",
            cls.TOTAL: "total",
        }
        return descriptions.get(value, "Unknown")

def check_esu(dataframe:pd.DataFrame):
    """Ensure that mantot file is coherent
    """
     #check that depth values are uniques
    cellnums = dataframe["cellnum"]
    uniques = pd.unique(cellnums)
    if len(cellnums) != len(uniques):
        raise Exception("dataframe does not have unique cell depth values")


def get_esu_layer_classes(dataframe:pd.DataFrame):
    """ Retrieve layer classes list per depht value
    Assumption is that this function works per esu and we have only one value per depht (or cellnum)
    """
    #we
    # cell_nums = dataframe.
    dataframe = dataframe.groupby(['cellnum']).sum()

    return dataframe

def print_esu_classes_stats(dataframe:pd.DataFrame, expected_classes :Enum):
    dataframe = get_esu_layer_classes(dataframe=dataframe)
    for index,row in dataframe.iterrows():
        # get a list of non nul values
        values = {col.name:row[col.name] for col in expected_classes if col.name in row and row[col.name]>0}
        print(f"depth [{row['depthstart']}:{row['depthend']}] associated classes :{list(values.keys())}")

def read_esu(filename:Path) -> pd.DataFrame:
    """Read man tot file that has already been processed"""
#    logging.info(f"Reading esu mantot file {filename.as_uri()}")

    rawdata = pd.read_csv(filename,parse_dates=["dateend","datestart"])
    rawdata.datestart = rawdata.datestart.dt.floor(freq='s')

    try:
        check_esu(rawdata)
    except Exception as e:
        raise BadAssumption(f"man tot file {filename} does not match expectations : {e}",e) from e
    return rawdata


def read_raw(filename: Path, year: int) -> pd.DataFrame:
    """
    Reads the mantot file and performs necessary data processing.
    
    Args:
    - filename (str): Name of the mantot file to be read.
    - year (int): Year for filtering the data.
    
    Returns:
    - pd.DataFrame: Processed dataframe containing class information per ESU and range.
    """
    default_time_format_start ="%Y-%m-%d %H:%M:%S.%f" 
    default_time_format_end ="%Y-%m-%d %H:%M:%S" 
    dict_format_start = {
        2012:"%Y-%d-%m %H:%M:%S.%f",
        2013:"%Y-%d-%m %H:%M:%S.%f",
        2014:"%Y-%d-%m %H:%M:%S.%f",
        2015:"%Y-%m-%d %H:%M:%S.%f",
        2016:"%Y-%m-%d %H:%M:%S.%f",
        2017:"%Y-%m-%d %H:%M:%S.%f",
        2018:"%Y-%m-%d %H:%M:%S.%f",
        2019:"%Y-%m-%d %H:%M:%S.%f",
        2021:"%Y-%m-%d %H:%M:%S.%f",
        2022:"%Y-%m-%d %H:%M:%S.%f",
        2023:"%Y-%m-%d %H:%M:%S.%f",
        2024:"%Y-%m-%d %H:%M:%S.%f",
    }
    dict_format_end = {
        2012:"%Y-%d-%m %H:%M:%S.%f",
        2013:"%Y-%d-%m %H:%M:%S.%f",
        2014:"%Y-%m-%d %H:%M:%S",
        2015:"%Y-%m-%d %H:%M:%S",
        2016:"%Y-%m-%d %H:%M:%S",
        2017:"%Y-%m-%d %H:%M:%S",
        2018:"%Y-%m-%d %H:%M:%S",
        2019:"%Y-%m-%d %H:%M:%S",
        2021:"%Y-%m-%d %H:%M:%S",
        2022:"%Y-%m-%d %H:%M:%S",
        2023:"%Y-%m-%d %H:%M:%S",
        2024:"%Y-%m-%d %H:%M:%S",
    }
    time_format_start = default_time_format_start if year not in dict_format_start.keys() else dict_format_start[year]
    time_format_end = default_time_format_end if year not in dict_format_end.keys() else dict_format_end[year]


    input_df: DataFrame = pd.read_csv(filename, sep=";")
    
    # Remove unnecessary columns
    rawdata = input_df[filtered_columns_list]
    
    # Remove invalid classes
    rawdata = rawdata[~rawdata["class"].isin(["TOTAL", "Remaining", "Total"])]
    
    # Keep only data referred to surface
    rawdata = rawdata[rawdata["celltype"] == CellType.REF_SURFACE.value]
    
    # Remove cells in the 150-400m range
    rawdata.drop(rawdata[rawdata["depthend"] > max_range].index, inplace=True)
    
    # Convert date columns to datetime format
    rawdata["datestart"] = pd.to_datetime(rawdata["datestart"], format=time_format_start)
    rawdata["dateend"] = pd.to_datetime(rawdata["dateend"],format=time_format_end)
    
    # Create a model per ESU and range containing class information
    try:
        classes_per_esu = rawdata.pivot(
        index=["datestart", "dateend", "depthstart", "depthend", "cellnum", "ID"],
        columns="class",
        values="sa",
    )
    except ValueError as e:
        #Value error is raised when we have duplicate values
        #retrieve duplicate values
        # we should have only one datetime definition per radial name
        duplicates = input_df.duplicated(subset=["datestart", "dateend", "depthstart", "depthend", "cellnum", "ID", "class"], keep='first')
        #get duplicated values radials names
        duplicated_radials_names = input_df[duplicates]["ID"]
        duplicated_radials_names = duplicated_radials_names.unique()
        #save csv without duplicated variables
        input_df[~duplicates].to_csv(f"{filename.parent}/{filename.stem}_filterduplicated.csv",index=False,sep=";")
        raise ValueError(f"Radial {duplicated_radials_names} have duplicated definition",e)  from e

    
    classes_per_esu = classes_per_esu.reset_index()
    
    return classes_per_esu


def time_filter(data:pd.DataFrame, esu_time_start, esu_time_stop) -> pd.DataFrame:
    """
    Filter all mantot data and retain only data for this esu, we consider data as part of an esu if the mid point in time is in datestart and da

    """
    tsum = (esu_time_stop.asm8.astype(np.int64) + esu_time_start.asm8.astype(np.int64))/2
    mid_point = pd.Timestamp(tsum.astype('<M8[ns]'))
    tmp = data[data["datestart"] <= mid_point ]
    return tmp[tmp["dateend"] >= mid_point]


def extract_esu_dataframe(df:pd.DataFrame,report:Report):
    """Extract ESU , remove duplicate and return as a dataframe"""
    #keep only the first layer
    filter_df= df[df["cellnum"]==0]
    #we should have only one datetime definition per radial name

    filter_df=filter_df[["datestart", "dateend", "ID"]]
    #find duplicates and fill report with them
    duplicated = filter_df[filter_df.duplicated()]
    #parse report
    for index,rows in duplicated.iterrows():
        msg = f"Radial [{rows['ID']}] ESU {rows['datestart'].strftime('%Y%m%d_%H%M%S')}_{rows['dateend'].strftime('%Y%m%d_%H%M%S')} has duplicated definition"
        report.error(msg=msg,channel="Duplicated ESU definition")

    filter_df=filter_df.drop_duplicates()
    return filter_df

def extract_radials(df:pd.DataFrame,report:Report):
    """Extract radials information """
    filter_df = extract_esu_dataframe(df=df,report=report)
    final_df: DataFrame=filter_df.groupby("ID").agg({"datestart":"min","dateend":"max"}).reset_index()
    #Reformat name to match convention used in screenshots and echo integrated files
    radial_valid_names = final_df["ID"].str.replace(r"-", "_", 1)
    final_df["ID"]=radial_valid_names
    final_df["datestart"]=final_df["datestart"].dt.floor("S")
    final_df["dateend"]=final_df["dateend"].dt.ceil("S")
    radials=final_df.set_index("ID")
    radials = radials.sort_index()
    return final_df

def extract_esu(df:pd.DataFrame, report : Report):
    """
    Extract ESU definition from read_raw output
    return a tuple containg the radial names, start date and, end date
    """
    filter_df = extract_esu_dataframe(df=df,report=report)

    dict_esu = filter_df.to_dict(orient="list")
    #rename radial to match names used in screenshot and echo integrated files
    radial_valid_names = [re.sub(r"-", "_", name, 1) for name in dict_esu["ID"] ]
    return radial_valid_names,dict_esu["datestart"],dict_esu["dateend"]
