# -*- coding: utf-8 -*-
"""
Created on Tue Nov 21 14:15:51 2023

@author: lberger
"""

import os
from pathlib import Path
import json
import netCDF4 as nc
import xarray as xr
import numpy as np
import pandas as pd
import copy

import matplotlib.pyplot as plt

root_directory = Path("D:/PELGAS23/Results_data_RMS/")
json_file_path = 'radial_list.json'

def read_json_file(file_path):
    try:
        with open(file_path, 'r') as file:
            data = json.load(file)
        return data
    except Exception as e:
        print(f"Error reading JSON file: {e}")
        return None

def read_netCDF_data_xarray(root_dir):

    # Initialize empty lists for each variable and filenames
    data_60 = []
    data_80 = []
    data_classes = []
    depths = []
    predicted_classes = []
    times = []
    filenames = []

    # Loop through each file in the folder
    for file in os.listdir(root_dir):
        if file.endswith(".nc"):  # Check for NetCDF files ending with "dataset"
            file_path = os.path.join(root_dir, file)
            
            # Open the NetCDF file with xarray
            with xr.open_dataset(file_path) as dataset:
                # Read and append data from each variable
                data_60.append(dataset['Data_60'].values)
                data_80.append(dataset['Data_80'].values)
                data_classes.append(dataset['Data_Classes'].values)
                depths.append(dataset['depth'].values)
                predicted_classes.append(dataset['Predicted_Data_classes'].values)
                times.append(dataset['time'].values)
                filenames.append(file_path)  # Append the folder name

    return data_60, data_80, data_classes, depths, predicted_classes, times, filenames

def merge_classes(classes):
    # Define the mapping of old classes to new classes
    class_mapping = {
        1: 'Fish', 2: 'Fish', 3: 'Fish', 4: 'Fish', 5: 'Fish', 
        8: 'Fish', 9: 'Fish', 13: 'Fish', 14: 'Fish', 
        11: 'Pk', 12: 'Pk', 
        15: 'Bg', 
        16: 'Sf', 
        10: 'Pa'
    }

    # Iterate over each array and replace class labels
    for i in range(len(classes)):
        classes[i] = np.vectorize(class_mapping.get)(classes[i])

    return classes

def lin(variable):
    """
    Turn variable into the linear domain.     
    
    Args:
        variable (float): array of elements to be transformed.    
    
    Returns:
        float:array of elements transformed
    """
    
    lin    = 10**(variable/10)
    return   lin  

def log(variable):
    """
    Turn variable into the logarithmic domain. This function will return -999
    in the case of values less or equal to zero (undefined logarithm). -999 is
    the convention for empty water or vacant sample in fisheries acoustics. 
    
    Args:
        variable (float): array of elements to be transformed.    
    
    Returns:
        float: array of elements transformed
    """
    
    # convert variable to float array
    back2single = False
    back2list   = False
    back2int    = False    
    if not isinstance(variable, np.ndarray):
        if isinstance(variable, list):
            variable  = np.array(variable)
            back2list = True
        else:
            variable    = np.array([variable])
            back2single = True            
    if variable.dtype=='int64':
        variable   = variable*1.0
        back2int = True
    
    # compute logarithmic value except for zeros values, which will be -999 dB    
        
    # pylint:disable=no-member
    mask           = np.ma.masked_less_equal(variable, 0).mask
    variable[mask] = np.nan
    log            = 10*np.log10(variable)
    log[mask]      = -999
    
    # convert back to original data format and return
    if back2int:
        log = np.int64(log)
    if back2list:
        log = log.tolist()
    if back2single:
        log = log[0]       
    return log

def Sv2NASC(Sv, r, r0, r1, operation='mean'):
    """
    Compute Nautical Area Scattering Soefficient (m2 nmi-2), by integrating Sv
    in a given range interval.
    
    Args:
        Sv (float)    : 2D array with Sv data (dB m-1)
        r  (float)    : 1D array with range data (m)
        r0 (int/float): Top range limit (m)
        r1 (int/float): Bottom range limit (m)
        method (str)  : Method for calculating sa. Accepts "mean" or "sum"
        
    Returns:
        float: 1D array with Nautical Area Scattering Coefficient data.
        float: 1D array with the percentage of vertical samples integrated.
    """
   
    # get r0 and r1 indexes
    r0 = np.argmin(abs(r-r0))
    r1 = np.argmin(abs(r-r1))
    
    # get number and height of samples 
    ns     = len(r[r0:r1])
    sh = np.r_[np.diff(r), np.nan]
    sh = np.tile(sh.reshape(-1,1), (1,len(Sv[0])))[r0:r1,:]
    
    # compute NASC    
    sv = lin(Sv[r0:r1, :])
    if operation=='mean':    
        NASC = np.nanmean(sv * sh, axis=0) * ns * 4*np.pi*1852**2
    elif operation=='sum':
        NASC = np.nansum (sv * sh, axis=0)      * 4*np.pi*1852**2
    else:
        raise Exception('Method not recognised')
    
    # compute percentage of valid values (not NAN) behind every NASC integration    
    per = (len(sv) - np.sum(np.isnan(sv*sh), axis=0)) / len(sv) * 100
    
    # correct sa with the proportion of valid values
    # NASC = NASC/(per/100)
        
    return NASC, per

def main():
    data_60, data_80, data_classes, depth, predicted_classes, time, filenames = read_netCDF_data_xarray(root_directory)
    
    # Truncate the depth dimension in data_80_normalized
    data_80 = [array[1, :150, :] for array in data_80]
    
    # Truncate the depth dimension in data_80_normalized
    data_60 = [array[1, :150, :] for array in data_60]
    
    # Truncate the depth dimension in data_classes
    data_classes = [array[:150, :] for array in data_classes]
    
    # Truncate the depth dimension in data_classes
    data_classes_predicted = [array[:150, :] for array in predicted_classes]
    
      
    maskFish     = [array[:,:]==1 for array in data_classes]
    maskPlankton = [array[:,:]==2 for array in data_classes]
    
    maskFishPredicted     = [array[:,:]==1 for array in data_classes_predicted]
    maskPlanktonPredicted = [array[:,:]==2 for array in data_classes_predicted]
    
    # Create a deep copy of data_60 and data_80
    data_Fish_60 = copy.deepcopy(data_60)
    data_Fish_80 = copy.deepcopy(data_80)

    for data, mask in zip(data_Fish_60, maskFish):
        data[~mask] = np.NAN
    
    NascFish=[]
    for index in range (len(data_Fish_60)):
        NascFish.append(np.nanmean(Sv2NASC(data_Fish_60[index],np.array(depth[index]),10,150,'sum')))
    
    data_Fish_60 = copy.deepcopy(data_60)
    data_Fish_80 = copy.deepcopy(data_80)
    
    for data, mask in zip(data_Fish_60, maskFishPredicted):
        data[~mask] = np.NAN
    
    NascFishPredicted=[]
    for index in range (len(data_Fish_60)):
        NascFishPredicted.append(np.nanmean(Sv2NASC(data_Fish_60[index],np.array(depth[index]),10,150,'sum')))
    
    radialNames = [os.path.basename(f).split('.')[0] for f in filenames]
    #plot Prediction against expert
    df = pd.DataFrame(np.column_stack((NascFish,NascFishPredicted)),
                 index=radialNames,
                 columns=pd.Index(['Expert', 'DeepLearning'], 
                 name='Prediction')).round(2)


    df.plot(kind='bar',figsize=(10,4))
    plt.ylabel('NASC')
    plt.show()
    

        
    print('toto')
 
if __name__ == "__main__":
    main()