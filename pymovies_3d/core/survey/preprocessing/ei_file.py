from datetime import datetime
from pathlib import Path
import pytz as tz
import netCDF4 as nc
import numpy as np
import xarray as xr

from pymovies_3d.core.survey.common.labels import ClassesPelgas2023


class XrStructure:
    """Naming and convention for xr structure dataset"""
    time_dim = "time"
    depth_dim = "depth"
    frequency = "frequency"

    variable_bs = "integrated_bs"
    variable_class = "classes"

expected_frequency_count = 6

class BadFile(Exception):
    def __init__(self, message="Bad file detected!"):
        super().__init__(message)

def filter(dataset:xr.Dataset,start_time:datetime, end_time:datetime)-> xr.Dataset:
    """
    Filter dataset on a time basis
    :param dataset:
    :param start_time:
    :param end_time:
    :return:
    """

    if start_time.tzinfo is None or start_time.tzinfo.utcoffset(start_time) is None:
        start_time = start_time.tz_localize(tz.utc)
    if end_time.tzinfo is None or end_time.tzinfo.utcoffset(end_time) is None:
        end_time = end_time.tz_localize(tz.utc)
    # print(f"ESU [{start_time} {end_time}]")
    # print(f"source [{dataset.time.min()} {dataset.time.max()}]")
    filtered_dataset=  dataset.where(dataset.time>=start_time,drop=True)
    filtered_dataset = filtered_dataset.where((dataset.time <= end_time),drop=True)
    # print(f"final [{filtered_dataset.time.min()} {filtered_dataset.time.max()}]")
    return  filtered_dataset


def _fill_class_info(ei_dataset:xr.Dataset, nc_dataset:nc.Dataset):

    bottom_range =  nc_dataset["Sonar"]["Grid_group1"]["detected_bottom_range"]
    bottom_range = bottom_range[:, 1]  # assume that all frequencies are at the same depth, take 38kHz frequency as reference
    #add an offset to convert range to depth
    #range is in fact depth referred to surface (but in ei)
    #expand depth values to get them in the same dimension as ei_dataset
    expanded_depth = ei_dataset['depth'].expand_dims(dim={'time': len(ei_dataset.time)}, axis=-1)

    ei_dataset["classes"] = ei_dataset["classes"].where(expanded_depth<=bottom_range,other=ClassesPelgas2023.SEAFLOOR.value)


def read_dataset(filename:Path):
    """Parse ei files and return as a xarray dataset a timebased multi frequency dataset"""

    # we could open the dataset with xarray but like this   ds = xr.open_dataset(str(filename),group= "/Sonar/Grid_group1")
    # but we want to be able to merge all datetime to a common value

    # raw_ds = xr.open_dataset(str(filename),group= "/Sonar/Grid_group1")

    with nc.Dataset(filename=str(filename)) as ncdataset:
        freq= _get_frequencies(dataset=ncdataset)
        freq = freq[:]
        time = _get_date_time(dataset=ncdataset,frequence=1) #we consider that time is uniform among all echo integrated datasets
        bs= _get_bs(dataset=ncdataset)
        depth = ncdataset["Sonar"]["Grid_group1"]["cell_depth"]
        depth = depth[:,0] #assume that all frequencies are at the same depth
        frequency_count = len(ncdataset["Sonar"]["Grid_group1"].dimensions["frequency"])
        if frequency_count != expected_frequency_count:
            raise BadFile(message=f'Expected {frequency_count} frequencies got {frequency_count} ')


        class_values = np.full(
            fill_value=ClassesPelgas2023.UNKNOWN.value,
            shape=(len(depth), len(time)),
        )
        class_dataarray = xr.DataArray(
            class_values,
            dims=[XrStructure.depth_dim, XrStructure.time_dim, ],
            attrs={
                "description": "list of class 0:Unknown"
            }
        )

        dataset= xr.Dataset(
            coords={XrStructure.time_dim:time,XrStructure.depth_dim:depth, XrStructure.frequency:freq},
            data_vars={
                XrStructure.variable_bs:([XrStructure.frequency, XrStructure.depth_dim,XrStructure.time_dim],bs),
                XrStructure.variable_class:class_dataarray
                       },
            attrs={"description":"integrated bs value"}
        )
        _fill_class_info(ei_dataset=dataset,nc_dataset=ncdataset)
        return  dataset


def _get_frequencies(dataset:nc.Dataset) -> nc.Dataset:
    freq = dataset["Sonar"]["Grid_group1"]["frequency"]
    #retrieve frequencies
    return freq

def _get_bs(dataset:nc.Dataset):
    """Read backscatter and apply range threshold if needed"""

    bs= dataset["Sonar"]["Grid_group1"]["integrated_backscatter"]
    bs=bs[:]
    bs = np.transpose(bs)
    return bs


def _get_date_time(dataset:nc.Dataset,frequence)->np.array:
    """Read ping date time for a given frequency"""
    # Format the dates
    datenano = dataset["Sonar"]["Grid_group1"]["cell_ping_time"]
    datefreq = datenano[:, frequence].data
    datefreq = np.around(datefreq / 1000000000, 1)

    date_echo = [None] * len(datefreq)
    for i in range(len(datefreq)):
        dt = datetime.fromtimestamp(datefreq[i],tz=tz.utc)
        date_echo[i] = dt#datetime.strftime(dt, "%Y-%m-%d %H:%M:%S.%f")
    return date_echo
