"""File manager will read all data (screen shots, mantot, echointegrated files) and move/filter them per ESU"""

# get radial list
import glob
from pathlib import Path
from re import S
from typing import Dict, List, Union, Set, Iterable

def get_radial_from_ei_name(file_name:str) -> str:
    """get radial name from ei file name like R22_ML-a2_21052023_T162956_to_21052023_T173809 -> R22_ML-a2"""
    sub_strings = file_name.split("_")
    return sub_strings[0] + "_" + sub_strings[1]


def get_seq_from_name(file_name: str) -> str:
    """get radial name from file name"""
    radial_name = file_name.split("PEL")[0]
    radial_name = radial_name[:-1]  # remove trailing "_"
    return radial_name


def get_seq_from_files(input_files: List[Union[str, Path]]) -> Dict[str, Path]:
    """parse directory and get radial names from files"""
    radials = {}
    for f in input_files:
        file = Path(str(f))
        radial_name = get_seq_from_name(file.name)
        radials[radial_name] = file
    return radials


def get_sequence_from_dir(input_dir: str, file_pattern="*ES38*.png") -> Dict[str,List[str]]:
    """parse directory and get radial names from files"""
    radials:Dict[str,List[str]] = {}
    for f in glob.glob(str(Path(input_dir) / file_pattern)):
        file = Path(f)
        seq_name = get_seq_from_name(file.name)
        if seq_name not in radials:
            radials[seq_name]=[file.name]
        else:
            radials[seq_name].append(file.name)
    return radials
