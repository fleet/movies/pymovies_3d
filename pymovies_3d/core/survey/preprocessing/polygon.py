"""
Read an parse polygons files

"""

from __future__ import annotations

import csv
import dataclasses
import datetime
import fnmatch
import json
import glob
import os
from collections import namedtuple
from pathlib import Path
from typing import List, Tuple

import numpy as np
import pandas as pd
import shapely
from pandas import Timestamp
from shapely import Polygon, Point
from tqdm import tqdm

from pymovies_3d.core.survey.common.labels import ClassesPelgas2023


@dataclasses.dataclass
class MLayer:
    start_time: datetime.datetime
    end_time: datetime.datetime
    depthstart: float
    depthend: float


class MPolygon:
    def __init__(self, filename: str) -> None:
        data_header = pd.read_csv(str(filename), sep=";", nrows=1)
        polygon_data: pd.DataFrame = pd.read_csv(filename, sep=";", skiprows=2)
        self.__data_header = data_header
        self.data_class = data_header.loc[0, r"MOVIES_EISupervised\class"]
        if pd.isna(self.data_class):
            self.data_class = None

        self.sa = data_header.loc[0, r"MOVIES_EISupervised\eilayer\sa"]
        self.filename = filename

        # Remove empty rows
        polygon_data = polygon_data.dropna(how="all")
        polygon_data = polygon_data.reset_index(drop=True)
        self.__polygon_data = polygon_data

        # Extract polygon time and depth information
        polygon_time = pd.to_datetime(polygon_data["datetime"])
        self.start_time = polygon_time.min()
        self.end_time = polygon_time.max()
        polygon_time_ns = polygon_time.values.astype(np.int64)
        polygon_depth = polygon_data["depth"]

        # Close the polygon
        polygon_time_ns = np.append(polygon_time_ns, polygon_time_ns[0])
        polygon_depth = np.append(polygon_depth, polygon_depth[0])

        polygone = np.column_stack((polygon_time_ns, polygon_depth))
        # TODO check if there is a loss of precision when storing time as float and if we matter

        self.shape = Polygon(polygone)
        self.min_depth = polygon_depth.min()

        self.max_depth = polygon_depth.max()

    __data_header: pd.DataFrame
    __polygon_data: pd.DataFrame
    data_class: str
    shape: Polygon
    filename: Path
    start_time: datetime.datetime
    end_time: datetime.datetime
    min_depth: float
    max_depth: float
    sa: float

    def __hash__(self) -> int:
        # partial hash of polygon
        return hash(
            (
                self.filename,
                self.shape,
                self.data_class,
                self.sa,
            )
        )

    def to_csv(self, filename):
        df: pd.DataFrame = self.__data_header
        df["MOVIES_EISupervised\class"] = self.data_class
        df.to_csv(filename, sep=";", index=False)
        with open(filename, "a+") as csvfile:
            csvfile.write("\n")
            # ensure that dataclass is well updated
            df: pd.DataFrame = self.__polygon_data
            csvfile.write("datetime;latitude;longitude;depth;\n")
            csv_value = df.to_csv(
                None, sep=";", index=False, lineterminator="\n", header=False
            )
            csvfile.write(csv_value)

    def is_similar(self, other: MPolygon) -> bool:
        if self.data_class != other.data_class:
            return False
        if self.sa != other.sa:
            return False
        if self.start_time != other.start_time:
            return False
        if self.end_time != other.end_time:
            return False
        if self.min_depth != other.min_depth:
            return False
        if self.max_depth != other.max_depth:
            return False
        if self.shape.wkt != other.shape.wkt:
            return False
        return True

    def __eq__(self, other: MPolygon) -> bool:
        # compare two polygons content
        if not self.is_similar(other):
            return False
        if not self.__data_header.equals(other.__data_header):
            return False
        if not self.__polygon_data.equals(other.__polygon_data):
            return False

        return True


def check(polygons: List[MPolygon]):
    """Check if polygon are coherent,
    check if they are associated with a valid class
    """
    error_files = []
    known_classes = [e.name for e in ClassesPelgas2023]
    for p in polygons:
        if p.data_class is None or p.data_class not in known_classes:
            error_files.append(p.filename)
    if len(error_files) > 0:
        raise Exception(
            f"{len(error_files)} polygons files have no classes associated : {[str(e.name) for e in error_files]}"
        )


def read_one_polygon(file: Path):
    """
    Read one single polygon
    """

    return MPolygon(filename=Path(file))


def write_polygon(polygon: MPolygon, filename: Path):
    """Write a polygon to a file as a csv file"""
    polygon.to_csv(filename)


def read_polygon(polygon_files: List[Path]) -> List[MPolygon]:
    """Read a list of polygon files"""
    ret = []
    for f in polygon_files:
        ret.append(read_one_polygon(f))
    return ret


def intersects(layer: MLayer, polygons: List[MPolygon]) -> List[MPolygon]:
    """
    Check if a layer intersect polygons
    :param layer the definition of a layer (MLayer)
    :polygons the list of MPolygon
    :returns the list of intersecting polygons
    """
    # create a polygon for the layer ie, create an array with all coordinates ABCD with A lower left, B upper left, C upper rigth
    # D lower right...
    # matching the layer
    a_time = b_time = pd.to_datetime(layer.start_time).to_datetime64()
    c_time = d_time = pd.to_datetime(layer.end_time).to_datetime64()
    a_depth = d_depth = float(layer.depthend)
    c_depth = b_depth = float(layer.depthstart)

    layers_times = np.array([a_time, b_time, c_time, d_time, a_time])
    layers_times = layers_times.astype(np.int64)
    layers_depth = np.array([a_depth, b_depth, c_depth, d_depth, a_depth])
    layer_polygon = Polygon(np.column_stack((layers_times, layers_depth)))

    intersecting_polygons = []
    for p in polygons:
        if shapely.intersects(p.shape, layer_polygon):
            intersecting_polygons.append(p)

    return intersecting_polygons


def filter(
    polygons: List[MPolygon], start_time: datetime.datetime, end_time: datetime.datetime
) -> List[MPolygon]:
    """
    Check if polygon intersects the interval start_time and end_time, this is done for early rejection and not for precise intersection computation
    """
    # polygon does not intersects if both time are stricly below or over start_time and end_time
    filtered_polygons = []
    for p in polygons:
        both_below = p.start_time < start_time and p.end_time < start_time
        both_over = p.start_time > end_time and p.end_time > end_time
        if not (both_below or both_over):
            filtered_polygons.append(p)
    return filtered_polygons


def get_min_max_depth(polygon: MPolygon) -> Tuple[float, float]:
    """get min and max values of polygon depths"""
    bounds = polygon.shape.bounds
    return min(bounds[1], bounds[3]), max(bounds[1], bounds[3])


def contains(polygon: MPolygon, time_value: Timestamp, depth_value: float) -> bool:
    """

    :param polygon:
    :param time_value:
    :param depth_value:
    :return:
    """
    encoded_time = pd.to_datetime(time_value).to_datetime64()
    encoded_time = encoded_time.astype(np.int64)

    return polygon.shape.contains(Point(encoded_time, depth_value))
