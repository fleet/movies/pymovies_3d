import csv
import os
import fnmatch
from datetime import datetime
from typing import Dict, Optional
import pytz
import netCDF4 as nc
import numpy as np
import pandas as pd
from shapely.geometry import Polygon, Point
import copy

# Global parameters
start_range = 10
max_range = 150
layer_thickness = 10
frequency_count = 6

temporal_offset_hour = 0  # TODO serieux doutes sur les heures indiquées, sont elles en UTC ou en heure locale ?


class BadFile(Exception):
    def __init__(self, message="Bad file detected!"):
        super().__init__(message)


def _get_bs(dataset: nc.Dataset):
    """Read backscatter and apply range threshold if needed"""

    bs = dataset["Sonar"]["Grid_group1"]["integrated_backscatter"]
    bs = bs[:]
    if bs.shape[2] != frequency_count:
        raise BadFile(
            message=f"Expected {frequency_count} frequencies got {bs.shape[2]} "
        )
    bs = np.transpose(bs)
    depth = dataset["Sonar"]["Grid_group1"]["cell_depth"]
    depth = depth[:, 1]
    depth_index = np.where((depth >= start_range) & (depth <= max_range))
    bs = bs[:, depth_index, :]
    bs = np.squeeze(bs)
    return bs


def read_concatenate_bs(datasets: Dict[str, nc.Dataset]) -> np.ndarray:
    """Read bacskatter values and concatenate the result in a list"""
    bs_list = []
    for filename, dataset in datasets.items():
        try:
            bs = _get_bs(dataset=dataset)
            bs_list.append(bs)
        except BadFile as e:
            print(f"File {filename} raised an exception {e}")
    concatenated_values = np.concatenate(bs_list, axis=2)
    return concatenated_values


def read_date_time(dataset: nc.Dataset, frequence):
    """Read ping date time for a given frequency"""
    # Format the dates
    datenano = dataset["Sonar"]["Grid_group1"]["cell_ping_time"]
    datefreq = datenano[:, frequence].data
    datefreq = np.around(datefreq / 1000000000, 1)

    date_echo = [None] * len(datefreq)
    for i in range(len(datefreq)):
        dt = datetime.fromtimestamp(datefreq[i], tz=pytz.utc)
        date_echo[i] = dt  # datetime.strftime(dt, "%Y-%m-%d %H:%M:%S.%f")
    return date_echo


def process_mantot(filename: str):
    """Parse manuel echo typing"""
    # Ecotyping Movies3D
    EcoMovies = pd.read_csv(filename, sep=";")
    columns_name = [
        "cellnum",
        "celltype",
        "depthstart",
        "depthend",
        "indexstart",
        "indexend",
        "datestart",
        "dateend",
        "diststart",
        "distend",
        "thresholdlow",
        "lat",
        "long",
        "sa",
        "class",
    ]

    # Filter the mantot
    EcoSimp = EcoMovies.loc[:, columns_name]

    # Filter based on cell type #TODO WHAT is CELLTYPE 0 ?
    EcoSimpF = EcoSimp[EcoSimp["celltype"] == 0]
    value_class = set(EcoSimpF["class"])
    value_class.remove("Remaining")  # TODO should we remove class "TOTAL" ?
    value_class.remove("TOTAL")  # TODO should we remove class "TOTAL" ?
    EcoSimpF = EcoSimpF[EcoSimpF["class"].isin(value_class)]

    # Remove cells in the 150-400m range
    EcoSimpF.drop(
        EcoSimpF[
            EcoSimpF["cellnum"] >= (max_range - start_range) // layer_thickness
        ].index,
        inplace=True,
    )  # TODO WE SHOULD PROBABLY Remove CELLs where depthstart >= 150 instead of this magic number

    # Pivot the DataFrame
    EcoSimpF = EcoSimpF.pivot(
        index=["datestart", "dateend", "depthstart", "depthend", "cellnum"],
        columns="class",
        values="sa",
    )
    EcoSimpF = EcoSimpF.reset_index()

    # Create a class variable
    classe_cellule = [None] * EcoSimpF.shape[0]
    if len(value_class) == 2:
        for i in range(EcoSimpF.shape[0]):
            classe_cellule[i] = "P" if EcoSimpF.loc[i, "D1"] > 0 else "A"
        EcoSimpF = EcoSimpF.assign(classe_cellule=classe_cellule)
    else:  # cas PELGAS23
        for i in range(EcoSimpF.shape[0]):
            if (EcoSimpF.loc[i, "D11"] > 0) or (EcoSimpF.loc[i, "D12"] > 0):
                classe_cellule[i] = "COUCHE"
            elif (
                (EcoSimpF.loc[i, "D2"] > 0)
                or (EcoSimpF.loc[i, "D4"] > 0)
                or (EcoSimpF.loc[i, "D8"] > 0)
                or (EcoSimpF.loc[i, "D9"] > 0)
                or (EcoSimpF.loc[i, "D13"] > 0)
            ):
                classe_cellule[i] = "CLUP"
            elif (
                (EcoSimpF.loc[i, "D1"] > 0)
                or (EcoSimpF.loc[i, "D3"] > 0)
                or (EcoSimpF.loc[i, "D5"] > 0)
                or (EcoSimpF.loc[i, "D14"] > 0)
            ):
                classe_cellule[i] = "NONCLUP"
            else:
                classe_cellule[i] = (
                    "NC"  # TODO A vérifier j'ai changé sinon tout était "A" par défaut
                )

        EcoSimpF = EcoSimpF.assign(classe_cellule=classe_cellule)

        CLUP = pd.DataFrame.sum(
            EcoSimpF.loc[:, ("D2", "D4", "D8", "D9", "D13")], axis=1
        )
        NONCLUP = pd.DataFrame.sum(EcoSimpF.loc[:, ("D1", "D3", "D5", "D14")], axis=1)
        Couche = pd.DataFrame.sum(EcoSimpF.loc[:, ("D11", "D12")], axis=1)
        EcoSimpF = EcoSimpF.assign(CLUP=CLUP, NONCLUP=NONCLUP, Couche=Couche)
        # for i in range(EcoSimpF.shape[0]):
        #     if ((EcoSimpF["classe_cellule"][i] == "CLUP") or (EcoSimpF["classe_cellule"][i] == "NONCLUP")) and (
        #             (EcoSimpF["Couche"][i] + EcoSimpF["D10"][i]) != 0 #TODO D10 is "Trash" we should alway mark it as NC
        #     ):
        #         EcoSimpF["classe_cellule"][i] = "NC"  # non-classified

    # Create the class matrix
    EcoSimpFF = EcoSimpF.pivot(
        index=["datestart", "dateend"],
        columns="cellnum",
        values="classe_cellule",
    )
    class_depth = np.array(EcoSimpFF)

    class_depth = class_depth.repeat(
        layer_thickness, axis=1
    )  # TODO VERY STRONG HYPOTHESE ECho integration is done every 10m, is it always true ?
    EcoSimpFF = EcoSimpFF.reset_index()
    EcoSimpFF["dateend"] = pd.to_datetime(
        EcoSimpFF["dateend"], utc=True
    ) + pd.Timedelta(hours=temporal_offset_hour)
    EcoSimpFF["datestart"] = pd.to_datetime(
        EcoSimpFF["datestart"], utc=True
    ) + pd.Timedelta(hours=temporal_offset_hour)
    return EcoSimpFF, class_depth


def creat_matrypage_multi(
    manuel_echotypes_file: str,
    echo_integrated_fine_60: Dict[str, nc.Dataset],
    frequence,
    seuil=np.nan,
    echo_integrated_fine_80: Optional[Dict[str, nc.Dataset]] = None,
):
    """
    The function creat_matrypage_multi is used to create a high-resolution reference echotype matrix by combining fine-scale echo integration at -60dB and manual echotyping.
    This reference, called matrypage, is used to calibrate and measure the effectiveness of different combinations of automatic echotyping methods and parameters.

    Arguments:
        - manuel_echotypes_file: Path to the file containing manual echotyping data.
        - echo_integrated_fine_60: List of files containing fine-scale echo integration data at -60dB (to create the matrix).
        - frequence: Frequency number to use for classification.
        - seuil: Integration threshold for classification (default is np.nan).
        - echo_integrated_fine_80: List of files containing echo integration data at -80dB (for automatic echotyping prediction).

    Returns:
        - matrypage_save: List of created classification matrices.
        - borne_save: List of temporal boundaries used for each matrix.
        - echonc80cut_save: List of cropped -80dB echo integration matrices to match the references.
    """
    matrypage_save, borne_save, echonc80cut_save = [], [], []

    if echo_integrated_fine_80 is not None:
        echointegration_80 = read_concatenate_bs(echo_integrated_fine_80)
        echoint_80 = echointegration_80  # TODO change the name

    # get man tot data
    EcoSimpFF, class_depth = process_mantot(filename=manuel_echotypes_file)

    for filename, echointegration_dataset in echo_integrated_fine_60.items():
        print(f"processing file {filename}")

        # Read the fine-scale echo integration netCDF file
        backscatter = _get_bs(echointegration_dataset)
        backscatter = backscatter[frequence, :, :]

        dates_ping = read_date_time(dataset=echointegration_dataset, frequence=1)

        # Crop the fine-scale echo on depth axis integration matrix to match manual echotyping
        inte_fine = backscatter[range(class_depth.shape[1])]

        # crop data to match min/max dates from manto files
        borne = (
            dates_ping > np.repeat(min(EcoSimpFF["datestart"]), len(dates_ping))
        ) & (dates_ping < np.repeat(max(EcoSimpFF["dateend"]), len(dates_ping)))
        inte_fine = inte_fine[
            :, borne
        ]  # TODO We could have retrieve the first index and last in the temporal area and use it to cut the data

        # Assign classes to the fine-scale echo integration matrix
        matrypage = np.full_like(inte_fine, fill_value=np.nan)
        depth_dimension = inte_fine.shape[0]
        ping_dimension = inte_fine.shape[1]
        for depth_index in range(depth_dimension):
            j = 0
            for ping in range(ping_dimension):
                while (
                    dates_ping[ping] > EcoSimpFF.loc[j, "dateend"]
                ):  # find the starting index in mantot file
                    j += 1
                if j > 0:
                    j -= 1
                # TODO we should also check if date is more than mantot datestart
                if (
                    dates_ping[ping] > EcoSimpFF.loc[j, "datestart"]
                    and not inte_fine.mask[depth_index, ping]
                ):  # is data valid  and its date is in the cell:
                    # if  dates_ping[ping] > EcoSimpFF.loc[j,"datestart"] :#its date is in the cell:
                    if class_depth[j, depth_index] == "COUCHE":
                        matrypage[depth_index, ping] = 0  # Other
                    # elif class_depth[j, depth_index] == "NC":
                    #     matrypage[depth_index, ping] = np.nan  # Non-classified
                    elif class_depth[j, depth_index] == "CLUP":
                        matrypage[depth_index, ping] = 1  # Fish (Poisson clupéidés)
                    elif class_depth[j, depth_index] == "NONCLUP":
                        matrypage[depth_index, ping] = 2  # Fish (Poisson non clupeidés)

                # elif (class_depth[j, d] == "P") or (class_depth[j, d] == "A"):
                #    # matrypage[d, i] = 0  # Other
                # elif class_depth[j, d] == "NC":
                #     matrypage[d, i] = np.nan  # Non-classified
                else:
                    matrypage[depth_index, ping] = np.nan

        if echo_integrated_fine_80 is not None:
            cut = echoint_80[:, :, range(matrypage.shape[1])]
            # something is strange here we concatenate everything at first, then we remove slice by slice the data
            echoint_80 = np.delete(echoint_80, range(matrypage.shape[1]), axis=2)
            echonc80cut_save.append(cut)

        matrypage_save.append(matrypage)
        borne_save.append(borne)

    return matrypage_save, borne_save, echonc80cut_save


########################################################################################################################
def process_polygons(path_polygons, result_matrypage, EcoIntfine):
    """
    This function integrates the polygons from the automatic echotyping with the matrypage created using the previous function.

    Arguments:
        - path_polygons: Path of the folder containing the polygon files.
        - result_matrypage: Result of the creat_matrypage_multi function.
        - EcoIntfine: List of fine-scale echointegration data files.

    Returns:
        - matrypage_polygon: List of matrypage matrices containing polygon information.
    """

    files_polygons = os.listdir(path_polygons)
    # List to store the polygons and their associated classes
    polygons, polygon_classes = [], []

    # Iterate through the files and create the corresponding polygons
    for file in files_polygons:
        if fnmatch.fnmatch(file, "*.csv"):
            file_path = os.path.join(path_polygons, file)
            # Automatically detect the delimiter
            with open(file_path, "r") as file:
                dialect = csv.Sniffer().sniff(file.read(1024))
                separator = dialect.delimiter

            # Read the CSV file with the detected delimiter
            polygon_data = pd.read_csv(file_path, sep=separator)

            # Remove empty rows
            polygon_data = polygon_data.dropna(how="all")
            polygon_data = polygon_data.reset_index(drop=True)

            # Close the polygon
            # polygon_data = pd.concat([polygon_data, np.transpose(polygon_data.loc[2, :])],ignore_index=True)
            polygon_data = polygon_data.append(
                polygon_data.loc[2, :], ignore_index=True
            )

            # Extract polygon time and depth information
            polygon_time = pd.to_datetime(polygon_data.loc[2:, "MOVIES_EISupervised"])
            polygon_depth = polygon_data.loc[2:, "MOVIES_EISupervised\sndset\sndident"]
            polygon_class = polygon_data.loc[0, "MOVIES_EISupervised\class"]
            polygon_time_ns = polygon_time.values.astype(np.int64)
            polygone = np.column_stack((polygon_time_ns, polygon_depth))
            polygon = Polygon(polygone)

            polygons.append(polygon)
            polygon_classes.append(polygon_class)

    date_tot, cut = [], []

    # Convert the dictionary keys into a list
    ecointfine_keys = list(EcoIntfine.keys())

    for i in range(len(result_matrypage[1])):
        # Get the key at index i
        key_at_i = ecointfine_keys[i]

        # Access the dictionary using the key
        dataset = EcoIntfine[key_at_i]

        # Your remaining code here
        date = dataset["Sonar"]["Grid_group1"]["cell_ping_time"][:, 1].data[
            result_matrypage[1][i]
        ]
        cut.append(len(date))
        date_tot.append(date)

    date_tot = np.concatenate(date_tot, axis=0)

    depth = range(start_range, max_range)  # echointegration from 10 to 200

    # Create the class matrix
    matrice_classes = np.full(
        (len(depth), len(date_tot)), "D0", dtype="object"
    )  # D0: no trace
    # Iterate through all the polygons and assign the corresponding class in the class matrix
    for polygon, polygon_class in zip(polygons, polygon_classes):
        xmin, ymin, xmax, ymax = polygon.bounds

        # Restrict search within the bounds of the polygon
        for i, x in enumerate(date_tot):
            if x >= xmin and x <= xmax:
                for j, y in enumerate(depth):
                    if y >= ymin and y <= ymax:
                        if polygon.contains(Point(x, y)):
                            matrice_classes[j, i] = polygon_class

    # Save a version before cutting
    matrice_classes_bis = np.copy(matrice_classes)

    # List to store the cut matrices according to the netcdf files
    matrices_polygon = []

    # Cut the matrix to have one for each netcdf file
    for i in cut:
        # Cut the matrice_classes using the appropriate indices
        matrice_decoupee = matrice_classes_bis[:, range(i)]
        matrice_classes_bis = np.delete(matrice_classes_bis, range(i), axis=1)
        # Add the cut matrix to the list
        matrices_polygon.append(matrice_decoupee)

    matrypage_polygon = []

    for i in range(len(matrices_polygon)):
        # Get the key at index i
        key_at_i = ecointfine_keys[i]
        # Access the dataset using the key
        nc_file = EcoIntfine[key_at_i]
        bs = nc_file["Sonar"]["Grid_group1"]["integrated_backscatter"][
            result_matrypage[1][i]
        ].data
        depth = nc_file["Sonar"]["Grid_group1"]["cell_depth"]
        depth = depth[:, 1]
        depth_index = np.where((depth >= start_range) & (depth <= max_range))
        bs = bs[:, depth_index, 0]
        bs = np.squeeze(bs)

        # Initialize matrypage and polygons
        matrypage = result_matrypage[0][i]
        polygon = matrices_polygon[i]

        # Create a fish mask based on the classes
        CLUP = np.isin(polygon, ["D2", "D4", "D8", "D9", "D13"])
        # Add the condition that the cell is not null
        mask_CLUP = np.logical_and(CLUP, bs.T < 0)
        # mask_CLUP = CLUP
        NONCLUP = np.isin(polygon, ["D1", "D3", "D5", "D14"])
        # Add the condition that the cell is not null
        mask_NONCLUP = np.logical_and(NONCLUP, bs.T < 0)  # TODO actually nonnan
        # mask_NONCLUP = NONCLUP

        # Create a mask for other classes
        autre = np.isin(polygon, ["D11", "D12"])
        mask_autre = np.logical_and(autre, bs.T < 0)
        # mask_autre = autre

        # Only fish cells within the echo integration threshold are considered as fish
        matrypage[mask_CLUP] = 1
        matrypage[mask_NONCLUP] = 2
        matrypage[mask_autre] = 0

        # Update the original matrypage with changes from the copy
        matrypage_polygon.append(matrypage)

    return matrypage_polygon
