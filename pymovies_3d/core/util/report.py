"""
Tool used to recreate report on file processing,

"""
import sys
from pathlib import Path
from typing import List, Dict, Set, Optional
from collections import namedtuple

class Report:
    """
    Report class, roughly a dict indexed by channel (kind of error or warning), with for each channel stored
    """
    Message = namedtuple("Message",["level","content","radial"])  
    values:Dict[str,List[Message]] = {}

    radial_errors:Set[str] = set() #set of radial names that have some errors

    def __init__(self):
        """
        """
        self.separator:str=";"
        self.returnline:str="\n"

    def print(self,file:Optional[Path]=None):
        """print content values"""
        def __print_stream(stream):
            stream.write(f"channel{self.separator}level{self.separator}radial{self.separator}content{self.returnline}")

            for channel,msg_list in self.values.items():
                msg_list = self.values[channel]
                for msg in msg_list:
                    stream.write(f"{channel}{self.separator}{msg.level}{self.separator}{msg.radial}{self.separator}{msg.content}{self.returnline}")

        #print to stdout
        __print_stream(stream=sys.stdout)
        #then to file
        if file is not None:
            with open(file=str(file),mode='w',encoding = "utf8") as f:
                __print_stream(f)
     
    def has_errors(self):
        return len(self.radial_errors)>0

    def info(self,msg:str,channel:str,radial_name:str=""):
        """report an information message"""
        if channel not in self.values:
            self.values[channel]=[]
        self.values[channel].append(Report.Message(level="INFO",content=msg,radial=radial_name))  
       
    def warn(self,msg:str,channel:str,radial_name:str=""):
        """report a warning message"""
        if channel not in self.values:
            self.values[channel]=[]
        self.values[channel].append(Report.Message(level="WARNING",content=msg,radial=radial_name))

    def error(self,msg:str,channel:str,radial_name:str):
        if channel not in self.values:
            self.values[channel] = []
        self.values[channel].append(Report.Message(level="ERROR",content=msg,radial=radial_name))
        self.radial_errors.add(radial_name)
#        raise Exception(msg)

