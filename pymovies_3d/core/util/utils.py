# -*- coding: utf-8 -*-
"""
Created on Fri Apr 17 16:31:04 2020

@author: Guillaume Brosse
"""

import datetime
import pytz

# conversion de date et heure en chaine de caract au format datetime


def convert_string_to_datetime(date_time_str):
    date_time_obj = []
    utc_tz = pytz.utc
    for date_time_str_i in date_time_str:
        date_time_naive = datetime.datetime.strptime(
            date_time_str_i, "%d/%m/%Y%H:%M:%S"
        )
        date_time_obj.append(date_time_naive.replace(tzinfo=utc_tz))

    return date_time_obj
