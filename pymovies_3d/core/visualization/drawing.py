﻿#!/usr/bin/python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
from matplotlib import colors

# fonction originale
def DrawEchoGramIni(echoData, title=None, bottoms=None):

    # Définition d'une nouvelle figure
    fig, ax = plt.subplots()

    # Définition de la liste des couleurs pour la colormap
    cmap = colors.ListedColormap(
        list(
            reversed(
                [
                    "#000000",
                    "#800000",
                    "#ac0000",
                    "#d00000",
                    "#f00000",
                    "#ff0000",
                    "#ff7979",
                    "#ff8040",
                    "#ff8000",
                    "#ffff00",
                    "#ffff80",
                    "#00ff00",
                    "#80ff80",
                    "#0080c0",
                    "#0080ff",
                    "#80ffff",
                    "#C0C0C0",
                    "#FFFFFF",
                ]
            )
        )
    )
    # cmap=None

    # On affiche l'échogramme
    cax = plt.imshow(echoData, cmap=cmap, vmin=-80, vmax=-30, aspect="auto")

    # Affichage de la colorbar
    cbar = fig.colorbar(cax)

    # Affichage d'un titre
    if title is not None:
        ax.set_title(title)

    # Affichage du fond
    if bottoms is not None:
        plt.autoscale(
            False
        )  # Pour que les dimensions de la fenêtres soient calées sur l'image originale
        plt.plot(bottoms, "w", linewidth=2)

    # Affichage de la grille
    plt.grid()

    # Affichage de la/les figures définies ci-dessus
    plt.show()


# Fonction DrawEchoGram a été modifiée dans un soucis pratique : ajout de extent dans le imshow


def DrawEchoGram(echoData, depth, title=None, bottoms=None):

    # Définition d'une nouvelle figure
    # fig, ax = plt.subplots()

    # Définition de la liste des couleurs pour la colormap
    cmap = colors.ListedColormap(
        list(
            reversed(
                [
                    "#000000",
                    "#800000",
                    "#ac0000",
                    "#d00000",
                    "#f00000",
                    "#ff0000",
                    "#ff7979",
                    "#ff8040",
                    "#ff8000",
                    "#ffff00",
                    "#ffff80",
                    "#00ff00",
                    "#80ff80",
                    "#0080c0",
                    "#0080ff",
                    "#80ffff",
                    "#C0C0C0",
                    "#FFFFFF",
                ]
            )
        )
    )
    # cmap=None

    # On affiche l'échogramme
    plt.imshow(
        echoData,
        cmap=cmap,
        extent=[0, len(echoData[0]), -depth[0, (len(depth[0]) - 1)], -depth[0, 0]],
        aspect="auto",
        vmin=-100,
        vmax=-30,
    )

    # Affichage de la colorbar
    # cbar = plt.colorbar(cax)

    # Affichage d'un titre
    if title is not None:
        plt.title(title)

    # Affichage du fond
    if bottoms is not None:
        plt.autoscale(
            False
        )  # Pour que les dimensions de la fenêtres soient calées sur l'image originale
        plt.plot(bottoms, "w", linewidth=2)

    # Affichage de la grille
    plt.grid()

    # Affichage de la/les figures définies ci-dessus
    plt.show()


# DrawMultiEchogram permet d'afficher plusieurs echogrammes dans la même figure


# fonction pour afficher plusieurs echogrammes dans la même figure
def DrawMultiEchoGram(
    echoData, depth, nb_echograms, freqs=None, title=None, bottoms=None
):

    multi_title = title
    for x in range(nb_echograms):
        plt.subplot((int)((nb_echograms + 1) / 2), 2, x + 1)
        plt.xlabel("N*ESU")
        plt.ylabel("profondeur (en m)")
        if title is not None:
            if freqs is not None:
                multi_title = title % (freqs[x])
        DrawEchoGram(echoData[x], depth, title=multi_title, bottoms=bottoms)

    plt.subplots_adjust(bottom=0.1, right=0.84, top=0.9)
    cax = plt.axes([0.85, 0.1, 0.01, 0.8])
    cbar = plt.colorbar(cax=cax)
    cbar.set_label("intensité acoustique en dB")
    plt.show()
