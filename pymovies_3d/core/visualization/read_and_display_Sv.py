﻿#!/usr/bin/python
# -*- coding: utf-8 -*-

import calendar
# import plotly.express as px
import math
import os
import time
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from pyMovies import pyMovies as mv
from pymovies_3d.core.hac_util.hac_util import hac_goto
from pymovies_3d.core.visualization.drawing import DrawEchoGramIni

MaxRange = 15

# Set path to test .HAC file in pymovies_3D repository
test_dir = Path("Ref_data/")
file_name = test_dir.joinpath("ESTECH17_CW_20170922_144241.hac").absolute()

# chemin_cfg = "C:/Users/nlebouff/Desktop/Config_M3D/"
# mv.moLoadConfig(chemin_cfg)

# Open .HAC file in pymovies_3D repository
file = mv.moOpenHac(str(file_name))
FileStatus = mv.moGetFileStatus()

# Go to .HAC file start and read data
datestart = calendar.timegm(time.strptime("2017/09/22 14:42:41", "%Y/%m/%d %H:%M:%S"))
dateend = calendar.timegm(time.strptime("2017/09/22 14:43:39", "%Y/%m/%d %H:%M:%S"))

FileStatus = mv.moGetFileStatus()
hac_goto(datestart)
last_time = datestart
while (not FileStatus.m_StreamClosed) & (last_time < dateend):
    mv.moReadChunk()
    MX = mv.moGetPingFan(mv.moGetNumberOfPingFan() - 1)
    last_time = MX.m_meanTime.m_TimeCpu + MX.m_meanTime.m_TimeFraction / 10000

list_sounder = mv.moGetSounderDefinition()
nb_snd = list_sounder.GetNbSounder()

print("nb sondeurs = " + str(nb_snd))
for isdr in range(nb_snd):
    sounder = list_sounder.GetSounder(isdr)
    nb_transduc = sounder.m_numberOfTransducer
    print(
        "sondeur "
        + str(isdr)
        + ":    index: "
        + str(sounder.m_SounderId)
        + "   nb trans:"
        + str(nb_transduc)
    )
    for itr in range(nb_transduc):
        trans = sounder.GetTransducer(itr)
        for ibeam in range(trans.m_numberOfSoftChannel):
            softChan = trans.getSoftChannelPolarX(ibeam)
            print(
                "   trans "
                + str(itr)
                + ":    nom: "
                + trans.m_transName
                + "   freq: "
                + str(softChan.m_acousticFrequency / 1000)
                + " kHz"
            )

# Select sounder and channel
index_sondeur = 0
index_trans = 1
sounder = list_sounder.GetSounder(index_sondeur)
transducer = sounder.GetTransducer(index_trans)
nb_beams = transducer.m_numberOfSoftChannel
nb_pings = mv.moGetNumberOfPingFan()
nb_ech = int(math.floor(MaxRange / transducer.m_beamsSamplesSpacing))

dpth_ech = np.full([nb_pings, nb_beams], np.nan)
time_ping = np.full(nb_pings, np.nan)
u = np.full([nb_pings, nb_ech, nb_beams], np.nan)
along = np.full([nb_pings, nb_ech, nb_beams], np.nan)
athwart = np.full([nb_pings, nb_ech, nb_beams], np.nan)
indexping = 0

for index in range(nb_pings):
    MX = mv.moGetPingFan(index)
    SounderDesc = MX.m_pSounder
    if SounderDesc.m_SounderId == sounder.m_SounderId:
        SounderDesc_ref = SounderDesc
        polarMat = MX.GetPolarMatrix(index_trans)
        echoValues = np.array(polarMat.m_Amplitude) / 100.0
        alongValues = np.array(polarMat.m_AlongAngle)
        athwartValues = np.array(polarMat.m_AthwartAngle)
        if np.size(alongValues) > 0:
            for iEcho in range(min(len(polarMat.m_Amplitude), nb_ech)):
                u[indexping, iEcho, :] = echoValues[iEcho, :]
                along[indexping, iEcho, :] = alongValues[iEcho, :]
                athwart[indexping, iEcho, :] = athwartValues[iEcho, :]
        else:
            for iEcho in range(min(len(polarMat.m_Amplitude), nb_ech)):
                u[indexping, iEcho, :] = echoValues[iEcho, :]

        if SounderDesc.m_isMultiBeam:
            for b in range(nb_beams):
                if MX.beam_data[b].m_bottomWasFound:
                    dpth_ech[indexping, b] = (
                        MX.beam_data[b].m_bottomRange
                        / SounderDesc.GetTransducer(index_trans).m_beamsSamplesSpacing
                    )
        else:
            dpth_ech[indexping] = (
                MX.beam_data[index_trans].m_bottomRange
                / SounderDesc.GetTransducer(index_trans).m_beamsSamplesSpacing
            )
        time_ping[indexping] = (
            MX.m_meanTime.m_TimeCpu + MX.m_meanTime.m_TimeFraction / 10000
        )
        indexping = indexping + 1

u.resize((indexping, nb_ech, nb_beams))
time_ping.resize((indexping))
dpth_ech.resize((indexping, nb_beams))

steering = np.zeros([nb_beams])
for b in range(nb_beams):
    steering[b] = (
        SounderDesc_ref.GetTransducer(index_trans)
            .getSoftChannelPolarX(b)
            .m_mainBeamAthwartSteeringAngleRad
    )

dateindex = np.where(np.logical_and(time_ping > datestart, time_ping < dateend))

# Plot Sv and along and athwart angles echograms

for b in range(nb_beams):

    if abs(steering[b] * 180 / math.pi) < 1:

        # affichage
        DrawEchoGramIni(
            np.transpose(u[dateindex, :, b][0]),
            title=str(
                SounderDesc_ref.GetTransducer(index_trans)
                .getSoftChannelPolarX(b)
                .m_acousticFrequency
                / 1000
            )
                  + " kHz",
            bottoms=dpth_ech[dateindex, b][0] + 1.5,
        )

        if np.size(alongValues) > 0:
            plt.figure()
            plt.imshow(
                np.transpose(along[dateindex, :, b][0]) * 180 / np.pi, aspect="auto"
            )
            plt.title("Along angle (°)")
            plt.colorbar()

            plt.figure()
            plt.imshow(
                np.transpose(athwart[dateindex, :, b][0]) * 180 / np.pi, aspect="auto"
            )
            plt.title("Athwart angle (°)")
            plt.colorbar()
