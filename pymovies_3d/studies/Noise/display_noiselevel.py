from pymovies import pyMovies as mv

parameter = mv.moLoadReaderParameter()
mv.moLoadConfig('C:/Users/lberger/Desktop/bruitPropre/bruit_ref_ESTECH17')

mv.moOpenHac('C:/Users/lberger/Desktop/PELGAS21/PELGAS2021_025_20210520_155631.hac')

nl = mv.moNoiseLevel()
nl.setEnable(True)

fileStatus = mv.moGetFileStatus()

esuManager = mv.moGetEsuManager()
esuContainer = esuManager.GetEsuContainer()

nbEsu = 0
esuContainer.GetESUNb()
while not fileStatus.m_StreamClosed :
    mv.moReadChunk()
    fileStatus = mv.moGetFileStatus()
    
    nbEsuStart = nbEsu
    nbEsuEnd = esuContainer.GetESUNb()
    nbEsu = nbEsuEnd
    for esuIdx in range(nbEsuStart, nbEsuEnd) :        
        esu = esuContainer.GetESUWithIdx(esuIdx)
        esuResult = nl.GetResults(esu)
        print(' esu id : {esu.m_esuId}')
        for itEsuResult in esuResult :
            sounderId = itEsuResult.key()
            sounderResult = itEsuResult.data()
            print(f'  result {sounderId} : ')
            
            print(f'   nombre de pings passifs')
            for it in sounderResult.nbPassivePings : 
                print(f'    {it.key()} : {it.data()}')
            
            print(f'   niveau de bruit')
            for it in sounderResult.noiseLevels : 
                print(f'    {it.key()} : {it.data()}')
            
            print(f'   niveau de bruit de reference')
            for it in sounderResult.refNoiseLevels : 
                print(f'    {it.key()} : {it.data()}')
            
            print(f'   portée')
            for it in sounderResult.ranges : 
                print(f'    {it.key()} : {it.data()}')
            
            print(f'   portée de référence')
            for it in sounderResult.refRanges : 
                print(f'    {it.key()} : {it.data()}')