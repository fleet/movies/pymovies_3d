# -*- coding: utf-8 -*-
"""
Created on Wed Sep  8 11:21:05 2021

@author: amaurice
"""

import pytz

from pymovies_3d.core.echointegration.batch_ei_multi_threshold import (
    batch_ei_multi_threshold,
)
from pymovies_3d.core.echointegration.ei_bind import ei_bind

import os
import numpy as np
import pickle
import datetime
import time
import netCDF4 as nc
import numpy.ma as ma
import scipy

from matplotlib import pyplot as plt
import matplotlib.dates as md
from matplotlib import colors
import matplotlib.patches as mpatches

import math
import numpy.matlib
import pymovies_3d.visualization.drawing as dr
import pymovies_3d.core.log_events.import_casino as ic

import csv
import glob2
import pymovies_3d.core.hac_util.hac_util as au

import pymovies_3d.core.echointegration.ei_survey as ei
import pymovies_3d.core.echointegration.ei_bind as ei_bind
import pymovies_3d.core.echointegration.ei_standardizing as standardizing
import pymovies_3d.core.echointegration.ei_classification as classif
import pymovies_3d.core.util.utils as ut
import pymovies_3d.visualization.map_drawing as md

from sklearn.manifold import LocallyLinearEmbedding
from sklearn.cluster import KMeans
import re

import scipy.cluster.hierarchy as sc
from sklearn.cluster import AgglomerativeClustering

import pymovies_3d.core.TS_analysis.samplecomputeTS as TS
import pymovies_3d.core.TS_analysis.TS_bind as ts_bind




def sorted_alphanumeric(data):
    """
    Fonction permettant de parcourir les chemins vers les fichiers dans le bon ordre
    La fonction sort() ne marchant pas avec les valeurs alpha-numériques
    Parameters
    ----------
    data : LISTE DE NOMS DE FICHIERS DANS UN ORDRE ALEATOIRE

    Returns
    -------
    LISTE DE NOMS DE FICHIERS TRIEE

    """
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(data, key=alphanum_key)

def calcDistance3D(lat1, lon1, lat2, lon2, d1, d2):
    a = 6378137
    b = 6356752
    e2 = 1-(b**2/a**2)
    X1 = (a/np.sqrt(1-e2*np.sin(math.radians(lat1))**2)-d1)*np.cos(math.radians(lat1))*np.cos(math.radians(lon1))
    Y1 = (a/np.sqrt(1-e2*np.sin(math.radians(lat1))**2)-d1)*np.cos(math.radians(lat1))*np.sin(math.radians(lon1))
    Z1 = ((b**2/a**2)*a/np.sqrt(1-e2*np.sin(math.radians(lat1))**2)+d1)*np.sin(math.radians(lat1))
    X2 = (a/np.sqrt(1-e2*np.sin(math.radians(lat2))**2)-d2)*np.cos(math.radians(lat2))*np.cos(math.radians(lon2))
    Y2 = (a/np.sqrt(1-e2*np.sin(math.radians(lat2))**2)-d2)*np.cos(math.radians(lat2))*np.sin(math.radians(lon2))
    Z2 = ((b**2/a**2)*a/np.sqrt(1-e2*np.sin(math.radians(lat2))**2)+d2)*np.sin(math.radians(lat2))
    dist3D = np.sqrt((X2-X1)**2 + (Y2-Y1)**2 + (Z2-Z1)**2)
    return dist3D,X1,Y1,X2,Y2,Z1,Z2

def calcHeading(lat1, long1, lat2, long2): 
    #https://stackoverflow.com/questions/47659249/calculate-cardinal-direction-from-gps-coordinates-in-python
    dLon = (long2 - long1)
    x = math.cos(math.radians(lat2)) * math.sin(math.radians(dLon))
    y = math.cos(math.radians(lat1)) * math.sin(math.radians(lat2)) - math.sin(math.radians(lat1)) * math.cos(math.radians(lat2)) * math.cos(math.radians(dLon))
    bearing = math.atan2(x,y)  
    bearing = math.degrees(bearing)

    return bearing

def calcAngleVect(X1,Y1,X2,Y2):
    u=np.array([X1,Y1])
    v=np.array([X2,Y2])
    angle = np.math.atan2(np.linalg.det([u,v]),np.dot(u,v))
    angle=np.degrees(angle)
    return angle

path_evt='H:/WBAT/CasinoEchosondeBis_Leg2_avecTrouDonnee.txt'
#path_evt='H:/CasinoManuel_bistxt.txt'
#path_evt='H:/ECHOSONDE2018/Casino_manuel_Echosonde2018.txt'
#path_evt='E:/ECHOSONDE2019-2/CasinoManuelEchoSonde2019-2.csv'
#path_evt='E:/ECHOSONDE2019-1/CasinoManuelEchoSonde2019-1 - Copie.csv'
#path_config ='H:/Config_M3D_bis/configsAnalyseM3D_Echosonde/EIlay'
path_config ='H:/WBAT/configMovies3Dter_chgt_offsets_TS'
thresholds = [-80] #[-84]
path_config = path_config + "/" + str(thresholds[0])
#path_hac_survey = 'H:/Echosonde_rearchivage/SBES_MBES/ECHOSONDE2017-1'
path_hac_survey='H:/WBAT/HERMES/LEG2/SBES_MBES/ECHOSONDE_BIS/RUN127_129/'
#path_hac_survey='H:/Echosonde_rearchivage/SBES_MBES/ECHOSONDE2018/RUN1011'
# path_config ='E:/Config_M3D_bis/configsAnalyseM3D/EIlay'
# thresholds = [-80]
#path_hac_survey = 'E:/ACOU03_33/'
type_campagne='echosonde_bis'
if type_campagne=='echosonde':
    analyse_couple_sondeur='non'
    analyse_par_saison='oui'
else:
    analyse_couple_sondeur='non'
    analyse_par_saison='non'
# Chemins linux
# path_hac_survey = '/echosonde/data/PHOENIX2018/HERMES'
# path_config = '/echosonde/data/PHOENIX2018/Config_M3D/ConfigsAnalyseM3D/EIlay'

# EI, modification ou non de l'EI et classification
var_choix='y'
save_data = True
reorganisation_EI = True
clustering = True
methode_EI = 'netcdf'

# Sélection des radiales, transducteurs et fréquences d'interêt 
ind_decoup=list(range(0,1442,1))
indice_freq_max=342

if analyse_couple_sondeur=='oui':
    nbfreqs = 2
    nb_transduc=2
    #etude par couple de transducteur ou par transducteur sur les radiales où un seul transducteur est présent
    if nb_transduc==2:
        indices_transduc_choisis = np.array([0,1])
    
        # indice_freq_min1=45000
        # indice_freq_max1=90000
        indice_freq_min1=119500
        indice_freq_max1=121500
        # indice_freq_min1=90000
        # indice_freq_max1=150000
        # indice_freq_min1=90000
        # indice_freq_max1=170000
        indice_freq_min2=170000
        indice_freq_max2=260000
        # indice_freq_min2=280000
        # indice_freq_max2=445000
        freq_nominale1=120000
        freq_nominale2=200000
        freq_custom =[freq_nominale1,freq_nominale2]  
    else:
        indices_transduc_choisis = np.array([0])

        indice_freq_min=119500
        indice_freq_max=120500
        # indice_freq_min=90000
        # indice_freq_max=170000
        freq_nominale=120000
        freq_custom =[freq_nominale]
else:
    nbfreqs = 1
    indices_transduc_choisis = np.array([0])
    nb_transduc=1
    freq_custom =[70000]

# Classification de la donnée
nombre_de_categories_sv=3
#pas=1000
pas=200
methode_clustering="LLE_KMeans"
type_acq_TS='ECHOSONDE_BIS'

#Extension de la zone de levé
extension_zone=[-3.05, -2.45, 47, 47.4]

indexSounder = 0    
indexTransducer = 0    
critere_nb_detect_TS = 4#4
critere_ect_TS = 2
nombre_de_categories_ts=5

################################################################################
#DEBUT DU CALCUL
################################################################################
# C, Entete, index_events, date_heure = ic.import_casino(path_evt)
# (
#     datedebut,
#     datefin,
#     heuredebut,
#     heurefin,
#     nameTransect,
#     vit_vent_vrai,
#     dir_vent_vrai,
# ) = ic.infos_radiales(C)

# C, Entete, index_events, date_heure = ic.import_casino(path_evt)
# (
#     datedebut,
#     datefin,
#     heuredebut,
#     heurefin,
#     nameTransect,
#     vit_vent_vrai,
#     dir_vent_vrai,
# ) = ic.infos_radiales_echosonde(C)

C, Entete, index_events, date_heure = ic.import_casino_echosonde_bis(path_evt)
(
    datedebut,
    datefin,
    heuredebut,
    heurefin,
    nameTransect,
) = ic.infos_radiales_echosonde_bis(C)

# supression des variables qui ne sont plus utiles
del C, Entete, date_heure

indsuppr=[]

#on recherche les radiales non uniques et on les supprime
transects_doublons = [idx for idx, val in enumerate(nameTransect) if val in nameTransect[:idx]]
indsuppr.extend(transects_doublons)
indsuppr.sort()

nbsuppr = 0
for i in range(len(indsuppr)):
    nbsuppr = nbsuppr + 1
    del (
        datedebut[indsuppr[i] - nbsuppr],
        datefin[indsuppr[i] - nbsuppr],
        heuredebut[indsuppr[i] - nbsuppr],
        heurefin[indsuppr[i] - nbsuppr],
        nameTransect[indsuppr[i] - nbsuppr],
    )

datedebutini=datedebut
heuredebutini=heuredebut
datefinini=datefin
heurefinini=heurefin

len_Sv1 = []
unites = np.array([])

compteur=0

for indexd in ind_decoup:
    premiere_date=indexd
    derniere_date=indexd+1
    
    datedebut = datedebutini[premiere_date:derniere_date]
    heuredebut = heuredebutini[premiere_date:derniere_date]
    datefin=datefinini[premiere_date:derniere_date]
    heurefin=heurefinini[premiere_date:derniere_date]
    
    date_time_debut= []
    date_time_fin= []
    for i in range (len(datedebut)):
        date_time_debut.append("%s%s" %(datedebut[i],heuredebut[i]))
        date_time_fin.append("%s%s" %(datefin[i],heurefin[i]))
        
        
    #path windows
    # survey_name = 'Echosonde2018'+'_'+str(premiere_date)+'_'+str(derniere_date)
    # fpath='H:/Rearchivage/ECHOSONDE2018/70_120_200kHz/Echosonde2018_netcdf/Result/'
    # path_save = fpath + survey_name + "/"   
    # survey_name = 'PHOENIX18'+'_'+str(premiere_date)+'_'+str(derniere_date)
    # fpath='E:/ACOU37_50_netcdf/Result/'
    # path_save = fpath + survey_name + "/"    
    survey_name = 'ECHOSONDE_BIS'+'_'+str(premiere_date)+'_'+str(derniere_date)
    fpath='H:/WBAT/ResultTS_Echosonde_bis_leg2/'
    #fpath='H:/WBAT/ResultTS_readerPfan50_80dB_ect2_nbDetec5_618_bis/'
    path_save = fpath + survey_name + "/" 
    #path_Fnetcdf= path_file_netcdf+survey_name+'/'


    path_csv = path_save + "csv/"
    path_png = path_save + "png/"
    path_html = path_save + "html/"
    path_netcdf = path_save + "netcdf/"
    path_txt = path_save + "txt/"
    # If save path does not exist, create it
    if not os.path.exists(path_save):
        os.makedirs(path_save)
    
    if not os.path.exists(path_csv):
        os.makedirs(path_csv)
    
    if not os.path.exists(path_png):
        os.makedirs(path_png)
    
    if not os.path.exists(path_html):
        os.makedirs(path_html)
    
    if not os.path.exists(path_netcdf):
        os.makedirs(path_netcdf)
        
    if not os.path.exists(path_txt):
        os.makedirs(path_txt)
    
    # filename_EK80 = "%s/%s-TH%d-EK80-EIlay.pickle" % (path_save, survey_name, thresholds[0])

       
    # if save_data:
    #     filename_data_EK80 = "%s/%s-TH%d-saved_data_for_EK80_all.pickle" % (path_save, survey_name, thresholds[0])

    nameTube = nameTransect[indexd]
    #A COMMENTER POUR RECHARGER LES DONNEES OU METTRE var_choix="n"
    if var_choix == "y":
        TS.sample_compute_TS(
            path_hac_survey,
            path_config,
            path_save,
            nameTransect=nameTube,
            dateStart=datedebut,
            timeStart=heuredebut,
            dateEnd=datefin,
            timeEnd=heurefin,
        )
    
    filename = path_save + "/" + nameTube + "_results_TS.pickle"
    #filename = path_Fnetcdf + nameTube + "_results_TS.pickle"
    
    print("chargement des donnees...")
    with open(filename, "rb") as f:
        (
            timeTarget_all,
            TSrange_all,
            TScomp_all,
            TSucomp_all,
            TSalong_all,
            TSathwart_all,
            TSposition_all,
            TSpositionGPS_all,
            TSlabel_all,
            TSfreq_all,
            name_sdr_tsd,
        ) = pickle.load(f)
        
    
    Time = np.squeeze(np.asarray(timeTarget_all[indexSounder][indexTransducer]))
    TSvalues = np.squeeze(np.asarray(TScomp_all[indexSounder][indexTransducer]))
    TSposition = np.squeeze(np.asarray(TSpositionGPS_all[indexSounder][indexTransducer]))
    TSfreq = np.unique(TSfreq_all[indexSounder][indexTransducer])
    TSlabel = np.squeeze(np.asarray(TSlabel_all[indexSounder][indexTransducer]))
    TSmean = np.zeros(np.size(TSfreq))
    for f in range(np.size(TSfreq)):
        TSmean[f] = np.mean(
            TSvalues[np.where(TSfreq_all[indexSounder][indexTransducer] == TSfreq[f])]
        )
            
    TS_values_new=TSvalues.reshape((-1,len(TSfreq)))
    realTS=TS_values_new
    realTS_mean=np.array([])
    for x in np.unique(TSlabel):
        if len(np.where(TSlabel==x)[0])>critere_nb_detect_TS:
            indice = np.where(TSlabel == x)
            if np.std(np.mean(realTS[indice,:],1))<critere_ect_TS:
                realTS_mean=np.append(realTS_mean,np.mean(realTS[indice,:],1))
    
    realTS_mean=realTS_mean.reshape((-1,len(TSfreq)))  
    if len(realTS_mean)<nombre_de_categories_ts:
        com=np.array([])
        com=np.append(com,"nombre de TS insuffisant")
        np.savetxt(path_save+'/'+'rapport_erreurs.txt',com,fmt='%s')
        continue
    
    TSpos_mean=np.array([])
    #bearing=np.array([])
    angle=np.array([])
    v = np.array([])
    time_spot_target=np.array([])
    for x in np.unique(TSlabel):
        l=[]
        ld=[]
        if len(np.where(TSlabel==x)[0])>critere_nb_detect_TS:
            indice = np.where(TSlabel == x)
            l.append(indice[0][0])
            l.append(indice[0][-1])
            if np.std(np.mean(realTS[indice,:],1))<critere_ect_TS:
                TSpos_mean=np.append(TSpos_mean,np.mean(TSposition[indice,:],1))
                #bearing=np.append(bearing,calcHeading(TSposition[l[0],0],TSposition[l[0],1],TSposition[l[-1],0],TSposition[l[-1],1]))
                #v=[]
                lat1, lon1,d1, lat2, lon2, d2 = TSposition[l[0],:][0],TSposition[l[0],:][1],TSposition[l[0],:][2],TSposition[l[-1],:][0],TSposition[l[-1],:][1],TSposition[l[-1],:][2]
                X1,Y1,X2,Y2=calcDistance3D(lat1, lon1, lat2, lon2, d1, d2)[1:5]
                angle=np.append(angle,calcAngleVect(X1,Y1,X2,Y2))
                time_spot_target=np.append(time_spot_target,timeTarget_all[0][0][l[0]].strftime("%d/%m/%y %H:%M:%S"))
                #if len(np.where((np.sort(timeTarget_all[0][0])==timeTarget_all[0][0])==False)[0]):
                diff_t=timeTarget_all[0][0][l[-1]]-timeTarget_all[0][0][l[0]]
                indi=0
                d3d=np.array([])
                d3d=np.append(d3d,calcDistance3D(TSposition[indice[0][indi],0],TSposition[indice[0][indi],1],TSposition[indice[0][indi+1],0],TSposition[indice[0][indi+1],1],TSposition[indice[0][indi],2],TSposition[indice[0][indi+1],2])[0])
                for indi in range(2,len(indice[0])):
                    d3d=np.append(d3d,calcDistance3D(TSposition[indice[0][indi-1],0],TSposition[indice[0][indi-1],1],TSposition[indice[0][indi],0],TSposition[indice[0][indi],1],TSposition[indice[0][indi-1],2],TSposition[indice[0][indi],2])[0])
                D3d = np.cumsum(d3d)[-1]
                v=np.append(v,D3d/diff_t.seconds)
                
    TSpos_mean=TSpos_mean.reshape((-1,TSposition.shape[1]))
    #bearing=bearing.reshape((-1,1))
    angle=angle.reshape((-1,1))
    v=v.reshape((-1,1))
    #BV = np.hstack((bearing,v))

    np.savetxt(path_txt+'TS_moyen_parCibleTraquee.txt',realTS_mean)
    np.savetxt(path_txt+'PositionMoyenne_parCibleTraquee.txt',TSpos_mean)
    #np.savetxt(path_txt+'Direction_parCibleTraquee.txt',bearing)
    np.savetxt(path_txt+'ChgtDirection_parCibleTraquee.txt',angle)
    np.savetxt(path_txt+'Vitesse_parCibleTraquee.txt',v)
    np.savetxt(path_txt+'Temps_reperage_cible.txt',time_spot_target,fmt='%s')


        
    #display TS en depth histograms and mean TS as a function of frequency
    num_bins = 10
    plt.figure(2, figsize=(30, 15))
    plt.clf()
    plt.tight_layout()
    # the histogram of TS
    n, bins, patches = plt.hist(TSvalues, num_bins)#, normed=1)
    plt.xlabel("TS (dB)")
    plt.grid(True)
    plt.savefig(path_png + "/hist_TS_freq.png")
    plt.close()
        
    plt.figure(3, figsize=(30, 15))
    plt.clf()
    plt.tight_layout()
    # the histogram of the depth
    n, bins, patches = plt.hist(
        TSposition[:, 2], num_bins, facecolor="blue", alpha=0.5
    )
    plt.xlabel("Depth (m)")
    plt.grid(True)
    plt.savefig(path_png + "/hist_TS_depth.png")
    plt.close()
    
    plt.figure(4, figsize=(30, 15))
    plt.clf()
    plt.tight_layout()
    # the mean TS
    plt.plot(np.squeeze(TSfreq / 1000), np.squeeze(TSmean))
    plt.xlabel("Frequency (kHz)")
    plt.ylabel("TS (dB)")
    plt.grid(True)
    plt.savefig(path_png + "/hist_meanTS_freq.png")
    plt.close()
            
    # plt.figure(5, figsize=(30, 15))
    # x=np.linspace(0,nombre_de_categories_ts,len(Sv))
    # if type_acq_TS=='TUBE':
    #     y=np.arange(-Depth[1,0]-float(prof_tube[index]),-Depth[1,(len(Depth[1])-1)]-1-float(prof_tube[index]),-1)
    # else:
    #     y=np.arange(-Depth[1,0],-Depth[1,(len(Depth[1])-1)]-1,-1)            
    # plt.pcolormesh(x,y,np.transpose(labels_sv), cmap=cmap)
    # plt.xlabel("Classes")
    # plt.ylabel("Profondeur du TUBE (m)")
    # plt.colorbar()  
  
    #choix d'utilisation de LLE si les dimensions le permettent
    if methode_clustering=='LLE_KMeans':
        nb_neighbors = 10
        if len(realTS_mean)>nb_neighbors:
            lle = LocallyLinearEmbedding(n_components=4, n_neighbors = nb_neighbors,method = 'modified', n_jobs = 4,  random_state=0)
            lle.fit(realTS_mean[:,:])
            X_lle = lle.transform(realTS_mean)
            kmeans =KMeans(n_clusters=nombre_de_categories_ts, random_state=0).fit(X_lle)
            # lle.fit(BV[:,:])
            # X_lle_bv = lle.transform(BV)
            # kmeans_bv =KMeans(n_clusters=nombre_de_categories_ts, random_state=0).fit(X_lle_bv)
            # kmeans_b =KMeans(n_clusters=nombre_de_categories_ts, random_state=0).fit(bearing)
            # kmeans_v =KMeans(n_clusters=nombre_de_categories_ts, random_state=0).fit(v)
        else:
            kmeans = KMeans(n_clusters=nombre_de_categories_ts, random_state=0).fit(realTS_mean)


    else:
        #choix d'utilisation de KMeans seul
        kmeans = KMeans(n_clusters=nombre_de_categories_ts, random_state=0).fit(realTS_mean)
        #kmeans_bv = KMeans(n_clusters=nombre_de_categories_ts, random_state=0).fit(BV)
        # kmeans_b =KMeans(n_clusters=nombre_de_categories_ts, random_state=0).fit(bearing)
        # kmeans_v =KMeans(n_clusters=nombre_de_categories_ts, random_state=0).fit(v)
    
    j=0
    vectLabel=np.empty([len(realTS_mean)])
    vectLabel.fill(np.nan)
    for i in range(len(vectLabel)):
        vectLabel[i]=kmeans.labels_[j]
        j=j+1
    
    labels = np.squeeze(np.reshape(vectLabel,(len(realTS_mean))))
    
    couleurs = plt.cm.jet(np.linspace(0, 1, nombre_de_categories_ts))
    cmap = colors.ListedColormap(couleurs)
    # else:
    #     couleurs = np.array([[0.        , 0.50392157, 1.        , 1.        ],
    #            [0.5       , 0.        , 0.        , 1.        ],
    #            [1.        , 0.58169935, 0.        , 1.        ],
    #            [0.        , 0.        , 0.5       , 1.        ],
    #            [0.49019608, 1.        , 0.47754586, 1.        ]])
    #     cmap = colors.ListedColormap(couleurs)
    
    # if type_acq_TS=='TUBE':
    #     plt.scatter(labels,-TSpos_mean[:,2]-float(prof_tube[index]),c=labels,cmap=cmap,s=100)
    # else:
    #     plt.scatter(realTS_mean,-TSpos_mean[:,2],c=labels,cmap=cmap,s=100)
    
    fig = plt.figure()
    ax = fig.add_subplot()
    ax.invert_xaxis()
    for f in range(len(TSfreq)):
        plt.scatter(realTS_mean[:,f],(-TSpos_mean[:,2]+30)*(-1),c=labels,cmap=cmap,s=25) 
        #(en transposant pour mettre surface et fond dans "bon sens" ?)
        
    plt.colorbar()
    plt.xlabel('Mean TS per track (dB)')
    plt.ylabel('Depth (m)')
    plt.savefig(path_png + "/TS_depth.png")
    plt.close()
    
    #plt.figure()
    Q1_ = np.zeros([nombre_de_categories_ts,len(TSfreq)])   
    Q3_ = np.zeros([nombre_de_categories_ts,len(TSfreq)])
    med = np.zeros([nombre_de_categories_ts,len(TSfreq)])            
    for j in range(nombre_de_categories_ts):
        liste_temp_q =np.squeeze(realTS_mean[np.where((np.squeeze(kmeans.labels_))==j),:])
        liste_temp_q=liste_temp_q.reshape((len(liste_temp_q),-1))
        if liste_temp_q.shape[1]==1:
            liste_temp_q=liste_temp_q.T
        Q1_[j,:]=np.quantile(liste_temp_q, 0.25,axis=0)
        Q3_[j,:]=np.quantile(liste_temp_q, 0.75,axis=0)
        med[j,:]=np.quantile(liste_temp_q ,0.5,axis=0)
        
    angleMLabel=[]
    vMoyLabel=[]
    posMoyLabel=[]
    TSMoyLabel=[]
    for x in np.unique(labels):
        indice = np.where(labels == x)
        angleMLabel.append(np.mean(angle[indice]))
        vMoyLabel.append(np.mean(v[indice]))
        posMoyLabel.append(np.mean(TSpos_mean[indice]))
        TSMoyLabel.append(np.mean(realTS_mean[indice]))
        
    angleMLabel=np.array(angleMLabel).reshape((len(angleMLabel),1))
    vMoyLabel=np.array(vMoyLabel).reshape((len(vMoyLabel),1))
    posMoyLabel=np.array(posMoyLabel).reshape((len(posMoyLabel),1))
    TSMoyLabel=np.array(TSMoyLabel).reshape((len(TSMoyLabel),1))
    
    np.savetxt(path_txt+'ChgtDirection_parCibleTraqueeMoyparLabel.txt',angleMLabel)
    np.savetxt(path_txt+'Vitesse_parCibleTraqueeMoyparLabel.txt',vMoyLabel)
    np.savetxt(path_txt+'TS_moyen_parLabel.txt',TSMoyLabel)
    np.savetxt(path_txt+'PositionMoyenne_parLabel.txt',posMoyLabel)    
    
    plt.figure(6, figsize=(30, 15))
    plt.xlabel('Freq (Hz)')
    plt.ylabel('TS dB')
    plt.title('TS par cluster')
    for j in range(nombre_de_categories_ts):
        plt.plot(TSfreq,Q1_[j],'-.',color=couleurs[j])
        plt.plot(TSfreq,Q3_[j],'-.',color=couleurs[j])
        plt.plot(TSfreq,med[j],'o-',color=couleurs[j])
    plt.grid()
    plt.savefig(path_png + "/TS_freq.png")
    plt.close()
    
    
    # plt.scatter(bearing,-TSpos_mean[:,2],c=labels,cmap=cmap,s=100)
        
    # plt.colorbar()
    # plt.savefig(path_png + "/TS_bearing.png")
    # plt.close()

    
    #plt.scatter(time_spot_target,v,c=labels_v,cmap=cmap,s=100)
    plt.figure(figsize=(30, 15))
    #plt.scatter(bearing,v,c=labels,cmap=cmap,s=100)
    plt.scatter(angle,v,c=labels,cmap=cmap,s=100)
        
    plt.colorbar()
    plt.xlabel('Difference between initial bearing and final bearing (deg)')
    plt.ylabel('Mean velocity of the track (m/s)')
    plt.savefig(path_png + "/TS_direction_velocity.png")
    plt.close() 

    # j=0
    # vectLabel=np.empty([len(bearing)])
    # vectLabel.fill(np.nan)
    # for i in range(len(vectLabel)):
    #     vectLabel[i]=kmeans.labels_[j]
    #     j=j+1
    
    # labels_b = np.squeeze(np.reshape(vectLabel,(len(bearing))))
    
    # couleurs = plt.cm.jet(np.linspace(0, 1, nombre_de_categories_ts))
    # cmap = colors.ListedColormap(couleurs)
    
    # plt.scatter(bearing,-TSpos_mean[:,2],c=labels_b,cmap=cmap,s=100)
        
    # plt.colorbar()
    # plt.savefig(path_png + "/TS_bearing_classifOnlybearing.png")
    # plt.close()

    # j=0
    # vectLabel=np.empty([len(v)])
    # vectLabel.fill(np.nan)
    # for i in range(len(vectLabel)):
    #     vectLabel[i]=kmeans.labels_[j]
    #     j=j+1
    
    # labels_v = np.squeeze(np.reshape(vectLabel,(len(v))))
    
    # couleurs = plt.cm.jet(np.linspace(0, 1, nombre_de_categories_ts))
    # cmap = colors.ListedColormap(couleurs)
    
    # #plt.scatter(time_spot_target,v,c=labels_v,cmap=cmap,s=100)
    # plt.scatter(bearing,v,c=labels_v,cmap=cmap,s=100)
        
    # plt.colorbar()
    # plt.savefig(path_png + "/TS_velocity_classifOnlyVelocity.png")
    # plt.close()        
        

    
    np.savetxt(path_txt+'Labels_RF.txt',labels)
    #np.savetxt(path_txt+'Labels_BV.txt',labels_bv)
    #np.savetxt(path_txt+'Labels_b.txt',labels_b)
    #np.savetxt(path_txt+'Labels_v.txt',labels_v)
    np.savetxt(path_txt+'Med_RF.txt',med)
    np.savetxt(path_txt+'Q1_RF.txt',Q1_)
    np.savetxt(path_txt+'Q3_RF.txt',Q3_)
    
    # export csv
    header = ["Time", "Latitude", "Longitude", "Depth", "TS (dB)", "Label Track"]
    for f in range(np.size(TSfreq)):
        if np.size(TSfreq) > 1:
            indexTS = np.squeeze(
                np.where(TSfreq_all[indexSounder][indexTransducer] == TSfreq[f])
            )
            with open(
                "%s/%s-TH%d-EK80-%s-%s-TS_freq_%dkHz.csv"
                % (
                    path_csv,
                    survey_name,
                    thresholds[0],
                    indexSounder,
                    indexTransducer,
                    TSfreq[f],
                ),
                "w",
                newline="",
            ) as csvfile:
                writer = csv.writer(csvfile, delimiter=" ")
                writer.writerow(h for h in header)
                for i in range(len(Time)):
                    writer.writerow(
                        (
                            Time[i].strftime("%d/%m/%Y %H:%M:%S"),
                            TSposition[i, 0],
                            TSposition[i, 1],
                            TSposition[i, 2],
                            TSvalues[indexTS[i]],
                            TSlabel[i],
                        )
                    )
        else:
            with open(
                "%s/%s-TH%d-EK80-%s-%s-TS_freq_%dkHz.csv"
                % (
                    path_csv,
                    survey_name,
                    thresholds[0],
                    indexSounder,
                    indexTransducer,
                    TSfreq,
                ),
                "w",
                newline="",
            ) as csvfile:
                writer = csv.writer(csvfile, delimiter=" ")
                writer.writerow(h for h in header)
                for i in range(len(Time)):
                    writer.writerow(
                        (
                            Time[i].strftime("%d/%m/%Y %H:%M:%S"),
                            TSposition[i, 0],
                            TSposition[i, 1],
                            TSposition[i, 2],
                            TSvalues[i],
                            TSlabel[i],
                        )
                    )
                    
    
#fig = plt.figure()
#ax = fig.add_subplot(projection='3d')
#ax.scatter(X1, Y1, Z1)  

##############################################################################
#RECOMBINAISON TS
##############################################################################
      
nombre_de_categories_sv_campEntiere=5
dirlistQ1_ = []
dirlistQ3_ = []
dirlistMed = []
#dirlistLabels=[]
dirlistBearing=[]
dirlistV=[]
dirlistPos=[]
dirlist_TSmoy=[]
for root, subFolders, files in os.walk(fpath):
    print(root,subFolders,files)
    for file in files:
        print(file)
        if file.endswith("Q1_RF.txt"):
            dirlistQ1_.append(os.path.join(root, file))
        elif file.endswith("Q3_RF.txt"):
            dirlistQ3_.append(os.path.join(root, file))
        elif file.endswith("Med_RF.txt"):
            dirlistMed.append(os.path.join(root, file))
        # elif file.endswith("Labels_RF.txt"):
        #     dirlistLabels.append(os.path.join(root, file))
        elif file.endswith("ChgtDirection_parCibleTraqueeMoyparLabel.txt"):
            dirlistBearing.append(os.path.join(root, file))
        elif file.endswith("Vitesse_parCibleTraqueeMoyparLabel.txt"):
            dirlistV.append(os.path.join(root, file))
        elif file.endswith("PositionMoyenne_parLabel.txt"):
            dirlistPos.append(os.path.join(root, file))
        elif file.endswith("TS_moyen_parLabel.txt"):
            dirlist_TSmoy.append(os.path.join(root, file))
            


filelistQ1_ = sorted_alphanumeric(dirlistQ1_)
filelistQ3_ = sorted_alphanumeric(dirlistQ3_)
filelistMed = sorted_alphanumeric(dirlistMed)
#filelistLabels = sorted_alphanumeric(dirlistLabels)
filelistBearing = sorted_alphanumeric(dirlistBearing)
filelistV = sorted_alphanumeric(dirlistV)
filelistPos = sorted_alphanumeric(dirlistPos)
filelistTSmoy = sorted_alphanumeric(dirlist_TSmoy)


Q1_tot=np.loadtxt(filelistQ1_[0])
Q3_tot=np.loadtxt(filelistQ3_[0])
med_tot=np.loadtxt(filelistMed[0])
#labels_tot=np.loadtxt(filelistLabels[0])
bearing_tot=np.loadtxt(filelistBearing[0])
v_tot=np.loadtxt(filelistV[0])
posTS_tot=np.loadtxt(filelistPos[0])
TS_tot=np.loadtxt(filelistTSmoy[0])
#realTSmean_tot=np.loadtxt(filelistTSmoy[0])
for i in range(1,len(filelistQ1_)):
    Q1_tot=np.vstack((Q1_tot,np.loadtxt(filelistQ1_[i])))
    Q3_tot=np.vstack((Q3_tot,np.loadtxt(filelistQ3_[i])))
    med_tot=np.vstack((med_tot,np.loadtxt(filelistMed[i])))   
    # labels=np.loadtxt(filelistLabels[i])
    # labels_tot=np.hstack((labels_tot,labels))
    bearing=np.loadtxt(filelistBearing[i])
    bearing_tot=np.hstack((bearing_tot,bearing))
    v=np.loadtxt(filelistV[i])
    v_tot=np.hstack((v_tot,v))
    p=np.loadtxt(filelistPos[i])
    posTS_tot=np.hstack((posTS_tot,p))
    ts=np.loadtxt(filelistTSmoy[i])
    TS_tot=np.hstack((TS_tot,ts))
    # rTS=np.loadtxt(filelistTSmoy[i])
    # realTSmean_tot=np.vstack((realTSmean_tot,rTS))

lle = LocallyLinearEmbedding(n_components=32, n_neighbors = 35,method = 'modified', n_jobs = 4,  random_state=0)
lle.fit(med_tot)
med_lle = lle.transform(med_tot)

kmeans_sv=AgglomerativeClustering(n_clusters=nombre_de_categories_sv_campEntiere,affinity='euclidean',linkage='ward').fit(med_lle)

couleurs = plt.cm.jet(np.linspace(0,1,nombre_de_categories_sv_campEntiere))
cmap = colors.ListedColormap(couleurs) 

plt.figure(figsize=(30,15))
for x in np.unique(kmeans_sv.labels_):
    indice = np.where(kmeans_sv.labels_ == x)
    plt.plot(TSfreq, np.mean( Q1_tot[indice], axis=0),'-.',color=couleurs[x])
    plt.plot(TSfreq, np.mean( Q3_tot[indice], axis=0),'-.',color=couleurs[x])
    plt.plot(TSfreq, np.mean( med_tot[indice], axis=0),'o-',color=couleurs[x],label="Classe"+str(x))
plt.legend()
plt.xlabel('Mean TS per Label per transect (dB)')
plt.ylabel('Depth (m)')
plt.savefig(fpath+'TSClustersRF_full_survey.png')

fig = plt.figure(figsize=(30, 15))
ax = fig.add_subplot()
ax.invert_xaxis()
plt.scatter(TS_tot,(-posTS_tot+30)*(-1),c=kmeans_sv.labels_,cmap=cmap,s=25) 
    
plt.colorbar()
plt.xlabel('Mean TS (dB)')
plt.ylabel('Depth (m)')
plt.savefig(fpath + "/TS_depth_full_survey.png")

plt.figure(figsize=(30, 15))
#plt.scatter(bearing,v,c=labels,cmap=cmap,s=100)
plt.scatter(bearing_tot,v_tot,c=kmeans_sv.labels_,cmap=cmap,s=100)
    
plt.colorbar()
plt.xlabel('Difference between initial bearing and final bearing (deg)')
plt.ylabel('Mean velocity (m/s)')
plt.savefig(fpath + "/TS_direction_velocity_full_survey.png")
