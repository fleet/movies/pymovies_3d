# -*- coding: utf-8 -*-
"""
Created on Wed Nov 25 09:05:45 2020

@author: lberger
|- import de librairies 
|
|- Sélection des limites horaires de l'analyse TS'
|
|- Initialisation des variables, des chemins d'accès aux données
|
|- Extraction des échos simples et concaténation dans un fichier pickle
|
|- Affichage des histogrammes de TS et de profondeur et du TS moyen par fréquence
|
|- Export au format csv


"""

import os
import pickle
import numpy as np
import matplotlib.pyplot as plt
import csv
import pymovies_3d.core.echointegration.ei_survey as ei
import pymovies_3d.core.TS_analysis.samplecomputeTS as TS
import pymovies_3d.core.TS_analysis.TS_bind as ts_bind

dateStart = "12/10/2020"
timeStart = "00:00:00"
dateEnd = "12/10/2020"
timeEnd = "23:59:00"


# path
path_hac_survey = "C:/Users/lberger/Desktop/MAYOBS/MAYOBS15/MAYOBS15_20201012"
path_config = (
    "C:/Users/lberger/Desktop/MAYOBS/MAYOBS2/HAC/Config/TSAnlaysis"
)
survey_name = "MAYOBS15_18"


path_save = path_hac_survey + "/Result/" + survey_name + "/"

thresholds = [-30]

path_config = path_config + "/" + str(thresholds[0])

path_csv = path_save + "csv/"
path_png = path_save + "png/"

# If save path does not exist, create it
if not os.path.exists(path_save):
    os.makedirs(path_save)

if not os.path.exists(path_csv):
    os.makedirs(path_csv)

if not os.path.exists(path_png):
    os.makedirs(path_png)




var_choix = input("voulez vous modifier les données de TS?  \n   y or n \n")
if var_choix == "y":
    TS.sample_compute_TS(
        path_hac_survey,
        path_config,
        path_save,
        indexSounder=0,
        indexTransducer=0,
        nameTransect=survey_name,
        dateStart=dateStart,
        timeStart=timeStart,
        dateEnd=dateEnd,
        timeEnd=timeEnd,
    )

filename = path_save + "/" + survey_name + "_results_TS.pickle"

print("chargement des donnees...")
with open(filename, "rb") as f:
    (
        timeTarget_all,
        TSrange_all,
        TScomp_all,
        TSucomp_all,
        TSalong_all,
        TSathwart_all,
        TSposition_all,
        TSpositionGPS_all,
        TSlabel_all,
        TSfreq_all,
        name_sdr_tsd,
    ) = pickle.load(f)

indexSounder = 0
indexTransducer = 0

Time = np.squeeze(np.asarray(timeTarget_all[indexSounder][indexTransducer]))
TSvalues = np.squeeze(np.asarray(TScomp_all[indexSounder][indexTransducer]))
TSposition = np.squeeze(np.asarray(TSpositionGPS_all[indexSounder][indexTransducer]))
TSfreq = np.unique(TSfreq_all[indexSounder][indexTransducer])
TSlabel = np.squeeze(np.asarray(TSlabel_all[indexSounder][indexTransducer]))
TSmean = np.zeros(np.size(TSfreq))
for f in range(np.size(TSfreq)):
    TSmean[f] = np.mean(
        TSvalues[np.where(TSfreq_all[indexSounder][indexTransducer] == TSfreq[f])]
    )

# display TS en depth histograms and mean TS as a function of frequency
num_bins = 10
plt.figure(1, figsize=(30, 15))
plt.clf()
plt.tight_layout()
# the histogram of TS
n, bins, patches = plt.hist(TSvalues, num_bins)
plt.xlabel("TS (dB)")
plt.grid(True)
plt.show()

plt.figure(2, figsize=(30, 15))
plt.clf()
plt.tight_layout()
# the histogram of the depth
n, bins, patches = plt.hist(
    TSposition[:, 2], num_bins, facecolor="blue", alpha=0.5
)
plt.xlabel("Depth (m)")
plt.grid(True)
plt.show()

plt.figure(3, figsize=(30, 15))
plt.clf()
plt.tight_layout()
# the mean TS
plt.plot(np.squeeze(TSfreq / 1000), np.squeeze(TSmean))
plt.xlabel("Frequency (kHz)")
plt.ylabel("TS (dB)")
plt.grid(True)
plt.show()

# export csv
header = ["Time", "Latitude", "Longitude", "Depth", "Value", "Label Track"]
for f in range(np.size(TSfreq)):
    if np.size(TSfreq) > 1:
        index = np.squeeze(
            np.where(TSfreq_all[indexSounder][indexTransducer] == TSfreq[f])
        )
        with open(
            "%s/%s-TH%d-EK80-%s-%s-TS_freq_%dkHz.csv"
            % (
                path_csv,
                survey_name,
                thresholds[0],
                indexSounder,
                indexTransducer,
                TSfreq[f],
            ),
            "w",
            newline="",
        ) as csvfile:
            writer = csv.writer(csvfile, delimiter=" ")
            writer.writerow(h for h in header)
            for i in range(len(Time)):
                writer.writerow(
                    (
                        Time[i].strftime("%d/%m/%Y %H:%M:%S"),
                        TSposition[i, 0],
                        TSposition[i, 1],
                        TSposition[i, 2],
                        TSvalues[index[i]],
                        TSlabel[i],
                    )
                )
    else:
        with open(
            "%s/%s-TH%d-EK80-%s-%s-TS_freq_%dkHz.csv"
            % (
                path_csv,
                survey_name,
                thresholds[0],
                indexSounder,
                indexTransducer,
                TSfreq,
            ),
            "w",
            newline="",
        ) as csvfile:
            writer = csv.writer(csvfile, delimiter=";")
            writer.writerow(h for h in header)
            for i in range(len(Time)):
                writer.writerow(
                    (
                        Time[i].strftime("%d/%m/%Y %H:%M:%S"),
                        TSposition[i, 0],
                        TSposition[i, 1],
                        TSposition[i, 2],
                        TSvalues[i],
                        TSlabel[i],
                    )
                )
