# -*- coding: utf-8 -*-
"""
Created on Wed Nov 25 09:05:45 2020

@author: lberger
|- import de librairies 
|
|- Sélection des limites horaires de l'analyse TS'
|
|- Initialisation des variables, des chemins d'accès aux données
|
|- Extraction des échos simples et concaténation dans un fichier pickle
|
|- Affichage des histogrammes de TS et de profondeur et du TS moyen par fréquence
|
|- Export au format csv


"""

import os
import pickle
import numpy as np
import matplotlib.pyplot as plt
import csv
import math

import pymovies_3d.core.echointegration.ei_survey as ei
import pymovies_3d.core.TS_analysis.samplecomputeTS as TS
import pymovies_3d.core.TS_analysis.TS_bind as ts_bind
import pymovies_3d.core.log_events.import_casino as ic
import pymovies_3d.core.echointegration.ei_bind as ei_bind
import pymovies_3d.core.echointegration.ei_standardizing as standardizing
import pymovies_3d.core.echointegration.ei_classification_bis as classif
import pymovies_3d.visualization.drawing as dr

from matplotlib import colors
from sklearn.cluster import KMeans
from sklearn.manifold import LocallyLinearEmbedding

###############################################################################
# INITIALISATION DES VARIABLES
###############################################################################
"""
Les variables suivantes sont à initialiser:
- path_evt: chemin vers le fichier casino de la campagne
- path_hac_survey: chemin vers les fichiers .hac contenant les données
- thresholds_TS: seuil de l'EI (TS)
- thresholds_Sv: seuil de l'EI (Sv)
- path_config: chemin vers la configuration
/!\ bien penser à également changer les chemins suivants internes à la boucle du code
 selon la campagne: 
     - survey_name
     - path_save
     
- var_choix: ('y'/'n'), si var_choix='y': modification des données de l'EI
- reorganisation_EI: (True/False)
- clustering: (True/False)
 Si les variables reorganisation_EI et clustering sont False alors on charge par defaut
 le fichier filename_clustering s'il a été enregistré auparavant
- save_data: (True/False)

- ind_decoup: (liste d'int de valeurs possibles: [0:len(nameTransect)]
  permet de sélectionner les radiales souhaitées d'une campagne
- indices_transduc_choisis: (liste d'int de valeurs possibles: TUBE: 3 transducteurs max)
  choix des indices des transducteurs souhaités (à renseigner dans l'ordre croissant)
- nbfreqs: nombre de fréquences à afficher lors de l'affichage des Sv/ESU
 (sélection des fréquences nominales souhaitées)
 
- nombre_de_categories_sv: choix du nombre de classes (Sv)
- nombre_de_categories_ts: choix du nombre de classes (TS)
- methode_clustering: choix de la méthode à utiliser pour classifier les données
 (LLE+KMeans (modified LLE) ou KMeans seul)
 KMeans:
     https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html
 LLE+KMeans: 
     https://scikit-learn.org/stable/modules/generated/sklearn.manifold.LocallyLinearEmbedding.html
     https://scikit-learn.org/stable/modules/manifold.html#locally-linear-embedding (§2.2.3 & 2.2.4)
 
- critere_nb_detect_TS: filtre sur les tracks avant classification basé sur le nombre de détections de ce track
- critere_ect_TS: filtre sur les tracks avant classification basé sur l'écart-type de ce track (dB)

- type_acq_TS: acquisition des TS avec des sondeurs embarqués sur une sonde (TUBE echosonde par ex) ou sur navire (PELGAS2021 par ex)
"""
# Chemins windows
path_evt = 'C:/Users/amaurice/Desktop/PHOENIX18/PHOENIX2018_casino_plongees_TUBE_Copie.txt'
path_hac_survey = 'D:/PHOENIX18/SBES_MBES/PHOENIX2018_TUBE/TUBE1_21_partie1'
path_config_Sv = 'D:/Config_WBTTUBE_SV/configsAnalyseM3D/EIlay'
path_config_TS = 'D:/PHOENIX18_TUBE_calib_config/Configuration_M3D/TSAnalysis'
thresholds_Sv = [-90]
thresholds_TS = [-84]
path_config_Sv = path_config_Sv + "/" + str(thresholds_Sv[0])
path_config_TS = path_config_TS + "/" + str(thresholds_TS[0])


# EI, modification ou non de l'EI et classification
var_choix='y'
save_data = True
reorganisation_EI = True
clustering = True

# Sélection des radiales, transducteurs et fréquences d'interêt 
ind_decoup=list(range(41,55-1,1))
nbfreqs = 3 
indices_transduc_choisis = np.array([0, 1, 2])

# Classification de la donnée
nombre_de_categories_sv=3
nombre_de_categories_ts=8
methode_clustering="LLE_KMeans"

# Filtres sur les tracks avant classification
critere_nb_detect_TS = 4
critere_ect_TS = 2

# Matériel d'acquisition des TS
type_acq_TS = 'TUBE'

###############################################################################
# DEBUT DU CALCUL
###############################################################################
if type_acq_TS == 'TUBE':
    C, Entete, index_events, date_heure = ic.import_casino_TUBE(path_evt)
    (
        datedebut,
        datefin,
        heuredebut,
        heurefin,
        nameTransect,
        prof_tube,
        vit_vent_vrai,
        dir_vent_vrai,
    ) = ic.infos_radiales_TUBE(C)
    
else:
    C, Entete, index_events, date_heure = ic.import_casino(path_evt)
    (
        datedebut,
        datefin,
        heuredebut,
        heurefin,
        nameTransect,
        vit_vent_vrai,
        dir_vent_vrai,
    ) = ic.infos_radiales(C)

# supression des variables qui ne sont plus utiles
del C, Entete, date_heure

indsuppr=[]
        
#on recherche les radiales non uniques et on les supprime
transects_doublons = [idx for idx, val in enumerate(nameTransect) if val in nameTransect[:idx]]
indsuppr.extend(transects_doublons)
indsuppr.sort()

nbsuppr = 0
for i in range(len(indsuppr)):
    nbsuppr = nbsuppr + 1
    del (
        datedebut[indsuppr[i] - nbsuppr],
        datefin[indsuppr[i] - nbsuppr],
        heuredebut[indsuppr[i] - nbsuppr],
        heurefin[indsuppr[i] - nbsuppr],
        nameTransect[indsuppr[i] - nbsuppr],
    )

datedebutini=datedebut
heuredebutini=heuredebut
datefinini=datefin
heurefinini=heurefin

###############################################################################
# EI et CLASSIFICATION
###############################################################################
for index in ind_decoup:
    premiere_date=index
    derniere_date=index+1
    
    datedebut = datedebutini[premiere_date:derniere_date]
    heuredebut = heuredebutini[premiere_date:derniere_date]
    datefin=datefinini[premiere_date:derniere_date]
    heurefin=heurefinini[premiere_date:derniere_date]

    date_time_debut= []
    date_time_fin= []
    for i in range (len(datedebut)):
        date_time_debut.append("%s%s" %(datedebut[i],heuredebut[i]))
        date_time_fin.append("%s%s" %(datefin[i],heurefin[i]))
        
    #path windows
    survey_name='PHOENIX18'+'_'+str(premiere_date)+'_'+str(derniere_date)
    
    path_save = 'C:/Users/amaurice/Desktop/TUBE/' + "WBTTUBE_SV/Result/" + survey_name + "/"  
    
    path_csv = path_save + "csv/"
    path_png = path_save + "png/"
    path_html = path_save + "html/"
    path_netcdf = path_save + "netcdf/"
    path_txt = path_save + "txt/"
    # If save path does not exist, create it
    if not os.path.exists(path_save):
        os.makedirs(path_save)
    
    if not os.path.exists(path_csv):
        os.makedirs(path_csv)
    
    if not os.path.exists(path_png):
        os.makedirs(path_png)
    
    if not os.path.exists(path_html):
        os.makedirs(path_html)
    
    if not os.path.exists(path_netcdf):
        os.makedirs(path_netcdf)
        
    if not os.path.exists(path_txt):
        os.makedirs(path_txt)
    
    filename_ME70 = "%s/%s-TH%d-ME70-EIlay.pickle" % (path_save, survey_name, thresholds_Sv[0])
    filename_EK80 = "%s/%s-TH%d-EK80-EIlay.pickle" % (path_save, survey_name, thresholds_Sv[0])
    filename_EK80h = "%s/%s-TH%d-EK80h-EIlay.pickle" % (
        path_save,
        survey_name,
        thresholds_Sv[0],
    )
    
    
    if save_data:
    
        filename_data_ME70 = "%s/%s-TH%d-saved_data_for_ME70_all.pickle" % (path_save, survey_name, thresholds_Sv[0])
        filename_data_EK80 = "%s/%s-TH%d-saved_data_for_EK80_all.pickle" % (path_save, survey_name, thresholds_Sv[0])
        filename_data_EK80h = "%s/%s-TH%d-saved_data_for_EK80h_all.pickle" % (path_save, survey_name, thresholds_Sv[0])
        filename_clustering_ME70 = "%s/%s-TH%d-saved_clustering_for_ME70_all.pickle" % (path_save, survey_name, thresholds_Sv[0])
        filename_clustering_EK80 = "%s/%s-TH%d-saved_clustering_for_EK80_all.pickle" % (path_save, survey_name, thresholds_Sv[0])
        filename_clustering_EK80h = "%s/%s-TH%d-saved_clustering_for_EK80h_all.pickle" % (path_save, survey_name, thresholds_Sv[0])
    
    
    ####################################################################################################
    #
    # Echo-integration
    #
    ####################################################################################################
    
    
    # var_choix = input("voulez vous modifier les données de l EI ?  \n   y or n \n")
    var_choix='y'
    if var_choix == "y":
        ei.ei_survey_transects(
            path_hac_survey,
            path_config_Sv,
            path_save,
            datedebut,
            datefin,
            heuredebut,
            heurefin,
            nameTransect[premiere_date:],
            False,
            True,
            False,
        )
    
    # concaténation de tous des résultats EI EK80 de tous les transects pour analyse MFR à l'échelle de la campagne
    ei_bind.ei_bind_transects(path_save, None, filename_EK80, None)
    
    # ####################################################################################################
    #
    # Remaniement des données et classification
    #
    ####################################################################################################
    # ré-organistion des données d'écho-intégration mise au format standard    
    # Commenter/Decommenter pour choisir quels fichiers charger
    print("chargement des donnees...")
    with open(filename_EK80, "rb") as f:
        (
            time_EK80_db,
            depth_surface_EK80_db,
            depth_bottom_EK80_db,
            Sv_surfEK80_db,
            Sv_botEK80_db,
            Sa_surfEK80_db,
            Sa_botEK80_db,
            lat_surfEK80_db,
            lon_surfEK80_db,
            lat_botEK80_db,
            lon_botEK80_db,
            vol_surfEK80_db,
            freqs_EK80_db,
        ) = pickle.load(f)
    
    # with open(filename_EK80h) as f:
    #    time_EK80h_db,depth_surface_EK80h_db,Sv_surfEK80h_db,Sa_surfEK80h_db,lat_surfEK80h_db,lon_surfEK80h_db,vol_surfEK80h_db,freqs_EK80_db = pickle.load(f)

    # with open(filename_ME70) as f:
    #    time_ME70_db,depth_surface_ME70_db,depth_bottom_ME70_db,Sv_surfME70_db,Sv_botME70_db,Sa_surfME70_db,Sa_botME70_db,lat_surfME70_db,lon_surfME70_db,lat_botME70_db,lon_botME70_db,vol_surfME70_db,freqs_ME70_db = pickle.load(f)

    standardizing.scaling(
    freqs_EK80_db,
    Sv_surfEK80_db,
    Sa_surfEK80_db,
    depth_surface_EK80_db,
    depth_bottom_EK80_db,
    lat_surfEK80_db,
    lon_surfEK80_db,
    time_EK80_db,
    indices_transduc_choisis,
    date_time_debut,
    date_time_fin,
    premiere_date,
    nameTransect,
    save=save_data,
    filename=filename_data_EK80,
)
    
    del (
        time_EK80_db,
        depth_surface_EK80_db,
        depth_bottom_EK80_db,
        Sv_surfEK80_db,
        Sv_botEK80_db,
        Sa_surfEK80_db,
        Sa_botEK80_db,
        lat_surfEK80_db,
        lon_surfEK80_db,
        lat_botEK80_db,
        lon_botEK80_db,
        vol_surfEK80_db,
        freqs_EK80_db,
    )

    with open(filename_data_EK80, "rb") as f:
        (
            freq_MFR,
            freqs_moy_transducteur,
            Sv,
            Sv_moy_transducteur_x,
            Sa,
            Depth,
            Lat,
            Lon,
            Time,
            nb_transduc,
            tableau_radiales,
            nom_numero_radiales,
        ) = pickle.load(f)    
        
        Sv_temp=Sv[:,:,:]
        
        classif.clustering(
        Sv_temp,
        Sv_moy_transducteur_x,
        freq_MFR,
        freqs_moy_transducteur,
        nombre_de_categories_sv,
        Sa,
        Time,
        Depth,
        Lat,
        Lon,
        nb_transduc,
        tableau_radiales,
        nom_numero_radiales,
        methode_clustering,
        save=save_data,
        filename=filename_clustering_EK80,
    )
        
        with open(filename_clustering_EK80, "rb") as f:
            (
                kmeans_sv,
                labels_sv,
                Q1_sv,
                Q3_sv,
                med_sv,
                nombre_de_categories,
                Sv_temp,
                Sv_moy_transducteur_x,
                Sa,
                Time,
                freq_MFR,
                freqs_moy_transducteur,
                Depth,
                Lat,
                Lon,
                nb_transduc,
                tableau_radiales,
                nom_numero_radiales,
                methode,
            ) = pickle.load(f)        
        

    freq_voulues = []
    space_log=[]
    space_lin=[]
    space_log_temp =  np.logspace(math.log10(freq_MFR[1]),math.log10(freq_MFR[len(freq_MFR)-1]),nbfreqs) 
    space_lin_temp =  np.linspace(freq_MFR[1],(freq_MFR[len(freq_MFR)-1]),nbfreqs) 
    space_custom_temp =[120000,200000,333000]
    
    space_custom = []
    for i in range (len(space_custom_temp)) :
        space_custom.append(min(freq_MFR, key=lambda x:abs(x-space_custom_temp[i])))
    for i in range (len(space_log_temp)) :
        space_log.append(min(freq_MFR, key=lambda x:abs(x-space_log_temp[i])))     
    for i in range(len(space_lin_temp)) :
        space_lin.append(min(freq_MFR, key=lambda x:abs(x-space_lin_temp[i])))
            
    freq_voulues = space_custom 
    
    couleurs = plt.cm.jet(np.linspace(0, 1, nombre_de_categories_sv))
    cmap = colors.ListedColormap(couleurs)
    
    x=np.arange(0,len(Sv),1)
    if type_acq_TS=='TUBE':
        y=np.arange(-Depth[1,0]-float(prof_tube[index]),-Depth[1,(len(Depth[1])-1)]-1-float(prof_tube[index]),-1)
    else:
        y=np.arange(-Depth[1,0],-Depth[1,(len(Depth[1])-1)]-1,-1)

    # # Affichage de la classification et de la réponse fréquentielle par clusters (sans log)
    fig,ax2 = plt.subplots(figsize=(30,15))
    #image de classification
    plt.subplot(211)
    plt.xlabel('N*ESU')
    plt.ylabel('profondeur (en m)')
    plt.title(' ')
    plt.pcolormesh(x,y,np.transpose(labels_sv), cmap=cmap)
    plt.grid()
    plt.colorbar()
    plt.show()
    
    #réponse en fréquence
    plt.subplot(212)
    plt.xlabel('Freq')
    plt.ylabel('Sv dB')
    plt.title('Reponse en frequence par cluster')
    for j in range(nombre_de_categories_sv):
        plt.plot(freq_MFR,Q1_sv[j],'-.',color=couleurs[j])
        plt.plot(freq_MFR,Q3_sv[j],'-.',color=couleurs[j])
        plt.plot(freq_MFR,med_sv[j],'o-',color=couleurs[j])
    plt.grid()
    fig.savefig("%s/figure_clustering.png" % path_png)
    plt.close()
    
    Sv_affiche = []
    freqs_affichage = []
    for x in range(len(freq_voulues)):
        indice = np.where(freq_MFR == freq_voulues[x])
        Sv_affiche.append(np.transpose(np.squeeze(Sv_temp[:, :, indice])))
        freqs_affichage.append((freq_voulues[x]) / 1000)
    Sv_affiche = np.array(Sv_affiche)
    fig, ax3 = plt.subplots(figsize=(24, 35))
    dr.DrawMultiEchoGram(
         Sv_affiche,
         -Depth,
         len(freq_voulues),
         freqs=freqs_affichage,
         title="Sv sondeur %d kHz",
     )
    fig.savefig(path_png+"Figure_Sv.png")
    plt.close()
           
    indexSounder = 0    
    for j in range(0,3,1):    
        indexTransducer = j
        if indexTransducer==0:
            indice_freq1=0
            indice_freq2=46
        elif indexTransducer==1:
            indice_freq1=46
            indice_freq2=114
        else:
            indice_freq1=114
            indice_freq2=-1
        
        survey_name='PHOENIX18'+'_'+str(premiere_date)+'_'+str(derniere_date)
        path_save = 'C:/Users/amaurice/Desktop/TUBE/' + "WBTTUBE_TS/Result/" + survey_name + "/" +str(j) + "/"
        
        path_csv = path_save + "csv/"
        path_png = path_save + "png/"
        
        # If save path does not exist, create it
        if not os.path.exists(path_save):
            os.makedirs(path_save)
        
        if not os.path.exists(path_csv):
            os.makedirs(path_csv)
        
        if not os.path.exists(path_png):
            os.makedirs(path_png)
        

        nameTube = nameTransect[index]
        
        if var_choix == "y":
            TS.sample_compute_TS(
                path_hac_survey,
                path_config_TS,
                path_save,
                nameTransect=nameTube,
                dateStart=datedebut,
                timeStart=heuredebut,
                dateEnd=datefin,
                timeEnd=heurefin,
            )
        
        filename = path_save + "/" + nameTube + "_results_TS.pickle"
        
        print("chargement des donnees...")
        with open(filename, "rb") as f:
            (
                timeTarget_all,
                TSrange_all,
                TScomp_all,
                TSucomp_all,
                TSalong_all,
                TSathwart_all,
                TSposition_all,
                TSpositionGPS_all,
                TSlabel_all,
                TSfreq_all,
                name_sdr_tsd,
            ) = pickle.load(f)
                
                
        couleurs = plt.cm.jet(np.linspace(0, 1, nombre_de_categories_sv))
        cmap = colors.ListedColormap(couleurs)
        
        plt.figure(1, figsize=(30, 15))
        plt.xlabel("Freq")
        plt.ylabel("Sv dB")
        plt.title("Reponse en frequence par cluster")
        for j in range(nombre_de_categories_sv):
            plt.plot(freq_MFR[indice_freq1:indice_freq2], Q1_sv[j][indice_freq1:indice_freq2], "-.", color=couleurs[j])
            plt.plot(freq_MFR[indice_freq1:indice_freq2], Q3_sv[j][indice_freq1:indice_freq2], "-.", color=couleurs[j])
            plt.plot(freq_MFR[indice_freq1:indice_freq2], med_sv[j][indice_freq1:indice_freq2], "o-", color=couleurs[j])
        plt.grid()
        plt.savefig(path_png + "/Sv_freq.png")
        plt.close()  
            
            
        Time = np.squeeze(np.asarray(timeTarget_all[indexSounder][indexTransducer]))
        TSvalues = np.squeeze(np.asarray(TScomp_all[indexSounder][indexTransducer]))
        TSposition = np.squeeze(np.asarray(TSpositionGPS_all[indexSounder][indexTransducer]))
        TSfreq = np.unique(TSfreq_all[indexSounder][indexTransducer])
        TSlabel = np.squeeze(np.asarray(TSlabel_all[indexSounder][indexTransducer]))
        TSmean = np.zeros(np.size(TSfreq))
        for f in range(np.size(TSfreq)):
            TSmean[f] = np.mean(
                TSvalues[np.where(TSfreq_all[indexSounder][indexTransducer] == TSfreq[f])]
            )
                
        TS_values_new=TSvalues.reshape((-1,len(TSfreq)))
        realTS=TS_values_new
        realTS_mean=np.array([])
        for x in np.unique(TSlabel):
            if len(np.where(TSlabel==x)[0])>critere_nb_detect_TS:
                indice = np.where(TSlabel == x)
                if np.std(np.mean(realTS[indice,:],1))<critere_ect_TS:
                    realTS_mean=np.append(realTS_mean,np.mean(realTS[indice,:],1))
        
        realTS_mean=realTS_mean.reshape((-1,len(TSfreq)))  
        if len(realTS_mean)<nombre_de_categories_ts:
            com=np.array([])
            com=np.append(com,"nombre de TS insuffisant")
            np.savetxt(path_save+'/'+'rapport_erreurs.txt',com,fmt='%s')
            continue
            
        
        TSpos_mean=np.array([])
        for x in np.unique(TSlabel):
            if len(np.where(TSlabel==x)[0])>critere_nb_detect_TS:
                indice = np.where(TSlabel == x)
                if np.std(np.mean(realTS[indice,:],1))<critere_ect_TS:
                    TSpos_mean=np.append(TSpos_mean,np.mean(TSposition[indice,:],1))
        TSpos_mean=TSpos_mean.reshape((-1,TSposition.shape[1]))
          
        # display TS en depth histograms and mean TS as a function of frequency
        num_bins = 10
        plt.figure(2, figsize=(30, 15))
        plt.clf()
        plt.tight_layout()
        # the histogram of TS
        n, bins, patches = plt.hist(TSvalues, num_bins)#, normed=1)
        plt.xlabel("TS (dB)")
        plt.grid(True)
        plt.savefig(path_png + "/hist_TS_freq.png")
        plt.close()
            
        plt.figure(3, figsize=(30, 15))
        plt.clf()
        plt.tight_layout()
        # the histogram of the depth
        n, bins, patches = plt.hist(
            TSposition[:, 2], num_bins, facecolor="blue", alpha=0.5
        )
        plt.xlabel("Depth (m)")
        plt.grid(True)
        plt.savefig(path_png + "/hist_TS_depth.png")
        plt.close()
        
        plt.figure(4, figsize=(30, 15))
        plt.clf()
        plt.tight_layout()
        # the mean TS
        plt.plot(np.squeeze(TSfreq / 1000), np.squeeze(TSmean))
        plt.xlabel("Frequency (kHz)")
        plt.ylabel("TS (dB)")
        plt.grid(True)
        plt.savefig(path_png + "/hist_meanTS_freq.png")
        plt.close()
                
        plt.figure(5, figsize=(30, 15))
        x=np.linspace(0,nombre_de_categories_ts,len(Sv))
        if type_acq_TS=='TUBE':
            y=np.arange(-Depth[1,0]-float(prof_tube[index]),-Depth[1,(len(Depth[1])-1)]-1-float(prof_tube[index]),-1)
        else:
            y=np.arange(-Depth[1,0],-Depth[1,(len(Depth[1])-1)]-1,-1)            
        plt.pcolormesh(x,y,np.transpose(labels_sv), cmap=cmap)
        plt.xlabel("Classes")
        plt.ylabel("Profondeur du TUBE (m)")
        plt.colorbar()  
        
        #choix d'utilisation de LLE si les dimensions le permettent
        if methode_clustering=='LLE_KMeans':
            nb_neighbors = 10
            if len(realTS_mean)>nb_neighbors:
                lle = LocallyLinearEmbedding(n_components=4, n_neighbors = nb_neighbors,method = 'modified', n_jobs = 4,  random_state=0)
                lle.fit(realTS_mean[:,:])
                X_lle = lle.transform(realTS_mean)
                kmeans =KMeans(n_clusters=nombre_de_categories_ts, random_state=0).fit(X_lle)
            else:
                kmeans = KMeans(n_clusters=nombre_de_categories_ts, random_state=0).fit(realTS_mean)
        else:
            #choix d'utilisation de KMeans seul
            kmeans = KMeans(n_clusters=nombre_de_categories_ts, random_state=0).fit(realTS_mean)
        
        j=0
        vectLabel=np.empty([len(realTS_mean)])
        vectLabel.fill(np.nan)
        for i in range(len(vectLabel)):
            vectLabel[i]=kmeans.labels_[j]
            j=j+1
        
        labels = np.squeeze(np.reshape(vectLabel,(len(realTS_mean))))
        
        couleurs = plt.cm.jet(np.linspace(0, 1, nombre_de_categories_ts))
        cmap = colors.ListedColormap(couleurs)
        
        if type_acq_TS=='TUBE':
            plt.scatter(labels,-TSpos_mean[:,2]-float(prof_tube[index]),c=labels,cmap=cmap,s=100)
        else:
            plt.scatter(labels,-TSpos_mean[:,2],c=labels,cmap=cmap,s=100)
            
        plt.colorbar()
        plt.savefig(path_png + "/TS_SV_depth.png")
        plt.close()
            
            
        Q1_ = np.zeros([nombre_de_categories_ts,len(TSfreq)])   
        Q3_ = np.zeros([nombre_de_categories_ts,len(TSfreq)])
        med = np.zeros([nombre_de_categories_ts,len(TSfreq)])            
        for j in range(nombre_de_categories_ts):
            liste_temp_q =np.squeeze(realTS_mean[np.where((np.squeeze(kmeans.labels_))==j),:])
            liste_temp_q=liste_temp_q.reshape((len(liste_temp_q),-1))
            if liste_temp_q.shape[1]==1:
                liste_temp_q=liste_temp_q.T
            Q1_[j,:]=np.quantile(liste_temp_q, 0.25,axis=0)
            Q3_[j,:]=np.quantile(liste_temp_q, 0.75,axis=0)
            med[j,:]=np.quantile(liste_temp_q ,0.5,axis=0)
        
        plt.figure(6, figsize=(30, 15))
        plt.xlabel('Freq (Hz)')
        plt.ylabel('TS dB')
        plt.title('TS par cluster')
        for j in range(nombre_de_categories_ts):
            plt.plot(TSfreq,Q1_[j],'-.',color=couleurs[j])
            plt.plot(TSfreq,Q3_[j],'-.',color=couleurs[j])
            plt.plot(TSfreq,med[j],'o-',color=couleurs[j])
        plt.grid()
        plt.savefig(path_png + "/TS_freq.png")
        plt.close()
        
        # export csv
        header = ["Time", "Latitude", "Longitude", "Depth", "TS (dB)", "Label Track"]
        for f in range(np.size(TSfreq)):
            if np.size(TSfreq) > 1:
                indexTS = np.squeeze(
                    np.where(TSfreq_all[indexSounder][indexTransducer] == TSfreq[f])
                )
                with open(
                    "%s/%s-TH%d-EK80-%s-%s-TS_freq_%dkHz.csv"
                    % (
                        path_csv,
                        survey_name,
                        thresholds_TS[0],
                        indexSounder,
                        indexTransducer,
                        TSfreq[f],
                    ),
                    "w",
                    newline="",
                ) as csvfile:
                    writer = csv.writer(csvfile, delimiter=" ")
                    writer.writerow(h for h in header)
                    for i in range(len(Time)):
                        writer.writerow(
                            (
                                Time[i].strftime("%d/%m/%Y %H:%M:%S"),
                                TSposition[i, 0],
                                TSposition[i, 1],
                                TSposition[i, 2],
                                TSvalues[indexTS[i]],
                                TSlabel[i],
                            )
                        )
            else:
                with open(
                    "%s/%s-TH%d-EK80-%s-%s-TS_freq_%dkHz.csv"
                    % (
                        path_csv,
                        survey_name,
                        thresholds_TS[0],
                        indexSounder,
                        indexTransducer,
                        TSfreq,
                    ),
                    "w",
                    newline="",
                ) as csvfile:
                    writer = csv.writer(csvfile, delimiter=" ")
                    writer.writerow(h for h in header)
                    for i in range(len(Time)):
                        writer.writerow(
                            (
                                Time[i].strftime("%d/%m/%Y %H:%M:%S"),
                                TSposition[i, 0],
                                TSposition[i, 1],
                                TSposition[i, 2],
                                TSvalues[i],
                                TSlabel[i],
                            )
                        )


            
        
