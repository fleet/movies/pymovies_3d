# -*- coding: utf-8 -*-
"""
Created on Mon Jun 20 16:50:48 2022

@author: lberger
"""
# echo intégration
##################
from pymovies_3d.core.echointegration import ei_survey as ei
import pymovies_3d.core.hac_util.hac_util as util
import pymovies_3d.core.log_events.import_casino as ic
import time
import os
import numpy as np

################################################################################
# %% 2. Set up
################################################################################

# Define paths 
pref='E:/PELGAS23/'
path_data=pref
# Path to survey metadata file:
path_evt=path_data+'Casino/jackpot_corrected2.csv'
# Path to MOVIES3D Echo-integration (EI) configuration folder: chunk de 2000 pings et sauvegarde de fichiers EI NetCDF de 500Mo max
path_config =pref+'/config/'
thresholds = [-80]
path_config = path_config + "/" + str(thresholds[0])
# Path to HAC or netCDF data files:
path_hac_survey=path_data+'CW/HACcor/'
# Save path
path_save =pref+'/echointegration/' + str(thresholds[0])
# If save path does not exist, create it
if not os.path.exists(path_save):
    os.makedirs(path_save)

################################################################################
# %% Metadata import
################################################################################

C, Entete, date_heure = ic.import_jackpot(path_evt)
(
    datedebut,
    datefin,
    heuredebut,
    heurefin,
    nameTransect,
) = ic.infos_radiales_jackpot(C)
    
# supression des variables qui ne sont plus utiles
del C, Entete, date_heure


datedebutini=datedebut
heuredebutini=heuredebut
datefinini=datefin
heurefinini=heurefin

ind_decoup=range(len(nameTransect)) # integer list to select the transects to process, based on metadata file. Possible values: [0:len(nameTransect)]

with open("d:\jackpot_radial.csv",mode="w") as f:
    for indexd in ind_decoup:
        print(indexd)
        premiere_date=indexd
        derniere_date=indexd+1
        
        if np.size(datedebutini)>1:
            datedebut = datedebutini[premiere_date:derniere_date]
            heuredebut = heuredebutini[premiere_date:derniere_date]
            datefin=datefinini[premiere_date:derniere_date]
            heurefin=heurefinini[premiere_date:derniere_date]
        
            date_time_debut= []
            date_time_fin= []
            for i in range (len(datedebut)):
                date_time_debut.append("%s%s" %(datedebut[i],heuredebut[i]))
                date_time_fin.append("%s%s" %(datefin[i],heurefin[i]))
                #ei.ei_survey_transects_netcdf_cpp(path_hac_survey,path_config,path_save,datedebut,datefin,heuredebut,heurefin,name_transects=nameTransect[premiere_date:])
                f.write(f"{datedebut[i]} {heuredebut[i]};{datefin[i]} {heurefin[i]};{nameTransect[premiere_date]}\n")