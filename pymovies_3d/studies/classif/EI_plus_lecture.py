# -*- coding: utf-8 -*-
"""
Created on Mon Jun 20 16:50:48 2022

@author: lberger
"""
# echo intégration
##################
from pymovies_3d.core.echointegration import ei_survey as ei
import pymovies_3d.core.hac_util.hac_util as util
import time

pref='C:/Users/lberger/Desktop/M3D_example_dataset/'
path_data=pref
path_evt=path_data+'/Casino/Casino_BSIFLUME.txt'
path_config =pref+'/configsMOVIES3D/EIlay/FineScaleContinentalShelf/'
thresholds = [-80]
path_config = path_config + "/" + str(thresholds[0])
path_hac_survey=path_data+'/PELGAS13/RUN003/'
path_save=path_data+'/Result/'

date_start=['28/04/2013']
date_end=['28/04/2013']
time_start=['14:00:00']
time_end=['16:30:00']
name_tr='RAD'
t = time.time()
ei.ei_survey_transects_netcdf_cpp(path_hac_survey,path_config,path_save,date_start,date_end,time_start,time_end,name_transects=name_tr)
elapsed = time.time() - t
print(elapsed)

# lecture des résultats
#######################
import matplotlib.pyplot as plt
import numpy as np
import os
import datetime
import pytz
from sonar_netcdf.utils.nc_reader import NcReader
from sonar_netcdf.utils.nc_reader_presenter import NcReaderPresenter

file_path=path_data+'/Result/'+'BFAC_1_Clean_MD239_MAYOBS23_001_20220718_200622_20230206_101023_1_-60.nc'

reader = NcReader(file_path,quiet=True)
presenter = NcReaderPresenter(reader)

depth_cell=presenter.dump_content(root="Sonar/Grid_group1/cell_depth")
depth_cell0=np.squeeze(depth_cell[0,:])
time_cell=presenter.dump_content(root="Sonar/Grid_group1/cell_ping_time")
time_cell0=np.squeeze(time_cell[0,:])

freq=presenter.dump_content(root="Sonar/Grid_group1/frequency")
Sv_C1=presenter.dump_content(root="Sonar/Grid_group1/integrated_backscatter")



mean_sv_ESU_C1 = 10*np.log10(np.mean(10.**(Sv_C1[1,:,:]/10),axis=0))


time = np.array(time_cell0,'datetime64[ns]')

plt.figure()
plt.plot(time,np.transpose(mean_sv_ESU_C1))
# plt.ylim([-100,-40])
# beautify the x-labels
plt.gcf().autofmt_xdate()
plt.xlabel("Time")
plt.ylabel("Sv (dB)")
plt.grid(True)
plt.legend()
plt.title('Sv 100m above seafloor BFAC')
plt.savefig(path_save+'Sv_BFAC_MAYOBS23.png')

plt.figure()
plt.hist(np.transpose(mean_sv_ESU_C1),100)
# plt.ylim([-100,-40])
plt.xlabel("Time")
plt.ylabel("Sv (dB)")
plt.grid(True)
plt.legend()
plt.title('Sv 100m above seafloor BFAC')
plt.savefig(path_save+'Sv_BFAC_MAYOBS23_hist.png')
