# -*- coding: utf-8 -*-

"""

|- import de librairies 
|
|- Ouverture d'un fichier Casino
|
|- Initialisation des variables, des chemins d'accès aux données et des 
|  préférences de visualisation
|
|- Echo-intégration et enregistrement des données brutes
|
|- Remaniement des données : données utiles à la classification 
|  mises sous forme de tableau dans les dimensions adéquates 
|                  |
|                  |-- enregistrement de Sv total, Sv moyen par transducteurs, Sa, de l'heure, 
|                      la profondeur, la latitude, la longitude, 
|                      la frequence, la frequence moyenne de chaque transducteur
|
|- Segmentation des radiales en petites unités et classification avec Kmeans ou LLE+Kmeans
|                  |
|                  |-- enregistrement de Sv total, Sv moyen par transducteurs, Sa, de l'heure, 
|                      la profondeur, la latitude, la longitude, 
|                      la frequence, la frequence moyenne de chaque transducteur,
|                      kmeans, labels, la réponse médiane par clusters
|
|- Selection des fréquences à afficher pour le Sv
|
|- Affichage du Sv moyen par transducteurs
|
|- Affichage des Sv en fonction de la plage de fréquences choisie 
|
|- Affichage des résultats de classification avec la réponse médiane en fréquence
|
|- Import des données Lat, Lon, Sa et Sa_log en CSV pour pouvoir visualiser (dans Qgis par exemple)
|  la densité de poissons dans un repère géographique.
|
|- Affichage de la densité de poisson sur une carte dans une figure 
|
|- Création de fichiers html pour visualiser la densité de poissons sur une carte
|  OpenStreetMap
|
|- Génération d'un rapport sur l'analyse de données (format .adoc)


"""

import pytz

from pymovies_3d.core.echointegration.batch_ei_multi_threshold import (
    batch_ei_multi_threshold,
)
from pymovies_3d.core.echointegration.ei_bind import ei_bind

import os
import numpy as np
import pickle
import datetime
import time
import netCDF4 as nc
import numpy.ma as ma
import scipy

from matplotlib import pyplot as plt
import matplotlib.dates as md
from matplotlib import colors
import matplotlib.patches as mpatches

import math
import numpy.matlib
import pymovies_3d.visualization.drawing as dr
import pymovies_3d.core.log_events.import_casino as ic

import csv
import glob2
import pymovies_3d.core.hac_util.hac_util as au

import pymovies_3d.core.echointegration.ei_survey as ei
import pymovies_3d.core.echointegration.ei_bind as ei_bind
import pymovies_3d.core.echointegration.ei_standardizing as standardizing
import pymovies_3d.core.echointegration.ei_classification as classif
import pymovies_3d.core.util.utils as ut
import pymovies_3d.visualization.map_drawing as md

from sklearn.manifold import LocallyLinearEmbedding
from sklearn.cluster import KMeans
import re

import scipy.cluster.hierarchy as sc
from sklearn.cluster import AgglomerativeClustering

def sorted_alphanumeric(data):
    #https://stackoverflow.com/questions/4813061/non-alphanumeric-list-order-from-os-listdir/48030307#48030307
    """
    Fonction permettant de parcourir les chemins vers les fichiers dans le bon ordre
    La fonction sort() ne marchant pas avec les valeurs alpha-numériques
    Parameters
    ----------
    data : LISTE DE NOMS DE FICHIERS DANS UN ORDRE ALEATOIRE

    Returns
    -------
    LISTE DE NOMS DE FICHIERS TRIEE

    """
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(data, key=alphanum_key)

################################################################################
#INITIALISATION DES VARIABLES
################################################################################
"""
Les variables suivantes sont à initialiser:
- path_evt: chemin vers le fichier casino de la campagne
- path_hac_survey: chemin vers les fichiers .hac contenant les données
- thresholds: seuil de l'EI 
- path_config: chemin vers la configuration
/!\ bien penser à également changer les chemins suivants internes à la boucle du code
 selon la campagne: 
     - survey_name
     - path_save
     
- var_choix: ('y'/'n'), si var_choix='y': modification des données de l'EI
- reorganisation_EI: (True/False)
- clustering: (True/False)
 Si les variables reorganisation_EI et clustering sont False alors on charge par defaut
 le fichier filename_clustering s'il a été enregistré auparavant
- save_data: (True/False)
- methode_EI: ('netcdf' ou 'pickle'), sauvegarde des résultats sous fichier pickle ou sous fichier netcdf

- ind_decoup: (liste d'int de valeurs possibles: [0:len(nameTransect)]
  permet de sélectionner les radiales souhaitées d'une campagne
- indices_transduc_choisis: (liste d'int de valeurs possibles: [0:5] (6 transducteurs max)
  choix des indices des transducteurs souhaités (à renseigner dans l'ordre croissant)
- indice_freq: (liste d'int de valeur max possible:len(freq_MFR))
  choix des indices des fréquences souhaitées
  permet de supprimer une partie des fréquences du transducteur à 333kHz (FM)
- nbfreqs: nombre de fréquences à afficher lors de l'affichage des Sv/ESU
 (sélection des fréquences nominales souhaitées)
 
- nombre_de_categories_sv: choix du nombre de classes
- pas: pas définissant comment les radiales de la campagne sont découpées en sous-unités
 (utile lors de l'utilisation de la méthode de réduction de dimensions LLE)
- methode_clustering: choix de la méthode à utiliser pour classifier les données
 ('LLE_KMeans' (modified LLE) ou 'KMeans_seul')
 KMeans:
     https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html
 LLE+KMeans: 
     https://scikit-learn.org/stable/modules/generated/sklearn.manifold.LocallyLinearEmbedding.html
     https://scikit-learn.org/stable/modules/manifold.html#locally-linear-embedding (§2.2.3 & 2.2.4)
 
"""
# Chemins windows
path_evt='H:/EchoSonde2021-1/CasinoManuelEchoSonde2021-1.txt'
path_config ='H:/Config_M3D_bis/configsAnalyseM3D_Echosonde/EIlay/'
thresholds = [-80]
path_config = path_config + "/" + str(thresholds[0])
path_hac_survey='D:/Hac_Echosonde2021/SBES_MBES/ECHOSONDE2021-1/RUN067/'

type_campagne='echosonde'
if type_campagne=='echosonde':
    analyse_couple_sondeur='non'
    analyse_par_saison='oui'
    if analyse_par_saison=='oui':
        #fichier casino contenant toutes les camapgnes echosonde
        path_evt_analyse_par_saison='H:/CasinoManuel2017_2019.txt'
        #chemin contenant tous les dossiers des campagnes Echosonde pour un couple de sondeurs donné
        fpath='E:/Rearchivage/comparaison_campagnes/couple_sondeurs_choisi/120_200kHz_Copie/'    
else:
    analyse_couple_sondeur='non'
    analyse_par_saison='non'
    
# Chemins linux
# path_hac_survey = '/echosonde/data/PHOENIX2018/HERMES'
# path_config = '/echosonde/data/PHOENIX2018/Config_M3D/ConfigsAnalyseM3D/EIlay'

# EI, modification ou non de l'EI et classification
var_choix='y'
save_data = True
reorganisation_EI = True
clustering = True
methode_EI = 'netcdf'

#Chemin de sauvegarde
fpath='H:/Rearchivage/ECHOSONDE2021/LEG1/'
#fpath='H:/Rearchivage/ECHOSONDE2019/LEG2/120/Echosonde2019_netcdf/Result/'

# Sélection des radiales, transducteurs et fréquences d'interêt 
ind_decoup=list(range(0,4,1))
indice_freq_max=342

if analyse_couple_sondeur=='oui':
    nbfreqs = 2
    nb_transduc=2
    #etude par couple de transducteur ou par transducteur sur les radiales où un seul transducteur est présent
    if nb_transduc==2:
        indices_transduc_choisis = np.array([0,1])

        indice_freq_min1=119500
        indice_freq_max1=121500
        indice_freq_min2=170000
        indice_freq_max2=260000
        freq_nominale1=120000
        freq_nominale2=200000
        freq_custom =[freq_nominale1,freq_nominale2]  
    else:
        indices_transduc_choisis = np.array([0])

        indice_freq_min=119500
        indice_freq_max=120500
        freq_nominale=120000
        freq_custom =[freq_nominale]
else:
    nbfreqs = 2
    indices_transduc_choisis = np.array([0,1])
    nb_transduc=2
    freq_custom =[120000,200000]

# Classification de la donnée
nombre_de_categories_sv=3 #classif par portion de radiale
nombre_de_categories_sv_campEntiere=5 #classif par recombinaison à l'échelle de la campagne entière
pas=1000
methode_clustering="LLE_KMeans"
if methode_clustering=="LLE_KMeans":
    nombre_composantes=4
    nombre_voisins=10
else:
    nombre_composantes=0
    nombre_voisins=0

#Extension de la zone de levé
extension_zone=[-3.05, -2.45, 47, 47.4]
offset=2

################################################################################
#DEBUT DU CALCUL
################################################################################
# C, Entete, index_events, date_heure = ic.import_casino(path_evt)
# (
#     datedebut,
#     datefin,
#     heuredebut,
#     heurefin,
#     nameTransect,
#     vit_vent_vrai,
#     dir_vent_vrai,
# ) = ic.infos_radiales(C)

C, Entete, index_events, date_heure = ic.import_casino(path_evt)
(
    datedebut,
    datefin,
    heuredebut,
    heurefin,
    nameTransect,
    vit_vent_vrai,
    dir_vent_vrai,
) = ic.infos_radiales_echosonde(C)

# C, Entete, index_events, date_heure = ic.import_casino_echosonde_bis(path_evt)
# (
#     datedebut,
#     datefin,
#     heuredebut,
#     heurefin,
#     nameTransect,
# ) = ic.infos_radiales_echosonde_bis(C)

# supression des variables qui ne sont plus utiles
del C, Entete, date_heure

indsuppr=[]

#on recherche les radiales non uniques et on les supprime
transects_doublons = [idx for idx, val in enumerate(nameTransect) if val in nameTransect[:idx]]
indsuppr.extend(transects_doublons)
indsuppr.sort()

nbsuppr = 0
for i in range(len(indsuppr)):
    nbsuppr = nbsuppr + 1
    del (
        datedebut[indsuppr[i] - nbsuppr],
        datefin[indsuppr[i] - nbsuppr],
        heuredebut[indsuppr[i] - nbsuppr],
        heurefin[indsuppr[i] - nbsuppr],
        nameTransect[indsuppr[i] - nbsuppr],
    )

datedebutini=datedebut
heuredebutini=heuredebut
datefinini=datefin
heurefinini=heurefin

len_Sv1 = []
unites = np.array([])

compteur=0

run_once=0

################################################################################
#EI ET CLASSIF: si echosonde création des Netcdf pour tous les sondeurs
################################################################################
for indexd in ind_decoup:
    premiere_date=indexd
    derniere_date=indexd+1
    
    datedebut = datedebutini[premiere_date:derniere_date]
    heuredebut = heuredebutini[premiere_date:derniere_date]
    datefin=datefinini[premiere_date:derniere_date]
    heurefin=heurefinini[premiere_date:derniere_date]
    
    date_time_debut= []
    date_time_fin= []
    for i in range (len(datedebut)):
        date_time_debut.append("%s%s" %(datedebut[i],heuredebut[i]))
        date_time_fin.append("%s%s" %(datefin[i],heurefin[i]))
        
        
    #path windows  
    survey_name = 'ECHOSONDE2021'+'_'+str(premiere_date)+'_'+str(derniere_date)
    path_save = fpath + survey_name + "/" 


    path_csv = path_save + "csv/"
    path_png = path_save + "png/"
    path_html = path_save + "html/"
    path_netcdf = path_save + "netcdf/"
    path_txt = path_save + "txt/"
    # If save path does not exist, create it
    if not os.path.exists(path_save):
        os.makedirs(path_save)
    
    if not os.path.exists(path_csv):
        os.makedirs(path_csv)
    
    if not os.path.exists(path_png):
        os.makedirs(path_png)
    
    if not os.path.exists(path_html):
        os.makedirs(path_html)
    
    if not os.path.exists(path_netcdf):
        os.makedirs(path_netcdf)
        
    if not os.path.exists(path_txt):
        os.makedirs(path_txt)
    
    filename_ME70 = "%s/%s-TH%d-ME70-EIlay.pickle" % (path_save, survey_name, thresholds[0])
    filename_EK80 = "%s/%s-TH%d-EK80-EIlay.pickle" % (path_save, survey_name, thresholds[0])
    filename_EK80h = "%s/%s-TH%d-EK80h-EIlay.pickle" % (
        path_save,
        survey_name,
        thresholds[0],
    )
    
    
    if save_data:
    
        filename_data_ME70 = "%s/%s-TH%d-saved_data_for_ME70_all.pickle" % (path_save, survey_name, thresholds[0])
        filename_data_EK80 = "%s/%s-TH%d-saved_data_for_EK80_all.pickle" % (path_save, survey_name, thresholds[0])
        filename_data_EK80h = "%s/%s-TH%d-saved_data_for_EK80h_all.pickle" % (path_save, survey_name, thresholds[0])
        filename_clustering_ME70 = "%s/%s-TH%d-saved_clustering_for_ME70_all.pickle" % (path_save, survey_name, thresholds[0])
        filename_clustering_EK80h = "%s/%s-TH%d-saved_clustering_for_EK80h_all.pickle" % (path_save, survey_name, thresholds[0])
    
    
    ####################################################################################################
    #
    # Echo-integration
    #
    ####################################################################################################
    if var_choix == "y":
        if methode_EI=='pickle':
        
            ei.ei_survey_transects(
                path_hac_survey,
                path_config,
                path_save,
                datedebut,
                datefin,
                heuredebut,
                heurefin,
                nameTransect[premiere_date:],
                False,
                True,
                False,
            )
        
            # concaténation de tous des résultats EI EK80 de tous les transects pour analyse MFR à l'échelle de la campagne
            ei_bind.ei_bind_transects(path_save, None, filename_EK80, None)
            
            # ####################################################################################################
            #
            # Remaniement des données et classification
            #
            ####################################################################################################
            # ré-organisation des données d'écho-intégration mise au format standard    
            # Commenter/Decommenter pour choisir quels fichiers charger
            print("chargement des donnees...")
            with open(filename_EK80, "rb") as f:
                (
                    time_EK80_db,
                    depth_surface_EK80_db,
                    depth_bottom_EK80_db,
                    Sv_surfEK80_db,
                    Sv_botEK80_db,
                    Sa_surfEK80_db,
                    Sa_botEK80_db,
                    lat_surfEK80_db,
                    lon_surfEK80_db,
                    lat_botEK80_db,
                    lon_botEK80_db,
                    vol_surfEK80_db,
                    freqs_EK80_db,
                ) = pickle.load(f)
            
            # with open(filename_EK80h) as f:
            #    time_EK80h_db,depth_surface_EK80h_db,Sv_surfEK80h_db,Sa_surfEK80h_db,lat_surfEK80h_db,lon_surfEK80h_db,vol_surfEK80h_db,freqs_EK80_db = pickle.load(f)
        
            # with open(filename_ME70) as f:
            #    time_ME70_db,depth_surface_ME70_db,depth_bottom_ME70_db,Sv_surfME70_db,Sv_botME70_db,Sa_surfME70_db,Sa_botME70_db,lat_surfME70_db,lon_surfME70_db,lat_botME70_db,lon_botME70_db,vol_surfME70_db,freqs_ME70_db = pickle.load(f)
        
            standardizing.scaling(
            freqs_EK80_db,
            Sv_surfEK80_db,
            Sa_surfEK80_db,
            depth_surface_EK80_db,
            depth_bottom_EK80_db,
            lat_surfEK80_db,
            lon_surfEK80_db,
            time_EK80_db,
            indices_transduc_choisis,
            date_time_debut,
            date_time_fin,
            premiere_date,
            nameTransect,
            save=save_data,
            filename=filename_data_EK80,
        )
            with open(filename_data_EK80, "rb") as f:
                (
                    freq_MFR,
                    freqs_moy_transducteur,
                    Sv,
                    Sv_moy_transducteur_x,
                    Sa,
                    Depth,
                    Lat,
                    Lon,
                    Time,
                    nb_transduc,
                    tableau_radiales,
                    nom_numero_radiales,
                ) = pickle.load(f) 
            
            del (
                time_EK80_db,
                depth_surface_EK80_db,
                depth_bottom_EK80_db,
                Sv_surfEK80_db,
                Sv_botEK80_db,
                Sa_surfEK80_db,
                Sa_botEK80_db,
                lat_surfEK80_db,
                lon_surfEK80_db,
                lat_botEK80_db,
                lon_botEK80_db,
                vol_surfEK80_db,
                freqs_EK80_db,
            )
            
        else:
            
            ei.ei_survey_transects_netcdf(
            path_hac_survey,
            path_config,
            path_netcdf,
            datedebut,
            datefin,
            heuredebut,
            heurefin,
            premiere_date,
            nameTransect,
        )
            file=path_netcdf+nameTransect[premiere_date:][0]+'_1.xsf.nc'
            dataset=nc.Dataset(file)
            Sv=dataset.groups['Sonar'].groups['Grid_group_1'].variables['integrated_backscatter'][:]
            freq_MFR=dataset.groups['Sonar'].groups['Grid_group_1'].variables['frequency'][:]
            freq_MFR=np.sort(freq_MFR)
            Time=dataset.groups['Sonar'].groups['Grid_group_1'].variables['cell_ping_time'][:]
            Depth=dataset.groups['Sonar'].groups['Grid_group_1'].variables['cell_depth'][:]
            Depth=Depth.T
            Lat=dataset.groups['Sonar'].groups['Grid_group_1'].variables['cell_latitude'][:]
            Lon=dataset.groups['Sonar'].groups['Grid_group_1'].variables['cell_longitude'][:]
            nom_numero_radiales=([nameTransect[premiere_date]],[premiere_date])
            
    else:
        if methode_EI=='pickle':
            with open(filename_data_EK80, "rb") as f:
                (
                    freq_MFR,
                    freqs_moy_transducteur,
                    Sv,
                    Sv_moy_transducteur_x,
                    Sa,
                    Depth,
                    Lat,
                    Lon,
                    Time,
                    nb_transduc,
                    tableau_radiales,
                    nom_numero_radiales,
                ) = pickle.load(f)      
                
        else:
            
            file=path_netcdf+nameTransect[premiere_date:][0]+'_1.xsf.nc'
            dataset=nc.Dataset(file)
            Sv=dataset.groups['Sonar'].groups['Grid_group_1'].variables['integrated_backscatter'][:]
            freq_MFR=dataset.groups['Sonar'].groups['Grid_group_1'].variables['frequency'][:]
            freq_MFR=np.sort(freq_MFR)
            Time=dataset.groups['Sonar'].groups['Grid_group_1'].variables['cell_ping_time'][:]
            Depth=dataset.groups['Sonar'].groups['Grid_group_1'].variables['cell_depth'][:]
            Depth=Depth.T
            Lat=dataset.groups['Sonar'].groups['Grid_group_1'].variables['cell_latitude'][:]
            Lon=dataset.groups['Sonar'].groups['Grid_group_1'].variables['cell_longitude'][:]
            nom_numero_radiales=([nameTransect[premiere_date]],[premiere_date])
            
    if run_once<1:
        tr=[]
        for i in range(len(dataset.groups['Sonar'].groups['Grid_group_1'].variables['beam'][:])):
            if int(dataset.groups['Sonar'].groups['Grid_group_1'].variables['beam'][:][i][9:]) in freq_custom:
                tr.append(i)
            else:
                pass
        if len(dataset.groups['Sonar'].groups['Grid_group_1'].variables['beam'][:])!=nb_transduc:
            run=input('Le nombre de transducteurs rentré par l\'utilisateur est différent de celui contenu dans les fichiers d\'EI, voulez vous poursuivre quand même ?')
            if run=='n':
                raise ValueError("Modifiez les paramètres suivants: nb_transduc, nbfreqs, indices_transduc_choisis et freq_custom")
            else: 
                pass
        elif len(tr)!=len(dataset.groups['Sonar'].groups['Grid_group_1'].variables['beam'][:]):
            run=input('Certaines fréquences rentrées par l\'utilisateur sont différentes de celles contenues dans les fichiers d\'EI ou il manque des fréquences, voulez vous poursuivre quand même ?')
            if run=='n':
                raise ValueError("Modifiez les paramètres suivants: nb_transduc, nbfreqs, indices_transduc_choisis et freq_custom")
            else: 
                pass
        else:
            pass
        run_once+=1
    else:
        pass
                
           
    len_Sv1.append(len(Sv[1]))

    compteur+=1
            
    unites = np.append(unites,np.repeat(pas,len(Sv)//pas))
    unites = np.append(unites,len(Sv)%pas)
            
    for k in range(0,len(Sv),pas):
        if len(Sv)-k<pas:
            Sv_temp=Sv[k:len(Sv),:,:indice_freq_max]
            freq_MFR=freq_MFR[:indice_freq_max]
            print("Sv_temp",Sv_temp.shape)
            Lat_t=Lat[k:len(Sv),:,:]
            Lon_t=Lon[k:len(Sv),:,:]
            Time_t=Time[k:len(Sv),:]
            
            filename_clustering_EK80 =path_save+'portion'+str(len(Sv))+ "saved_clustering_for_EK80_all.pickle" 
            save_labels=path_txt+'portion'+str(len(Sv))+'Labels.txt'
            save_Q1_=path_txt+'portion'+str(len(Sv))+'Q1.txt'
            save_Q3_=path_txt+'portion'+str(len(Sv))+'Q3.txt'
            save_med=path_txt+'portion'+str(len(Sv))+'Med.txt'        
            save_fig_clustering=path_png+'portion'+str(len(Sv))+'Fig_clustering.png'
            save_fig_Sv=path_png+str(len(Sv))+"Figure_Sv.png"
            save_html_Sa=path_html+'portion'+str(len(Sv))
            save_Sa_moy=path_txt+'portion'+str(len(Sv))+'Sa_moy.txt'
            save_Lat_moy=path_txt+'portion'+str(len(Sv))+'Lat_moy.txt'
            save_Lon_moy=path_txt+'portion'+str(len(Sv))+'Lon_moy.txt'

        else:       
            Sv_temp=Sv[k:k+pas,:,:indice_freq_max]
            freq_MFR=freq_MFR[:indice_freq_max]
            print("Sv_temp",Sv_temp.shape)
            Lat_t=Lat[k:k+pas,:,:]
            Lon_t=Lon[k:k+pas,:,:]
            Time_t=Time[k:k+pas,:]
        
            filename_clustering_EK80 =path_save+'portion'+str(k+pas)+ "saved_clustering_for_EK80_all.pickle" 
            save_labels=path_txt+'portion'+str(k+pas)+'Labels.txt'
            save_Q1_=path_txt+'portion'+str(k+pas)+'Q1.txt'
            save_Q3_=path_txt+'portion'+str(k+pas)+'Q3.txt'
            save_med=path_txt+'portion'+str(k+pas)+'Med.txt'        
            save_fig_clustering=path_png+'portion'+str(k+pas)+'Fig_clustering.png'
            save_fig_Sv=path_png+str(k+pas)+"Figure_Sv.png"
            save_html_Sa=path_html+'portion'+str(k+pas)
            save_Sa_moy=path_txt+'portion'+str(k+pas)+'Sa_moy.txt'
            save_Lat_moy=path_txt+'portion'+str(k+pas)+'Lat_moy.txt'
            save_Lon_moy=path_txt+'portion'+str(k+pas)+'Lon_moy.txt'
        
        if methode_EI=='pickle':
            classif.clustering(
            Sv_temp,
            Sv_moy_transducteur_x,
            freq_MFR,
            freqs_moy_transducteur,
            nombre_de_categories_sv,
            Sa,
            Time_t,
            Depth,
            Lat_t,
            Lon_t,
            nb_transduc,
            tableau_radiales,
            nom_numero_radiales,
            methode_clustering,
            nombre_composantes,
            nombre_voisins,
            save=save_data,
            filename=filename_clustering_EK80,
        )
            
            with open(filename_clustering_EK80, "rb") as f:
                (
                    kmeans,
                    labels,
                    Q1_,
                    Q3_,
                    med,
                    nombre_de_categories,
                    Sv_temp,
                    Sv_moy_transducteur_x,
                    Sa,
                    Time_t,
                    freq_MFR,
                    freqs_moy_transducteur,
                    Depth,
                    Lat_t,
                    Lon_t,
                    nb_transduc,
                    tableau_radiales,
                    nom_numero_radiales,
                    methode_clustering,
                ) = pickle.load(f)
                
        else:
            classif.clustering_netcdf(
            Sv_temp,
            freq_MFR,
            nombre_de_categories_sv,
            Time_t,
            Depth,
            Lat_t,
            Lon_t,
            nb_transduc,
            nom_numero_radiales,
            methode_clustering,
            nombre_composantes,
            nombre_voisins,
            save=save_data,
            filename=filename_clustering_EK80,
        )
            
            with open(filename_clustering_EK80, "rb") as f:
                (
                    kmeans,
                    labels,
                    Q1_,
                    Q3_,
                    med,
                    nombre_de_categories,
                    Sv_temp,
                    Time_t,
                    freq_MFR,
                    Depth,
                    Lat_t,
                    Lon_t,
                    nb_transduc,
                    nom_numero_radiales,
                    methode_clustering,
                ) = pickle.load(f)
                                     
        couleurs = plt.cm.jet(np.linspace(0, 1, nombre_de_categories_sv))
        cmap = colors.ListedColormap(couleurs)
        
        # # Affichage de la classification et de la réponse fréquentielle par clusters 
        x=np.arange(0,len(Sv_temp),1)
        #y=np.arange(-Depth[1,0],-Depth[1,(len(Depth[1])-1)]-1,-1)
        y=np.arange(-Depth[0,0],-Depth[0,(len(Depth[0])-1)]-1,-1)
    
        fig,ax2 = plt.subplots(figsize=(30, 15))
        #image de classification
        plt.subplot(211)
        plt.xlabel('N*ESU')
        plt.ylabel('profondeur (en m)')
        plt.title(' ')
        plt.pcolormesh(x,y,np.transpose(labels), cmap=cmap)
        plt.grid()
        plt.colorbar()
        plt.show()
        
        #réponse en fréquence
        plt.subplot(212)
        plt.xlabel('Freq')
        plt.ylabel('Sv dB')
        plt.title('Reponse en frequence par cluster')
        for j in range(nombre_de_categories_sv):
            plt.plot(freq_MFR[:indice_freq_max],Q1_[j],'-.',color=couleurs[j])
            plt.plot(freq_MFR[:indice_freq_max],Q3_[j],'-.',color=couleurs[j])
            plt.plot(freq_MFR[:indice_freq_max],med[j],'o-',color=couleurs[j])
        plt.grid()
        plt.close()

        
        
        np.savetxt(save_labels,labels)
        np.savetxt(save_Q1_,Q1_)
        np.savetxt(save_Q3_,Q3_)
        np.savetxt(save_med,med)
        fig.savefig(save_fig_clustering)
        

        freq_voulues = []
        freq_log=[]
        freq_lin=[]
        freq_log_temp =  np.logspace(math.log10(freq_MFR[1]),math.log10(freq_MFR[len(freq_MFR)-1]),nbfreqs) 
        freq_lin_temp =  np.linspace(freq_MFR[1],(freq_MFR[len(freq_MFR)-1]),nbfreqs) 


        freq_custom_nearest = []
        for i in range (len(freq_custom)) :
            freq_custom_nearest.append(min(freq_MFR, key=lambda x:abs(x-freq_custom[i])))
        for i in range (len(freq_log_temp)) :
            freq_log.append(min(freq_MFR, key=lambda x:abs(x-freq_log_temp[i])))     
        for i in range(len(freq_lin_temp)) :
            freq_lin.append(min(freq_MFR, key=lambda x:abs(x-freq_lin_temp[i])))
                
        freq_voulues = freq_custom_nearest
        
        Sa_affiche = []
        Sv_affiche = []
        freqs_affichage = []
        H_couche=(Depth[0, 1] - Depth[0, 0])
        temp=ma.masked_object(Sv_temp,-10000000.0)
        Sa_temp=10**(temp/10)*H_couche*1852**2*4*np.pi
        for f in range(len(freq_voulues)):
            indice = np.where(freq_MFR == freq_voulues[f])
            Sv_affiche.append(np.transpose(np.squeeze(Sv_temp[:, :, indice])))
            if methode_EI=='pickle':
                Sa_moy = np.squeeze(Sa[:, :, f])
            else:
                Sa_affiche.append(np.transpose(np.squeeze(Sa_temp[:, :, indice])))
            freqs_affichage.append((freq_voulues[f]) / 1000)
            Lat_temp=Lat_t[:,:,f]
            Lon_temp=Lon_t[:,:,f]
            Time_temp=Time_t[:,f]
            Time_temp=au.hac_read_day_time(Time_temp)
        Sv_affiche = np.array(Sv_affiche)
        fig, ax3 = plt.subplots(figsize=(30, 15))
        dr.DrawMultiEchoGram(
            Sv_affiche,
            -Depth,
            len(freq_voulues),
            freqs=freqs_affichage,
            title="Sv sondeur %d kHz",
        )
        fig.savefig(save_fig_Sv)
        plt.close()
        Sa_moy = np.array(Sa_affiche)
        
        # Suppression des valeurs n'ayant pas de sens dans la latitude et la longitude
        index = np.where((Lon_t[:, 0] == -200) & (Lat_t[:, 0] == -100))
    
        Lat_temp = np.delete(Lat_temp, (index), axis=0)
        Lon_temp = np.delete(Lon_temp, (index), axis=0)
    
        labels = np.delete(labels, (index), axis=0)
        Sa_moy = np.delete(Sa_moy, (index), axis=0)
        a = np.where(Sa_moy == 0)
        Sa_moy[a] = np.NaN

    
        Lat_moy = np.true_divide(Lat_temp.sum(1), (Lat_temp != 0).sum(1))
        Lon_moy = np.true_divide(Lon_temp.sum(1), (Lon_temp != 0).sum(1))
    
        # définition des couches surface et fond agrégées pour relier au chalutage
        surface_limit = 30
        indice_surface = int(
            np.floor((surface_limit - Depth[0, 0]) / H_couche)
        ) 

        Sa_moy_label_surface = []
        Sa_moy_label_fond = []
        Sa_moy_label = []
        Sa_moy_label_log = []
        for f in range(len(freq_voulues)):
            sa_temp=Sa_moy[f,:,:].T
            for x in range(nombre_de_categories_sv):
                Sa_label = np.zeros(sa_temp.shape)
                Sa_label[:, :] = np.nan
                index_lab = np.where(labels == x)
                Sa_label[index_lab] = sa_temp[index_lab]
                Sa_moy_label_surface.append(np.nansum(Sa_label[:, 0:indice_surface], axis=1))
                Sa_moy_label_fond.append(np.nansum(Sa_label[:, indice_surface + 1 :], axis=1))
                Sa_moy_label.append(np.nansum(Sa_label, axis=1))
                Sa_moy_label_log.append(10 * np.log10(np.nanmean(Sa_label, axis=1)))
        Sa_moy_label=np.array(Sa_moy_label)
        Sa_moy_label=Sa_moy_label.reshape((len(freq_voulues),nombre_de_categories_sv,-1))
        Sa_moy_label_log=np.array(Sa_moy_label_log)
        Sa_moy_label_log=Sa_moy_label_log.reshape((len(freq_voulues),nombre_de_categories_sv,-1))
        Sa_moy_label_fond=np.array(Sa_moy_label_fond)
        Sa_moy_label_fond=Sa_moy_label_fond.reshape((len(freq_voulues),nombre_de_categories_sv,-1))
        Sa_moy_label_surface=np.array(Sa_moy_label_surface)
        Sa_moy_label_surface=Sa_moy_label_surface.reshape((len(freq_voulues),nombre_de_categories_sv,-1))
        
        #les cercles n'apparaissent que pour les valeurs positives (faire coincider figure et legende en n'affichant que les resultats positifs)
        sa_log=np.zeros((nbfreqs,nombre_de_categories_sv,Sa_moy_label_log.shape[2]))
        for f in range(len(freq_voulues)):
            for x in range(nombre_de_categories_sv):
                for i in range(Sa_moy_label_log.shape[2]):
                    if Sa_moy_label_log[f][x][i]>0:
                        sa_log[f,x,i]=Sa_moy_label_log[f][x][i]+offset
                    elif Sa_moy_label_log[f][x][i]<=0 and np.isnan(Sa_moy_label_log[f][x][i])==False:
                        sa_log[f,x,i]=offset
                     
        ##################################################################################################
        #
        # Création de carte avec la librairie Cartopy : Carte simple , affichage dans les figures.
        # Sa en valeurs naturelles
        #
        ##################################################################################################
        ##################################################################################################
        #
        # Création de carte avec la librairie folium : Carte Openstreetmap, affichage dans un fichier HTML.
        # Sa en Log
        #
        ##################################################################################################


        plt.rc('legend',fontsize=15)    

        for f in range(len(freq_voulues)):
            if Sv_temp.shape[0]<pas:
                 save_html_Sa=path_html+'portion'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))
                 save_Sa_moy=path_txt+'portion'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))+'Sa_moy.txt'
                 save_fig_Sa=path_png+'portion'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))
                 save_fig_Salog=path_png+'portion_LogSa'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))
            else:
                 save_html_Sa=path_html+'portion'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))
                 save_Sa_moy=path_txt+'portion'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))+'Sa_moy.txt'
                 save_fig_Sa=path_png+'portion'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))
                 save_fig_Salog=path_png+'portion_LogSa'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))
            md.affichage_carte_france(nombre_de_categories, Lat_moy, Lon_moy, Sa_moy_label[f,:,:], extension_zone, 'Sa',save_fig_Sa)
            md.affichage_carte_france(nombre_de_categories, Lat_moy, Lon_moy, sa_log[f,:,:], extension_zone, 'LogSa',save_fig_Salog)
            md.save_map_html(nombre_de_categories, Lat_moy, Lon_moy, Sa_moy_label_log[f,:,:], save_html_Sa)
            np.savetxt(save_Sa_moy,Sa_moy[f,:,:])
            
            ##################################################################################################
            #
            # Export de Sa (en valeurs naturelles et log), Lat et Lon dans un fichier CSV
            #
            #################################################################################################
        
            for x in range(nombre_de_categories_sv):
                with open(
                    "%s/Sa_surface_label_%d_freq_%d.csv"
                    % (path_csv, x, freq_custom_nearest[f]),
                    "w",
                    newline="",
                ) as csvfile:
                    writer = csv.writer(csvfile, delimiter=" ")
                    for i in range(len(Lat_moy)):
                        if Sa_moy_label_surface[f][x][i] > 0:
                            writer.writerow(
                                (
                                    Time_temp[i].strftime("%d/%m/%Y %H:%M:%S"),
                                    Lat_moy[i],
                                    Lon_moy[i],
                                    Sa_moy_label_surface[f][x][i],
                                )
                            )
                            
                            
        
            for x in range(nombre_de_categories_sv):
                with open(
                    "%s/Sa_fond_label_%d_freq_%d.csv"
                    % (path_csv, x, freq_custom_nearest[f]),
                    "w",
                    newline="",
                ) as csvfile:
                    writer = csv.writer(csvfile, delimiter=" ")
                    for i in range(len(Lat_moy)):
                        if Sa_moy_label_fond[f][x][i] > 0:
                            writer.writerow(
                                (
                                    Time_temp[i].strftime("%d/%m/%Y %H:%M:%S"),
                                    Lat_moy[i],
                                    Lon_moy[i],
                                    Sa_moy_label_fond[f][x][i],
                                )
                            )
                            
        
            for x in range(nombre_de_categories_sv):
                with open(
                    "%s/Sa_label_%d_freq_%d.csv" % (path_csv, x, freq_custom_nearest[f]),
                    "w",
                    newline="",
                ) as csvfile:
                    writer = csv.writer(csvfile, delimiter=" ")
                    for i in range(len(Lat_moy)):
                        if Sa_moy_label[f][x][i] > 0:
                            writer.writerow(
                                (
                                    Time_temp[i].strftime("%d/%m/%Y %H:%M:%S"),
                                    Lat_moy[i],
                                    Lon_moy[i],
                                    Sa_moy_label[f][x][i],
                                )
                            )
        
        ##################################################################################################-
        
        filelist = glob2.glob("%s/**/*.hac" % path_hac_survey)
        if len(filelist) == 0:
            raise RuntimeError(path_hac_survey + " does not contain HAC files")
        
        list_sounder = au.hac_sounder_descr(filelist[0])
        nb_snd = list_sounder.GetNbSounder()
        
        with open("%s/rapport.adoc" % path_png, "w") as rapport:
        
            rapport.write(("= RAPPORT \n\n"))
            rapport.write(("==== Descriptif des sondeurs \n\n"))
            rapport.write((" nb sondeurs = " + str(nb_snd) + "\n"))
            for isdr in range(nb_snd):
                sounder = list_sounder.GetSounder(isdr)
                nb_transducs = sounder.m_numberOfTransducer
                rapport.write(
                    (
                        "sondeur "
                        + str(isdr)
                        + ":    index: "
                        + str(sounder.m_SounderId)
                        + "   nb trans:"
                        + str(nb_transducs)
                        + "\n"
                    )
                )
                for itr in range(nb_transducs):
                    trans = sounder.GetTransducer(itr)
                    for ibeam in range(trans.m_numberOfSoftChannel):
                        softChan = trans.getSoftChannelPolarX(ibeam)
                        rapport.write(
                            (
                                "   trans "
                                + str(itr)
                                + ":    nom: "
                                + trans.m_transName
                                + "   freq: "
                                + str(softChan.m_acousticFrequency / 1000)
                                + " kHz\n"
                            )
                        )
        
            rapport.write(
                ("\n==== Etude de la classification multifrequence sur le sondeur EK80\n\n")
            )
        
            #  for x in range (len(nom_numero_radiales[1])):
            #     rapport.write("===== Sv moyen par transducteurs pour la radiale %s\n\n" %(nom_numero_radiales[0][x]))
            #     rapport.write(("image:figure_Sv_moy_%d.png[] \n\n" %(nom_numero_radiales[1][x])))
        
            for x in range(len(nom_numero_radiales[1])):
                rapport.write("===== Sv pour la radiale %s\n\n" % (nom_numero_radiales[0][x]))
                rapport.write(("image:figure_Sv_%d.png[] \n\n" % (nom_numero_radiales[1][x])))
        
            rapport.write("===== Resultat de classification\n\n")
            rapport.write("image:figure_clustering.png[] \n\n")
        
            for x in range(nombre_de_categories_sv):
                rapport.write("===== Affichage du Sa pour le label %d\n\n" % x)
                rapport.write(("image:map_label_%d.png[] \n\n" % x))
        
        sounder = list_sounder.GetSounder(0)
        np.savetxt(save_Lat_moy,Lat_moy)
        np.savetxt(save_Lon_moy,Lon_moy)
        
np.savetxt(fpath+'UnitesDecoupageCampagne.txt',unites)
len_Sv1=np.array([len_Sv1])
np.savetxt(fpath+'Profondeur_max.txt',len_Sv1)


###############################################################################
#ANALYSE ECHOSONDE: création des couples de sondeurs pour analyser les résultats
#à l'échelle de la campagne entière
###############################################################################
if type_campagne=='echosonde':
    for indexd in ind_decoup:
        premiere_date=indexd
        derniere_date=indexd+1
        
        datedebut = datedebutini[premiere_date:derniere_date]
        heuredebut = heuredebutini[premiere_date:derniere_date]
        datefin=datefinini[premiere_date:derniere_date]
        heurefin=heurefinini[premiere_date:derniere_date]
        
        date_time_debut= []
        date_time_fin= []
        for i in range (len(datedebut)):
            date_time_debut.append("%s%s" %(datedebut[i],heuredebut[i]))
            date_time_fin.append("%s%s" %(datefin[i],heurefin[i]))
            
            
        #path windows
        survey_name = 'Echosonde2019_partie2'+'_'+str(premiere_date)+'_'+str(derniere_date)
        path_save = fpath + survey_name + "/"   
        path_netcdf=path_save+'netcdf/'

    
        path_csv = path_save + "csv/"
        path_png = path_save + "png/"
        path_html = path_save + "html/"
        path_txt = path_save + "txt/"
        # If save path does not exist, create it
        if not os.path.exists(path_save):
            os.makedirs(path_save)
        
        if not os.path.exists(path_csv):
            os.makedirs(path_csv)
        
        if not os.path.exists(path_png):
            os.makedirs(path_png)
        
        if not os.path.exists(path_html):
            os.makedirs(path_html)
        
        if not os.path.exists(path_netcdf):
            os.makedirs(path_netcdf)
            
        if not os.path.exists(path_txt):
            os.makedirs(path_txt)
        
        filename_ME70 = "%s/%s-TH%d-ME70-EIlay.pickle" % (path_save, survey_name, thresholds[0])
        filename_EK80 = "%s/%s-TH%d-EK80-EIlay.pickle" % (path_save, survey_name, thresholds[0])
        filename_EK80h = "%s/%s-TH%d-EK80h-EIlay.pickle" % (
            path_save,
            survey_name,
            thresholds[0],
        )
        
        
        if save_data:
        
            filename_data_ME70 = "%s/%s-TH%d-saved_data_for_ME70_all.pickle" % (path_save, survey_name, thresholds[0])
            filename_data_EK80 = "%s/%s-TH%d-saved_data_for_EK80_all.pickle" % (path_save, survey_name, thresholds[0])
            filename_data_EK80h = "%s/%s-TH%d-saved_data_for_EK80h_all.pickle" % (path_save, survey_name, thresholds[0])
            filename_clustering_ME70 = "%s/%s-TH%d-saved_clustering_for_ME70_all.pickle" % (path_save, survey_name, thresholds[0])
            filename_clustering_EK80h = "%s/%s-TH%d-saved_clustering_for_EK80h_all.pickle" % (path_save, survey_name, thresholds[0])
            
            
        file=path_netcdf+nameTransect[premiere_date:][0]+'_1.xsf.nc'
        dataset=nc.Dataset(file)
        Sv=dataset.groups['Sonar'].groups['Grid_group_1'].variables['integrated_backscatter'][:]
        Time=dataset.groups['Sonar'].groups['Grid_group_1'].variables['cell_ping_time'][:]
        Depth=dataset.groups['Sonar'].groups['Grid_group_1'].variables['cell_depth'][:]
        Depth=Depth.T
        Lat=dataset.groups['Sonar'].groups['Grid_group_1'].variables['cell_latitude'][:]
        Lon=dataset.groups['Sonar'].groups['Grid_group_1'].variables['cell_longitude'][:]
        nom_numero_radiales=([nameTransect[premiere_date]],[premiere_date])       
        
        for k in range(0,len(Sv),pas): 
            freq_MFR=dataset.groups['Sonar'].groups['Grid_group_1'].variables['frequency'][:]
            freq_MFR=np.sort(freq_MFR)
            if nb_transduc==2:   
                a1=np.where(freq_MFR>indice_freq_min1)
                b1=np.where(freq_MFR<indice_freq_max1)
                intersect1=numpy.intersect1d(a1,b1)
                a2=np.where(freq_MFR>indice_freq_min2)
                b2=np.where(freq_MFR<indice_freq_max2)
                intersect2=numpy.intersect1d(a2,b2)  
                l_elt=[]
                if len(intersect1)>len(intersect2):
                    for elt in intersect2:
                        if elt not in intersect1:
                            l_elt.append(elt)   
                else:
                    for elt in intersect1:
                        if elt not in intersect2:
                            l_elt.append(elt)  
                if len(intersect1)!=0 and len(intersect2)!=0 and len(l_elt)!=0:
                    if len(np.intersect1d(intersect1,intersect2))!=0:
                        for i in np.intersect1d(intersect1,intersect2):
                            if len(intersect1)>len(intersect2):
                                indice_intersect1=np.where(intersect1==i)
                                intersect1=np.delete(intersect1,indice_intersect1)
                            else:
                                indice_intersect2=np.where(intersect2==i)
                                intersect2=np.delete(intersect2,indice_intersect2)                       
                    else:
                        intersect1=intersect1
                        intersect2=intersect2
                    if len(intersect1)>=2:
                        freq_MFR1=freq_MFR[intersect1[0]:intersect1[-1]]
                    else:
                        freq_MFR1=freq_MFR[intersect1[0]]
                    if len(intersect2)>=2:
                        freq_MFR2=freq_MFR[intersect2[0]:intersect2[-1]+1]
                    else:
                        freq_MFR2=freq_MFR[intersect2[0]]
                    freq_MFR=np.hstack((freq_MFR1,freq_MFR2))
                    if len(Sv)-k<pas:
                        Sv_temp1=Sv[k:len(Sv),:,intersect1[0]:intersect1[-1]+1]
                        Sv_temp2=Sv[k:len(Sv),:,intersect2[0]:intersect2[-1]+1]
                        Sv_temp=np.vstack((Sv_temp1.T,Sv_temp2.T))
                        Sv_temp=Sv_temp.T
                        print("Sv_temp",Sv_temp.shape)
                        Lat_t=Lat[k:len(Sv),:,:]
                        Lon_t=Lon[k:len(Sv),:,:]
                        Time_t=Time[k:len(Sv),:]
                        
                        
                        filename_clustering_EK80 =path_save+'portion'+str(len(Sv))+ "saved_clustering_for_EK80_all.pickle" 
                        save_labels=path_txt+'portion'+str(len(Sv))+'Labels.txt'
                        save_Q1_=path_txt+'portion'+str(len(Sv))+'Q1.txt'
                        save_Q3_=path_txt+'portion'+str(len(Sv))+'Q3.txt'
                        save_med=path_txt+'portion'+str(len(Sv))+'Med.txt'        
                        save_fig_clustering=path_png+'portion'+str(len(Sv))+'Fig_clustering.png'
                        save_fig_Sv=path_png+str(len(Sv))+"Figure_Sv.png"
                        save_Lat_moy=path_txt+'portion'+str(len(Sv))+'Lat_moy.txt'
                        save_Lon_moy=path_txt+'portion'+str(len(Sv))+'Lon_moy.txt'
                
                    else:       
                        Sv_temp1=Sv[k:k+pas,:,intersect1[0]:intersect1[-1]+1]
                        Sv_temp2=Sv[k:k+pas,:,intersect2[0]:intersect2[-1]+1]
                        Sv_temp=np.vstack((Sv_temp1.T,Sv_temp2.T))
                        Sv_temp=Sv_temp.T
                        print("Sv_temp",Sv_temp.shape)
                        Lat_t=Lat[k:k+pas,:,:]
                        Lon_t=Lon[k:k+pas,:,:]
                        Time_t=Time[k:k+pas,:]
                    
                        filename_clustering_EK80 =path_save+'portion'+str(k+pas)+ "saved_clustering_for_EK80_all.pickle" 
                        save_labels=path_txt+'portion'+str(k+pas)+'Labels.txt'
                        save_Q1_=path_txt+'portion'+str(k+pas)+'Q1.txt'
                        save_Q3_=path_txt+'portion'+str(k+pas)+'Q3.txt'
                        save_med=path_txt+'portion'+str(k+pas)+'Med.txt'        
                        save_fig_clustering=path_png+'portion'+str(k+pas)+'Fig_clustering.png'
                        save_fig_Sv=path_png+str(k+pas)+"Figure_Sv.png"
                        save_Lat_moy=path_txt+'portion'+str(k+pas)+'Lat_moy.txt'
                        save_Lon_moy=path_txt+'portion'+str(k+pas)+'Lon_moy.txt'
                
                
                    classif.clustering_netcdf(
                    Sv_temp,
                    freq_MFR,
                    nombre_de_categories_sv,
                    Time_t,
                    Depth,
                    Lat_t,
                    Lon_t,
                    nb_transduc,
                    nom_numero_radiales,
                    methode_clustering,
                    nombre_composantes,
                    nombre_voisins,
                    save=save_data,
                    filename=filename_clustering_EK80,
                )
                    
                    with open(filename_clustering_EK80, "rb") as f:
                        (
                            kmeans,
                            labels,
                            Q1_,
                            Q3_,
                            med,
                            nombre_de_categories,
                            Sv_temp,
                            Time_t,
                            freq_MFR,
                            Depth,
                            Lat_t,
                            Lon_t,
                            nb_transduc,
                            nom_numero_radiales,
                            methode_clustering,
                        ) = pickle.load(f)
                                                 
                    couleurs = plt.cm.jet(np.linspace(0, 1, nombre_de_categories_sv))
                    cmap = colors.ListedColormap(couleurs)
                    
                    # # Affichage de la classification et de la réponse fréquentielle par clusters 
                    x=np.arange(0,len(Sv_temp),1)
                    y=np.arange(-Depth[1,0],-Depth[1,(len(Depth[1])-1)]-1,-1)
                
                    fig,ax2 = plt.subplots(figsize=(30, 15))
                    #image de classification
                    plt.subplot(211)
                    plt.xlabel('N*ESU')
                    plt.ylabel('profondeur (en m)')
                    plt.title(' ')
                    plt.pcolormesh(x,y,np.transpose(labels), cmap=cmap)
                    plt.grid()
                    plt.colorbar()
                    plt.show()
                    
                    #réponse en fréquence
                    plt.subplot(212)
                    plt.xlabel('Freq')
                    plt.ylabel('Sv dB')
                    plt.title('Reponse en frequence par cluster')
                    for j in range(nombre_de_categories_sv):
                        plt.plot(freq_MFR[:],Q1_[j],'-.',color=couleurs[j])
                        plt.plot(freq_MFR[:],Q3_[j],'-.',color=couleurs[j])
                        plt.plot(freq_MFR[:],med[j],'o-',color=couleurs[j])
                    plt.grid()
                    plt.close()
                
                    
                    
                    np.savetxt(save_labels,labels)
                    np.savetxt(save_Q1_,Q1_)
                    np.savetxt(save_Q3_,Q3_)
                    np.savetxt(save_med,med)
                    fig.savefig(save_fig_clustering)
                    
                
                        
            
                    freq_voulues = []
                    freq_log=[]
                    freq_lin=[]
                    freq_log_temp =  np.logspace(math.log10(freq_MFR[1]),math.log10(freq_MFR[len(freq_MFR)-1]),nbfreqs) 
                    freq_lin_temp =  np.linspace(freq_MFR[1],(freq_MFR[len(freq_MFR)-1]),nbfreqs) 
            
            
                    freq_custom_nearest = []
                    for i in range (len(freq_custom)) :
                        freq_custom_nearest.append(min(freq_MFR, key=lambda x:abs(x-freq_custom[i])))
                    for i in range (len(freq_log_temp)) :
                        freq_log.append(min(freq_MFR, key=lambda x:abs(x-freq_log_temp[i])))     
                    for i in range(len(freq_lin_temp)) :
                        freq_lin.append(min(freq_MFR, key=lambda x:abs(x-freq_lin_temp[i])))
                            
                    freq_voulues = freq_custom_nearest
                    
                    Sa_affiche = []
                    Sv_affiche = []
                    freqs_affichage = []
                    H_couche=(Depth[0, 1] - Depth[0, 0])
                    temp=ma.masked_object(Sv_temp,-10000000.0)
                    Sa_temp=10**(temp/10)*H_couche*1852**2*4*np.pi
                    for f in range(len(freq_voulues)):
                        indice = np.where(freq_MFR == freq_voulues[f])
                        Sv_affiche.append(np.transpose(np.squeeze(Sv_temp[:, :, indice])))
                        if methode_EI=='pickle':
                            Sa_moy = np.squeeze(Sa[:, :, f])
                        else:
                            Sa_affiche.append(np.transpose(np.squeeze(Sa_temp[:, :, indice])))
                        freqs_affichage.append((freq_voulues[f]) / 1000)
                        Lat_temp=Lat_t[:,:,f]
                        Lon_temp=Lon_t[:,:,f]
                        Time_temp=Time_t[:,f]
                        Time_temp=au.hac_read_day_time(Time_temp)
                    Sv_affiche = np.array(Sv_affiche)
                    fig, ax3 = plt.subplots(figsize=(24, 35))
                    dr.DrawMultiEchoGram(
                        Sv_affiche,
                        -Depth,
                        len(freq_voulues),
                        freqs=freqs_affichage,
                        title="Sv sondeur %d kHz",
                    )
                    fig.savefig(save_fig_Sv)
                    plt.close()
                    Sa_moy = np.array(Sa_affiche)
                    
                    # Suppression des valeurs n'ayant pas de sens dans la latitude et la longitude
                    index = np.where((Lon[:, 0] == -200) & (Lat[:, 0] == -100))
                
                    Lat_temp = np.delete(Lat_temp, (index), axis=0)
                    Lon_temp = np.delete(Lon_temp, (index), axis=0)
                
                    labels = np.delete(labels, (index), axis=0)
                    Sa_moy = np.delete(Sa_moy, (index), axis=0)
                    a = np.where(Sa_moy == 0)
                    Sa_moy[a] = np.NaN
            
                
                    Lat_moy = np.true_divide(Lat_temp.sum(1), (Lat_temp != 0).sum(1))
                    Lon_moy = np.true_divide(Lon_temp.sum(1), (Lon_temp != 0).sum(1))
                
                    # définition des couches surface et fond agrégées pour relier au chalutage
                    surface_limit = 30
                    indice_surface = int(
                        np.floor((surface_limit - Depth[0, 0]) / H_couche)
                    ) 
            
                    Sa_moy_label_surface = []
                    Sa_moy_label_fond = []
                    Sa_moy_label = []
                    Sa_moy_label_log = []
                    for f in range(len(freq_voulues)):
                        sa_temp=Sa_moy[f,:,:].T
                        for x in range(nombre_de_categories_sv):
                            Sa_label = np.zeros(sa_temp.shape)
                            Sa_label[:, :] = np.nan
                            index_lab = np.where(labels == x)
                            Sa_label[index_lab] = sa_temp[index_lab]
                            Sa_moy_label_surface.append(np.nansum(Sa_label[:, 0:indice_surface], axis=1))
                            Sa_moy_label_fond.append(np.nansum(Sa_label[:, indice_surface + 1 :], axis=1))
                            Sa_moy_label.append(np.nansum(Sa_label, axis=1))
                            Sa_moy_label_log.append(10 * np.log10(np.nanmean(Sa_label, axis=1)))
                    Sa_moy_label=np.array(Sa_moy_label)
                    Sa_moy_label=Sa_moy_label.reshape((len(freq_voulues),nombre_de_categories_sv,-1))
                    Sa_moy_label_log=np.array(Sa_moy_label_log)
                    Sa_moy_label_log=Sa_moy_label_log.reshape((len(freq_voulues),nombre_de_categories_sv,-1))
                    Sa_moy_label_fond=np.array(Sa_moy_label_fond)
                    Sa_moy_label_fond=Sa_moy_label_fond.reshape((len(freq_voulues),nombre_de_categories_sv,-1))
                    Sa_moy_label_surface=np.array(Sa_moy_label_surface)
                    Sa_moy_label_surface=Sa_moy_label_surface.reshape((len(freq_voulues),nombre_de_categories_sv,-1))
                    
                    #les cercles n'apparaissent que pour les valeurs positives (faire coincider figure et legende en n'affichant que les resultats positifs)
                    sa_log=np.zeros((nbfreqs,nombre_de_categories_sv,Sa_moy_label_log.shape[2]))
                    for f in range(len(freq_voulues)):
                        for x in range(nombre_de_categories_sv):
                            for i in range(Sa_moy_label_log.shape[2]):
                                if Sa_moy_label_log[f][x][i]>0:
                                    sa_log[f,x,i]=Sa_moy_label_log[f][x][i]+offset
                                elif Sa_moy_label_log[f][x][i]<=0 and np.isnan(Sa_moy_label_log[f][x][i])==False:
                                    sa_log[f,x,i]=offset
                    
                    ##################################################################################################
                    #
                    # Création de carte avec la librairie Cartopy : Carte simple , affichage dans les figures.
                    # Sa en valeurs naturelles
                    #
                    ##################################################################################################
                    ##################################################################################################
                    #
                    # Création de carte avec la librairie folium : Carte Openstreetmap, affichage dans un fichier HTML.
                    # Sa en Log
                    #
                    ##################################################################################################
            
            
                    plt.rc('legend',fontsize=15)    
            
                    for f in range(len(freq_voulues)):
                        if Sv_temp.shape[0]<pas:
                             save_html_Sa=path_html+'portion'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))
                             save_Sa_moy=path_txt+'portion'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))+'Sa_moy.txt'
                             save_fig_Sa=path_png+'portion'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))
                             save_fig_Salog=path_png+'portion_LogSa'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))
                        else:
                             save_html_Sa=path_html+'portion'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))
                             save_Sa_moy=path_txt+'portion'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))+'Sa_moy.txt'
                             save_fig_Sa=path_png+'portion'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))
                             save_fig_Salog=path_png+'portion_LogSa'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))
                        md.affichage_carte_france(nombre_de_categories, Lat_moy, Lon_moy, Sa_moy_label[f,:,:], extension_zone, 'Sa',save_fig_Sa)
                        md.affichage_carte_france(nombre_de_categories, Lat_moy, Lon_moy, sa_log[f,:,:], extension_zone, 'LogSa',save_fig_Salog)
                        md.save_map_html(nombre_de_categories, Lat_moy, Lon_moy, Sa_moy_label_log[f,:,:], save_html_Sa)
                        np.savetxt(save_Sa_moy,Sa_moy[f,:,:])
                        
                        ##################################################################################################
                        #
                        # Export de Sa (en valeurs naturelles et log), Lat et Lon dans un fichier CSV
                        #
                        #################################################################################################
                    
                        for x in range(nombre_de_categories_sv):
                            with open(
                                "%s/Sa_surface_label_%d_freq_%d.csv"
                                % (path_csv, x, freq_custom_nearest[f]),
                                "w",
                                newline="",
                            ) as csvfile:
                                writer = csv.writer(csvfile, delimiter=" ")
                                for i in range(len(Lat_moy)):
                                    if Sa_moy_label_surface[f][x][i] > 0:
                                        writer.writerow(
                                            (
                                                Time_temp[i].strftime("%d/%m/%Y %H:%M:%S"),
                                                Lat_moy[i],
                                                Lon_moy[i],
                                                Sa_moy_label_surface[f][x][i],
                                            )
                                        )
                                        
                                        
                    
                        for x in range(nombre_de_categories_sv):
                            with open(
                                "%s/Sa_fond_label_%d_freq_%d.csv"
                                % (path_csv, x, freq_custom_nearest[f]),
                                "w",
                                newline="",
                            ) as csvfile:
                                writer = csv.writer(csvfile, delimiter=" ")
                                for i in range(len(Lat_moy)):
                                    if Sa_moy_label_fond[f][x][i] > 0:
                                        writer.writerow(
                                            (
                                                Time_temp[i].strftime("%d/%m/%Y %H:%M:%S"),
                                                Lat_moy[i],
                                                Lon_moy[i],
                                                Sa_moy_label_fond[f][x][i],
                                            )
                                        )
                                        
                    
                        for x in range(nombre_de_categories_sv):
                            with open(
                                "%s/Sa_label_%d_freq_%d.csv" % (path_csv, x, freq_custom_nearest[f]),
                                "w",
                                newline="",
                            ) as csvfile:
                                writer = csv.writer(csvfile, delimiter=" ")
                                for i in range(len(Lat_moy)):
                                    if Sa_moy_label[f][x][i] > 0:
                                        writer.writerow(
                                            (
                                                Time_temp[i].strftime("%d/%m/%Y %H:%M:%S"),
                                                Lat_moy[i],
                                                Lon_moy[i],
                                                Sa_moy_label[f][x][i],
                                            )
                                        )
                    
                    ##################################################################################################-
                    
                    filelist = glob2.glob("%s/**/*.hac" % path_hac_survey)
                    if len(filelist) == 0:
                        raise RuntimeError(path_hac_survey + " does not contain HAC files")
                    
                    list_sounder = au.hac_sounder_descr(filelist[0])
                    nb_snd = list_sounder.GetNbSounder()
                    
                    with open("%s/rapport.adoc" % path_png, "w") as rapport:
                    
                        rapport.write(("= RAPPORT \n\n"))
                        rapport.write(("==== Descriptif des sondeurs \n\n"))
                        rapport.write((" nb sondeurs = " + str(nb_snd) + "\n"))
                        for isdr in range(nb_snd):
                            sounder = list_sounder.GetSounder(isdr)
                            nb_transducs = sounder.m_numberOfTransducer
                            rapport.write(
                                (
                                    "sondeur "
                                    + str(isdr)
                                    + ":    index: "
                                    + str(sounder.m_SounderId)
                                    + "   nb trans:"
                                    + str(nb_transducs)
                                    + "\n"
                                )
                            )
                            for itr in range(nb_transducs):
                                trans = sounder.GetTransducer(itr)
                                for ibeam in range(trans.m_numberOfSoftChannel):
                                    softChan = trans.getSoftChannelPolarX(ibeam)
                                    rapport.write(
                                        (
                                            "   trans "
                                            + str(itr)
                                            + ":    nom: "
                                            + trans.m_transName
                                            + "   freq: "
                                            + str(softChan.m_acousticFrequency / 1000)
                                            + " kHz\n"
                                        )
                                    )
                    
                        rapport.write(
                            ("\n==== Etude de la classification multifrequence sur le sondeur EK80\n\n")
                        )
                    
                        #  for x in range (len(nom_numero_radiales[1])):
                        #     rapport.write("===== Sv moyen par transducteurs pour la radiale %s\n\n" %(nom_numero_radiales[0][x]))
                        #     rapport.write(("image:figure_Sv_moy_%d.png[] \n\n" %(nom_numero_radiales[1][x])))
                    
                        for x in range(len(nom_numero_radiales[1])):
                            rapport.write("===== Sv pour la radiale %s\n\n" % (nom_numero_radiales[0][x]))
                            rapport.write(("image:figure_Sv_%d.png[] \n\n" % (nom_numero_radiales[1][x])))
                    
                        rapport.write("===== Resultat de classification\n\n")
                        rapport.write("image:figure_clustering.png[] \n\n")
                    
                        for x in range(nombre_de_categories_sv):
                            rapport.write("===== Affichage du Sa pour le label %d\n\n" % x)
                            rapport.write(("image:map_label_%d.png[] \n\n" % x))
                    
                    sounder = list_sounder.GetSounder(0)
                    np.savetxt(save_Lat_moy,Lat_moy)
                    np.savetxt(save_Lon_moy,Lon_moy)
                    
                else:
                    len_freq_MFR=len(intersect1)+len(intersect2)

                    freq_voulues = []
                    freq_log=[]
                    freq_lin=[]
                    freq_MFR=freq_MFR.reshape((len(freq_MFR),-1))
                    if freq_MFR.shape[1]!=1:
                        freq_log_temp =  np.logspace(math.log10(freq_MFR[1]),math.log10(freq_MFR[len(freq_MFR)-1]),nbfreqs) 
                        freq_lin_temp =  np.linspace(freq_MFR[1],(freq_MFR[len(freq_MFR)-1]),nbfreqs) 
                    else:
                        freq_log_temp =  np.logspace(math.log10(freq_MFR[0]),math.log10(freq_MFR[len(freq_MFR)-1]),nbfreqs) 
                        freq_lin_temp =  np.linspace(freq_MFR[0],(freq_MFR[len(freq_MFR)-1]),nbfreqs)
                    
                    freq_custom_nearest = []
                    for i in range (len(freq_custom)) :
                        freq_custom_nearest.append(min(freq_MFR, key=lambda x:abs(x-freq_custom[i])))
                    for i in range (len(freq_log_temp)) :
                        freq_log.append(min(freq_MFR, key=lambda x:abs(x-freq_log_temp[i])))     
                    for i in range(len(freq_lin_temp)) :
                        freq_lin.append(min(freq_MFR, key=lambda x:abs(x-freq_lin_temp[i])))
                          
                            
                    freq_voulues = freq_custom_nearest
                    if len(Sv)-k<pas:
                        Sv_temp=Sv[k:len(Sv),:,:]
                        print("Sv_temp",Sv_temp.shape)
                        
                        save_labels=path_txt+'portion'+str(len(Sv))+'Labels.txt'
                        save_med=path_txt+'portion'+str(len(Sv))+'Med.txt'
                        save_Q1=path_txt+'portion'+str(len(Sv))+'Q1.txt'        
                        save_Q3=path_txt+'portion'+str(len(Sv))+'Q3.txt'        
                        save_Sa_moy=path_txt+'portion'+str(len(Sv))+'Sa_moy.txt'
                        save_Lat_moy=path_txt+'portion'+str(len(Sv))+'Lat_moy.txt'
                        save_Lon_moy=path_txt+'portion'+str(len(Sv))+'Lon_moy.txt'
                        
                        labels=np.empty((Sv_temp.shape[0],Sv_temp.shape[1]))
                        labels.fill(np.nan)
                        np.savetxt(save_labels,labels)
                        Lat_moy=np.empty((Sv_temp.shape[0],))
                        Lat_moy.fill(np.nan)
                        np.savetxt(save_Lat_moy,Lat_moy)
                        Lon_moy=np.empty((Sv_temp.shape[0],))
                        Lon_moy.fill(np.nan)
                        np.savetxt(save_Lon_moy,Lon_moy)
                        med=np.empty((nombre_de_categories_sv,1000))
                        med.fill(np.nan)
                        np.savetxt(save_med,med)
                        Q1=np.empty((nombre_de_categories_sv,1000))
                        Q1.fill(np.nan)
                        np.savetxt(save_Q1,Q1)
                        Q3=np.empty((nombre_de_categories_sv,1000))
                        Q3.fill(np.nan)
                        np.savetxt(save_Q3,Q3)
                        for f in range(nbfreqs):
                            Sa_moy=np.empty((Sv_temp.shape[1],Sv_temp.shape[0]))
                            Sa_moy.fill(np.nan)
                            save_Sa_moy=path_txt+'portion'+str(len(Sv))+'_'+str(np.round(freq_custom[f]/1000))+'Sa_moy.txt'
                            np.savetxt(save_Sa_moy,Sa_moy)
                    else:
                        Sv_temp=Sv[k:k+pas,:,:]
                        print("Sv_temp",Sv_temp.shape)
                    
                        save_labels=path_txt+'portion'+str(k+pas)+'Labels.txt'
                        save_med=path_txt+'portion'+str(k+pas)+'Med.txt'    
                        save_Q1=path_txt+'portion'+str(k+pas)+'Q1.txt'        
                        save_Q3=path_txt+'portion'+str(k+pas)+'Q3.txt'
                        save_Sa_moy=path_txt+'portion'+str(k+pas)+'Sa_moy.txt'
                        save_Lat_moy=path_txt+'portion'+str(k+pas)+'Lat_moy.txt'
                        save_Lon_moy=path_txt+'portion'+str(k+pas)+'Lon_moy.txt'
                        
                        labels=np.empty((Sv_temp.shape[0],Sv_temp.shape[1]))
                        labels.fill(np.nan)
                        np.savetxt(save_labels,labels)
                        Lat_moy=np.empty((Sv_temp.shape[0],))
                        Lat_moy.fill(np.nan)
                        np.savetxt(save_Lat_moy,Lat_moy)
                        Lon_moy=np.empty((Sv_temp.shape[0],))
                        Lon_moy.fill(np.nan)
                        np.savetxt(save_Lon_moy,Lon_moy)
                        med=np.empty((nombre_de_categories_sv,1000))
                        med.fill(np.nan)
                        np.savetxt(save_med,med)
                        Q1=np.empty((nombre_de_categories_sv,1000))
                        Q1.fill(np.nan)
                        np.savetxt(save_Q1,Q1)
                        Q3=np.empty((nombre_de_categories_sv,1000))
                        Q3.fill(np.nan)
                        np.savetxt(save_Q3,Q3)
                        for f in range(nbfreqs):
                            Sa_moy=np.empty((Sv_temp.shape[1],Sv_temp.shape[0]))
                            Sa_moy.fill(np.nan)
                            save_Sa_moy=path_txt+'portion'+str(k+pas)+'_'+str(np.round(freq_custom[f]/1000))+'Sa_moy.txt'
                            np.savetxt(save_Sa_moy,Sa_moy)
            else:
                a=np.where(freq_MFR>indice_freq_min)
                b=np.where(freq_MFR<indice_freq_max)
                intersect=numpy.intersect1d(a,b)
                if len(intersect)!=0:
                    freq_MFR=freq_MFR[intersect[0]:intersect[-1]+1]
                    if len(Sv)-k<pas:
                        Sv_temp1=Sv[k:len(Sv),:,intersect[0]:intersect[-1]+1]
                        print("Sv_temp",Sv_temp.shape)
                        Lat_t=Lat[k:len(Sv),:,:]
                        Lon_t=Lon[k:len(Sv),:,:]
                        Time_t=Time[k:len(Sv),:]        
                        
                        filename_clustering_EK80 =path_save+'portion'+str(len(Sv))+ "saved_clustering_for_EK80_all.pickle" 
                        save_labels=path_txt+'portion'+str(len(Sv))+'Labels.txt'
                        save_Q1_=path_txt+'portion'+str(len(Sv))+'Q1.txt'
                        save_Q3_=path_txt+'portion'+str(len(Sv))+'Q3.txt'
                        save_med=path_txt+'portion'+str(len(Sv))+'Med.txt'        
                        save_fig_clustering=path_png+'portion'+str(len(Sv))+'Fig_clustering.png'
                        save_fig_Sv=path_png+str(len(Sv))+"Figure_Sv.png"
                        save_Lat_moy=path_txt+'portion'+str(len(Sv))+'Lat_moy.txt'
                        save_Lon_moy=path_txt+'portion'+str(len(Sv))+'Lon_moy.txt'
                
                    else:       
                        Sv_temp=Sv[k:k+pas,:,intersect[0]:intersect[-1]+1]
                        print("Sv_temp",Sv_temp.shape)
                        Lat_t=Lat[k:k+pas,:,:]
                        Lon_t=Lon[k:k+pas,:,:]
                        Time_t=Time[k:k+pas,:]
                    
                        filename_clustering_EK80 =path_save+'portion'+str(k+pas)+ "saved_clustering_for_EK80_all.pickle" 
                        save_labels=path_txt+'portion'+str(k+pas)+'Labels.txt'
                        save_Q1_=path_txt+'portion'+str(k+pas)+'Q1.txt'
                        save_Q3_=path_txt+'portion'+str(k+pas)+'Q3.txt'
                        save_med=path_txt+'portion'+str(k+pas)+'Med.txt'        
                        save_fig_clustering=path_png+'portion'+str(k+pas)+'Fig_clustering.png'
                        save_fig_Sv=path_png+str(k+pas)+"Figure_Sv.png"
                        save_Lat_moy=path_txt+'portion'+str(k+pas)+'Lat_moy.txt'
                        save_Lon_moy=path_txt+'portion'+str(k+pas)+'Lon_moy.txt'
                
                
                    classif.clustering_netcdf(
                    Sv_temp,
                    freq_MFR,
                    nombre_de_categories_sv,
                    Time_t,
                    Depth,
                    Lat_t,
                    Lon_t,
                    nb_transduc,
                    nom_numero_radiales,
                    methode_clustering,
                    nombre_composantes,
                    nombre_voisins,
                    save=save_data,
                    filename=filename_clustering_EK80,
                )
                    
                    with open(filename_clustering_EK80, "rb") as f:
                        (
                            kmeans,
                            labels,
                            Q1_,
                            Q3_,
                            med,
                            nombre_de_categories,
                            Sv_temp,
                            Time_t,
                            freq_MFR,
                            Depth,
                            Lat_t,
                            Lon_t,
                            nb_transduc,
                            nom_numero_radiales,
                            methode_clustering,
                        ) = pickle.load(f)
                                                 
                    couleurs = plt.cm.jet(np.linspace(0, 1, nombre_de_categories_sv))
                    cmap = colors.ListedColormap(couleurs)
                    
                    # # Affichage de la classification et de la réponse fréquentielle par clusters 
                    x=np.arange(0,len(Sv_temp),1)
                    if len(Depth)==1:
                        y=np.arange(-Depth[0,0],-Depth[0,(len(Depth[0])-1)]-1,-1)
                    else:
                        y=np.arange(-Depth[1,0],-Depth[1,(len(Depth[1])-1)]-1,-1)
                
                    fig,ax2 = plt.subplots(figsize=(30, 15))
                    #image de classification
                    plt.subplot(211)
                    plt.xlabel('N*ESU')
                    plt.ylabel('profondeur (en m)')
                    plt.title(' ')
                    plt.pcolormesh(x,y,np.transpose(labels), cmap=cmap)
                    plt.grid()
                    plt.colorbar()
                    plt.show()
                    
                    #réponse en fréquence
                    plt.subplot(212)
                    plt.xlabel('Freq')
                    plt.ylabel('Sv dB')
                    plt.title('Reponse en frequence par cluster')
                    for j in range(nombre_de_categories_sv):
                        plt.plot(freq_MFR[:],Q1_[j],'-.',color=couleurs[j])
                        plt.plot(freq_MFR[:],Q3_[j],'-.',color=couleurs[j])
                        plt.plot(freq_MFR[:],med[j],'o-',color=couleurs[j])
                    plt.grid()
                    plt.close()
                
                    
                    
                    np.savetxt(save_labels,labels)
                    np.savetxt(save_Q1_,Q1_)
                    np.savetxt(save_Q3_,Q3_)
                    np.savetxt(save_med,med)
                    fig.savefig(save_fig_clustering)
                    
                
                    freq_voulues = []
                    freq_log=[]
                    freq_lin=[]
                    freq_MFR=freq_MFR.reshape((len(freq_MFR),-1))
                    if freq_MFR.shape[1]!=1:
                        freq_log_temp =  np.logspace(math.log10(freq_MFR[1]),math.log10(freq_MFR[len(freq_MFR)-1]),nbfreqs) 
                        freq_lin_temp =  np.linspace(freq_MFR[1],(freq_MFR[len(freq_MFR)-1]),nbfreqs) 
                    else:
                        freq_log_temp =  np.logspace(math.log10(freq_MFR[0]),math.log10(freq_MFR[len(freq_MFR)-1]),nbfreqs) 
                        freq_lin_temp =  np.linspace(freq_MFR[0],(freq_MFR[len(freq_MFR)-1]),nbfreqs)
                    
                    freq_custom_nearest = []
                    for i in range (len(freq_custom)) :
                        freq_custom_nearest.append(min(freq_MFR, key=lambda x:abs(x-freq_custom[i])))
                    for i in range (len(freq_log_temp)) :
                        freq_log.append(min(freq_MFR, key=lambda x:abs(x-freq_log_temp[i])))     
                    for i in range(len(freq_lin_temp)) :
                        freq_lin.append(min(freq_MFR, key=lambda x:abs(x-freq_lin_temp[i])))
                            
                    freq_voulues = freq_custom_nearest

                    freqs_affichage = []
                    H_couche=(Depth[0, 1] - Depth[0, 0])
                    temp=ma.masked_object(Sv_temp,-10000000.0)
                    Sa_temp=10**(temp/10)*H_couche*1852**2*4*np.pi
                    for f in range(len(freq_voulues)):
                        Sv_affiche=Sv_temp
                        if methode_EI=='pickle':
                            Sa_moy = np.squeeze(Sa[:, :, f])
                        else:
                            #Sa_affiche.append(np.transpose(np.squeeze(Sa_temp[:, :, indice])))
                            Sa_affiche=Sa_temp
                        freqs_affichage.append((freq_voulues[f]) / 1000)
                        Lat_temp=Lat_t[:,:,f]
                        Lon_temp=Lon_t[:,:,f]
                        Time_temp=Time_t[:,f]
                        Time_temp=au.hac_read_day_time(Time_temp)
                    fig, ax3 = plt.subplots(figsize=(24, 35))
                    dr.DrawMultiEchoGram(
                        Sv_affiche.T,
                        -Depth,
                        len(freq_voulues),
                        freqs=freqs_affichage,
                        title="Sv sondeur %d kHz",
                    )
                    fig.savefig(save_fig_Sv)
                    plt.close()
                    Sa_moy=Sa_affiche.T
                    
                    # Suppression des valeurs n'ayant pas de sens dans la latitude et la longitude
                    index = np.where((Lon[:, 0] == -200) & (Lat[:, 0] == -100))
                
                    Lat_temp = np.delete(Lat_temp, (index), axis=0)
                    Lon_temp = np.delete(Lon_temp, (index), axis=0)
                
                    labels = np.delete(labels, (index), axis=0)
                    Sa_moy = np.delete(Sa_moy, (index), axis=0)
                    a = np.where(Sa_moy == 0)
                    Sa_moy[a] = np.NaN
            
                
                    Lat_moy = np.true_divide(Lat_temp.sum(1), (Lat_temp != 0).sum(1))
                    Lon_moy = np.true_divide(Lon_temp.sum(1), (Lon_temp != 0).sum(1))
                
                    # définition des couches surface et fond agrégées pour relier au chalutage
                    surface_limit = 30
                    indice_surface = int(
                        np.floor((surface_limit - Depth[0, 0]) / H_couche)
                    ) 
            
                    Sa_moy_label_surface = []
                    Sa_moy_label_fond = []
                    Sa_moy_label = []
                    Sa_moy_label_log = []
                    for f in range(len(freq_voulues)):
                        sa_temp=Sa_moy[f,:,:].T
                        for x in range(nombre_de_categories_sv):
                            Sa_label = np.zeros(sa_temp.shape)
                            Sa_label[:, :] = np.nan
                            index_lab = np.where(labels == x)
                            Sa_label[index_lab] = sa_temp[index_lab]
                            Sa_moy_label_surface.append(np.nansum(Sa_label[:, 0:indice_surface], axis=1))
                            Sa_moy_label_fond.append(np.nansum(Sa_label[:, indice_surface + 1 :], axis=1))
                            Sa_moy_label.append(np.nansum(Sa_label, axis=1))
                            Sa_moy_label_log.append(10 * np.log10(np.nanmean(Sa_label, axis=1)))
                    Sa_moy_label=np.array(Sa_moy_label)
                    Sa_moy_label=Sa_moy_label.reshape((len(freq_voulues),nombre_de_categories_sv,-1))
                    Sa_moy_label_log=np.array(Sa_moy_label_log)
                    Sa_moy_label_log=Sa_moy_label_log.reshape((len(freq_voulues),nombre_de_categories_sv,-1))
                    Sa_moy_label_fond=np.array(Sa_moy_label_fond)
                    Sa_moy_label_fond=Sa_moy_label_fond.reshape((len(freq_voulues),nombre_de_categories_sv,-1))
                    Sa_moy_label_surface=np.array(Sa_moy_label_surface)
                    Sa_moy_label_surface=Sa_moy_label_surface.reshape((len(freq_voulues),nombre_de_categories_sv,-1))
                    
                    #les cercles n'apparaissent que pour les valeurs positives (faire coincider figure et legende en n'affichant que les resultats positifs)
                    sa_log=np.zeros((nbfreqs,nombre_de_categories_sv,Sa_moy_label_log.shape[2]))
                    for f in range(len(freq_voulues)):
                        for x in range(nombre_de_categories_sv):
                            for i in range(Sa_moy_label_log.shape[2]):
                                if Sa_moy_label_log[f][x][i]>0:
                                    sa_log[f,x,i]=Sa_moy_label_log[f][x][i]+offset
                                elif Sa_moy_label_log[f][x][i]<=0 and np.isnan(Sa_moy_label_log[f][x][i])==False:
                                    sa_log[f,x,i]=offset
                    
                    ##################################################################################################
                    #
                    # Création de carte avec la librairie Cartopy : Carte simple , affichage dans les figures.
                    # Sa en valeurs naturelles
                    #
                    ##################################################################################################
                    ##################################################################################################
                    #
                    # Création de carte avec la librairie folium : Carte Openstreetmap, affichage dans un fichier HTML.
                    # Sa en Log
                    #
                    ##################################################################################################
            
            
                    plt.rc('legend',fontsize=15)    
            
                    for f in range(len(freq_voulues)):
                        if Sv_temp.shape[0]<pas:
                             save_html_Sa=path_html+'portion'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))
                             save_Sa_moy=path_txt+'portion'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))+'Sa_moy.txt'
                             save_fig_Sa=path_png+'portion'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))
                             save_fig_Salog=path_png+'portion_LogSa'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))
                        else:
                             save_html_Sa=path_html+'portion'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))
                             save_Sa_moy=path_txt+'portion'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))+'Sa_moy.txt'
                             save_fig_Sa=path_png+'portion'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))
                             save_fig_Salog=path_png+'portion_LogSa'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))
                        md.affichage_carte_france(nombre_de_categories, Lat_moy, Lon_moy, Sa_moy_label[f,:,:], extension_zone, 'Sa',save_fig_Sa)
                        md.affichage_carte_france(nombre_de_categories, Lat_moy, Lon_moy, sa_log[f,:,:], extension_zone, 'LogSa',save_fig_Salog)
                        md.save_map_html(nombre_de_categories, Lat_moy, Lon_moy, Sa_moy_label_log[f,:,:], save_html_Sa)
                        np.savetxt(save_Sa_moy,Sa_moy[f,:,:])
                        
                        ##################################################################################################
                        #
                        # Export de Sa (en valeurs naturelles et log), Lat et Lon dans un fichier CSV
                        #
                        #################################################################################################
                    
                        for x in range(nombre_de_categories_sv):
                            with open(
                                "%s/Sa_surface_label_%d_freq_%d.csv"
                                % (path_csv, x, freq_custom_nearest[f]),
                                "w",
                                newline="",
                            ) as csvfile:
                                writer = csv.writer(csvfile, delimiter=" ")
                                for i in range(len(Lat_moy)):
                                    if Sa_moy_label_surface[f][x][i] > 0:
                                        writer.writerow(
                                            (
                                                Time_temp[i].strftime("%d/%m/%Y %H:%M:%S"),
                                                Lat_moy[i],
                                                Lon_moy[i],
                                                Sa_moy_label_surface[f][x][i],
                                            )
                                        )
                                        
                                        
                    
                        for x in range(nombre_de_categories_sv):
                            with open(
                                "%s/Sa_fond_label_%d_freq_%d.csv"
                                % (path_csv, x, freq_custom_nearest[f]),
                                "w",
                                newline="",
                            ) as csvfile:
                                writer = csv.writer(csvfile, delimiter=" ")
                                for i in range(len(Lat_moy)):
                                    if Sa_moy_label_fond[f][x][i] > 0:
                                        writer.writerow(
                                            (
                                                Time_temp[i].strftime("%d/%m/%Y %H:%M:%S"),
                                                Lat_moy[i],
                                                Lon_moy[i],
                                                Sa_moy_label_fond[f][x][i],
                                            )
                                        )
                                        
                    
                        for x in range(nombre_de_categories_sv):
                            with open(
                                "%s/Sa_label_%d_freq_%d.csv" % (path_csv, x, freq_custom_nearest[f]),
                                "w",
                                newline="",
                            ) as csvfile:
                                writer = csv.writer(csvfile, delimiter=" ")
                                for i in range(len(Lat_moy)):
                                    if Sa_moy_label[f][x][i] > 0:
                                        writer.writerow(
                                            (
                                                Time_temp[i].strftime("%d/%m/%Y %H:%M:%S"),
                                                Lat_moy[i],
                                                Lon_moy[i],
                                                Sa_moy_label[f][x][i],
                                            )
                                        )
                    
                    ##################################################################################################-
                    
                    filelist = glob2.glob("%s/**/*.hac" % path_hac_survey)
                    if len(filelist) == 0:
                        raise RuntimeError(path_hac_survey + " does not contain HAC files")
                    
                    list_sounder = au.hac_sounder_descr(filelist[0])
                    nb_snd = list_sounder.GetNbSounder()
                    
                    with open("%s/rapport.adoc" % path_png, "w") as rapport:
                    
                        rapport.write(("= RAPPORT \n\n"))
                        rapport.write(("==== Descriptif des sondeurs \n\n"))
                        rapport.write((" nb sondeurs = " + str(nb_snd) + "\n"))
                        for isdr in range(nb_snd):
                            sounder = list_sounder.GetSounder(isdr)
                            nb_transducs = sounder.m_numberOfTransducer
                            rapport.write(
                                (
                                    "sondeur "
                                    + str(isdr)
                                    + ":    index: "
                                    + str(sounder.m_SounderId)
                                    + "   nb trans:"
                                    + str(nb_transducs)
                                    + "\n"
                                )
                            )
                            for itr in range(nb_transducs):
                                trans = sounder.GetTransducer(itr)
                                for ibeam in range(trans.m_numberOfSoftChannel):
                                    softChan = trans.getSoftChannelPolarX(ibeam)
                                    rapport.write(
                                        (
                                            "   trans "
                                            + str(itr)
                                            + ":    nom: "
                                            + trans.m_transName
                                            + "   freq: "
                                            + str(softChan.m_acousticFrequency / 1000)
                                            + " kHz\n"
                                        )
                                    )
                    
                        rapport.write(
                            ("\n==== Etude de la classification multifrequence sur le sondeur EK80\n\n")
                        )
                    
                        #  for x in range (len(nom_numero_radiales[1])):
                        #     rapport.write("===== Sv moyen par transducteurs pour la radiale %s\n\n" %(nom_numero_radiales[0][x]))
                        #     rapport.write(("image:figure_Sv_moy_%d.png[] \n\n" %(nom_numero_radiales[1][x])))
                    
                        for x in range(len(nom_numero_radiales[1])):
                            rapport.write("===== Sv pour la radiale %s\n\n" % (nom_numero_radiales[0][x]))
                            rapport.write(("image:figure_Sv_%d.png[] \n\n" % (nom_numero_radiales[1][x])))
                    
                        rapport.write("===== Resultat de classification\n\n")
                        rapport.write("image:figure_clustering.png[] \n\n")
                    
                        for x in range(nombre_de_categories_sv):
                            rapport.write("===== Affichage du Sa pour le label %d\n\n" % x)
                            rapport.write(("image:map_label_%d.png[] \n\n" % x))
                    
                    sounder = list_sounder.GetSounder(0)
                    np.savetxt(save_Lat_moy,Lat_moy)
                    np.savetxt(save_Lon_moy,Lon_moy)
                    
                else:
                    len_freq_MFR=len(intersect)

                    if len(Sv)-k<pas:
                        Sv_temp=Sv[k:len(Sv),:,:]
                        print("Sv_temp",Sv_temp.shape)
                        
                        save_labels=path_txt+'portion'+str(len(Sv))+'Labels.txt'
                        save_med=path_txt+'portion'+str(len(Sv))+'Med.txt'   
                        save_Q1=path_txt+'portion'+str(len(Sv))+'Q1.txt'        
                        save_Q3=path_txt+'portion'+str(len(Sv))+'Q3.txt' 
                        save_Lat_moy=path_txt+'portion'+str(len(Sv))+'Lat_moy.txt'
                        save_Lon_moy=path_txt+'portion'+str(len(Sv))+'Lon_moy.txt'
                        labels=np.empty((Sv_temp.shape[0],Sv_temp.shape[1]))
                        labels.fill(np.nan)
                        np.savetxt(save_labels,labels)
                        Lat_moy=np.empty((Sv_temp.shape[0],))
                        Lat_moy.fill(np.nan)
                        np.savetxt(save_Lat_moy,Lat_moy)
                        Lon_moy=np.empty((Sv_temp.shape[0],))
                        Lon_moy.fill(np.nan)
                        np.savetxt(save_Lon_moy,Lon_moy)
                        med=np.empty((nombre_de_categories_sv,1000))
                        med.fill(np.nan)
                        np.savetxt(save_med,med)
                        Q1=np.empty((nombre_de_categories_sv,1000))
                        Q1.fill(np.nan)
                        np.savetxt(save_Q1,Q1)
                        Q3=np.empty((nombre_de_categories_sv,1000))
                        Q3.fill(np.nan)
                        np.savetxt(save_Q3,Q3)
                        for f in range(nbfreqs):
                            Sa_moy=np.empty((Sv_temp.shape[1],Sv_temp.shape[0]))
                            Sa_moy.fill(np.nan)
                            save_Sa_moy=path_txt+'portion'+str(len(Sv))+'_'+str(np.round(freq_custom[f]/1000))+'Sa_moy.txt'
                            np.savetxt(save_Sa_moy,Sa_moy)
                        
                    else:
                        Sv_temp=Sv[k:k+pas,:,:]
                        print("Sv_temp",Sv_temp.shape)
                    
                        save_labels=path_txt+'portion'+str(k+pas)+'Labels.txt'
                        save_med=path_txt+'portion'+str(k+pas)+'Med.txt'   
                        save_Q1=path_txt+'portion'+str(k+pas)+'Q1.txt'        
                        save_Q3=path_txt+'portion'+str(k+pas)+'Q3.txt' 
                        save_Lat_moy=path_txt+'portion'+str(k+pas)+'Lat_moy.txt'
                        save_Lon_moy=path_txt+'portion'+str(k+pas)+'Lon_moy.txt'
                        
                        labels=np.empty((Sv_temp.shape[0],Sv_temp.shape[1]))
                        labels.fill(np.nan)
                        np.savetxt(save_labels,labels)
                        Lat_moy=np.empty((Sv_temp.shape[0],))
                        Lat_moy.fill(np.nan)
                        np.savetxt(save_Lat_moy,Lat_moy)
                        Lon_moy=np.empty((Sv_temp.shape[0],))
                        Lon_moy.fill(np.nan)
                        np.savetxt(save_Lon_moy,Lon_moy)
                        med=np.empty((nombre_de_categories_sv,1000))
                        med.fill(np.nan)
                        np.savetxt(save_med,med)
                        Q1=np.empty((nombre_de_categories_sv,1000))
                        Q1.fill(np.nan)
                        np.savetxt(save_Q1,Q1)
                        Q3=np.empty((nombre_de_categories_sv,1000))
                        Q3.fill(np.nan)
                        np.savetxt(save_Q3,Q3)
                        for f in range(nbfreqs):
                            Sa_moy=np.empty((Sv_temp.shape[1],Sv_temp.shape[0]))
                            Sa_moy.fill(np.nan)
                            save_Sa_moy=path_txt+'portion'+str(k+pas)+'_'+str(np.round(freq_custom[f]/1000))+'Sa_moy.txt'
                            np.savetxt(save_Sa_moy,Sa_moy)
else:
    pass                            
################################################################################
#RECOMBINAISON DES RESULTATS
################################################################################
"""
Recombinaison des portions de la campagne générées ci dessus et classification 
de la donnée par regroupement des RF semblables en clusters  
"""
len_Sv1=np.loadtxt(fpath+'Profondeur_max.txt',dtype='int')
indice_profondeur_max = np.max(len_Sv1)

for root, subFolders, files in os.walk(fpath):
    for file in files:
        if methode_EI=='pickle':
            if file.endswith("saved_clustering_for_EK80_all.pickle"):
                with open(root+'/'+file, "rb") as f:
                    (
                        freq_MFR,
                        freqs_moy_transducteur,
                        Sv,
                        Sv_moy_transducteur_x,
                        Sa,
                        Depth,
                        Lat,
                        Lon,
                        Time,
                        nb_transduc,
                        tableau_radiales,
                        nom_numero_radiales,
                    ) = pickle.load(f) 
                break
        else:
            if file.endswith("1.xsf.nc"):
                dataset=nc.Dataset(root+'/'+file)
                Sv=dataset.groups['Sonar'].groups['Grid_group_1'].variables['integrated_backscatter'][:]
                freq_MFR=dataset.groups['Sonar'].groups['Grid_group_1'].variables['frequency'][:]
                freq_MFR=np.sort(freq_MFR)
                Time=dataset.groups['Sonar'].groups['Grid_group_1'].variables['cell_ping_time'][:]
                Depth=dataset.groups['Sonar'].groups['Grid_group_1'].variables['cell_depth'][:]
                Depth=Depth.T
                Lat=dataset.groups['Sonar'].groups['Grid_group_1'].variables['cell_latitude'][:]
                Lon=dataset.groups['Sonar'].groups['Grid_group_1'].variables['cell_longitude'][:]
                break
            break
        break
    break
    
dirlistQ1_ = []
dirlistQ3_ = []
dirlistMed = []
dirlistLabels=[]
dirlistSa_moy=[]
dirlistLat_moy=[]
dirlistLon_moy=[]
for root, subFolders, files in os.walk(fpath):
    print(root,subFolders,files)
    for file in files:
        print(file)
        if file.endswith("Q1.txt"):
            dirlistQ1_.append(os.path.join(root, file))
        elif file.endswith("Q3.txt"):
            dirlistQ3_.append(os.path.join(root, file))
        elif file.endswith("Med.txt"):
            dirlistMed.append(os.path.join(root, file))
        elif file.endswith("Labels.txt"):
            dirlistLabels.append(os.path.join(root, file))
        elif file.endswith("Sa_moy.txt"):
            dirlistSa_moy.append(os.path.join(root, file))
        elif file.endswith("Lat_moy.txt"):
            dirlistLat_moy.append(os.path.join(root, file))
        elif file.endswith("Lon_moy.txt"):
            dirlistLon_moy.append(os.path.join(root, file))
            
            
filelistQ1_ = sorted_alphanumeric(dirlistQ1_)
filelistQ3_ = sorted_alphanumeric(dirlistQ3_)
filelistMed = sorted_alphanumeric(dirlistMed)
filelistLabels = sorted_alphanumeric(dirlistLabels)
filelistSa_moy = sorted_alphanumeric(dirlistSa_moy)
filelistLat_moy = sorted_alphanumeric(dirlistLat_moy)
filelistLon_moy = sorted_alphanumeric(dirlistLon_moy)

Lat_tot=[]
Lon_tot=[]
for i in range(len(filelistLat_moy)):
    Lat_tot=np.append(Lat_tot,np.loadtxt(filelistLat_moy[i]))
    Lon_tot=np.append(Lon_tot,np.loadtxt(filelistLon_moy[i]))

if type_campagne=='echosonde':
    len_freq_tot=[]
    for i in range(len(filelistMed)):
        if len(np.where(np.isnan(np.loadtxt(filelistMed[i])))[0])==0:
            len_freq_tot.append((np.loadtxt(filelistMed[i]).shape[1]))
    len_freq_tot_max=max(len_freq_tot)
            
    Q1_tot=np.loadtxt(filelistQ1_[0])
    Q3_tot=np.loadtxt(filelistQ3_[0])
    med_tot=np.loadtxt(filelistMed[0])
    if len(np.where(np.isnan(med_tot))[0])!=0 and med_tot.shape[1]>len_freq_tot_max:
        med_tot=med_tot[:,:len_freq_tot_max]
        Q1_tot=Q1_tot[:,:len_freq_tot_max]
        Q3_tot=Q3_tot[:,:len_freq_tot_max]
    else:
        med_tot=med_tot
        Q1_tot=Q1_tot
        Q3_tot=Q3_tot
    labels_tot=np.loadtxt(filelistLabels[0])
    if labels_tot.shape[1]<indice_profondeur_max:
        blanc = np.zeros((len(labels_tot),indice_profondeur_max-labels_tot.shape[1]))
        blanc.fill(np.nan)
        labels_tot=np.hstack((labels_tot,blanc))
    for i in range(1,len(filelistQ1_)):
        if len(np.where(np.isnan(np.loadtxt(filelistQ1_[i])))[0])!=0 and np.loadtxt(filelistQ1_[i]).shape[1]>len_freq_tot_max:
            med_tot_temp=np.loadtxt(filelistMed[i])[:,:len_freq_tot_max]
            Q1_tot_temp=np.loadtxt(filelistQ1_[i])[:,:len_freq_tot_max]
            Q3_tot_temp=np.loadtxt(filelistQ3_[i])[:,:len_freq_tot_max]
        else:
            med_tot_temp=np.loadtxt(filelistMed[i])
            Q1_tot_temp=np.loadtxt(filelistQ1_[i])
            Q3_tot_temp=np.loadtxt(filelistQ3_[i])
        Q1_tot=np.vstack((Q1_tot,Q1_tot_temp))
        Q3_tot=np.vstack((Q3_tot,Q3_tot_temp))
        med_tot=np.vstack((med_tot,med_tot_temp))  
        # if len(np.where(np.isnan(np.loadtxt(filelistMed[i]))))==0 and np.loadtxt(filelistMed[i]).shape[1]>len_freq_tot:
            
        # else:
        #     len_freq_tot=med_tot.shape[1]
        labels=np.loadtxt(filelistLabels[i])
        if labels.shape[1]<indice_profondeur_max:
            blanc = np.zeros((len(labels),indice_profondeur_max-labels.shape[1]))
            blanc.fill(np.nan)
            labels=np.hstack((labels,blanc))
        labels_tot=np.vstack((labels_tot,labels))
    
    len_med_tot=len(med_tot)
    
    Sa_moy_tot=[]
    for i in range(len(filelistSa_moy)):
        Sa=np.loadtxt(filelistSa_moy[i])
        if Sa.shape[0]!=indice_profondeur_max:
            Sa=Sa.T
        else:
            Sa=Sa
        Sa_moy_tot.append(Sa)
    
    mask=np.where(np.isnan(med_tot)==False)
    med_tot=med_tot[mask].reshape((-1,len_freq_tot_max))   
    Q1_tot=Q1_tot[mask].reshape((-1,len_freq_tot_max))   
    Q3_tot=Q3_tot[mask].reshape((-1,len_freq_tot_max))  
    
    if nombre_composantes>len(freq_MFR):
        raise ValueError('Nombre de composantes trop élevé par rapport au nombre de fréquences')
    elif nombre_voisins<nombre_composantes:
        raise ValueError('Nombre de voisins trop faible par rapport au nombre de composantes')
    else:
        lle = LocallyLinearEmbedding(n_components=nombre_composantes, n_neighbors = nombre_voisins,method = 'modified', n_jobs = 4,  random_state=0)
        lle.fit(med_tot)
        med_lle = lle.transform(med_tot)
    
    plt.figure()
    sc.dendrogram(sc.linkage(med_lle,method='ward'))
    
    plt.savefig(fpath+'Dendogramme_campagne_totale.png')
    
    
    kmeans_sv=AgglomerativeClustering(n_clusters=nombre_de_categories_sv_campEntiere,affinity='euclidean',linkage='ward').fit(med_lle)
    
    couleurs = plt.cm.jet(np.linspace(0,1,nombre_de_categories_sv_campEntiere))
    cmap = colors.ListedColormap(couleurs) 
    
    
    plt.figure(figsize=(30,15))
    for x in np.unique(kmeans_sv.labels_):
        indice = np.where(kmeans_sv.labels_ == x)
        plt.plot(freq_MFR[:], np.mean( Q1_tot[indice], axis=0),'-.',color=couleurs[x])
        plt.plot(freq_MFR[:], np.mean( Q3_tot[indice], axis=0),'-.',color=couleurs[x])
        plt.plot(freq_MFR[:], np.mean( med_tot[indice], axis=0),'o-',color=couleurs[x],label="Classe"+str(x))
    plt.legend() 
    
    plt.savefig(fpath+'ClustersRF_campagne_totale.png')
    kmeans_sv.labels_new=np.zeros((len_med_tot))
    kmeans_sv.labels_new.fill(np.nan)
    for i in range(len(np.unique(mask[0]))):
        kmeans_sv.labels_new[np.unique(mask[0])[i]]=kmeans_sv.labels_[i]
    kmeans_sv.labels_new=kmeans_sv.labels_new+nombre_de_categories_sv+1
    
    labels_tot_new=np.where(labels_tot==nombre_de_categories_sv,np.max(kmeans_sv.labels_new)+1,labels_tot)
    
    unites= np.loadtxt(fpath+'UnitesDecoupageCampagne.txt',dtype='int')
    len_Sv0_ent_tot_final=np.cumsum(unites)  
               
    j=0
    for i in range(nombre_de_categories_sv):
        labels_tot_new[:len_Sv0_ent_tot_final[j],:]=np.where(labels_tot_new[:len_Sv0_ent_tot_final[j],:]==i,kmeans_sv.labels_new[i+j],labels_tot_new[:len_Sv0_ent_tot_final[j],:])       
    
    kmeans_sv.labels_new_shape=kmeans_sv.labels_new.reshape((len(len_Sv0_ent_tot_final),nombre_de_categories_sv))     
    for j in range(1,len(len_Sv0_ent_tot_final)):
        for i in range(nombre_de_categories_sv):
            labels_tot_new[len_Sv0_ent_tot_final[j-1]:len_Sv0_ent_tot_final[j],:]=np.where(labels_tot_new[len_Sv0_ent_tot_final[j-1]:len_Sv0_ent_tot_final[j],:]==i,kmeans_sv.labels_new_shape[j,i],labels_tot_new[len_Sv0_ent_tot_final[j-1]:len_Sv0_ent_tot_final[j],:])       
             
    labels_tot_new=labels_tot_new-(nombre_de_categories_sv+1)
          
    couleurs = plt.cm.jet(np.linspace(0,1,nombre_de_categories_sv_campEntiere))
    cmap = colors.ListedColormap(couleurs) 
    
    x=np.arange(0,len_Sv0_ent_tot_final[-1],1)
    y=np.arange(-Depth[1,0],-Depth[1,(len(Depth[1])-1)]-1,-1)
    
    plt.figure(figsize=(30,15))
    plt.xlabel('N*ESU')
    plt.ylabel('profondeur (en m)')
    plt.title(' ')
    plt.pcolormesh(x,y,np.transpose(labels_tot_new), cmap=cmap)
    plt.grid()
    plt.colorbar()
    plt.show()
    
    plt.savefig(fpath+'Echogramme_clustering_campagne_totale.png')
                    
    
    Sa_moy_label_tot = []
    Sa_moy_label_log_tot = []
    u=0
    for f in range(nb_transduc,len(Sa_moy_tot)+1,nb_transduc):
        #print('boucle sur f:',f)
        sa_temp=np.array(Sa_moy_tot[f-nb_transduc:f])
        #print(sa_temp.shape)
        if np.cumsum(unites)[u]<=pas:
            unit0=0
            unit1=unites[0]
        else:
            unit0=np.cumsum(unites)[u-1]
            unit1=np.cumsum(unites)[u]
        # print('unit0',unit0)
        # print('unit1',unit1)
        for i in range(nb_transduc):
            #print('boucle sur i:',i)
            sa_t=sa_temp[i,:,:].T
            #print(sa_t.shape)
            for x in range(nombre_de_categories_sv_campEntiere):
                #print('boucle sur x:',x)
                Sa_label = np.zeros(sa_t.shape)
                Sa_label[:, :] = np.nan
                #print(Sa_label.shape)
                index_lab = np.where(labels_tot_new[unit0:unit1] == x)
                Sa_label[index_lab] = sa_t[index_lab]
                Sa_moy_label_tot.append(np.nansum(Sa_label, axis=1))
                Sa_moy_label_log_tot.append(10 * np.log10(np.nanmean(Sa_label, axis=1)))
        u+=1               
                    
    reindexage=np.arange(0,len(Sa_moy_label_tot),nb_transduc*nombre_de_categories_sv_campEntiere)
    Sa_moy_label_tot_final=Sa_moy_label_tot[0]
    Sa_moy_label_log_tot_final=Sa_moy_label_log_tot[0]
    for j in range(nb_transduc*nombre_de_categories_sv_campEntiere):
        for i in range(len(reindexage)):
            #print(reindexage[i])
            Sa_moy_label_tot_final=np.hstack((Sa_moy_label_tot_final,Sa_moy_label_tot[reindexage[i]]))
            Sa_moy_label_log_tot_final=np.hstack((Sa_moy_label_log_tot_final,Sa_moy_label_log_tot[reindexage[i]]))
        reindexage+=1
    Sa_moy_label_tot_final=Sa_moy_label_tot_final[unites[0]:]
    Sa_moy_label_tot_final=Sa_moy_label_tot_final.reshape((len(freq_voulues),nombre_de_categories_sv_campEntiere,-1))  
    Sa_moy_label_log_tot_final=Sa_moy_label_log_tot_final[unites[0]:]
    Sa_moy_label_log_tot_final=Sa_moy_label_log_tot_final.reshape((len(freq_voulues),nombre_de_categories_sv_campEntiere,-1))  
    
    #les cercles n'apparaissent que pour les valeurs positives (faire coincider figure et legende en n'affichant que les resultats positifs)
    sa_log=np.zeros((nbfreqs,nombre_de_categories_sv_campEntiere,Sa_moy_label_log_tot_final.shape[2]))
    for f in range(len(freq_voulues)):
        for x in range(nombre_de_categories_sv_campEntiere):
            for i in range(Sa_moy_label_log_tot_final.shape[2]):
                if Sa_moy_label_log_tot_final[f][x][i]>0:
                    sa_log[f,x,i]=Sa_moy_label_log_tot_final[f][x][i]+offset
                elif Sa_moy_label_log_tot_final[f][x][i]<=0 and np.isnan(Sa_moy_label_log_tot_final[f][x][i])==False:
                    sa_log[f,x,i]=offset
                    
                    
    mask_lat=np.where(np.isnan(Lat_tot)==False) 
    lat_tot=Lat_tot[mask_lat]
    lon_tot=Lon_tot[mask_lat]  
    sa_log=sa_log[:,:,mask_lat[0]]
    sa_moy_label_tot_final=Sa_moy_label_tot_final[:,:,mask_lat[0]]          
    sa_moy_label_log_tot_final=Sa_moy_label_log_tot_final[:,:,mask_lat[0]]               
                 
    plt.rc('legend',fontsize=15)    
    
    ##################################################################################################
    #
    # Création de carte avec la librairie Cartopy : Carte simple , affichage dans les figures.
    # Sa en valeurs naturelles
    #
    ##################################################################################################
    ##################################################################################################
    #
    # Création de carte avec la librairie folium : Carte Openstreetmap, affichage dans un fichier HTML.
    # Sa en Log
    #
    ##################################################################################################
    
    for f in range(len(freq_voulues)):   
        md.affichage_carte_france(
            nombre_de_categories_sv_campEntiere, lat_tot, lon_tot, sa_log[f,:,:], extension_zone,'LogSa', fpath+'LogSa'+str(np.round(freq_voulues[f]/1000))
        )
            
        md.affichage_carte_france(
            nombre_de_categories_sv_campEntiere, lat_tot, lon_tot, sa_moy_label_tot_final[f,:,:], extension_zone,'Sa', fpath+str(np.round(freq_voulues[f]/1000))
        )
    
        md.save_map_html(
            nombre_de_categories_sv_campEntiere, lat_tot, lon_tot, sa_moy_label_log_tot_final[f,:,:], fpath+str(np.round(freq_voulues[f]/1000))
        )
    
    nb_radiales=[]
    compt=0
    heured=np.array([])
    heuref=np.array([])
    for u in range(len(unites)):
        if u==0:
            hd=heuredebutini[compt]
            hf=heurefinini[compt]
            
            heured=np.append(heured,hd)
            heuref=np.append(heuref,hf)
        else:
            if unites[u]==pas and unites[u-1]!=1000:
                hd=heuredebutini[compt+1]
                hf=heurefinini[compt+1]
                if compt<=len(heuredebutini)-1:
                    compt+=1
                else:
                    break
                heured=np.append(heured,hd)
                heuref=np.append(heuref,hf)
            elif unites[u]!=pas and unites[u-1]!=pas:
                hd=heuredebutini[compt+1]
                hf=heurefinini[compt+1]
                if compt<=len(heuredebutini)-1:
                    compt+=1
                else:
                    break
            
                heured=np.append(heured,hd)
                heuref=np.append(heuref,hf)
            else:
                hd=heuredebutini[compt]
                hf=heurefinini[compt]
    
                heured=np.append(heured,hd)
                heuref=np.append(heuref,hf)
                
                
    Sa_jour=np.array([])
    Sa_nuit=np.array([])
    Sa_jour_log=np.array([])
    Sa_nuit_log=np.array([])
    Lat_jour=np.array([])
    Lat_nuit=np.array([])
    Lon_jour=np.array([])
    Lon_nuit=np.array([])
    for f in range(len(freq_voulues)):
        for x in range(nombre_de_categories_sv_campEntiere):
            for u in range(len(unites)):
                if heured[u]>'04:00:00' and heuref[u]<'21:00:00': #Temps UTC+2
                    if u==0:
                        Sa_jour=np.append(Sa_jour,Sa_moy_label_tot_final[f][x][u:np.cumsum(unites[u])[-1]])
                        Sa_jour_log=np.append(Sa_jour_log,Sa_moy_label_log_tot_final[f][x][u:np.cumsum(unites[u])[-1]])
                        Lat_jour=np.append(Lat_jour,Lat_tot[u:np.cumsum(unites[u])[-1]])
                        Lon_jour=np.append(Lon_jour,Lon_tot[u:np.cumsum(unites[u])[-1]])
                    else:
                        Sa_jour=np.append(Sa_jour,Sa_moy_label_tot_final[f][x][np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                        Sa_jour_log=np.append(Sa_jour_log,Sa_moy_label_log_tot_final[f][x][np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                        Lat_jour=np.append(Lat_jour,Lat_tot[np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                        Lon_jour=np.append(Lon_jour,Lon_tot[np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                else:
                    if u==0:
                        Sa_nuit=np.append(Sa_nuit,Sa_moy_label_tot_final[f][x][u:np.cumsum(unites[u])[-1]])
                        Sa_nuit_log=np.append(Sa_nuit_log,Sa_moy_label_log_tot_final[f][x][u:np.cumsum(unites[u])[-1]])
                        Lat_nuit=np.append(Lat_nuit,Lat_tot[u:np.cumsum(unites[u])[-1]])
                        Lon_nuit=np.append(Lon_nuit,Lon_tot[u:np.cumsum(unites[u])[-1]])
                    else:
                        Sa_nuit=np.append(Sa_nuit,Sa_moy_label_tot_final[f][x][np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                        Sa_nuit_log=np.append(Sa_nuit_log,Sa_moy_label_log_tot_final[f][x][np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                        Lat_nuit=np.append(Lat_nuit,Lat_tot[np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                        Lon_nuit=np.append(Lon_nuit,Lon_tot[np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])           
    Sa_jour=Sa_jour.reshape((nbfreqs,nombre_de_categories_sv_campEntiere,-1))
    Sa_nuit=Sa_nuit.reshape((nbfreqs,nombre_de_categories_sv_campEntiere,-1))
    Sa_jour_log=Sa_jour_log.reshape((nbfreqs,nombre_de_categories_sv_campEntiere,-1))
    Sa_nuit_log=Sa_nuit_log.reshape((nbfreqs,nombre_de_categories_sv_campEntiere,-1))
    ind_lat_j=int(Lat_jour.shape[0]/(nombre_de_categories_sv_campEntiere*nbfreqs))
    ind_lat_n=int(Lat_nuit.shape[0]/(nombre_de_categories_sv_campEntiere*nbfreqs))
    ind_lon_j=int(Lon_jour.shape[0]/(nombre_de_categories_sv_campEntiere*nbfreqs))
    ind_lon_n=int(Lon_nuit.shape[0]/(nombre_de_categories_sv_campEntiere*nbfreqs))
    Lat_jour=Lat_jour[:ind_lat_j]
    Lat_nuit=Lat_nuit[:ind_lat_n]
    Lon_jour=Lon_jour[:ind_lon_j]
    Lon_nuit=Lon_nuit[:ind_lon_n]
    
    #les cercles n'apparaissent que pour les valeurs positives (faire coincider figure et legende en n'affichant que les resultats positifs)
    sa_log_jour=np.zeros((nbfreqs,nombre_de_categories_sv_campEntiere,Sa_jour_log.shape[2]))
    for f in range(len(freq_voulues)):
        for x in range(nombre_de_categories_sv_campEntiere):
            for i in range(Sa_jour_log.shape[2]):
                if Sa_jour_log[f][x][i]>0:
                    sa_log_jour[f,x,i]=Sa_jour_log[f][x][i]+offset
                elif Sa_jour_log[f][x][i]<=0 and np.isnan(Sa_jour_log[f][x][i])==False:
                    sa_log_jour[f,x,i]=offset
                    
    #les cercles n'apparaissent que pour les valeurs positives (faire coincider figure et legende en n'affichant que les resultats positifs)
    sa_log_nuit=np.zeros((nbfreqs,nombre_de_categories_sv_campEntiere,Sa_nuit_log.shape[2]))
    for f in range(len(freq_voulues)):
        for x in range(nombre_de_categories_sv_campEntiere):
            for i in range(Sa_nuit_log.shape[2]):
                if Sa_nuit_log[f][x][i]>0:
                    sa_log_nuit[f,x,i]=Sa_nuit_log[f][x][i]+offset
                elif Sa_nuit_log[f][x][i]<=0 and np.isnan(Sa_nuit_log[f][x][i])==False:
                    sa_log_nuit[f,x,i]=offset
                    
    mask_lat_jour=np.where(np.isnan(Lat_jour)==False)
    mask_lat_nuit=np.where(np.isnan(Lat_nuit)==False) 
    lat_tot_jour=Lat_jour[mask_lat_jour]
    lon_tot_jour=Lon_jour[mask_lat_jour] 
    lat_tot_nuit=Lat_nuit[mask_lat_nuit]
    lon_tot_nuit=Lon_nuit[mask_lat_nuit]              
    sa_log_jour=sa_log_jour[:,:,mask_lat_jour[0]]
    Sa_jour=Sa_jour[:,:,mask_lat_jour[0]]          
    Sa_jour_log=Sa_jour_log[:,:,mask_lat_jour[0]]  
    sa_log_nuit=sa_log_nuit[:,:,mask_lat_nuit[0]]
    Sa_nuit=Sa_nuit[:,:,mask_lat_nuit[0]]          
    Sa_nuit_log=Sa_nuit_log[:,:,mask_lat_nuit[0]]  
                    
    ##################################################################################################
    #
    # Création de carte avec la librairie Cartopy : Carte simple , affichage dans les figures.
    # Sa en valeurs naturelles
    #
    ##################################################################################################
    ##################################################################################################
    #
    # Création de carte avec la librairie folium : Carte Openstreetmap, affichage dans un fichier HTML.
    # Sa en Log
    #
    ##################################################################################################
    
    for f in range(len(freq_voulues)):
        #Affichage des cartes de Sa et de log(Sa) par label et par période jour/nuit
        md.affichage_carte_france(
            nombre_de_categories_sv_campEntiere, lat_tot_jour, lon_tot_jour, Sa_jour[f,:,:], extension_zone,'Sa', fpath+str(np.round(freq_voulues[f]/1000))+'jour'
        )
    
        md.save_map_html(
            nombre_de_categories_sv_campEntiere, lat_tot_jour, lon_tot_jour, Sa_jour_log[f,:,:], fpath+str(np.round(freq_voulues[f]/1000))+'jour'
        )
        
        md.affichage_carte_france(
            nombre_de_categories_sv_campEntiere,lat_tot_jour, lon_tot_jour, sa_log_jour[f,:,:], extension_zone,'LogSa', fpath+'LogSa_jour_corr'+str(np.round(freq_voulues[f]/1000))
        )
        if Sa_nuit.shape[-1]!=0:
            md.affichage_carte_france(
                nombre_de_categories_sv_campEntiere, lat_tot_nuit, lon_tot_nuit, Sa_nuit[f,:,:], extension_zone,'Sa', fpath+str(np.round(freq_voulues[f]/1000))+'nuit'
            )
        
            md.save_map_html(
                nombre_de_categories_sv_campEntiere, lat_tot_nuit, lon_tot_nuit, Sa_nuit_log[f,:,:], fpath+str(np.round(freq_voulues[f]/1000))+'nuit'
            )
            
            md.affichage_carte_france(
            nombre_de_categories_sv_campEntiere,lat_tot_nuit, lon_tot_nuit, sa_log_nuit[f,:,:], extension_zone,'LogSa', fpath+'LogSa_nuit_corr'+str(np.round(freq_voulues[f]/1000))
            )  
        else:
            pass
                          

else:
    Q1_tot=np.loadtxt(filelistQ1_[0])
    Q3_tot=np.loadtxt(filelistQ3_[0])
    med_tot=np.loadtxt(filelistMed[0])
    labels_tot=np.loadtxt(filelistLabels[0])
    if labels_tot.shape[1]<indice_profondeur_max:
        blanc = np.zeros((len(labels_tot),indice_profondeur_max-labels_tot.shape[1]))
        blanc.fill(np.nan)
        labels_tot=np.hstack((labels_tot,blanc))
    for i in range(1,len(filelistQ1_)):
        Q1_tot=np.vstack((Q1_tot,np.loadtxt(filelistQ1_[i])))
        Q3_tot=np.vstack((Q3_tot,np.loadtxt(filelistQ3_[i])))
        med_tot=np.vstack((med_tot,np.loadtxt(filelistMed[i])))   
        labels=np.loadtxt(filelistLabels[i])
        if labels.shape[1]<indice_profondeur_max:
            blanc = np.zeros((len(labels),indice_profondeur_max-labels.shape[1]))
            blanc.fill(np.nan)
            labels=np.hstack((labels,blanc))
        labels_tot=np.vstack((labels_tot,labels))
        
    Sa_moy_tot=[]
    for i in range(len(filelistSa_moy)):
        Sa=np.loadtxt(filelistSa_moy[i])
        Sa_moy_tot.append(Sa)
    
    if nombre_composantes>len(freq_MFR):
        raise ValueError('Nombre de composantes trop élevé par rapport au nombre de fréquences')
    elif nombre_voisins<nombre_composantes:
        raise ValueError('Nombre de voisins trop faible par rapport au nombre de composantes')
    else:
        lle = LocallyLinearEmbedding(n_components=nombre_composantes, n_neighbors = nombre_voisins,method = 'modified', n_jobs = 4,  random_state=0)
        lle.fit(med_tot)
        med_lle = lle.transform(med_tot)
    
    plt.figure()
    sc.dendrogram(sc.linkage(med_lle,method='ward'))
    
    plt.savefig(fpath+'Dendogramme_campagne_totale.png')
    
    
    kmeans_sv=AgglomerativeClustering(n_clusters=nombre_de_categories_sv_campEntiere,affinity='euclidean',linkage='ward').fit(med_lle)
    
    couleurs = plt.cm.jet(np.linspace(0,1,nombre_de_categories_sv_campEntiere))
    cmap = colors.ListedColormap(couleurs) 
    
    plt.figure(figsize=(30,15))
    for x in np.unique(kmeans_sv.labels_):
        indice = np.where(kmeans_sv.labels_ == x)
        plt.plot(freq_MFR[:indice_freq_max], np.mean( Q1_tot[indice], axis=0),'-.',color=couleurs[x])
        plt.plot(freq_MFR[:indice_freq_max], np.mean( Q3_tot[indice], axis=0),'-.',color=couleurs[x])
        plt.plot(freq_MFR[:indice_freq_max], np.mean( med_tot[indice], axis=0),'o-',color=couleurs[x],label="Classe"+str(x))
    plt.legend()

    plt.savefig(fpath+'ClustersRF_campagne_totale.png')
    
    kmeans_sv.labels_new=kmeans_sv.labels_+nombre_de_categories_sv+1
    labels_tot_new=np.where(labels_tot==nombre_de_categories_sv,np.max(kmeans_sv.labels_new)+1,labels_tot)
    
    unites= np.loadtxt(fpath+'UnitesDecoupageCampagne.txt',dtype='int')
    len_Sv0_ent_tot_final=np.cumsum(unites)  
               
    j=0
    for i in range(nombre_de_categories_sv):
        labels_tot_new[:len_Sv0_ent_tot_final[j],:]=np.where(labels_tot_new[:len_Sv0_ent_tot_final[j],:]==i,kmeans_sv.labels_new[i+j],labels_tot_new[:len_Sv0_ent_tot_final[j],:])       
    
    kmeans_sv.labels_new_shape=kmeans_sv.labels_new.reshape((len(len_Sv0_ent_tot_final),nombre_de_categories_sv))     
    for j in range(1,len(len_Sv0_ent_tot_final)):
        for i in range(nombre_de_categories_sv):
            labels_tot_new[len_Sv0_ent_tot_final[j-1]:len_Sv0_ent_tot_final[j],:]=np.where(labels_tot_new[len_Sv0_ent_tot_final[j-1]:len_Sv0_ent_tot_final[j],:]==i,kmeans_sv.labels_new_shape[j,i],labels_tot_new[len_Sv0_ent_tot_final[j-1]:len_Sv0_ent_tot_final[j],:])       
             
    labels_tot_new=labels_tot_new-(nombre_de_categories_sv+1)
          
    couleurs = plt.cm.jet(np.linspace(0,1,nombre_de_categories_sv_campEntiere))
    cmap = colors.ListedColormap(couleurs) 
    
    x=np.arange(0,len_Sv0_ent_tot_final[-1],1)
    #y=np.arange(-Depth[1,0],-Depth[1,(len(Depth[1])-1)]-1,-1)
    y=np.arange(-Depth[0,0],-Depth[0,(len(Depth[0])-1)]-1,-1)
    
    plt.figure(figsize=(30,15))
    plt.xlabel('N*ESU')
    plt.ylabel('profondeur (en m)')
    plt.title(' ')
    plt.pcolormesh(x,y,np.transpose(labels_tot_new), cmap=cmap)
    plt.grid()
    plt.colorbar()
    plt.show()
    
    plt.savefig(fpath+'Echogramme_clustering_campagne_totale.png')
                    
    
    Sa_moy_label_tot = []
    Sa_moy_label_log_tot = []
    u=0
    #for f in range(nb_transduc,len(Sa_moy_tot)+1,nb_transduc):
    for f in range(nb_transduc,len(Sa_moy_tot)+1,nb_transduc):
        #print('boucle sur f:',f)
        sa_temp=np.array(Sa_moy_tot[f-nb_transduc:f])
        #print(sa_temp.shape)
        if np.cumsum(unites)[u]<=pas:
            unit0=0
            unit1=unites[0]
        else:
            unit0=np.cumsum(unites)[u-1]
            unit1=np.cumsum(unites)[u]
        # print('unit0',unit0)
        # print('unit1',unit1)
        for i in range(nb_transduc):
            #print('boucle sur i:',i)
            sa_t=sa_temp[i,:,:].T
            #print(sa_t.shape)
            for x in range(nombre_de_categories_sv_campEntiere):
                #print('boucle sur x:',x)
                Sa_label = np.zeros(sa_t.shape)
                Sa_label[:, :] = np.nan
                #print(Sa_label.shape)
                index_lab = np.where(labels_tot_new[unit0:unit1] == x)
                Sa_label[index_lab] = sa_t[index_lab]
                Sa_moy_label_tot.append(np.nansum(Sa_label, axis=1))
                Sa_moy_label_log_tot.append(10 * np.log10(np.nanmean(Sa_label, axis=1)))
        u+=1               
                    
    reindexage=np.arange(0,len(Sa_moy_label_tot),nb_transduc*nombre_de_categories_sv_campEntiere)
    Sa_moy_label_tot_final=Sa_moy_label_tot[0]
    Sa_moy_label_log_tot_final=Sa_moy_label_log_tot[0]
    for j in range(nb_transduc*nombre_de_categories_sv_campEntiere):
        for i in range(len(reindexage)):
            #print(reindexage[i])
            Sa_moy_label_tot_final=np.hstack((Sa_moy_label_tot_final,Sa_moy_label_tot[reindexage[i]]))
            Sa_moy_label_log_tot_final=np.hstack((Sa_moy_label_log_tot_final,Sa_moy_label_log_tot[reindexage[i]]))
        reindexage+=1
    Sa_moy_label_tot_final=Sa_moy_label_tot_final[unites[0]:]
    Sa_moy_label_tot_final=Sa_moy_label_tot_final.reshape((len(freq_voulues),nombre_de_categories_sv_campEntiere,-1))  
    Sa_moy_label_log_tot_final=Sa_moy_label_log_tot_final[unites[0]:]
    Sa_moy_label_log_tot_final=Sa_moy_label_log_tot_final.reshape((len(freq_voulues),nombre_de_categories_sv_campEntiere,-1))  
    
    #les cercles n'apparaissent que pour les valeurs positives (faire coincider figure et legende en n'affichant que les resultats positifs)
    sa_log=np.zeros((nbfreqs,nombre_de_categories_sv_campEntiere,Sa_moy_label_log_tot_final.shape[2]))
    for f in range(len(freq_voulues)):
        for x in range(nombre_de_categories_sv_campEntiere):
            for i in range(Sa_moy_label_log_tot_final.shape[2]):
                if Sa_moy_label_log_tot_final[f][x][i]>0:
                    sa_log[f,x,i]=Sa_moy_label_log_tot_final[f][x][i]+offset
                elif Sa_moy_label_log_tot_final[f][x][i]<=0 and np.isnan(Sa_moy_label_log_tot_final[f][x][i])==False:
                    sa_log[f,x,i]=offset
                    
                    
                 
    plt.rc('legend',fontsize=15)    
    
    ##################################################################################################
    #
    # Création de carte avec la librairie Cartopy : Carte simple , affichage dans les figures.
    # Sa en valeurs naturelles
    #
    ##################################################################################################
    ##################################################################################################
    #
    # Création de carte avec la librairie folium : Carte Openstreetmap, affichage dans un fichier HTML.
    # Sa en Log
    #
    ##################################################################################################
    
    for f in range(len(freq_voulues)):   
        md.affichage_carte_france(
            nombre_de_categories_sv_campEntiere, Lat_tot, Lon_tot, sa_log[f,:,:], extension_zone,'LogSa', fpath+'LogSa'+str(np.round(freq_voulues[f]/1000))
        )
            
        md.affichage_carte_france(
            nombre_de_categories_sv_campEntiere, Lat_tot, Lon_tot, Sa_moy_label_tot_final[f,:,:], extension_zone,'Sa', fpath+str(np.round(freq_voulues[f]/1000))
        )
    
        md.save_map_html(
            nombre_de_categories_sv_campEntiere, Lat_tot, Lon_tot, Sa_moy_label_log_tot_final[f,:,:], fpath+str(np.round(freq_voulues[f]/1000))
        )
    
    nb_radiales=[]
    compt=ind_decoup[0]
    heured=np.array([])
    heuref=np.array([])
    for u in range(len(unites)):
        if u==0:
            hd=heuredebut[compt]
            hf=heurefin[compt]
            
            heured=np.append(heured,hd)
            heuref=np.append(heuref,hf)
        else:
            if unites[u]==pas and unites[u-1]!=1000:
                hd=heuredebut[compt+1]
                hf=heurefin[compt+1]
                if compt<=len(heuredebut)-1:
                    compt+=1
                else:
                    break
                heured=np.append(heured,hd)
                heuref=np.append(heuref,hf)
            elif unites[u]!=pas and unites[u-1]!=pas:
                hd=heuredebut[compt+1]
                hf=heurefin[compt+1]
                if compt<=len(heuredebut)-1:
                    compt+=1
                else:
                    break
            
                heured=np.append(heured,hd)
                heuref=np.append(heuref,hf)
            else:
                hd=heuredebut[compt]
                hf=heurefin[compt]
    
                heured=np.append(heured,hd)
                heuref=np.append(heuref,hf)
                
                
    Sa_jour=np.array([])
    Sa_nuit=np.array([])
    Sa_jour_log=np.array([])
    Sa_nuit_log=np.array([])
    Lat_jour=np.array([])
    Lat_nuit=np.array([])
    Lon_jour=np.array([])
    Lon_nuit=np.array([])
    for f in range(len(freq_voulues)):
        for x in range(nombre_de_categories_sv_campEntiere):
            for u in range(len(unites)):
                if heured[u]>'04:00:00' and heuref[u]<'21:00:00': #temps UTC+2
                    if u==0:
                        Sa_jour=np.append(Sa_jour,Sa_moy_label_tot_final[f][x][u:np.cumsum(unites[u])[-1]])
                        Sa_jour_log=np.append(Sa_jour_log,Sa_moy_label_log_tot_final[f][x][u:np.cumsum(unites[u])[-1]])
                        Lat_jour=np.append(Lat_jour,Lat_tot[u:np.cumsum(unites[u])[-1]])
                        Lon_jour=np.append(Lon_jour,Lon_tot[u:np.cumsum(unites[u])[-1]])
                    else:
                        Sa_jour=np.append(Sa_jour,Sa_moy_label_tot_final[f][x][np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                        Sa_jour_log=np.append(Sa_jour_log,Sa_moy_label_log_tot_final[f][x][np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                        Lat_jour=np.append(Lat_jour,Lat_tot[np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                        Lon_jour=np.append(Lon_jour,Lon_tot[np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                else:
                    if u==0:
                        Sa_nuit=np.append(Sa_nuit,Sa_moy_label_tot_final[f][x][u:np.cumsum(unites[u])[-1]])
                        Sa_nuit_log=np.append(Sa_nuit_log,Sa_moy_label_log_tot_final[f][x][u:np.cumsum(unites[u])[-1]])
                        Lat_nuit=np.append(Lat_nuit,Lat_tot[u:np.cumsum(unites[u])[-1]])
                        Lon_nuit=np.append(Lon_nuit,Lon_tot[u:np.cumsum(unites[u])[-1]])
                    else:
                        Sa_nuit=np.append(Sa_nuit,Sa_moy_label_tot_final[f][x][np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                        Sa_nuit_log=np.append(Sa_nuit_log,Sa_moy_label_log_tot_final[f][x][np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                        Lat_nuit=np.append(Lat_nuit,Lat_tot[np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                        Lon_nuit=np.append(Lon_nuit,Lon_tot[np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])           
    Sa_jour=Sa_jour.reshape((nbfreqs,nombre_de_categories_sv_campEntiere,-1))
    Sa_nuit=Sa_nuit.reshape((nbfreqs,nombre_de_categories_sv_campEntiere,-1))
    Sa_jour_log=Sa_jour_log.reshape((nbfreqs,nombre_de_categories_sv_campEntiere,-1))
    Sa_nuit_log=Sa_nuit_log.reshape((nbfreqs,nombre_de_categories_sv_campEntiere,-1))
    ind_lat_j=int(Lat_jour.shape[0]/(nombre_de_categories_sv_campEntiere*nbfreqs))
    ind_lat_n=int(Lat_nuit.shape[0]/(nombre_de_categories_sv_campEntiere*nbfreqs))
    ind_lon_j=int(Lon_jour.shape[0]/(nombre_de_categories_sv_campEntiere*nbfreqs))
    ind_lon_n=int(Lon_nuit.shape[0]/(nombre_de_categories_sv_campEntiere*nbfreqs))
    Lat_jour=Lat_jour[:ind_lat_j]
    Lat_nuit=Lat_nuit[:ind_lat_n]
    Lon_jour=Lon_jour[:ind_lon_j]
    Lon_nuit=Lon_nuit[:ind_lon_n]
    
    #les cercles n'apparaissent que pour les valeurs positives (faire coincider figure et legende en n'affichant que les resultats positifs)
    sa_log_jour=np.zeros((nbfreqs,nombre_de_categories_sv_campEntiere,Sa_jour_log.shape[2]))
    for f in range(len(freq_voulues)):
        for x in range(nombre_de_categories_sv_campEntiere):
            for i in range(Sa_jour_log.shape[2]):
                if Sa_jour_log[f][x][i]>0:
                    sa_log_jour[f,x,i]=Sa_jour_log[f][x][i]+offset
                elif Sa_jour_log[f][x][i]<=0 and np.isnan(Sa_jour_log[f][x][i])==False:
                    sa_log_jour[f,x,i]=offset
                    
    #les cercles n'apparaissent que pour les valeurs positives (faire coincider figure et legende en n'affichant que les resultats positifs)
    sa_log_nuit=np.zeros((nbfreqs,nombre_de_categories_sv_campEntiere,Sa_nuit_log.shape[2]))
    for f in range(len(freq_voulues)):
        for x in range(nombre_de_categories_sv_campEntiere):
            for i in range(Sa_nuit_log.shape[2]):
                if Sa_nuit_log[f][x][i]>0:
                    sa_log_nuit[f,x,i]=Sa_nuit_log[f][x][i]+offset
                elif Sa_nuit_log[f][x][i]<=0 and np.isnan(Sa_nuit_log[f][x][i])==False:
                    sa_log_nuit[f,x,i]=offset
                    
    ##################################################################################################
    #
    # Création de carte avec la librairie Cartopy : Carte simple , affichage dans les figures.
    # Sa en valeurs naturelles
    #
    ##################################################################################################
    ##################################################################################################
    #
    # Création de carte avec la librairie folium : Carte Openstreetmap, affichage dans un fichier HTML.
    # Sa en Log
    #
    ##################################################################################################
    
    for f in range(len(freq_voulues)):
        #Affichage des cartes de Sa et de log(Sa) par label et par période jour/nuit
        md.affichage_carte_france(
            nombre_de_categories_sv_campEntiere, Lat_jour, Lon_jour, Sa_jour[f,:,:], extension_zone,'Sa', fpath+str(np.round(freq_voulues[f]/1000))+'jour'
        )
    
        md.save_map_html(
            nombre_de_categories_sv_campEntiere, Lat_jour, Lon_jour, Sa_jour_log[f,:,:], fpath+str(np.round(freq_voulues[f]/1000))+'jour'
        )
    
        md.affichage_carte_france(
            nombre_de_categories_sv_campEntiere,Lat_jour, Lon_jour, sa_log_jour[f,:,:], extension_zone,'LogSa', fpath+'LogSa_jour_corr'+str(np.round(freq_voulues[f]/1000))
        )
        
        if Sa_nuit.shape[-1]!=0:
            md.affichage_carte_france(
                nombre_de_categories_sv_campEntiere, Lat_nuit, Lon_nuit, Sa_nuit[f,:,:], extension_zone,'Sa', fpath+str(np.round(freq_voulues[f]/1000))+'nuit'
            )
        
            md.save_map_html(
                nombre_de_categories_sv_campEntiere, Lat_nuit, Lon_nuit, Sa_nuit_log[f,:,:], fpath+str(np.round(freq_voulues[f]/1000))+'nuit'
            )
            
            md.affichage_carte_france(
            nombre_de_categories_sv_campEntiere,Lat_nuit, Lon_nuit, sa_log_nuit[f,:,:], extension_zone,'LogSa', fpath+'LogSa_nuit_corr'+str(np.round(freq_voulues[f]/1000))
            )
        else:
            pass

################################################################################
#ECHOSONDE: ANALYSE PAR ANNEE ET SAISON
################################################################################
if analyse_par_saison=='oui':
    import cartopy.crs as ccrs
    import cartopy.feature as cfeature
    from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
    
    import matplotlib.pyplot as plt
    import matplotlib.colors as colors
    import matplotlib.ticker as mticker
    import matplotlib.font_manager as fm
    
    import numpy as np
    
    import folium
    
    from mpl_toolkits.axes_grid1.anchored_artists import AnchoredSizeBar
    
    for root, subFolders, files in os.walk(fpath):
        for file in files:
            if methode_EI=='pickle':
                if file.endswith("saved_clustering_for_EK80_all.pickle"):
                    with open(root+'/'+file, "rb") as f:
                        (
                            freq_MFR,
                            freqs_moy_transducteur,
                            Sv,
                            Sv_moy_transducteur_x,
                            Sa,
                            Depth,
                            Lat,
                            Lon,
                            Time,
                            nb_transduc,
                            tableau_radiales,
                            nom_numero_radiales,
                        ) = pickle.load(f) 
                    break
            else:
                if file.endswith("saved_clustering_for_EK80_all.pickle"):
                    with open(root+'/'+file, "rb") as f:
                        (
                            kmeans,
                            labels,
                            Q1_,
                            Q3_,
                            med,
                            nombre_de_categories,
                            Sv_temp,
                            Time_t,
                            freq_MFR,
                            Depth,
                            Lat_t,
                            Lon_t,
                            nb_transduc,
                            nom_numero_radiales,
                            methode_clustering,
                        ) = pickle.load(f)
                    break
                break
            break

    
    C, Entete, index_events, date_heure = ic.import_casino(path_evt_analyse_par_saison)
    (
        datedebut,
        datefin,
        heuredebut,
        heurefin,
        nameTransect,
        vit_vent_vrai,
        dir_vent_vrai,
    ) = ic.infos_radiales_echosonde(C)
    
    
    dirlist_profondeur = []
    
    dirlist_unites = []
    
    for root, subFolders, files in os.walk(fpath):
        for file in files:
            if file.endswith("Profondeur_max.txt"):
                dirlist_profondeur.append(os.path.join(root, file))
            elif file.endswith("UnitesDecoupageCampagne.txt"):
                dirlist_unites.append(os.path.join(root, file))
                
                
    filelist_unites = sorted_alphanumeric(dirlist_unites)
    filelist_profondeur = sorted_alphanumeric(dirlist_profondeur)
    
    unites_tot=[]
    profondeur_tot=[]
    for i in range(len(filelist_unites)):
        unites_tot.append(np.loadtxt(filelist_unites[i],dtype='int'))
        profondeur_tot.append(np.loadtxt(filelist_profondeur[i],dtype='int'))
        
    unites=[]

    for i in range(nombre_de_categories_sv_campEntiere):
        for j in range(len(unites_tot[i])):
            unites.append(unites_tot[i][j])
            
    
    unites=np.array(unites)
    
    unites=np.cumsum(unites)
    
    le=[]
    for j in range(len(unites_tot)):
        l=[]
        for i in range(1,len(unites_tot[j]),1):
            if unites_tot[j][i-1]==pas and unites_tot[j][i]!=pas:
                l.append(i)
        le.append(len(l))
    le=np.cumsum(np.array(le))
        
    indi=[]
    for j in range(len(unites_tot)):
        indi.append(len(unites_tot[j]))
    indi=np.array(indi)
    
    dirlistQ1_ = []
    dirlistQ3_ = []
    dirlistMed = []
    dirlistLabels=[]
    dirlistSa_moy=[]
    dirlistLat_moy=[]
    dirlistLon_moy=[]
    for root, subFolders, files in os.walk(fpath):
        print(root,subFolders,files)
        for file in files:
            print(file)
            if file.endswith("Q1.txt"):
                dirlistQ1_.append(os.path.join(root, file))
            elif file.endswith("Q3.txt"):
                dirlistQ3_.append(os.path.join(root, file))
            elif file.endswith("Med.txt"):
                dirlistMed.append(os.path.join(root, file))
            elif file.endswith("Labels.txt"):
                dirlistLabels.append(os.path.join(root, file))
            elif file.endswith("Sa_moy.txt"):
                dirlistSa_moy.append(os.path.join(root, file))
            elif file.endswith("Lat_moy.txt"):
                dirlistLat_moy.append(os.path.join(root, file))
            elif file.endswith("Lon_moy.txt"):
                dirlistLon_moy.append(os.path.join(root, file))
                
                
    filelistQ1_ = sorted_alphanumeric(dirlistQ1_)
    filelistQ3_ = sorted_alphanumeric(dirlistQ3_)
    filelistMed = sorted_alphanumeric(dirlistMed)
    filelistLabels = sorted_alphanumeric(dirlistLabels)
    filelistSa_moy = sorted_alphanumeric(dirlistSa_moy)
    filelistLat_moy = sorted_alphanumeric(dirlistLat_moy)
    filelistLon_moy = sorted_alphanumeric(dirlistLon_moy)
    
    Lat_tot=[]
    Lon_tot=[]
    for i in range(len(filelistLat_moy)):
        Lat_tot=np.append(Lat_tot,np.loadtxt(filelistLat_moy[i]))
        Lon_tot=np.append(Lon_tot,np.loadtxt(filelistLon_moy[i]))
        
    len_freq_tot=[]
    for i in range(len(filelistMed)):
        if len(np.where(np.isnan(np.loadtxt(filelistMed[i])))[0])==0:
            len_freq_tot.append((np.loadtxt(filelistMed[i]).reshape((len(np.loadtxt(filelistMed[i])),-1)).shape[1]))
    len_freq_tot_max=max(len_freq_tot)
            
    Q1_tot=np.loadtxt(filelistQ1_[0])
    Q3_tot=np.loadtxt(filelistQ3_[0])
    med_tot=np.loadtxt(filelistMed[0])
    if len(np.where(np.isnan(med_tot))[0])!=0 and med_tot.shape[1]>len_freq_tot_max:
        med_tot=med_tot[:,:len_freq_tot_max]
        Q1_tot=Q1_tot[:,:len_freq_tot_max]
        Q3_tot=Q3_tot[:,:len_freq_tot_max]
    else:
        med_tot=med_tot
        Q1_tot=Q1_tot
        Q3_tot=Q3_tot
    labels_tot=np.loadtxt(filelistLabels[0])
    if labels_tot.shape[1]<indice_profondeur_max:
        blanc = np.zeros((len(labels_tot),indice_profondeur_max-labels_tot.shape[1]))
        blanc.fill(np.nan)
        labels_tot=np.hstack((labels_tot,blanc))
    for i in range(1,len(filelistQ1_)):
        if len(np.where(np.isnan(np.loadtxt(filelistQ1_[i])))[0])!=0 and np.loadtxt(filelistQ1_[i]).shape[1]>len_freq_tot_max:
            med_tot_temp=np.loadtxt(filelistMed[i])[:,:len_freq_tot_max]
            Q1_tot_temp=np.loadtxt(filelistQ1_[i])[:,:len_freq_tot_max]
            Q3_tot_temp=np.loadtxt(filelistQ3_[i])[:,:len_freq_tot_max]
        else:
            med_tot_temp=np.loadtxt(filelistMed[i])
            Q1_tot_temp=np.loadtxt(filelistQ1_[i])
            Q3_tot_temp=np.loadtxt(filelistQ3_[i])
        Q1_tot=np.vstack((Q1_tot,Q1_tot_temp))
        Q3_tot=np.vstack((Q3_tot,Q3_tot_temp))
        med_tot=np.vstack((med_tot,med_tot_temp))  
        # if len(np.where(np.isnan(np.loadtxt(filelistMed[i]))))==0 and np.loadtxt(filelistMed[i]).shape[1]>len_freq_tot:
            
        # else:
        #     len_freq_tot=med_tot.shape[1]
        labels=np.loadtxt(filelistLabels[i])
        if labels.shape[1]<indice_profondeur_max:
            blanc = np.zeros((len(labels),indice_profondeur_max-labels.shape[1]))
            blanc.fill(np.nan)
            labels=np.hstack((labels,blanc))
        labels_tot=np.vstack((labels_tot,labels))
    
    len_med_tot=len(med_tot)
    
    Sa_moy_tot=[]
    for i in range(len(filelistSa_moy)):
        Sa=np.loadtxt(filelistSa_moy[i])
        if Sa.shape[0]!=indice_profondeur_max:
            Sa=Sa.T
        else:
            Sa=Sa
        Sa_moy_tot.append(Sa)
        
    len_Sv0_ent_tot_final=unites
    
    mask=np.where(np.isnan(med_tot)==True)
    mask_kmeans=np.where(np.isnan(med_tot)==False)
    
    med_tot_add=med_tot[mask].reshape((-1,len_freq_tot_max))
    Q1_tot_add=Q1_tot[mask].reshape((-1,len_freq_tot_max))
    Q3_tot_add=Q3_tot[mask].reshape((-1,len_freq_tot_max))
    add=np.zeros(np.unique(mask[0]).shape)
    add.fill(np.nan)
    kmeans_sv_labels_add=add

    
    
    med_tot_kmeans=med_tot[mask_kmeans].reshape((-1,len_freq_tot_max))
    Q1_tot_kmeans=Q1_tot[mask_kmeans].reshape((-1,len_freq_tot_max))
    Q3_tot_kmeans=Q3_tot[mask_kmeans].reshape((-1,len_freq_tot_max))
    Q3_tot_kmeans=Q3_tot[mask_kmeans].reshape((-1,len_freq_tot_max))
    
            
    if nombre_composantes>len(freq_MFR):
        raise ValueError('Nombre de composantes trop élevé par rapport au nombre de fréquences')
    elif nombre_voisins<nombre_composantes:
        raise ValueError('Nombre de voisins trop faible par rapport au nombre de composantes')
    else:
        lle = LocallyLinearEmbedding(n_components=nombre_composantes, n_neighbors = nombre_voisins,method = 'modified', n_jobs = 4,  random_state=0)
        lle.fit(med_tot)
        med_lle = lle.transform(med_tot)
    
    
    kmeans_sv=AgglomerativeClustering(n_clusters=nombre_de_categories_sv_campEntiere,affinity='euclidean',linkage='ward').fit(med_lle)
    
    kmeans_sv_labels_kmeans=kmeans_sv.labels_
    
    
    if len(mask[0])!=0:
        ind_mask0=mask[0][0]
        
        med_tot_temp0=med_tot_kmeans[:ind_mask0]
        med_tot_temp0=np.vstack((med_tot_temp0,med_tot_add))
        med_tot_temp0=np.vstack((med_tot_temp0,med_tot_kmeans[ind_mask0:]))
        Q1_tot_temp0=Q1_tot_kmeans[:ind_mask0]
        Q1_tot_temp0=np.vstack((Q1_tot_temp0,Q1_tot_add))
        Q1_tot_temp0=np.vstack((Q1_tot_temp0,Q1_tot_kmeans[ind_mask0:]))
        Q3_tot_temp0=Q3_tot_kmeans[:ind_mask0]
        Q3_tot_temp0=np.vstack((Q3_tot_temp0,Q3_tot_add))
        Q3_tot_temp0=np.vstack((Q3_tot_temp0,Q3_tot_kmeans[ind_mask0:]))
        kmeans_sv_temp0=kmeans_sv_labels_kmeans[:ind_mask0]
        kmeans_sv_temp0=np.hstack((kmeans_sv_temp0,kmeans_sv_labels_add))
        kmeans_sv_temp0=np.hstack((kmeans_sv_temp0,kmeans_sv_labels_kmeans[ind_mask0:]))
    else:
        med_tot_temp0=med_tot_kmeans
        Q1_tot_temp0=Q1_tot_kmeans
        Q3_tot_temp0=Q3_tot_kmeans
        kmeans_sv_temp0=kmeans_sv_labels_kmeans
        
    
    indi_t=np.cumsum(indi)
    for indij in range(len(indi)):
        if indij==0:
            globals()['med_tot%s' % indij] = med_tot_temp0[:indi_t[indij]*nombre_de_categories_sv]
            globals()['Q1_tot%s' % indij] = Q1_tot_temp0[:indi_t[indij]*nombre_de_categories_sv]
            globals()['Q3_tot%s' % indij] = Q3_tot_temp0[:indi_t[indij]*nombre_de_categories_sv]
            globals()['kmeans_sv_labels%s' % indij] = kmeans_sv_temp0[:indi_t[indij]*nombre_de_categories_sv]
            globals()['len_Sv0_ent_tot_final%s' % indij]=len_Sv0_ent_tot_final[:indi_t[indij]]
        else:
            globals()['med_tot%s' % indij] = med_tot_temp0[indi_t[indij-1]*nombre_de_categories_sv:indi_t[indij]*nombre_de_categories_sv]
            globals()['Q1_tot%s' % indij] = Q1_tot_temp0[indi_t[indij-1]*nombre_de_categories_sv:indi_t[indij]*nombre_de_categories_sv]
            globals()['Q3_tot%s' % indij] = Q3_tot_temp0[indi_t[indij-1]*nombre_de_categories_sv:indi_t[indij]*nombre_de_categories_sv]
            globals()['kmeans_sv_labels%s' % indij] = kmeans_sv_temp0[indi_t[indij-1]*nombre_de_categories_sv:indi_t[indij]*nombre_de_categories_sv]
            globals()['len_Sv0_ent_tot_final%s' % indij]=len_Sv0_ent_tot_final[indi_t[indij-1]:indi_t[indij]]
            
    for indij in range(len(indi)):
        globals()['med_tot%s' % indij]=globals()['med_tot%s' % indij][np.where(np.isnan(globals()['med_tot%s' % indij])==False)].reshape((-1,len_freq_tot_max))
        globals()['Q1_tot%s' % indij]=globals()['Q1_tot%s' % indij][np.where(np.isnan(globals()['Q1_tot%s' % indij])==False)].reshape((-1,len_freq_tot_max))
        globals()['Q3_tot%s' % indij]=globals()['Q3_tot%s' % indij][np.where(np.isnan(globals()['Q3_tot%s' % indij])==False)].reshape((-1,len_freq_tot_max))
        globals()['kmeans_sv_labels%s' % indij]=globals()['kmeans_sv_labels%s' % indij][np.where(np.isnan(globals()['kmeans_sv_labels%s' % indij])==False)]
        
        
        
    nb_l=int(np.ceil(len(indi)/2))
    nb_cell=[]
    mult=nb_l*nb_l
    couleurs = plt.cm.jet(np.linspace(0,1,nombre_de_categories_sv_campEntiere))
    cmap = colors.ListedColormap(couleurs)
    for i in range(len(indi),mult+1,1):
        if i%2==0 and i%3==0:
            nb_cell.append(i)
    nb_c=max(nb_cell)
    if len(indi)<=2:
        fig, axs = plt.subplots(nb_l, nb_l)
    else:
        fig, axs = plt.subplots(nb_c//3, nb_c//2)

    for j in range(len(indi)): 
        periode=datedebut[le[j]-1][3:]
        if len(indi)<=2:
            if j<nb_l:
                for x in np.unique(globals()['kmeans_sv_labels%s' % j]):
                    indice = np.where(globals()['kmeans_sv_labels%s' % j] == x)
                    axs[0, j].plot(freq_MFR[:], np.mean( globals()['Q1_tot%s' % j][indice], axis=0),'-.',color=couleurs[int(x)])
                    axs[0, j].plot(freq_MFR[:], np.mean( globals()['Q3_tot%s' % j][indice], axis=0),'-.',color=couleurs[int(x)])
                    axs[0, j].plot(freq_MFR[:], np.mean( globals()['med_tot%s' % j][indice], axis=0),'o-',color=couleurs[int(x)],label="Classe"+str(int(x)))
                    axs[0, j].set_title(periode)
                    axs[0, j].set_xlabel('fréquence (kHz)')
                    axs[0, j].set_ylabel('profondeur (en m)')
                    axs[0, j].legend() 
            else:
                for x in np.unique(globals()['kmeans_sv_labels%s' % j]):
                    indice = np.where(globals()['kmeans_sv_labels%s' % j] == x)
                    axs[1, j-nb_l].plot(freq_MFR[:], np.mean( globals()['Q1_tot%s' % j][indice], axis=0),'-.',color=couleurs[int(x)])
                    axs[1, j-nb_l].plot(freq_MFR[:], np.mean( globals()['Q3_tot%s' % j][indice], axis=0),'-.',color=couleurs[int(x)])
                    axs[1, j-nb_l].plot(freq_MFR[:], np.mean( globals()['med_tot%s' % j][indice], axis=0),'o-',color=couleurs[int(x)],label="Classe"+str(int(x)))
                    axs[1, j-nb_l].set_title(periode)
                    axs[1, j-nb_l].set_xlabel('fréquence (kHz)')
                    axs[1, j-nb_l].set_ylabel('profondeur (en m)')
                    axs[1, j-nb_l].legend() 
        else:
            if j<nb_c//2:
                for x in np.unique(globals()['kmeans_sv_labels%s' % j]):
                    indice = np.where(globals()['kmeans_sv_labels%s' % j] == x)
                    axs[0, j].plot(freq_MFR[:], np.mean( globals()['Q1_tot%s' % j][indice], axis=0),'-.',color=couleurs[int(x)])
                    axs[0, j].plot(freq_MFR[:], np.mean( globals()['Q3_tot%s' % j][indice], axis=0),'-.',color=couleurs[int(x)])
                    axs[0, j].plot(freq_MFR[:], np.mean( globals()['med_tot%s' % j][indice], axis=0),'o-',color=couleurs[int(x)],label="Classe"+str(int(x)))
                    axs[0, j].set_title(periode)
                    axs[0, j].set_xlabel('fréquence (kHz)')
                    axs[0, j].set_ylabel('profondeur (en m)')
                    axs[0, j].legend() 
            else:
                for x in np.unique(globals()['kmeans_sv_labels%s' % j]):
                    indice = np.where(globals()['kmeans_sv_labels%s' % j] == x)
                    axs[1, j-nb_l].plot(freq_MFR[:], np.mean( globals()['Q1_tot%s' % j][indice], axis=0),'-.',color=couleurs[int(x)])
                    axs[1, j-nb_l].plot(freq_MFR[:], np.mean( globals()['Q3_tot%s' % j][indice], axis=0),'-.',color=couleurs[int(x)])
                    axs[1, j-nb_l].plot(freq_MFR[:], np.mean( globals()['med_tot%s' % j][indice], axis=0),'o-',color=couleurs[int(x)],label="Classe"+str(int(x)))
                    axs[1, j-nb_l].set_title(periode)
                    axs[1, j-nb_l].set_xlabel('fréquence (kHz)')
                    axs[1, j-nb_l].set_ylabel('profondeur (en m)')
                    axs[1, j-nb_l].legend() 
    
            
    kmeans_sv_temp0_new=kmeans_sv_temp0+nombre_de_categories_sv_campEntiere
    labels_tot_new=np.where(labels_tot==nombre_de_categories_sv,np.nanmax(kmeans_sv_temp0_new)+1,labels_tot)
    
    j=0
    for i in range(nombre_de_categories_sv):
        labels_tot_new[:len_Sv0_ent_tot_final[j],:]=np.where(labels_tot_new[:len_Sv0_ent_tot_final[j],:]==i,kmeans_sv_temp0_new[i+j],labels_tot_new[:len_Sv0_ent_tot_final[j],:])       
    
    kmeans_sv_temp0_new_shape=kmeans_sv_temp0_new.reshape((len(len_Sv0_ent_tot_final),nombre_de_categories_sv))     
    for j in range(1,len(len_Sv0_ent_tot_final)):
        for i in range(nombre_de_categories_sv):
            labels_tot_new[len_Sv0_ent_tot_final[j-1]:len_Sv0_ent_tot_final[j],:]=np.where(labels_tot_new[len_Sv0_ent_tot_final[j-1]:len_Sv0_ent_tot_final[j],:]==i,kmeans_sv_temp0_new_shape[j,i],labels_tot_new[len_Sv0_ent_tot_final[j-1]:len_Sv0_ent_tot_final[j],:])
            
    labels_tot_new=labels_tot_new-nombre_de_categories_sv_campEntiere 
    
    for j in range(len(indi)):
        if j==0:
            globals()['labels_tot_new%s' % j]=labels_tot_new[:globals()['len_Sv0_ent_tot_final%s' % j][-1]]
        else:
            globals()['labels_tot_new%s' % j]=labels_tot_new[globals()['len_Sv0_ent_tot_final%s' % j][0]-pas:globals()['len_Sv0_ent_tot_final%s' % j][-1]]
                 
        
    nb_l=int(np.ceil(len(indi)/2))
    nb_cell=[]
    mult=nb_l*nb_l
    couleurs = plt.cm.jet(np.linspace(0,1,nombre_de_categories_sv_campEntiere))
    cmap = colors.ListedColormap(couleurs)
    for i in range(len(indi),mult+1,1):
        if i%2==0 and i%3==0:
            nb_cell.append(i)
    nb_c=max(nb_cell)
    if len(indi)<=2:
        fig, axs = plt.subplots(nb_l, nb_l)
    else:
        fig, axs = plt.subplots(nb_c//3, nb_c//2)
    from mpl_toolkits.axes_grid1 import make_axes_locatable
    for j in range(len(indi)): 
        periode=datedebut[le[j]-1][3:]
        x=np.arange(0,len(globals()['labels_tot_new%s' % j]),1)
        y=np.arange(-Depth[1,0],-Depth[1,(len(Depth[1])-1)]-1,-1)
        if len(indi)<=2:
            if j<nb_l:
                axs[0, j].set_xlabel('N*ESU')
                axs[0, j].set_ylabel('profondeur (en m)')
                axs[0, j].set_title(periode)
                norm=colors.Normalize(vmin=0,vmax=nombre_de_categories_sv_campEntiere-1)
                im=axs[0, j].pcolormesh(x,y,np.transpose(globals()['labels_tot_new%s' % j]), cmap=cmap,norm=norm)
                axs[0, j].grid()
                divider = make_axes_locatable(axs[0, j])
        
            else:
                axs[1, j-nb_l].set_xlabel('N*ESU')
                axs[1, j-nb_l].set_ylabel('profondeur (en m)')
                axs[1, j-nb_l].set_title(periode)
                norm=colors.Normalize(vmin=0,vmax=nombre_de_categories_sv_campEntiere-1)
                axs[1, j-nb_l].pcolormesh(x,y,np.transpose(globals()['labels_tot_new%s' % j]), cmap=cmap,norm=norm)
                axs[1, j-nb_l].grid()
                divider = make_axes_locatable(axs[1, j-nb_l])

        else:
            if j<nb_c//2:
                axs[0, j].set_xlabel('N*ESU')
                axs[0, j].set_ylabel('profondeur (en m)')
                axs[0, j].set_title(periode)
                norm=colors.Normalize(vmin=0,vmax=nombre_de_categories_sv_campEntiere-1)
                im=axs[0, j].pcolormesh(x,y,np.transpose(globals()['labels_tot_new%s' % j]), cmap=cmap,norm=norm)
                axs[0, j].grid()
                divider = make_axes_locatable(axs[0, j])
        
            else:
                axs[1, j-nb_l].set_xlabel('N*ESU')
                axs[1, j-nb_l].set_ylabel('profondeur (en m)')
                axs[1, j-nb_l].set_title(periode)
                norm=colors.Normalize(vmin=0,vmax=nombre_de_categories_sv_campEntiere-1)
                axs[1, j-nb_l].pcolormesh(x,y,np.transpose(globals()['labels_tot_new%s' % j]), cmap=cmap,norm=norm)
                axs[1, j-nb_l].grid()
                divider = make_axes_locatable(axs[1, j-nb_l])

        fig.subplots_adjust(right=0.8)
        cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
        fig.colorbar(im, cax=cbar_ax)     
            
        
        Sa_moy_label_tot = []
        Sa_moy_label_log_tot = []
        u=0
        for f in range(nb_transduc,len(Sa_moy_tot)+1,nb_transduc):
            #print('boucle sur f:',f)
            sa_temp=np.array(Sa_moy_tot[f-nb_transduc:f])
            #print(sa_temp.shape)
            if np.cumsum(unites)[u]<=pas:
                unit0=0
                unit1=unites[0]
            else:
                unit0=unites[u-1]
                unit1=unites[u]
            # print('unit0',unit0)
            # print('unit1',unit1)
            for i in range(nb_transduc):
                #print('boucle sur i:',i)
                sa_t=sa_temp[i,:,:].T
                #print(sa_t.shape)
                for x in range(nombre_de_categories_sv_campEntiere):
                    #print('boucle sur x:',x)
                    Sa_label = np.zeros(sa_t.shape)
                    Sa_label[:, :] = np.nan
                    #print(Sa_label.shape)
                    index_lab = np.where(labels_tot_new[unit0:unit1] == x)
                    Sa_label[index_lab] = sa_t[index_lab]
                    Sa_moy_label_tot.append(np.nansum(Sa_label, axis=1))
                    Sa_moy_label_log_tot.append(10 * np.log10(np.nanmean(Sa_label, axis=1)))
            u+=1               
                        
        reindexage=np.arange(0,len(Sa_moy_label_tot),nb_transduc*nombre_de_categories_sv_campEntiere)
        Sa_moy_label_tot_final=Sa_moy_label_tot[0]
        Sa_moy_label_log_tot_final=Sa_moy_label_log_tot[0]
        for j in range(nb_transduc*nombre_de_categories_sv_campEntiere):
            for i in range(len(reindexage)):
                #print(reindexage[i])
                Sa_moy_label_tot_final=np.hstack((Sa_moy_label_tot_final,Sa_moy_label_tot[reindexage[i]]))
                Sa_moy_label_log_tot_final=np.hstack((Sa_moy_label_log_tot_final,Sa_moy_label_log_tot[reindexage[i]]))
            reindexage+=1
        Sa_moy_label_tot_final=Sa_moy_label_tot_final[unites[0]:]
        Sa_moy_label_tot_final=Sa_moy_label_tot_final.reshape((len(freq_voulues),nombre_de_categories_sv_campEntiere,-1))  
        Sa_moy_label_log_tot_final=Sa_moy_label_log_tot_final[unites[0]:]
        Sa_moy_label_log_tot_final=Sa_moy_label_log_tot_final.reshape((len(freq_voulues),nombre_de_categories_sv_campEntiere,-1))  
        
        #les cercles n'apparaissent que pour les valeurs positives (faire coincider figure et legende en n'affichant que les resultats positifs)
        sa_log=np.zeros((nbfreqs,nombre_de_categories_sv_campEntiere,Sa_moy_label_log_tot_final.shape[2]))
        for f in range(len(freq_voulues)):
            for x in range(nombre_de_categories_sv_campEntiere):
                for i in range(Sa_moy_label_log_tot_final.shape[2]):
                    if Sa_moy_label_log_tot_final[f][x][i]>0:
                        sa_log[f,x,i]=Sa_moy_label_log_tot_final[f][x][i]+offset
                    elif Sa_moy_label_log_tot_final[f][x][i]<=0 and np.isnan(Sa_moy_label_log_tot_final[f][x][i])==False:
                        sa_log[f,x,i]=offset


                        
    for j in range(len(indi)):
        if j==0:
            globals()['lat%s' % j]=Lat_tot[:globals()['len_Sv0_ent_tot_final%s' % j][-1]]
            globals()['lon%s' % j]=Lon_tot[:globals()['len_Sv0_ent_tot_final%s' % j][-1]]
            globals()['sa_log_t%s' % j]=sa_log[:,:,:globals()['len_Sv0_ent_tot_final%s' % j][-1]]
        else:
            globals()['lat%s' % j]=Lat_tot[globals()['len_Sv0_ent_tot_final%s' % j][0]-pas:globals()['len_Sv0_ent_tot_final%s' % j][-1]]
            globals()['lon%s' % j]=Lon_tot[globals()['len_Sv0_ent_tot_final%s' % j][0]-pas:globals()['len_Sv0_ent_tot_final%s' % j][-1]]
            globals()['sa_log_t%s' % j]=sa_log[:,:,globals()['len_Sv0_ent_tot_final%s' % j][0]-pas:globals()['len_Sv0_ent_tot_final%s' % j][-1]]
            
    for j in range(len(indi)):
        mask_lat=np.where(np.isnan(globals()['lat%s' % j])==False) 
        globals()['lat_tot%s' % j]=globals()['lat%s' % j][mask_lat]
        globals()['lon_tot%s' % j]=globals()['lon%s' % j][mask_lat]  
        globals()['sa_log_tot%s' % j]=globals()['sa_log_t%s' % j][:,:,mask_lat[0]]
        
        
    plt.rc('legend',fontsize=8) 
    nb_l=int(np.ceil(len(indi)/2))
    ech='LogSa'
    for f in range(len(freq_voulues)): 
        couleurs = plt.cm.jet(np.linspace(0, 1, nombre_de_categories_sv_campEntiere))
        for x in range(nombre_de_categories_sv_campEntiere):
            plt.figure(figsize=(35,20))
            for j in range(len(indi)): 
                periode=datedebut[le[j]-1][3:]
                globals()['ax%s' % j] = plt.subplot(len(indi), 1, j+1, projection=ccrs.PlateCarree())
                globals()['ax%s' % j].set_extent(extension_zone)
                Grider = globals()['ax%s' % j].gridlines(draw_labels=True)
                Grider.xformatter = LONGITUDE_FORMATTER
                Grider.yformatter = LATITUDE_FORMATTER
                Grider.xlabel_style = {'size': 15,'weight': 'bold'}
                Grider.ylabel_style = {'size': 15,'weight': 'bold'}
                Grider.xlabels_top  = False
                Grider.ylabels_right  = False
                fontprops = fm.FontProperties(size=18)
                Grider.xlocator = mticker.MaxNLocator(2)
                Grider.ylocator = mticker.MaxNLocator(2)
            
                globals()['ax%s' % j].add_feature(cfeature.OCEAN.with_scale("50m"))
                globals()['ax%s' % j].add_feature(cfeature.RIVERS.with_scale("50m"))
                globals()['ax%s' % j].add_feature(cfeature.BORDERS.with_scale("50m"), linestyle=":")
                if ech=='Sa':
                    globals()['ax%s' % j].set_title(periode+','+str(np.round(freq_voulues[f]/1000))+'kHz:'+r'$S_A$' ' en dB re 1($m^{2} nmi^{-2}$)',size=15)
                else:
                    globals()['ax%s' % j].set_title(periode+','+str(np.round(freq_voulues[f]/1000))+'kHz:'+r'log($S_A$)',size=15)
                globals()['ax%s' % j].legend(fontsize=15)
                scalebar = AnchoredSizeBar(globals()['ax%s' % j].transData,
                                       0.016, '1 MN', 'lower center', 
                                       pad=0.1,
                                       color='white',
                                       frameon=False,
                                       size_vertical=0.001,
                                       fontproperties=fontprops)
            
                globals()['ax%s' % j].add_artist(scalebar)
                
            
                        
                l=globals()['ax%s' % j].scatter(globals()['lon_tot%s' % j],globals()['lat_tot%s' % j], s=globals()['sa_log_tot%s' % j][f,:,:][x], color=couleurs[x])
                legend=globals()['ax%s' % j].legend(*l.legend_elements("sizes", num=6,color=couleurs[x]),labelspacing=1,loc='upper left')
    
    
            
        
else:
    pass
    

################################################################################
#PHASE DE CORRECTION
################################################################################
from sklearn.ensemble import IsolationForest
import copy

"""
1) Détection de possibles RF aberrantes à partir de la classification finale et 
   récupération des indices des RF en question  
2) Investigation de chaque RF constituant le cluster identifié comme posant problème 
   Selon la forme de la RF et l'échogramme, l'utilisateur décide s'il veut procéder 
   à une phase de correction     
3) Selon les résultats retournés par les différents algorithmes de correction,
   l'utilisateur décide ou non de conserver les corrections
4) La classification finale est refaite à partir des données corrigées
"""

#investigation de valeurs aberrantes à partir de la classification finale
run_corr1=input("do you want to investigate possible outliers ? \n")
#critere basé sur le nombre de RF constituant un cluster, si un cluster est 
#constitué de critere_nb_RF_clusters ou moins, il est repéré (l'utilisateur 
#confirmera plus tard s'il pense qu'il s'agit effectivement de valeurs aberrantes
#ou plutôt d'une RF correspondant à des organismes peu observés au cours de la campagne) 
critere_nb_RF_clusters=2

if run_corr1=='y':
    num_label_RF_outlier=np.array([],dtype=int)
    for i in range(nombre_de_categories_sv_campEntiere):
        if len(np.where(kmeans_sv.labels_==i)[0])<=critere_nb_RF_clusters:
            for x in np.where(kmeans_sv.labels_==i)[0]:
                num_label_RF_outlier=np.append(num_label_RF_outlier,x)
                
    dirlistCluster=[]
    for root, subFolders, files in os.walk(fpath):
        for file in files:
            if file.endswith("saved_clustering_for_EK80_all.pickle"):
                dirlistCluster.append(os.path.join(root, file))
                
    filelistCluster = sorted_alphanumeric(dirlistCluster)
                
    filelist_indice=np.arange(0,len(med_tot),1)
    filelist_indice=filelist_indice.reshape((len(filelistLabels),nombre_de_categories_sv))  
    #affichage de chaque RF constituant le cluster précédemment repéré comme pouvant contenir des outliers
    for ind in num_label_RF_outlier:
        # labels=np.loadtxt(filelistLabels[np.where(filelist_indice==ind)[0][0]])
        # Q1_=np.loadtxt(filelistQ1_[np.where(filelist_indice==ind)[0][0]])
        # Q3_=np.loadtxt(filelistQ3_[np.where(filelist_indice==ind)[0][0]])
        # med=np.loadtxt(filelistMed[np.where(filelist_indice==ind)[0][0]])
        with open(filelistCluster[np.where(filelist_indice==ind)[0][0]], "rb") as f:
            (
                kmeans,
                labels,
                Q1_,
                Q3_,
                med,
                nombre_de_categories,
                Sv_temp,
                Time_t,
                freq_MFR,
                Depth,
                Lat_t,
                Lon_t,
                nb_transduc,
                nom_numero_radiales,
                methode_clustering,
            ) = pickle.load(f)
        

            
        #investigate transects where RF ressemble outliers
        #plt.ion()
        couleurs = plt.cm.jet(np.linspace(0,1,nombre_de_categories))
        cmap = colors.ListedColormap(couleurs)         
        x=np.arange(0,len(Sv_temp),1)
        y=np.arange(-Depth[1,0],-Depth[1,(len(Depth[1])-1)]-1,-1)
        fig,ax2 = plt.subplots(figsize=(30, 15))
        #image de classification
        plt.subplot(211)
        plt.xlabel('N*ESU')
        plt.ylabel('profondeur (en m)')
        plt.title('Portion de radiale à investiguer pour possibles outliers')
        plt.pcolormesh(x,y,np.transpose(labels), cmap=cmap)
        plt.grid()
        plt.colorbar()
        
        #réponse en fréquence
        plt.subplot(212)
        plt.xlabel('Freq')
        plt.ylabel('Sv dB')
        plt.title('Reponse en frequence par cluster')
        for j in range(nombre_de_categories):
            plt.plot(freq_MFR[:indice_freq_max],Q1_[j],'-.',color=couleurs[j])
            plt.plot(freq_MFR[:indice_freq_max],Q3_[j],'-.',color=couleurs[j])
            plt.plot(freq_MFR[:indice_freq_max],med[j],'o-',color=couleurs[j])
        plt.grid()
        interval = 10 #secondes d'interaction avec la figure données à l'utilisateur
        #pour choisir si une correction est nécessaire
        plt.pause(interval)
        plt.show()
        plt.close()
               
        #après affichage de chaque RF constituant le cluster précédemment repéré comme
        #pouvant contenir des outliers, l'utilisateur confirme ou non au script s'il 
        #faut continuer l'investigation ou s'arrêter car la RF en question correspond 
        #réellement à un organisme observé
        run_corr2=input("do you want to run an outlier detection algorithm ? \n")
        if run_corr2=='y':
            #6 critères de test pour chaque portion:
            num_label_outlier=np.array([])
            for i in range(len(med)):
                if len(np.where(labels==i)[0])<5/1000*len(labels):
                    print("cluster number "+str(i)+" size is very short, outliers suspected")
                    num_label_outlier=np.append(num_label_outlier,i)
                if len(np.where(med[i,:]>-35)[0])!=0:
                    print("intensity of backscattered signal of cluster number "+str(i)+" is very high, outliers suspected")
                    num_label_outlier=np.append(num_label_outlier,i)
                clf = IsolationForest(random_state=0).fit_predict(med[i,:].reshape((len(med[i,:]),1)))
                clf=clf.reshape((len(med[i,:]),1))
                if len(np.where(clf==-1)[0])>10/100*len(med[i,:]):
                    print("high number of values in RF for which number of splittings (based on a randomly selected feature) required to isolate said values in cluster number "+str(i)+" is very low, outliers suspected")
                    num_label_outlier=np.append(num_label_outlier,i)
                span=np.max(med[i,:])-np.min(med[i,:])
                if span>40:
                    print("span of values larger than 40dB, outliers suspected")
                    num_label_outlier=np.append(num_label_outlier,i)
                if np.sqrt((np.cumsum(med[2,:]**2)[0])/len(med[2,:]))>25/100*span:
                    print("high RMS, outliers suspected")
                    num_label_outlier=np.append(num_label_outlier,i)
                if len(np.where((np.abs(med[2,:]-np.mean(med[2,:])))/len(med[2,:])>0.01)[0])>90/100*len(med[i,:]):
                    print("mean distance to the mean very high for some values, outliers suspected")
                    num_label_outlier=np.append(num_label_outlier,i)


            #si une des RF a été identifiée comme valeur aberrante par au moins 
            #num_detect_critere_outlier des criteres ci-dessus alors on génère 
            #un nouveau fichier de classification avec les corrections qui va être 
            #soumis pour vérification à l'utilisateur
            num_detect_critere_outlier=4
            labels_copy=copy.deepcopy(labels)
            for i in range(len(med)):
                with open(filelistCluster[np.where(filelist_indice==ind)[0][0]], "rb") as f:
                    (
                        kmeans,
                        labels,
                        Q1_,
                        Q3_,
                        med,
                        nombre_de_categories,
                        Sv_temp,
                        Time_t,
                        freq_MFR,
                        Depth,
                        Lat_t,
                        Lon_t,
                        nb_transduc,
                        nom_numero_radiales,
                        methode_clustering,
                    ) = pickle.load(f)
                    
  
                H_couche=(Depth[0, 1] - Depth[0, 0])
                temp=ma.masked_object(Sv_temp,-10000000.0)
                Sa_temp=10**(temp/10)*H_couche*1852**2*4*np.pi
                if len(np.where(num_label_outlier==i)[0])>=num_detect_critere_outlier:
                    Sv_temp_bis=np.empty(Sv_temp.shape)
                    Sv_temp_bis[:,:,:]=Sv_temp[:,:,:]
                    Lat_temp_bis=np.empty(Lat_t.shape)
                    Lat_temp_bis[:,:,:]=Lat_t[:,:,:]
                    Lon_temp_bis=np.empty(Lon_t.shape)
                    Lon_temp_bis[:,:,:]=Lon_t[:,:,:]
                    Time_temp_bis=np.empty(Time_t.shape)
                    Time_temp_bis[:,:,:]=Time_t[:,:]
                    Sa_temp_bis=np.empty(Sa_temp.shape)
                    Sa_temp_bis[:,:,:]=Sa_temp[:,:,:]
                    for ind_i in np.where(labels_copy==i)[0]:
                        #print(i,ind_i)
                        for ind_j in np.where(labels_copy==i)[1]:
                            #print(i,ind_j)
                            Sv_temp_bis[ind_i,ind_j,:]=-9999999.9
                            Sa_temp_bis[ind_i,ind_j,:]=-9999999.9
                            Lat_temp_bis[ind_i,ind_j,:]=-9999999.9
                            Lon_temp_bis[ind_i,ind_j,:]=-9999999.9
                            Time_temp_bis[ind_i,ind_j]=-9999999.9

                    Time_temp_bis=au.hac_read_day_time(Time_temp_bis)
                    os.rename(filelistCluster[np.where(filelist_indice==ind)[0][0]],filelistCluster[np.where(filelist_indice==ind)[0][0]][:-7]+'_old'+str(i)+'.pickle')
                    filename_clustering_EK80_corr=filelistCluster[np.where(filelist_indice==ind)[0][0]]       
                    classif.clustering_netcdf(
                    Sv_temp_bis,
                    freq_MFR,
                    nombre_de_categories_sv,
                    Time_temp_bis,
                    Depth,
                    Lat_temp_bis,
                    Lon_temp_bis,
                    nb_transduc,
                    nom_numero_radiales,
                    methode_clustering,
                    nombre_composantes,
                    nombre_voisins,
                    save=save_data,
                    filename=filename_clustering_EK80_corr,
                    )
            
                    with open(filename_clustering_EK80_corr, "rb") as f_corr:
                        (
                            kmeans_corr,
                            labels_corr,
                            Q1_corr,
                            Q3_corr,
                            med_corr,
                            nombre_de_categories,
                            Sv_temp_bis,
                            Time_temp_bis,
                            freq_MFR,
                            Depth,
                            Lat_temp_bis,
                            Lon_temp_bis,
                            nb_transduc,
                            nom_numero_radiales,
                            methode_clustering,
                        ) = pickle.load(f_corr)

                    #print(i)
            
                    #on affiche les résultats de la classification corrigée
                    # # Affichage de la classification et de la réponse fréquentielle par clusters 
                    x=np.arange(0,len(Sv_temp_bis),1)
                    y=np.arange(-Depth[1,0],-Depth[1,(len(Depth[1])-1)]-1,-1)
                    fig,ax2 = plt.subplots(figsize=(30, 15))
                    #image de classification
                    plt.subplot(211)
                    plt.xlabel('N*ESU')
                    plt.ylabel('profondeur (en m)')
                    plt.title('Portion corrigée des possibles outliers')
                    plt.pcolormesh(x,y,np.transpose(labels_corr), cmap=cmap)
                    plt.grid()
                    plt.colorbar()
                    plt.show()
                    
                    #réponse en fréquence
                    plt.subplot(212)
                    plt.xlabel('Freq')
                    plt.ylabel('Sv dB')
                    plt.title('Reponse en frequence par cluster')
                    for j in range(nombre_de_categories_sv):
                        plt.plot(freq_MFR[:indice_freq_max],Q1_corr[j],'-.',color=couleurs[j])
                        plt.plot(freq_MFR[:indice_freq_max],Q3_corr[j],'-.',color=couleurs[j])
                        plt.plot(freq_MFR[:indice_freq_max],med_corr[j],'o-',color=couleurs[j])
                    plt.grid()
                    plt.pause(interval)
                    plt.show()
                    plt.close()
                    
                    #print(i)
                    
                    #l'utilisateur décide si ces corrections doivent être conservées et appliquées à tous les fichiers
                    #les anciens fichiers sont conservés mais renommés _old.extension
                    save_corr=input("do you want to apply corrections ? \n")
                    if save_corr=='y':
                        os.rename(filelistLabels[np.where(filelist_indice==ind)[0][0]],filelistLabels[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                        np.savetxt(filelistLabels[np.where(filelist_indice==ind)[0][0]],labels_corr)
                        os.rename(filelistQ1_[np.where(filelist_indice==ind)[0][0]],filelistQ1_[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                        np.savetxt(filelistQ1_[np.where(filelist_indice==ind)[0][0]],Q1_corr)
                        os.rename(filelistQ3_[np.where(filelist_indice==ind)[0][0]],filelistQ3_[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                        np.savetxt(filelistQ3_[np.where(filelist_indice==ind)[0][0]],Q3_corr)
                        os.rename(filelistMed[np.where(filelist_indice==ind)[0][0]],filelistMed[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                        np.savetxt(filelistMed[np.where(filelist_indice==ind)[0][0]],med_corr)    
                        fig.savefig(filelistLabels[np.where(filelist_indice==ind)[0][0]][:-25]+'png/Figure_clustering_corr'+str(ind)+'_'+str(ind)+'.png')
  
                        Sa_affiche = []
                        Sv_affiche = []
                        freqs_affichage = []
                        for f in range(len(freq_voulues)):
                            indice = np.where(freq_MFR == freq_voulues[f])
                            Sv_affiche.append(np.transpose(np.squeeze(Sv_temp_bis[:, :, indice])))
                            if methode_EI=='pickle':
                                Sa_moy = np.squeeze(Sa_temp_bis[:, :, f])
                            else:
                                Sa_affiche.append(np.transpose(np.squeeze(Sa_temp_bis[:, :, indice])))
                                Lat_temp=Lat_temp_bis[:,:,f]
                                Lon_temp=Lon_temp_bis[:,:,f]
                                Time_temp=Time_temp_bis[:,f]
                            Time_temp=au.hac_read_day_time(Time_temp)
                            freqs_affichage.append((freq_voulues[f]) / 1000)
                        Sv_affiche = np.array(Sv_affiche)
                        Sa_moy = np.array(Sa_affiche)
                        fig, ax3 = plt.subplots(figsize=(24, 35))
                        dr.DrawMultiEchoGram(
                            Sv_affiche,
                            -Depth,
                            len(freq_voulues),
                            freqs=freqs_affichage,
                            title="Sv sondeur %d kHz",
                        )
                        fig.savefig(filelistLabels[np.where(filelist_indice==ind)[0][0]][:-25]+'png/Figure_Sv_corr'+str(ind)+'_'+str(ind)+'.png')
                        plt.close()
                        
                        # Suppression des valeurs n'ayant pas de sens dans la latitude et la longitude
                        index = np.where((Lon[:, 0] == -200) & (Lat[:, 0] == -100))
                    
                        Lat_temp = np.delete(Lat_temp, (index), axis=0)
                        Lon_temp = np.delete(Lon_temp, (index), axis=0)
                    
                        labels = np.delete(labels, (index), axis=0)
                        Sa_moy = np.delete(Sa_moy, (index), axis=0)
                        a = np.where(Sa_moy == 0)
                        Sa_moy[a] = np.NaN

                    
                        Lat_moy = np.true_divide(Lat_temp.sum(1), (Lat_temp != 0).sum(1))
                        Lon_moy = np.true_divide(Lon_temp.sum(1), (Lon_temp != 0).sum(1))
                    
                        # définition des couches surface et fond agrégées pour relier au chalutage
                        surface_limit = 30
                        indice_surface = int(
                            np.floor((surface_limit - Depth[0, 0]) / H_couche)
                        ) 
                        
                        Sa_moy_label_surface = []
                        Sa_moy_label_fond = []
                        Sa_moy_label = []
                        Sa_moy_label_log = []
                        for f in range(len(freq_voulues)):
                            sa_temp=Sa_moy[f,:,:].T
                            for x in range(nombre_de_categories_sv):
                                Sa_label = np.zeros(sa_temp.shape)
                                Sa_label[:, :] = np.nan
                                index_lab = np.where(labels == x)
                                Sa_label[index_lab] = sa_temp[index_lab]
                                Sa_moy_label_surface.append(np.nansum(Sa_label[:, 0:indice_surface], axis=1))
                                Sa_moy_label_fond.append(np.nansum(Sa_label[:, indice_surface + 1 :], axis=1))
                                Sa_moy_label.append(np.nansum(Sa_label, axis=1))
                                Sa_moy_label_log.append(10 * np.log10(np.nanmean(Sa_label, axis=1)))
                        Sa_moy_label=np.array(Sa_moy_label)
                        Sa_moy_label=Sa_moy_label.reshape((len(freq_voulues),nombre_de_categories_sv,-1))
                        Sa_moy_label_log=np.array(Sa_moy_label_log)
                        Sa_moy_label_log=Sa_moy_label_log.reshape((len(freq_voulues),nombre_de_categories_sv,-1))
                        Sa_moy_label_fond=np.array(Sa_moy_label_fond)
                        Sa_moy_label_fond=Sa_moy_label_fond.reshape((len(freq_voulues),nombre_de_categories_sv,-1))
                        Sa_moy_label_surface=np.array(Sa_moy_label_surface)
                        Sa_moy_label_surface=Sa_moy_label_surface.reshape((len(freq_voulues),nombre_de_categories_sv,-1))                        
            
                        #les cercles n'apparaissent que pour les valeurs positives (faire coincider figure et legende en n'affichant que les resultats positifs)
                        sa_log=np.zeros((nbfreqs,nombre_de_categories_sv,Sa_moy_label_log.shape[2]))
                        for f in range(len(freq_voulues)):
                            for x in range(nombre_de_categories_sv):
                                for i in range(Sa_moy_label_log.shape[2]):
                                    if Sa_moy_label_log[f][x][i]>0:
                                        sa_log[f,x,i]=Sa_moy_label_log[f][x][i]+offset
                                    elif Sa_moy_label_log[f][x][i]<=0 and np.isnan(Sa_moy_label_log[f][x][i])==False:
                                        sa_log[f,x,i]=offset
                                     
                        ##################################################################################################
                        #
                        # Création de carte avec la librairie Cartopy : Carte simple , affichage dans les figures.
                        # Sa en valeurs naturelles
                        #
                        ##################################################################################################
                        ##################################################################################################
                        #
                        # Création de carte avec la librairie folium : Carte Openstreetmap, affichage dans un fichier HTML.
                        # Sa en Log
                        #
                        ##################################################################################################


                        plt.rc('legend',fontsize=15)    

                        for f in range(len(freq_voulues)):
                            if Sv_temp.shape[0]<pas:
                                 save_html_Sa=path_html+'portion_corr'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))
                                 save_fig_Sa=path_png+'portion_corr'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))
                                 save_fig_Salog=path_png+'portion_corr_LogSa'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))
                                 os.rename(filelistSa_moy[np.where(filelist_indice==ind)[0][0]],filelistSa_moy[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                                 save_Sa_moy=path_txt+'portion'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))+'Sa_moy.txt'
                            else:
                                 save_html_Sa=path_html+'portion_corr'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))
                                 save_fig_Sa=path_png+'portion_corr'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))
                                 save_fig_Salog=path_png+'portion_corr_LogSa'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))
                                 os.rename(filelistSa_moy[np.where(filelist_indice==ind)[0][0]],filelistSa_moy[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                                 save_Sa_moy=path_txt+'portion'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))+'Sa_moy.txt'
                            md.affichage_carte_france(nombre_de_categories, Lat_moy, Lon_moy, Sa_moy_label[f,:,:],'Sa', extension_zone, save_fig_Sa)
                            md.affichage_carte_france(nombre_de_categories, Lat_moy, Lon_moy, sa_log[f,:,:], extension_zone,'LogSa', save_fig_Salog)
                            md.save_map_html(nombre_de_categories, Lat_moy, Lon_moy, Sa_moy_label_log[f,:,:], save_html_Sa)
                            np.savetxt(save_Sa_moy,Sa_moy[f,:,:])
                            
                            ##################################################################################################
                            #
                            # Export de Sa (en valeurs naturelles et log), Lat et Lon dans un fichier CSV
                            #
                            #################################################################################################
                        
                            for x in range(nombre_de_categories_sv):
                                with open(
                                    "%s/Sa_surface_label_%d_freq_%d.csv"
                                    % (path_csv, x, freq_custom_nearest[f]),
                                    "w",
                                    newline="",
                                ) as csvfile:
                                    writer = csv.writer(csvfile, delimiter=" ")
                                    for i in range(len(Lat_moy)):
                                        if Sa_moy_label_surface[f][x][i] > 0:
                                            writer.writerow(
                                                (
                                                    Time_temp[i].strftime("%d/%m/%Y %H:%M:%S"),
                                                    Lat_moy[i],
                                                    Lon_moy[i],
                                                    Sa_moy_label_surface[f][x][i],
                                                )
                                            )
                                            
                                            
                        
                            for x in range(nombre_de_categories_sv):
                                with open(
                                    "%s/Sa_fond_label_%d_freq_%d.csv"
                                    % (path_csv, x, freq_custom_nearest[f]),
                                    "w",
                                    newline="",
                                ) as csvfile:
                                    writer = csv.writer(csvfile, delimiter=" ")
                                    for i in range(len(Lat_moy)):
                                        if Sa_moy_label_fond[f][x][i] > 0:
                                            writer.writerow(
                                                (
                                                    Time_temp[i].strftime("%d/%m/%Y %H:%M:%S"),
                                                    Lat_moy[i],
                                                    Lon_moy[i],
                                                    Sa_moy_label_fond[f][x][i],
                                                )
                                            )
                                            
                        
                            for x in range(nombre_de_categories_sv):
                                with open(
                                    "%s/Sa_label_%d_freq_%d.csv" % (path_csv, x, freq_custom_nearest[f]),
                                    "w",
                                    newline="",
                                ) as csvfile:
                                    writer = csv.writer(csvfile, delimiter=" ")
                                    for i in range(len(Lat_moy)):
                                        if Sa_moy_label[f][x][i] > 0:
                                            writer.writerow(
                                                (
                                                    Time_temp[i].strftime("%d/%m/%Y %H:%M:%S"),
                                                    Lat_moy[i],
                                                    Lon_moy[i],
                                                    Sa_moy_label[f][x][i],
                                                )
                                            )                       

                        os.rename(filelistLat_moy[np.where(filelist_indice==ind)[0][0]],filelistLat_moy[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                        np.savetxt(filelistLat_moy[np.where(filelist_indice==ind)[0][0]],Lat_moy)
                        os.rename(filelistLon_moy[np.where(filelist_indice==ind)[0][0]],filelistLon_moy[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                        np.savetxt(filelistLon_moy[np.where(filelist_indice==ind)[0][0]],Lon_moy)
                        
                    else:
                        pass
                else:
                    print('no outliers found for cluster number '+str(i)+', no corrections needed')
                    save_corr=input('do you still want to apply some corrections on cluster number '+str(i)+' (y/n) ? \n')
                    if save_corr=='y':
                        Sv_temp_bis=np.empty(Sv_temp.shape)
                        Sv_temp_bis[:,:,:]=Sv_temp[:,:,:]
                        H_couche=(Depth[0, 1] - Depth[0, 0])
                        temp=ma.masked_object(Sv_temp,-10000000.0)
                        Sa_temp=10**(temp/10)*H_couche*1852**2*4*np.pi
                        Lat_temp_bis=np.empty(Lat_t.shape)
                        Lat_temp_bis[:,:,:]=Lat_t[:,:,:]
                        Lon_temp_bis=np.empty(Lon_t.shape)
                        Lon_temp_bis[:,:,:]=Lon_t[:,:,:]
                        Time_temp_bis=np.empty(Time_t.shape)
                        Time_temp_bis[:,:,:]=Time_t[:,:]
                        Sa_temp_bis=np.empty(Sa_temp.shape)
                        Sa_temp_bis[:,:,:]=Sa_temp[:,:,:]
                        for ind_i in np.where(labels_copy==i)[0]:
                            for ind_j in np.where(labels_copy==i)[1]:
                                Sv_temp_bis[ind_i,ind_j,:]=-9999999.9
                                Sa_temp_bis[ind_i,ind_j,:]=-9999999.9
                                Lat_temp_bis[ind_i,ind_j,:]=-9999999.9
                                Lon_temp_bis[ind_i,ind_j,:]=-9999999.9
                                Time_temp_bis[ind_i,ind_j]=-9999999.9
                        
                        os.rename(filelistCluster[np.where(filelist_indice==ind)[0][0]],filelistCluster[np.where(filelist_indice==ind)[0][0]][:-7]+'_old'+str(i)+'.pickle')
                        filename_clustering_EK80_corr=filelistCluster[np.where(filelist_indice==ind)[0][0]]          
                        classif.clustering_netcdf(
                        Sv_temp_bis,
                        freq_MFR,
                        nombre_de_categories_sv,
                        Time_temp_bis,
                        Depth,
                        Lat_temp_bis,
                        Lon_temp_bis,
                        nb_transduc,
                        nom_numero_radiales,
                        methode_clustering,
                        nombre_composantes,
                        nombre_voisins,
                        save=save_data,
                        filename=filename_clustering_EK80_corr,
                        )
                
                        with open(filename_clustering_EK80_corr, "rb") as f_corr:
                            (
                                kmeans_corr,
                                labels_corr,
                                Q1_corr,
                                Q3_corr,
                                med_corr,
                                nombre_de_categories,
                                Sv_temp_bis,
                                Time_temp_bis,
                                freq_MFR,
                                Depth,
                                Lat_temp_bis,
                                Lon_temp_bis,
                                nb_transduc,
                                nom_numero_radiales,
                                methode_clustering,
                            ) = pickle.load(f_corr)                      
        
                
                        #on affiche les résultats de la classification corrigée
                        # # Affichage de la classification et de la réponse fréquentielle par clusters 
                        x=np.arange(0,len(Sv_temp_bis),1)
                        y=np.arange(-Depth[1,0],-Depth[1,(len(Depth[1])-1)]-1,-1)
                        fig,ax2 = plt.subplots(figsize=(30, 15))
                        #image de classification
                        plt.subplot(211)
                        plt.xlabel('N*ESU')
                        plt.ylabel('profondeur (en m)')
                        plt.title('Portion corrigée des possibles outliers')
                        plt.pcolormesh(x,y,np.transpose(labels_corr), cmap=cmap)
                        plt.grid()
                        plt.colorbar()
                        plt.show()
                        
                        #réponse en fréquence
                        plt.subplot(212)
                        plt.xlabel('Freq')
                        plt.ylabel('Sv dB')
                        plt.title('Reponse en frequence par cluster')
                        for j in range(nombre_de_categories_sv):
                            plt.plot(freq_MFR[:indice_freq_max],Q1_corr[j],'-.',color=couleurs[j])
                            plt.plot(freq_MFR[:indice_freq_max],Q3_corr[j],'-.',color=couleurs[j])
                            plt.plot(freq_MFR[:indice_freq_max],med_corr[j],'o-',color=couleurs[j])
                        plt.grid()
                        plt.pause(interval)
                        plt.show()
                        plt.close()
                
                        #l'utilisateur décide si ces corrections doivent être conservées et appliquées à tous les fichiers
                        #les anciens fichiers sont conservés mais renommés _old.extension
                        save_corr=input("do you want to apply corrections ? \n")
                        if save_corr=='y':
                            os.rename(filelistLabels[np.where(filelist_indice==ind)[0][0]],filelistLabels[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                            np.savetxt(filelistLabels[np.where(filelist_indice==ind)[0][0]],labels_corr)
                            os.rename(filelistQ1_[np.where(filelist_indice==ind)[0][0]],filelistQ1_[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                            np.savetxt(filelistQ1_[np.where(filelist_indice==ind)[0][0]],Q1_corr)
                            os.rename(filelistQ3_[np.where(filelist_indice==ind)[0][0]],filelistQ3_[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                            np.savetxt(filelistQ3_[np.where(filelist_indice==ind)[0][0]],Q3_corr)
                            os.rename(filelistMed[np.where(filelist_indice==ind)[0][0]],filelistMed[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                            np.savetxt(filelistMed[np.where(filelist_indice==ind)[0][0]],med_corr)   
                            fig.savefig(filelistLabels[np.where(filelist_indice==ind)[0][0]][:-25]+'png/Figure_clustering_corr'+str(ind)+'_'+str(ind)+'.png')
   
                            Sa_affiche = []
                            Sv_affiche = []
                            freqs_affichage = []
                            for f in range(len(freq_voulues)):
                                indice = np.where(freq_MFR == freq_voulues[f])
                                Sv_affiche.append(np.transpose(np.squeeze(Sv_temp_bis[:, :, indice])))
                                if methode_EI=='pickle':
                                    Sa_moy = np.squeeze(Sa_temp_bis[:, :, f])
                                else:
                                    Sa_affiche.append(np.transpose(np.squeeze(Sa_temp_bis[:, :, indice])))
                                    Lat_temp=Lat_temp_bis[:,:,f]
                                    Lon_temp=Lon_temp_bis[:,:,f]
                                    Time_temp=Time_temp_bis[:,f]
                                Time_temp=au.hac_read_day_time(Time_temp)
                                freqs_affichage.append((freq_voulues[f]) / 1000)
                            Sv_affiche = np.array(Sv_affiche)
                            Sa_affiche = np.array(Sa_affiche)
                            fig, ax3 = plt.subplots(figsize=(24, 35))
                            dr.DrawMultiEchoGram(
                                Sv_affiche,
                                -Depth,
                                len(freq_voulues),
                                freqs=freqs_affichage,
                                title="Sv sondeur %d kHz",
                            )
                            fig.savefig(filelistLabels[np.where(filelist_indice==ind)[0][0]][:-25]+'png/Figure_Sv_corr'+str(ind)+'_'+str(ind)+'.png')
                            plt.close()
                        
                            # Suppression des valeurs n'ayant pas de sens dans la latitude et la longitude
                            index = np.where((Lon[:, 0] == -200) & (Lat[:, 0] == -100))
                        
                            Lat_temp = np.delete(Lat_temp, (index), axis=0)
                            Lon_temp = np.delete(Lon_temp, (index), axis=0)
                        
                            labels = np.delete(labels, (index), axis=0)
                            Sa_moy = np.delete(Sa_moy, (index), axis=0)
                            a = np.where(Sa_moy == 0)
                            Sa_moy[a] = np.NaN

                        
                            Lat_moy = np.true_divide(Lat_temp.sum(1), (Lat_temp != 0).sum(1))
                            Lon_moy = np.true_divide(Lon_temp.sum(1), (Lon_temp != 0).sum(1))
                        
                            # définition des couches surface et fond agrégées pour relier au chalutage
                            surface_limit = 30
                            indice_surface = int(
                                np.floor((surface_limit - Depth[0, 0]) / H_couche)
                            ) 
                            
                            Sa_moy_label_surface = []
                            Sa_moy_label_fond = []
                            Sa_moy_label = []
                            Sa_moy_label_log = []
                            for f in range(len(freq_voulues)):
                                sa_temp=Sa_affiche[f,:,:].T
                                for x in range(nombre_de_categories_sv):
                                    Sa_label = np.zeros(sa_temp.shape)
                                    Sa_label[:, :] = np.nan
                                    index_lab = np.where(labels == x)
                                    Sa_label[index_lab] = sa_temp[index_lab]
                                    Sa_moy_label_surface.append(np.nansum(Sa_label[:, 0:indice_surface], axis=1))
                                    Sa_moy_label_fond.append(np.nansum(Sa_label[:, indice_surface + 1 :], axis=1))
                                    Sa_moy_label.append(np.nansum(Sa_label, axis=1))
                                    Sa_moy_label_log.append(10 * np.log10(np.nanmean(Sa_label, axis=1)))
                            Sa_moy_label=np.array(Sa_moy_label)
                            Sa_moy_label=Sa_moy_label.reshape((len(freq_voulues),nombre_de_categories_sv,-1))
                            Sa_moy_label_log=np.array(Sa_moy_label_log)
                            Sa_moy_label_log=Sa_moy_label_log.reshape((len(freq_voulues),nombre_de_categories_sv,-1))
                            Sa_moy_label_fond=np.array(Sa_moy_label_fond)
                            Sa_moy_label_fond=Sa_moy_label_fond.reshape((len(freq_voulues),nombre_de_categories_sv,-1))
                            Sa_moy_label_surface=np.array(Sa_moy_label_surface)
                            Sa_moy_label_surface=Sa_moy_label_surface.reshape((len(freq_voulues),nombre_de_categories_sv,-1))                        
                
                            #les cercles n'apparaissent que pour les valeurs positives (faire coincider figure et legende en n'affichant que les resultats positifs)
                            sa_log=np.zeros((nbfreqs,nombre_de_categories_sv,Sa_moy_label_log.shape[2]))
                            for f in range(len(freq_voulues)):
                                for x in range(nombre_de_categories_sv):
                                    for i in range(Sa_moy_label_log.shape[2]):
                                        if Sa_moy_label_log[f][x][i]>0:
                                            sa_log[f,x,i]=Sa_moy_label_log[f][x][i]+offset
                                        elif Sa_moy_label_log[f][x][i]<=0 and np.isnan(Sa_moy_label_log[f][x][i])==False:
                                            sa_log[f,x,i]=offset
                                            
                                            
                                            
                            ##################################################################################################
                            #
                            # Création de carte avec la librairie Cartopy : Carte simple , affichage dans les figures.
                            # Sa en valeurs naturelles
                            #
                            ##################################################################################################
                            ##################################################################################################
                            #
                            # Création de carte avec la librairie folium : Carte Openstreetmap, affichage dans un fichier HTML.
                            # Sa en Log
                            #
                            ##################################################################################################


                            plt.rc('legend',fontsize=15)    

                            for f in range(len(freq_voulues)):
                                if Sv_temp.shape[0]<pas:
                                     save_html_Sa=path_html+'portion_corr'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))
                                     save_fig_Sa=path_png+'portion_corr'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))
                                     save_fig_Salog=path_png+'portion_corr_LogSa'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))
                                     os.rename(filelistSa_moy[np.where(filelist_indice==ind)[0][0]],filelistSa_moy[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                                     save_Sa_moy=path_txt+'portion'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))+'Sa_moy.txt'
                                else:
                                     save_html_Sa=path_html+'portion_corr'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))
                                     save_fig_Sa=path_png+'portion_corr'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))
                                     save_fig_Salog=path_png+'portion_corr_LogSa'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))
                                     os.rename(filelistSa_moy[np.where(filelist_indice==ind)[0][0]],filelistSa_moy[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                                     save_Sa_moy=path_txt+'portion'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))+'Sa_moy.txt'
                                md.affichage_carte_france(nombre_de_categories, Lat_moy, Lon_moy, Sa_moy_label[f,:,:],'Sa', extension_zone, save_fig_Sa)
                                md.affichage_carte_france(nombre_de_categories, Lat_moy, Lon_moy, sa_log[f,:,:],'LogSa', extension_zone, save_fig_Salog)
                                md.save_map_html(nombre_de_categories, Lat_moy, Lon_moy, Sa_moy_label_log[f,:,:], save_html_Sa)
                                np.savetxt(save_Sa_moy,Sa_affiche[f,:,:])
                                
                                ##################################################################################################
                                #
                                # Export de Sa (en valeurs naturelles et log), Lat et Lon dans un fichier CSV
                                #
                                #################################################################################################
                            
                                for x in range(nombre_de_categories_sv):
                                    with open(
                                        "%s/Sa_surface_label_%d_freq_%d.csv"
                                        % (path_csv, x, freq_custom_nearest[f]),
                                        "w",
                                        newline="",
                                    ) as csvfile:
                                        writer = csv.writer(csvfile, delimiter=" ")
                                        for i in range(len(Lat_moy)):
                                            if Sa_moy_label_surface[f][x][i] > 0:
                                                writer.writerow(
                                                    (
                                                        Time_temp[i].strftime("%d/%m/%Y %H:%M:%S"),
                                                        Lat_moy[i],
                                                        Lon_moy[i],
                                                        Sa_moy_label_surface[f][x][i],
                                                    )
                                                )
                                                
                                                
                            
                                for x in range(nombre_de_categories_sv):
                                    with open(
                                        "%s/Sa_fond_label_%d_freq_%d.csv"
                                        % (path_csv, x, freq_custom_nearest[f]),
                                        "w",
                                        newline="",
                                    ) as csvfile:
                                        writer = csv.writer(csvfile, delimiter=" ")
                                        for i in range(len(Lat_moy)):
                                            if Sa_moy_label_fond[f][x][i] > 0:
                                                writer.writerow(
                                                    (
                                                        Time_temp[i].strftime("%d/%m/%Y %H:%M:%S"),
                                                        Lat_moy[i],
                                                        Lon_moy[i],
                                                        Sa_moy_label_fond[f][x][i],
                                                    )
                                                )
                                                
                            
                                for x in range(nombre_de_categories_sv):
                                    with open(
                                        "%s/Sa_label_%d_freq_%d.csv" % (path_csv, x, freq_custom_nearest[f]),
                                        "w",
                                        newline="",
                                    ) as csvfile:
                                        writer = csv.writer(csvfile, delimiter=" ")
                                        for i in range(len(Lat_moy)):
                                            if Sa_moy_label[f][x][i] > 0:
                                                writer.writerow(
                                                    (
                                                        Time_temp[i].strftime("%d/%m/%Y %H:%M:%S"),
                                                        Lat_moy[i],
                                                        Lon_moy[i],
                                                        Sa_moy_label[f][x][i],
                                                    )
                                                )                       

                            os.rename(filelistLat_moy[np.where(filelist_indice==ind)[0][0]],filelistLat_moy[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                            np.savetxt(filelistLat_moy[np.where(filelist_indice==ind)[0][0]],Lat_moy)
                            os.rename(filelistLon_moy[np.where(filelist_indice==ind)[0][0]],filelistLon_moy[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                            np.savetxt(filelistLon_moy[np.where(filelist_indice==ind)[0][0]],Lon_moy)

                    else:
                        Sv_temp_bis=Sv_temp
        else:
            pass
    else:
        pass
    


################################################################################
#RECOMBINAISON DES RESULTATS APRES CORRECTION
################################################################################
"""
Recombinaison des portions de la campagne après correction et classification 
de la donnée par regroupement des RF semblables en clusters  
"""
    
if save_corr=='y':
    
    for root, subFolders, files in os.walk(fpath):
        for file in files:
            if methode_EI=='pickle':
                if file.endswith("saved_clustering_for_EK80_all.pickle"):
                    with open(root+'/'+file, "rb") as f:
                        (
                            freq_MFR,
                            freqs_moy_transducteur,
                            Sv,
                            Sv_moy_transducteur_x,
                            Sa,
                            Depth,
                            Lat,
                            Lon,
                            Time,
                            nb_transduc,
                            tableau_radiales,
                            nom_numero_radiales,
                        ) = pickle.load(f) 
                    break
            else:
                if file.endswith("1.xsf.nc"):
                    dataset=nc.Dataset(root+'/'+file)
                    Sv=dataset.groups['Sonar'].groups['Grid_group_1'].variables['integrated_backscatter'][:]
                    freq_MFR=dataset.groups['Sonar'].groups['Grid_group_1'].variables['frequency'][:]
                    freq_MFR=np.sort(freq_MFR)
                    Time=dataset.groups['Sonar'].groups['Grid_group_1'].variables['cell_ping_time'][:]
                    Depth=dataset.groups['Sonar'].groups['Grid_group_1'].variables['cell_depth'][:]
                    Depth=Depth.T
                    Lat=dataset.groups['Sonar'].groups['Grid_group_1'].variables['cell_latitude'][:]
                    Lon=dataset.groups['Sonar'].groups['Grid_group_1'].variables['cell_longitude'][:]
                    break
                break
            break
        break
    
    dirlistQ1_ = []
    dirlistQ3_ = []
    dirlistMed = []
    dirlistLabels=[]
    dirlistSa_moy=[]
    dirlistLat_moy=[]
    dirlistLon_moy=[]
    for root, subFolders, files in os.walk(fpath):
        print(root,subFolders,files)
        for file in files:
            print(file)
            if file.endswith("Q1.txt"):
                dirlistQ1_.append(os.path.join(root, file))
            elif file.endswith("Q3.txt"):
                dirlistQ3_.append(os.path.join(root, file))
            elif file.endswith("Med.txt"):
                dirlistMed.append(os.path.join(root, file))
            elif file.endswith("Labels.txt"):
                dirlistLabels.append(os.path.join(root, file))
            elif file.endswith("Sa_moy.txt"):
                dirlistSa_moy.append(os.path.join(root, file))
            elif file.endswith("Lat_moy.txt"):
                dirlistLat_moy.append(os.path.join(root, file))
            elif file.endswith("Lon_moy.txt"):
                dirlistLon_moy.append(os.path.join(root, file))
                
                
    filelistQ1_ = sorted_alphanumeric(dirlistQ1_)
    filelistQ3_ = sorted_alphanumeric(dirlistQ3_)
    filelistMed = sorted_alphanumeric(dirlistMed)
    filelistLabels = sorted_alphanumeric(dirlistLabels)
    filelistSa_moy = sorted_alphanumeric(dirlistSa_moy)
    filelistLat_moy = sorted_alphanumeric(dirlistLat_moy)
    filelistLon_moy = sorted_alphanumeric(dirlistLon_moy)
    
    Q1_tot=np.loadtxt(filelistQ1_[0])
    Q3_tot=np.loadtxt(filelistQ3_[0])
    med_tot=np.loadtxt(filelistMed[0])
    labels_tot=np.loadtxt(filelistLabels[0])
    if labels_tot.shape[1]<indice_profondeur_max:
        blanc = np.zeros((len(labels_tot),indice_profondeur_max-labels_tot.shape[1]))
        blanc.fill(np.nan)
        labels_tot=np.hstack((labels_tot,blanc))
    for i in range(1,len(filelistQ1_)):
        Q1_tot=np.vstack((Q1_tot,np.loadtxt(filelistQ1_[i])))
        Q3_tot=np.vstack((Q3_tot,np.loadtxt(filelistQ3_[i])))
        med_tot=np.vstack((med_tot,np.loadtxt(filelistMed[i])))    
        labels=np.loadtxt(filelistLabels[i])
        if labels.shape[1]<indice_profondeur_max:
            blanc = np.zeros((len(labels),indice_profondeur_max-labels.shape[1]))
            blanc.fill(np.nan)
            labels=np.hstack((labels,blanc))
        labels_tot=np.vstack((labels_tot,labels))
        
    Lat_tot=[]
    Lon_tot=[]
    for i in range(len(filelistLat_moy)):
        Lat_tot=np.append(Lat_tot,np.loadtxt(filelistLat_moy[i]))
        Lon_tot=np.append(Lon_tot,np.loadtxt(filelistLon_moy[i]))
    
    
    Sa_moy_tot=[]
    for i in range(len(filelistSa_moy)):
        Sa=np.loadtxt(filelistSa_moy[i])
        Sa_moy_tot.append(Sa)
        
    if nombre_composantes>len(freq_MFR):
        raise ValueError('Nombre de composantes trop élevé par rapport au nombre de fréquences')
    elif nombre_voisins<nombre_composantes:
        raise ValueError('Nombre de voisins trop faible par rapport au nombre de composantes')
    else:
        lle = LocallyLinearEmbedding(n_components=nombre_composantes, n_neighbors = nombre_voisins,method = 'modified', n_jobs = 4,  random_state=0)
        lle.fit(med_tot)
        med_lle = lle.transform(med_tot)
    
    plt.figure()
    sc.dendrogram(sc.linkage(med_lle,method='ward'))
    
    plt.savefig(fpath+'Dendogramme_campagne_totale_corr.png')
    
    
    kmeans_sv=AgglomerativeClustering(n_clusters=nombre_de_categories_sv_campEntiere,affinity='euclidean',linkage='ward').fit(med_lle)
    
         
    couleurs = plt.cm.jet(np.linspace(0,1,nombre_de_categories_sv_campEntiere))
    cmap = colors.ListedColormap(couleurs) 
    
    plt.figure(figsize=(30,15))
    for x in np.unique(kmeans_sv.labels_):
        indice = np.where(kmeans_sv.labels_ == x)
        plt.plot(freq_MFR[:indice_freq_max], np.mean( Q1_tot[indice], axis=0),'-.',color=couleurs[x])
        plt.plot(freq_MFR[:indice_freq_max], np.mean( Q3_tot[indice], axis=0),'-.',color=couleurs[x])
        plt.plot(freq_MFR[:indice_freq_max], np.mean( med_tot[indice], axis=0),'o-',color=couleurs[x],label="Classe"+str(x))
    plt.legend()
    
    plt.savefig(fpath+'ClustersRF_campagne_totale_corr.png')
    
    kmeans_sv.labels_new=kmeans_sv.labels_+nombre_de_categories_sv+1
    labels_tot_new=np.where(labels_tot==nombre_de_categories_sv,np.max(kmeans_sv.labels_new)+1,labels_tot)
    
    unites= np.loadtxt(fpath+'UnitesDecoupageCampagne.txt',dtype='int')
    len_Sv0_ent_tot_final=np.cumsum(unites)  
               
    j=0
    for i in range(nombre_de_categories_sv):
        labels_tot_new[:len_Sv0_ent_tot_final[j],:]=np.where(labels_tot_new[:len_Sv0_ent_tot_final[j],:]==i,kmeans_sv.labels_new[i+j],labels_tot_new[:len_Sv0_ent_tot_final[j],:])       
    
    kmeans_sv.labels_new_shape=kmeans_sv.labels_new.reshape((len(len_Sv0_ent_tot_final),nombre_de_categories_sv))     
    for j in range(1,len(len_Sv0_ent_tot_final)):
        for i in range(nombre_de_categories_sv):
            labels_tot_new[len_Sv0_ent_tot_final[j-1]:len_Sv0_ent_tot_final[j],:]=np.where(labels_tot_new[len_Sv0_ent_tot_final[j-1]:len_Sv0_ent_tot_final[j],:]==i,kmeans_sv.labels_new_shape[j,i],labels_tot_new[len_Sv0_ent_tot_final[j-1]:len_Sv0_ent_tot_final[j],:])       
             
    labels_tot_new=labels_tot_new-(nombre_de_categories_sv+1)
          
    couleurs = plt.cm.jet(np.linspace(0,1,nombre_de_categories_sv_campEntiere))
    cmap = colors.ListedColormap(couleurs) 
    
    x=np.arange(0,len_Sv0_ent_tot_final[-1],1)
    y=np.arange(-Depth[1,0],-Depth[1,(len(Depth[1])-1)]-1,-1)
    
    plt.figure(figsize=(30,15))
    plt.xlabel('N*ESU')
    plt.ylabel('profondeur (en m)')
    plt.title(' ')
    plt.pcolormesh(x,y,np.transpose(labels_tot_new), cmap=cmap)
    plt.grid()
    plt.colorbar()
    plt.show()
    
    plt.savefig(fpath+'Echogramme_clustering_campagne_totale_corr.png')
    
    
    Sa_moy_label_tot = []
    Sa_moy_label_log_tot = []
    u=0
    for f in range(nb_transduc,len(Sa_moy_tot)+1,nb_transduc):
        #print('boucle sur f:',f)
        sa_temp=np.array(Sa_moy_tot[f-nb_transduc:f])
        #print(sa_temp.shape)
        if np.cumsum(unites)[u]<=pas:
            unit0=0
            unit1=unites[0]
        else:
            unit0=np.cumsum(unites)[u-1]
            unit1=np.cumsum(unites)[u]
        # print('unit0',unit0)
        # print('unit1',unit1)
        for i in range(nb_transduc):
            #print('boucle sur i:',i)
            sa_t=sa_temp[i,:,:].T
            #print(sa_t.shape)
            for x in range(nombre_de_categories_sv_campEntiere):
                #print('boucle sur x:',x)
                Sa_label = np.zeros(sa_t.shape)
                Sa_label[:, :] = np.nan
                #print(Sa_label.shape)
                index_lab = np.where(labels_tot_new[unit0:unit1] == x)
                Sa_label[index_lab] = sa_t[index_lab]
                Sa_moy_label_tot.append(np.nansum(Sa_label, axis=1))
                Sa_moy_label_log_tot.append(10 * np.log10(np.nanmean(Sa_label, axis=1)))
        u+=1
         
          
    reindexage=np.arange(0,len(Sa_moy_label_tot),nb_transduc*nombre_de_categories_sv_campEntiere)
    Sa_moy_label_tot_final=Sa_moy_label_tot[0]
    Sa_moy_label_log_tot_final=Sa_moy_label_log_tot[0]
    for j in range(nb_transduc*nombre_de_categories_sv_campEntiere):
        for i in range(len(reindexage)):
            #print(reindexage[i])
            Sa_moy_label_tot_final=np.hstack((Sa_moy_label_tot_final,Sa_moy_label_tot[reindexage[i]]))
            Sa_moy_label_log_tot_final=np.hstack((Sa_moy_label_log_tot_final,Sa_moy_label_log_tot[reindexage[i]]))
        reindexage+=1
    Sa_moy_label_tot_final=Sa_moy_label_tot_final[unites[0]:]
    Sa_moy_label_tot_final=Sa_moy_label_tot_final.reshape((len(freq_voulues),nombre_de_categories_sv_campEntiere,-1))  
    Sa_moy_label_log_tot_final=Sa_moy_label_log_tot_final[unites[0]:]
    Sa_moy_label_log_tot_final=Sa_moy_label_log_tot_final.reshape((len(freq_voulues),nombre_de_categories_sv_campEntiere,-1))  
    
    #les cercles n'apparaissent que pour les valeurs positives (faire coincider figure et legende en n'affichant que les resultats positifs)
    sa_log=np.zeros((nbfreqs,nombre_de_categories_sv_campEntiere,Sa_moy_label_log_tot_final.shape[2]))
    for f in range(len(freq_voulues)):
        for x in range(nombre_de_categories_sv_campEntiere):
            for i in range(Sa_moy_label_log_tot_final.shape[2]):
                if Sa_moy_label_log_tot_final[f][x][i]>0:
                    sa_log[f,x,i]=Sa_moy_label_log_tot_final[f][x][i]+offset
                elif Sa_moy_label_log_tot_final[f][x][i]<=0 and np.isnan(Sa_moy_label_log_tot_final[f][x][i])==False:
                    sa_log[f,x,i]=offset
                    
                    
                 
    plt.rc('legend',fontsize=15)    
    
    ##################################################################################################
    #
    # Création de carte avec la librairie Cartopy : Carte simple , affichage dans les figures.
    # Sa en valeurs naturelles
    #
    ##################################################################################################
    ##################################################################################################
    #
    # Création de carte avec la librairie folium : Carte Openstreetmap, affichage dans un fichier HTML.
    # Sa en Log
    #
    ##################################################################################################
    
    for f in range(len(freq_voulues)):   
        md.affichage_carte_france(
            nombre_de_categories_sv_campEntiere, Lat_tot, Lon_tot, sa_log[f,:,:], extension_zone,'LogSa', fpath+'LogSa_corr'+str(np.round(freq_voulues[f]/1000))
        )
            
        md.affichage_carte_france(
            nombre_de_categories_sv_campEntiere, Lat_tot, Lon_tot, Sa_moy_label_tot_final[f,:,:], extension_zone,'Sa', fpath+'corr'+str(np.round(freq_voulues[f]/1000))
        )
    
        md.save_map_html(
            nombre_de_categories_sv_campEntiere, Lat_tot, Lon_tot, Sa_moy_label_log_tot_final[f,:,:], fpath+'corr'+str(np.round(freq_voulues[f]/1000))
        )
    
    nb_radiales=[]
    compt=0
    heured=np.array([])
    heuref=np.array([])
    for u in range(len(unites)):
        if u==0:
            hd=heuredebut[compt]
            hf=heurefin[compt]
            
            heured=np.append(heured,hd)
            heuref=np.append(heuref,hf)
        else:
            if unites[u]==pas and unites[u-1]!=1000:
                hd=heuredebut[compt+1]
                hf=heurefin[compt+1]
                if compt<=len(heuredebut)-1:
                    compt+=1
                else:
                    break
                heured=np.append(heured,hd)
                heuref=np.append(heuref,hf)
            elif unites[u]!=pas and unites[u-1]!=pas:
                hd=heuredebut[compt+1]
                hf=heurefin[compt+1]
                if compt<=len(heuredebut)-1:
                    compt+=1
                else:
                    break
            
                heured=np.append(heured,hd)
                heuref=np.append(heuref,hf)
            else:
                hd=heuredebut[compt]
                hf=heurefin[compt]
    
                heured=np.append(heured,hd)
                heuref=np.append(heuref,hf)
                
                
    Sa_jour=np.array([])
    Sa_nuit=np.array([])
    Sa_jour_log=np.array([])
    Sa_nuit_log=np.array([])
    Lat_jour=np.array([])
    Lat_nuit=np.array([])
    Lon_jour=np.array([])
    Lon_nuit=np.array([])
    for f in range(len(freq_voulues)):
        for x in range(nombre_de_categories_sv_campEntiere):
            for u in range(len(unites)):
                if heured[u]>'04:00:00' and heuref[u]<'21:00:00': #Temps UTC+2
                    if u==0:
                        Sa_jour=np.append(Sa_jour,Sa_moy_label_tot_final[f][x][u:np.cumsum(unites[u])[-1]])
                        Sa_jour_log=np.append(Sa_jour_log,Sa_moy_label_log_tot_final[f][x][u:np.cumsum(unites[u])[-1]])
                        Lat_jour=np.append(Lat_jour,Lat_tot[u:np.cumsum(unites[u])[-1]])
                        Lon_jour=np.append(Lon_jour,Lon_tot[u:np.cumsum(unites[u])[-1]])
                    else:
                        Sa_jour=np.append(Sa_jour,Sa_moy_label_tot_final[f][x][np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                        Sa_jour_log=np.append(Sa_jour_log,Sa_moy_label_log_tot_final[f][x][np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                        Lat_jour=np.append(Lat_jour,Lat_tot[np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                        Lon_jour=np.append(Lon_jour,Lon_tot[np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                else:
                    if u==0:
                        Sa_nuit=np.append(Sa_nuit,Sa_moy_label_tot_final[f][x][u:np.cumsum(unites[u])[-1]])
                        Sa_nuit_log=np.append(Sa_nuit_log,Sa_moy_label_log_tot_final[f][x][u:np.cumsum(unites[u])[-1]])
                        Lat_nuit=np.append(Lat_nuit,Lat_tot[u:np.cumsum(unites[u])[-1]])
                        Lon_nuit=np.append(Lon_nuit,Lon_tot[u:np.cumsum(unites[u])[-1]])
                    else:
                        Sa_nuit=np.append(Sa_nuit,Sa_moy_label_tot_final[f][x][np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                        Sa_nuit_log=np.append(Sa_nuit_log,Sa_moy_label_log_tot_final[f][x][np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                        Lat_nuit=np.append(Lat_nuit,Lat_tot[np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                        Lon_nuit=np.append(Lon_nuit,Lon_tot[np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])           
    Sa_jour=Sa_jour.reshape((nbfreqs,nombre_de_categories_sv_campEntiere,-1))
    Sa_nuit=Sa_nuit.reshape((nbfreqs,nombre_de_categories_sv_campEntiere,-1))
    Sa_jour_log=Sa_jour_log.reshape((nbfreqs,nombre_de_categories_sv_campEntiere,-1))
    Sa_nuit_log=Sa_nuit_log.reshape((nbfreqs,nombre_de_categories_sv_campEntiere,-1))
    ind_lat_j=int(Lat_jour.shape[0]/(nombre_de_categories_sv_campEntiere*nbfreqs))
    ind_lat_n=int(Lat_nuit.shape[0]/(nombre_de_categories_sv_campEntiere*nbfreqs))
    ind_lon_j=int(Lon_jour.shape[0]/(nombre_de_categories_sv_campEntiere*nbfreqs))
    ind_lon_n=int(Lon_nuit.shape[0]/(nombre_de_categories_sv_campEntiere*nbfreqs))
    Lat_jour=Lat_jour[:ind_lat_j]
    Lat_nuit=Lat_nuit[:ind_lat_n]
    Lon_jour=Lon_jour[:ind_lon_j]
    Lon_nuit=Lon_nuit[:ind_lon_n]
    
    #les cercles n'apparaissent que pour les valeurs positives (faire coincider figure et legende en n'affichant que les resultats positifs)
    sa_log_jour=np.zeros((nbfreqs,nombre_de_categories_sv_campEntiere,Sa_jour_log.shape[2]))
    for f in range(len(freq_voulues)):
        for x in range(nombre_de_categories_sv_campEntiere):
            for i in range(Sa_jour_log.shape[2]):
                if Sa_jour_log[f][x][i]>0:
                    sa_log_jour[f,x,i]=Sa_jour_log[f][x][i]+offset
                elif Sa_jour_log[f][x][i]<=0 and np.isnan(Sa_jour_log[f][x][i])==False:
                    sa_log_jour[f,x,i]=offset
                    
    #les cercles n'apparaissent que pour les valeurs positives (faire coincider figure et legende en n'affichant que les resultats positifs)
    sa_log_nuit=np.zeros((nbfreqs,nombre_de_categories_sv_campEntiere,Sa_nuit_log.shape[2]))
    for f in range(len(freq_voulues)):
        for x in range(nombre_de_categories_sv_campEntiere):
            for i in range(Sa_nuit_log.shape[2]):
                if Sa_nuit_log[f][x][i]>0:
                    sa_log_nuit[f,x,i]=Sa_nuit_log[f][x][i]+offset
                elif Sa_nuit_log[f][x][i]<=0 and np.isnan(Sa_nuit_log[f][x][i])==False:
                    sa_log_nuit[f,x,i]=offset
                    
    ##################################################################################################
    #
    # Création de carte avec la librairie Cartopy : Carte simple , affichage dans les figures.
    # Sa en valeurs naturelles
    #
    ##################################################################################################
    ##################################################################################################
    #
    # Création de carte avec la librairie folium : Carte Openstreetmap, affichage dans un fichier HTML.
    # Sa en Log
    #
    ##################################################################################################
    
    for f in range(len(freq_voulues)):
        #Affichage des cartes de Sa et de log(Sa) par label et par période jour/nuit
        md.affichage_carte_france(
            nombre_de_categories_sv_campEntiere, Lat_jour, Lon_jour, Sa_jour[f,:,:], extension_zone,'Sa', fpath+str(np.round(freq_voulues[f]/1000))+'jour_corr'
        )
    
        md.save_map_html(
            nombre_de_categories_sv_campEntiere, Lat_jour, Lon_jour, Sa_jour_log[f,:,:], fpath+str(np.round(freq_voulues[f]/1000))+'jour_corr'
        )
        
        md.affichage_carte_france(
            nombre_de_categories_sv_campEntiere,Lat_jour, Lon_jour, sa_log_jour[f,:,:],'LogSa', extension_zone, fpath+'LogSa_jour_corr'+str(np.round(freq_voulues[f]/1000))
        )
    
        if Sa_nuit.shape[-1]!=0:
            md.affichage_carte_france(
                nombre_de_categories_sv_campEntiere, Lat_nuit, Lon_nuit, Sa_nuit[f,:,:], extension_zone,'Sa', fpath+str(np.round(freq_voulues[f]/1000))+'nuit_corr'
            )
        
            md.save_map_html(
                nombre_de_categories_sv_campEntiere, Lat_nuit, Lon_nuit, Sa_nuit_log[f,:,:], fpath+str(np.round(freq_voulues[f]/1000))+'nuit_corr'
            )
            
            md.affichage_carte_france(
            nombre_de_categories_sv_campEntiere,Lat_nuit, Lon_nuit, sa_log_nuit[f,:,:], extension_zone,'LogSa', fpath+'LogSa_nuit_corr'+str(np.round(freq_voulues[f]/1000))
            )
        else:
            pass
else:
    pass

    
    
    
    
    
    
    
    
    
