# -*- coding: utf-8 -*-

"""
| Script to perform echo-integration and echogram segementation 
| of narrow or broadband fisheries acoustic data in . HAC or sonarnetCDF formats
|
|- Library import
|
|- Paths and options setup
|
|- Metadata import
|
|
|- Echo-integration (EI)
|
|- EI results import. Formatting of data useful for echogram segmentation (classification):
|                  |
|                  |-- total Sv, mean Sv per transducer, Sa, time, 
|                      seabed depth, latitude, longitude, 
|                      frequency, mean frequency per transducer
|
|- Echogram segmentation in acoustic transects sub-units (ESU batches) with Kmeans or LLE+Kmeans
|                  |
|                  |-- saved outputs: total Sv, mean Sv per transducer, Sa, time, 
|                      saebed depth, latitude, longitude, 
|                      frequency, mean frequency per transducer,
|                      cluster labels, median frequency response per cluster
|
| KMeans implementation: https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html
| LLE implementation: https://scikit-learn.org/stable/modules/generated/sklearn.manifold.LocallyLinearEmbedding.html
| https://scikit-learn.org/stable/modules/manifold.html#locally-linear-embedding (§2.2.3 & 2.2.4)
| 
|- Segmentation diagnosis plots: 
|
|- mean Sv per transducer, echogram with clusters labels, median frequency response per cluster
|- Sa per cluster bubble maps in png and html formats
|- Lat, Lon, Sa and Sa_log exported as CSV for further mapping 
| 
| See https://gitlab.ifremer.fr/fleet/movies/pymovies_3d/-/wikis/Echogram-segmentation
| for details

"""
__author__      = "Alicia Maurice", "laurent.berger@ifremer.fr", "mathieu.doray@ifremer.fr"
__copyright__   = "GPL"

#%% 1. Load packages

import pytz

from pymovies_3d.core.echointegration.batch_ei_multi_threshold import (
    batch_ei_multi_threshold,
)
from pymovies_3d.core.echointegration.ei_bind import ei_bind

import os
import numpy as np
import pickle
import datetime
import time
import netCDF4 as nc
import numpy.ma as ma
import scipy

from matplotlib import pyplot as plt
import matplotlib.dates as md
from matplotlib import colors
import matplotlib.patches as mpatches

import math
import numpy.matlib
import pymovies_3d.visualization.drawing as dr
import pymovies_3d.core.log_events.import_casino as ic

import csv
import glob2
import pymovies_3d.core.hac_util.hac_util as au

import pymovies_3d.core.echointegration.ei_survey as ei
import pymovies_3d.core.echointegration.ei_bind as ei_bind
import pymovies_3d.core.echointegration.ei_standardizing as standardizing
import pymovies_3d.core.echointegration.ei_classification as classif
import pymovies_3d.core.util.utils as ut
import pymovies_3d.visualization.map_drawing as md

from sklearn.manifold import LocallyLinearEmbedding
from sklearn.cluster import KMeans
import re

import scipy.cluster.hierarchy as sc
from sklearn.cluster import AgglomerativeClustering

def sorted_alphanumeric(data):
    #https://stackoverflow.com/questions/4813061/non-alphanumeric-list-order-from-os-listdir/48030307#48030307
    """
    Fonction permettant de parcourir les chemins vers les fichiers dans le bon ordre
    La fonction sort() ne marchant pas avec les valeurs alpha-numériques
    Parameters
    ----------
    data : LISTE DE NOMS DE FICHIERS DANS UN ORDRE ALEATOIRE

    Returns
    -------
    LISTE DE NOMS DE FICHIERS TRIEE

    """
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(data, key=alphanum_key)

################################################################################
# %% 2. Set up
################################################################################

# Define paths 
pref='/home/mathieu/'
# Path to survey metadata file:
path_evt=pref+'ownCloud/documents/MOVIES3D/MOVIEStestDataset/Casino_MOVIEStestDataset.txt'
# Path to MOVIES3D Echo-integration (EI) configuration folder:
path_config =pref+'ownCloud/documents/MOVIES3D/configsMOVIES3D/EIlay/FineScaleContinentalShelf/'
thresholds = [-80]             # EI minimum threshold (dB)
path_config = path_config + "/" + str(thresholds[0])
# Path to HAC or netCDF data files:
path_data=pref+'Documents/Data/Acoustique/MoviesTestDataset/PELGAS2013/RUN003/HAC/'
path_hac_survey=path_data+'MonosMulti/'
# Save path
fpath=path_hac_survey+'ClassifResultsHighRes_LLEbatch3000/'
fpath=path_hac_survey+'ClassifResultsHighRes_LLEbatch1500/'
fpath=path_hac_survey+'ClassifResultsHighRes_LLEbatch500/'
# If save path does not exist, create it
if not os.path.exists(fpath):
    os.makedirs(fpath)
survey_name0='MoviesTestDataset' 
   
# EI, modification ou non de l'EI et classification
type_campagne='Default'             # Metadata format, see https://gitlab.ifremer.fr/fleet/movies/pymovies_3d/-/wikis/Echo-integration-metadata-file
echointegration=False               # (re)perform Echo-intergation (EI) ?
save_data = True                    # save EI and classification results?
clustering = True                   # Perform classification on EI results?
methode_EI = 'netcdf'               # EI results format ('netcdf' or 'pickle')
correction = False                  # Correct unwanted frequency responses (FR)?
recombinaison = True                # Recombine results after FR correction?
AllPlots=False                      # Display echograms per clusters and NASC per cluster bubble maps 

# Transects, transducers and frequencies of interest
#ind_decoup=range(0,100,1)
sounder_ident=1             # ID of echosounder (integer) to use for EI and classification
ind_decoup=[0]              # integer list to select the transects to process, based on metadata file. Possible values: [0:len(nameTransect)]
indices_transduc_choisis = np.array([1,2,3,4,5])   #Indices of transducers to be used, in ascending order
indice_freq_max=5           # Max no. of frequencies to be used in classif. Integers list, with max=len(freq_MFR)
nbfreqs = 117               # Max no. of frequencies to display in figures
nb_transduc=5
freq_custom =[18000,38000,70000,120000,200000]     #Frequencies custom names for labelling

# Multifrequency echogram classification
nombre_de_categories_sv=3             # No. clusters per transect batch
nombre_de_categories_sv_campEntiere=3 # No. clusters for the whole survey
pas=500                              # no. of ESUs to process at a time (transect batch size)
methode_clustering="LLE_KMeans"       # Classification method: "LLE_KMeans" or "Kmeans"
if methode_clustering=="LLE_KMeans":
    nombre_composantes=4              # No. of LLE components to use for LLE in batchs
    nombre_voisins=10                 # No. of neighborings EI cells to use in LLE calculations in batches
    nombre_composantes_global=3       # No. of LLE components to use for global LLE to recombine batches
    nombre_voisins_global=4          # No. of neighborings EI cells to use in LLE calculations to recombine batches
else:
    nombre_composantes=0
    nombre_voisins=0

# Maps lat,long range
extension_zone=[-1, -6, 43.5, 48]
offset=2                              # Log(Sa+offset) for Sa bubble maps

################################################################################
# %% Metadata import
################################################################################

if type_campagne=='Default':
    C, Entete, index_events, date_heure = ic.import_casino_echosonde_bis(path_evt)
    (
        datedebut,
        datefin,
        heuredebut,
        heurefin,
        nameTransect,
    ) = ic.infos_radiales_echosonde_bis(C)
elif type_campagne=='PELGAS':
    C, Entete, date_heure = ic.import_jackpot(path_evt)
    (
        datedebut,
        datefin,
        heuredebut,
        heurefin,
        nameTransect,
    ) = ic.infos_radiales_jackpot(C)
else:
    C, Entete, index_events, date_heure = ic.import_casino(path_evt)
    (
        datedebut,
        datefin,
        heuredebut,
        heurefin,
        nameTransect,
        vit_vent_vrai,
        dir_vent_vrai,
    ) = ic.infos_radiales(C)



# supression des variables qui ne sont plus utiles
del C, Entete, date_heure

indsuppr=[]

#on recherche les radiales non uniques et on les supprime
if np.size(datedebut)>1:
    transects_doublons = [idx for idx, val in enumerate(nameTransect) if val in nameTransect[:idx]]
    indsuppr.extend(transects_doublons)
    indsuppr.sort()
    
    nbsuppr = 0
    for i in range(len(indsuppr)):
        nbsuppr = nbsuppr + 1
        del (
            datedebut[indsuppr[i] - nbsuppr],
            datefin[indsuppr[i] - nbsuppr],
            heuredebut[indsuppr[i] - nbsuppr],
            heurefin[indsuppr[i] - nbsuppr],
            nameTransect[indsuppr[i] - nbsuppr],
        )

datedebutini=datedebut
heuredebutini=heuredebut
datefinini=datefin
heurefinini=heurefin

len_Sv1 = []
unites = np.array([])

################################################################################
# %% EI AND/OR ECHOGRAM SEGMENTATION
################################################################################
for indexd in ind_decoup:
    print(indexd)
    premiere_date=indexd
    derniere_date=indexd+1
    
    if np.size(datedebutini)>1:
        datedebut = datedebutini[premiere_date:derniere_date]
        heuredebut = heuredebutini[premiere_date:derniere_date]
        datefin=datefinini[premiere_date:derniere_date]
        heurefin=heurefinini[premiere_date:derniere_date]
    
        date_time_debut= []
        date_time_fin= []
        for i in range (len(datedebut)):
            date_time_debut.append("%s%s" %(datedebut[i],heuredebut[i]))
            date_time_fin.append("%s%s" %(datefin[i],heurefin[i]))
        
        
    #path windows  
    survey_name = survey_name0 +'_'+str(premiere_date)+'_'+str(derniere_date)
    path_save = fpath + survey_name + "/" 


    path_csv = path_save + "csv/"
    path_png = path_save + "png/"
    path_html = path_save + "html/"
    path_netcdf = path_save + "netcdf/"
    path_txt = path_save + "txt/"
    # If save path does not exist, create it
    if not os.path.exists(path_save):
        os.makedirs(path_save)
    
    if not os.path.exists(path_csv):
        os.makedirs(path_csv)
    
    if not os.path.exists(path_png):
        os.makedirs(path_png)
    
    if not os.path.exists(path_html):
        os.makedirs(path_html)
    
    if not os.path.exists(path_netcdf):
        os.makedirs(path_netcdf)
        
    if not os.path.exists(path_txt):
        os.makedirs(path_txt)
    
    filename_ME70 = "%s/%s-TH%d-ME70-EIlay.pickle" % (path_save, survey_name, thresholds[0])
    filename_EK80 = "%s/%s-TH%d-EK80-EIlay.pickle" % (path_save, survey_name, thresholds[0])
    filename_EK80h = "%s/%s-TH%d-EK80h-EIlay.pickle" % (
        path_save,
        survey_name,
        thresholds[0],
    )
    
    
    if save_data:
    
        filename_data_ME70 = "%s/%s-TH%d-saved_data_for_ME70_all.pickle" % (path_save, survey_name, thresholds[0])
        filename_data_EK80 = "%s/%s-TH%d-saved_data_for_EK80_all.pickle" % (path_save, survey_name, thresholds[0])
        filename_data_EK80h = "%s/%s-TH%d-saved_data_for_EK80h_all.pickle" % (path_save, survey_name, thresholds[0])
        filename_clustering_ME70 = "%s/%s-TH%d-saved_clustering_for_ME70_all.pickle" % (path_save, survey_name, thresholds[0])
        filename_clustering_EK80h = "%s/%s-TH%d-saved_clustering_for_EK80h_all.pickle" % (path_save, survey_name, thresholds[0])
    
    
    ####################################################################################################
    #
    #### If echointegration==True, perform echo-integration
    #
    ####################################################################################################
    if echointegration:
    #### EI pickle        
        if methode_EI=='pickle':
        
            ei.ei_survey_transects(
                path_hac_survey,
                path_config,
                path_save,
                datedebut,
                datefin,
                heuredebut,
                heurefin,
                nameTransect[premiere_date:],
                False,
                True,
                False,
            )
            print('EI completed')    
            # concaténation de tous des résultats EI EK80 de tous les transects pour analyse MFR à l'échelle de la campagne
            ei_bind.ei_bind_transects(path_save, None, filename_EK80, None)
            print('EI results merged') 
            
            # ####################################################################################################
            #
            # Remaniement des données et classification
            #
            ####################################################################################################
            # ré-organisation des données d'écho-intégration mise au format standard    
            # Commenter/Decommenter pour choisir quels fichiers charger
            print("chargement des donnees...")
            with open(filename_EK80, "rb") as f:
                (
                    time_EK80_db,
                    depth_surface_EK80_db,
                    depth_bottom_EK80_db,
                    Sv_surfEK80_db,
                    Sv_botEK80_db,
                    Sa_surfEK80_db,
                    Sa_botEK80_db,
                    lat_surfEK80_db,
                    lon_surfEK80_db,
                    lat_botEK80_db,
                    lon_botEK80_db,
                    vol_surfEK80_db,
                    freqs_EK80_db,
                ) = pickle.load(f)
            
            # with open(filename_EK80h) as f:
            #    time_EK80h_db,depth_surface_EK80h_db,Sv_surfEK80h_db,Sa_surfEK80h_db,lat_surfEK80h_db,lon_surfEK80h_db,vol_surfEK80h_db,freqs_EK80_db = pickle.load(f)
        
            # with open(filename_ME70) as f:
            #    time_ME70_db,depth_surface_ME70_db,depth_bottom_ME70_db,Sv_surfME70_db,Sv_botME70_db,Sa_surfME70_db,Sa_botME70_db,lat_surfME70_db,lon_surfME70_db,lat_botME70_db,lon_botME70_db,vol_surfME70_db,freqs_ME70_db = pickle.load(f)
        
            standardizing.scaling(
            freqs_EK80_db,
            Sv_surfEK80_db,
            Sa_surfEK80_db,
            depth_surface_EK80_db,
            depth_bottom_EK80_db,
            lat_surfEK80_db,
            lon_surfEK80_db,
            time_EK80_db,
            indices_transduc_choisis,
            date_time_debut,
            date_time_fin,
            premiere_date,
            nameTransect,
            save=save_data,
            filename=filename_data_EK80,
        )
            
            del (
                time_EK80_db,
                depth_surface_EK80_db,
                depth_bottom_EK80_db,
                Sv_surfEK80_db,
                Sv_botEK80_db,
                Sa_surfEK80_db,
                Sa_botEK80_db,
                lat_surfEK80_db,
                lon_surfEK80_db,
                lat_botEK80_db,
                lon_botEK80_db,
                vol_surfEK80_db,
                freqs_EK80_db,
            )
            
        else:
            #### EI netCDF
            ei.ei_survey_transects_netcdf(
            path_hac_survey,
            path_config,
            path_netcdf,
            datedebut,
            datefin,
            heuredebut,
            heurefin,
            premiere_date,
            nameTransect,
            sounder_ident
        )
            print('EI completed')  
            
            #### Get EI netCDF data
            if np.size(datedebutini)>1:
                file=path_netcdf+nameTransect[premiere_date:][0]+'_'+str(sounder_ident)+'.xsf.nc'
            else:
                file=path_netcdf+nameTransect+'_'+str(sounder_ident)+'.xsf.nc'
            dataset=nc.Dataset(file)
            Sv=dataset.groups['Sonar'].groups['Gridded_1'].variables['integrated_backscatter'][:]
            freq_MFR=dataset.groups['Sonar'].groups['Gridded_1'].variables['frequency'][:]
            freq_MFR=np.sort(freq_MFR)
            Time=dataset.groups['Sonar'].groups['Gridded_1'].variables['cell_ping_time'][:]
            Depth=dataset.groups['Sonar'].groups['Gridded_1'].variables['cell_depth'][:]
            Depth=Depth.T
            Lat=dataset.groups['Sonar'].groups['Gridded_1'].variables['cell_latitude'][:]
            Lon=dataset.groups['Sonar'].groups['Gridded_1'].variables['cell_longitude'][:]
            nom_numero_radiales=([nameTransect[premiere_date]],[premiere_date])
            
    else:
        #### If echointegration==False, load EI results
        if methode_EI=='pickle':
            with open(filename_data_EK80, "rb") as f:
                (
                    freq_MFR,
                    freqs_moy_transducteur,
                    Sv,
                    Sv_moy_transducteur_x,
                    Sa,
                    Depth,
                    Lat,
                    Lon,
                    Time,
                    nb_transduc,
                    tableau_radiales,
                    nom_numero_radiales,
                ) = pickle.load(f)      
                
        else:
            
            if np.size(datedebutini)>1:
                file=path_netcdf+nameTransect[premiere_date:][0]+'_'+str(sounder_ident)+'.xsf.nc'
            else:
                file=path_netcdf+nameTransect+'_'+str(sounder_ident)+'.xsf.nc'
            dataset=nc.Dataset(file)
            Sv=dataset.groups['Sonar'].groups['Gridded_1'].variables['integrated_backscatter'][:]
            freq_MFR=dataset.groups['Sonar'].groups['Gridded_1'].variables['frequency'][:]
            freq_MFR=np.sort(freq_MFR)
            Time=dataset.groups['Sonar'].groups['Gridded_1'].variables['cell_ping_time'][:]
            Depth=dataset.groups['Sonar'].groups['Gridded_1'].variables['cell_depth'][:]
            Depth=Depth.T
            Lat=dataset.groups['Sonar'].groups['Gridded_1'].variables['cell_latitude'][:]
            Lon=dataset.groups['Sonar'].groups['Gridded_1'].variables['cell_longitude'][:]
            nom_numero_radiales=([nameTransect[premiere_date]],[premiere_date])
            
         
    len_Sv1.append(len(Sv[1]))
            
    unites = np.append(unites,np.repeat(pas,len(Sv)//pas))
    unites = np.append(unites,len(Sv)%pas)
    #### EI cell classification
    if clustering:
                      
        for k in range(0,len(Sv),pas):
            if len(Sv)-k<pas:
                Sv_temp=Sv[k:len(Sv),:,:indice_freq_max]
                freq_MFR=freq_MFR[:indice_freq_max]
                print("Sv_temp",Sv_temp.shape)
                Lat_t=Lat[k:len(Sv),:,:]
                Lon_t=Lon[k:len(Sv),:,:]
                Time_t=Time[k:len(Sv),:]
                
                filename_clustering_EK80 =path_save+'portion'+str(len(Sv))+ "saved_clustering_for_EK80_all.pickle" 
                save_labels=path_txt+'portion'+str(len(Sv))+'Labels.txt'
                save_Q1_=path_txt+'portion'+str(len(Sv))+'Q1.txt'
                save_Q3_=path_txt+'portion'+str(len(Sv))+'Q3.txt'
                save_med=path_txt+'portion'+str(len(Sv))+'Med.txt'        
                save_fig_clustering=path_png+'portion'+str(len(Sv))+'Fig_clustering.png'
                save_fig_Sv=path_png+str(len(Sv))+"Figure_Sv.png"
                save_html_Sa=path_html+'portion'+str(len(Sv))
                save_Sa_moy=path_txt+'portion'+str(len(Sv))+'Sa_moy.txt'
                save_Lat_moy=path_txt+'portion'+str(len(Sv))+'Lat_moy.txt'
                save_Lon_moy=path_txt+'portion'+str(len(Sv))+'Lon_moy.txt'
    
            else:       
                Sv_temp=Sv[k:k+pas,:,:indice_freq_max]
                freq_MFR=freq_MFR[:indice_freq_max]
                #print("Sv_temp",Sv_temp.shape)
                Lat_t=Lat[k:k+pas,:,:]
                Lon_t=Lon[k:k+pas,:,:]
                Time_t=Time[k:k+pas,:]
            
                filename_clustering_EK80 =path_save+'portion'+str(k+pas)+ "saved_clustering_for_EK80_all.pickle" 
                save_labels=path_txt+'portion'+str(k+pas)+'Labels.txt'
                save_Q1_=path_txt+'portion'+str(k+pas)+'Q1.txt'
                save_Q3_=path_txt+'portion'+str(k+pas)+'Q3.txt'
                save_med=path_txt+'portion'+str(k+pas)+'Med.txt'        
                save_fig_clustering=path_png+'portion'+str(k+pas)+'Fig_clustering.png'
                save_fig_Sv=path_png+str(k+pas)+"Figure_Sv.png"
                save_html_Sa=path_html+'portion'+str(k+pas)
                save_Sa_moy=path_txt+'portion'+str(k+pas)+'Sa_moy.txt'
                save_Lat_moy=path_txt+'portion'+str(k+pas)+'Lat_moy.txt'
                save_Lon_moy=path_txt+'portion'+str(k+pas)+'Lon_moy.txt'
            
            if methode_EI=='pickle':
                #### LLE+Kmeans clustering based on pickle EI
                print('Echogram segmentation, please wait')
                classif.clustering(
                Sv_temp,
                Sv_moy_transducteur_x,
                freq_MFR,
                freqs_moy_transducteur,
                nombre_de_categories_sv,
                Sa,
                Time_t,
                Depth,
                Lat_t,
                Lon_t,
                nb_transduc,
                tableau_radiales,
                nom_numero_radiales,
                methode_clustering,
                nombre_composantes,
                nombre_voisins,
                save=save_data,
                filename=filename_clustering_EK80,
            )
                print('Segmentation done')
                
                with open(filename_clustering_EK80, "rb") as f:
                    (
                        kmeans,
                        labels,
                        Q1_,
                        Q3_,
                        med,
                        nombre_de_categories,
                        Sv_temp,
                        Sv_moy_transducteur_x,
                        Sa,
                        Time_t,
                        freq_MFR,
                        freqs_moy_transducteur,
                        Depth,
                        Lat_t,
                        Lon_t,
                        nb_transduc,
                        tableau_radiales,
                        nom_numero_radiales,
                        methode_clustering,
                    ) = pickle.load(f)
                    
            else:
                #### LLE+Kmeans clustering based on netCDF EI
                print('Echogram segmentation, please wait')
                classif.clustering_netcdf(
                Sv_temp,
                freq_MFR,
                nombre_de_categories_sv,
                Time_t,
                Depth,
                Lat_t,
                Lon_t,
                nb_transduc,
                nom_numero_radiales,
                methode_clustering,
                nombre_composantes,
                nombre_voisins,
                save=save_data,
                filename=filename_clustering_EK80,
            )
                print('Segmentation done')
                
                with open(filename_clustering_EK80, "rb") as f:
                    (
                        kmeans,
                        labels,
                        Q1_,
                        Q3_,
                        med,
                        nombre_de_categories,
                        Sv_temp,
                        Time_t,
                        freq_MFR,
                        Depth,
                        Lat_t,
                        Lon_t,
                        nb_transduc,
                        nom_numero_radiales,
                        methode_clustering,
                    ) = pickle.load(f)
                                         
            couleurs = plt.cm.jet(np.linspace(0, 1, nombre_de_categories_sv))
            cmap = colors.ListedColormap(couleurs)
            
            #### Display classification and frequency response per cluster 
            x=np.arange(0,len(Sv_temp),1)
            #y=np.arange(-Depth[1,0],-Depth[1,(len(Depth[1])-1)]-1,-1)
            y=np.arange(-Depth[0,0],-Depth[0,(len(Depth[0])-1)]-1,-1)
        
            fig,ax2 = plt.subplots(figsize=(30, 15))
            #Segmented echogram
            plt.subplot(211)
            plt.xlabel('N*ESU')
            plt.ylabel('profondeur (en m)')
            plt.title(' ')
            plt.pcolormesh(x,y,np.transpose(labels), cmap=cmap)
            plt.grid()
            plt.colorbar()
             
            #Frequency response
            plt.subplot(212)
            plt.xlabel('Freq')
            plt.ylabel('Sv dB')
            plt.title('Reponse en frequence par cluster')
            for j in range(nombre_de_categories_sv):
                plt.plot(freq_MFR[:indice_freq_max],Q1_[j],'-.',color=couleurs[j])
                plt.plot(freq_MFR[:indice_freq_max],Q3_[j],'-.',color=couleurs[j])
                plt.plot(freq_MFR[:indice_freq_max],med[j],'o-',color=couleurs[j])
            plt.grid()
            #plt.close()
            plt.show()
            
            np.savetxt(save_labels,labels)
            np.savetxt(save_Q1_,Q1_)
            np.savetxt(save_Q3_,Q3_)
            np.savetxt(save_med,med)
            fig.savefig(save_fig_clustering)
    
            freq_voulues = []
            freq_log=[]
            freq_lin=[]
            freq_log_temp =  np.logspace(math.log10(freq_MFR[1]),math.log10(freq_MFR[len(freq_MFR)-1]),nbfreqs) 
            freq_lin_temp =  np.linspace(freq_MFR[1],(freq_MFR[len(freq_MFR)-1]),nbfreqs) 
    
    
            freq_custom_nearest = []
            for i in range (len(freq_custom)) :
                freq_custom_nearest.append(min(freq_MFR, key=lambda x:abs(x-freq_custom[i])))
            for i in range (len(freq_log_temp)) :
                freq_log.append(min(freq_MFR, key=lambda x:abs(x-freq_log_temp[i])))     
            for i in range(len(freq_lin_temp)) :
                freq_lin.append(min(freq_MFR, key=lambda x:abs(x-freq_lin_temp[i])))
                    
            freq_voulues = freq_custom_nearest
            
            Sa_affiche = []
            Sv_affiche = []
            freqs_affichage = []
            H_couche=(Depth[0, 1] - Depth[0, 0])
            temp=ma.masked_object(Sv_temp,-10000000.0)
            Sa_temp=10**(temp/10)*H_couche*1852**2*4*np.pi
            for f in range(len(freq_voulues)):
                indice = np.where(freq_MFR == freq_voulues[f])
                Sv_affiche.append(np.transpose(np.squeeze(Sv_temp[:, :, indice])))
                if methode_EI=='pickle':
                    Sa_moy = np.squeeze(Sa[:, :, f])
                else:
                    Sa_affiche.append(np.transpose(np.squeeze(Sa_temp[:, :, indice])))
                freqs_affichage.append((freq_voulues[f]) / 1000)
                Lat_temp=Lat_t[:,:,f]
                Lon_temp=Lon_t[:,:,f]
                Time_temp=Time_t[:,f]
                Time_temp=au.hac_read_day_time(Time_temp)
            Sv_affiche = np.array(Sv_affiche)
            fig, ax3 = plt.subplots(figsize=(30, 15))
            if AllPlots==True:
                dr.DrawMultiEchoGram(
                    Sv_affiche,
                    -Depth,
                    len(freq_voulues),
                    freqs=freqs_affichage,
                    title="Sv sondeur %d kHz",
                )
                fig.savefig(save_fig_Sv)
                plt.close()
 
            Sa_moy = np.array(Sa_affiche)
            
            # Suppression des valeurs n'ayant pas de sens dans la latitude et la longitude
            index = np.where((Lon_t[:, 0] == -200) & (Lat_t[:, 0] == -100))
        
            Lat_temp = np.delete(Lat_temp, (index), axis=0)
            Lon_temp = np.delete(Lon_temp, (index), axis=0)
        
            labels = np.delete(labels, (index), axis=0)
            Sa_moy = np.delete(Sa_moy, (index), axis=0)
            a = np.where(Sa_moy == 0)
            Sa_moy[a] = np.NaN
    
        
            Lat_moy = np.true_divide(Lat_temp.sum(1), (Lat_temp != 0).sum(1))
            Lon_moy = np.true_divide(Lon_temp.sum(1), (Lon_temp != 0).sum(1))
        
            # définition des couches surface et fond agrégées pour relier au chalutage
            surface_limit = 30
            indice_surface = int(
                np.floor((surface_limit - Depth[0, 0]) / H_couche)
            ) 
    
            Sa_moy_label_surface = []
            Sa_moy_label_fond = []
            Sa_moy_label = []
            Sa_moy_label_log = []
            for f in range(len(freq_voulues)):
                sa_temp=Sa_moy[f,:,:].T
                for x in range(nombre_de_categories_sv):
                    Sa_label = np.zeros(sa_temp.shape)
                    Sa_label[:, :] = np.nan
                    index_lab = np.where(labels == x)
                    Sa_label[index_lab] = sa_temp[index_lab]
                    Sa_moy_label_surface.append(np.nansum(Sa_label[:, 0:indice_surface], axis=1))
                    Sa_moy_label_fond.append(np.nansum(Sa_label[:, indice_surface + 1 :], axis=1))
                    Sa_moy_label.append(np.nansum(Sa_label, axis=1))
                    Sa_moy_label_log.append(10 * np.log10(np.nanmean(Sa_label, axis=1)))
            Sa_moy_label=np.array(Sa_moy_label)
            Sa_moy_label=Sa_moy_label.reshape((len(freq_voulues),nombre_de_categories_sv,-1))
            Sa_moy_label_log=np.array(Sa_moy_label_log)
            Sa_moy_label_log=Sa_moy_label_log.reshape((len(freq_voulues),nombre_de_categories_sv,-1))
            Sa_moy_label_fond=np.array(Sa_moy_label_fond)
            Sa_moy_label_fond=Sa_moy_label_fond.reshape((len(freq_voulues),nombre_de_categories_sv,-1))
            Sa_moy_label_surface=np.array(Sa_moy_label_surface)
            Sa_moy_label_surface=Sa_moy_label_surface.reshape((len(freq_voulues),nombre_de_categories_sv,-1))
            
            #les cercles n'apparaissent que pour les valeurs positives (faire coincider figure et legende en n'affichant que les resultats positifs)
            sa_log=np.zeros((nbfreqs,nombre_de_categories_sv,Sa_moy_label_log.shape[2]))
            for f in range(len(freq_voulues)):
                for x in range(nombre_de_categories_sv):
                    for i in range(Sa_moy_label_log.shape[2]):
                        if Sa_moy_label_log[f][x][i]>0:
                            sa_log[f,x,i]=Sa_moy_label_log[f][x][i]+offset
                        elif Sa_moy_label_log[f][x][i]<=0 and np.isnan(Sa_moy_label_log[f][x][i])==False:
                            sa_log[f,x,i]=offset
    
                 
            ##################################################################################################
            #
            # Création de carte avec la librairie Cartopy : Carte simple , affichage dans les figures.
            # Sa en valeurs naturelles
            #
            ##################################################################################################
            ##################################################################################################
            #
            # Création de carte avec la librairie folium : Carte Openstreetmap, affichage dans un fichier HTML.
            # Sa en Log
            #
            ##################################################################################################
    
            plt.rc('legend',fontsize=15)    
    
            for f in range(len(freq_voulues)):
                if Sv_temp.shape[0]<pas:
                     save_html_Sa=path_html+'portion'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))
                     save_Sa_moy=path_txt+'portion'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))+'Sa_moy.txt'
                     save_fig_Sa=path_png+'portion'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))
                     save_fig_Salog=path_png+'portion_LogSa'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))
                else:
                     save_html_Sa=path_html+'portion'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))
                     save_Sa_moy=path_txt+'portion'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))+'Sa_moy.txt'
                     save_fig_Sa=path_png+'portion'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))
                     save_fig_Salog=path_png+'portion_LogSa'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))
                # md.affichage_carte_france(nombre_de_categories, Lat_moy, Lon_moy, Sa_moy_label[f,:,:], extension_zone, 'Sa',save_fig_Sa)
                md.affichage_carte_france(nombre_de_categories, Lat_moy, Lon_moy, sa_log[f,:,:], extension_zone, 'LogSa',save_fig_Salog)
                # md.save_map_html(nombre_de_categories, Lat_moy, Lon_moy, Sa_moy_label_log[f,:,:], save_html_Sa)
                np.savetxt(save_Sa_moy,Sa_moy[f,:,:])
                
                ##################################################################################################
                #
                # Export de Sa (en valeurs naturelles et log), Lat et Lon dans un fichier CSV
                #
                #################################################################################################
            
                for x in range(nombre_de_categories_sv):
                    with open(
                        "%s/Sa_surface_label_%d_freq_%d.csv"
                        % (path_csv, x, freq_custom_nearest[f]),
                        "w",
                        newline="",
                    ) as csvfile:
                        writer = csv.writer(csvfile, delimiter=" ")
                        for i in range(len(Lat_moy)):
                            if Sa_moy_label_surface[f][x][i] > 0:
                                writer.writerow(
                                    (
                                        Time_temp[i].strftime("%d/%m/%Y %H:%M:%S"),
                                        Lat_moy[i],
                                        Lon_moy[i],
                                        Sa_moy_label_surface[f][x][i],
                                    )
                                )
                                
                                
            
                for x in range(nombre_de_categories_sv):
                    with open(
                        "%s/Sa_fond_label_%d_freq_%d.csv"
                        % (path_csv, x, freq_custom_nearest[f]),
                        "w",
                        newline="",
                    ) as csvfile:
                        writer = csv.writer(csvfile, delimiter=" ")
                        for i in range(len(Lat_moy)):
                            if Sa_moy_label_fond[f][x][i] > 0:
                                writer.writerow(
                                    (
                                        Time_temp[i].strftime("%d/%m/%Y %H:%M:%S"),
                                        Lat_moy[i],
                                        Lon_moy[i],
                                        Sa_moy_label_fond[f][x][i],
                                    )
                                )
                                
            
                for x in range(nombre_de_categories_sv):
                    with open(
                        "%s/Sa_label_%d_freq_%d.csv" % (path_csv, x, freq_custom_nearest[f]),
                        "w",
                        newline="",
                    ) as csvfile:
                        writer = csv.writer(csvfile, delimiter=" ")
                        for i in range(len(Lat_moy)):
                            if Sa_moy_label[f][x][i] > 0:
                                writer.writerow(
                                    (
                                        Time_temp[i].strftime("%d/%m/%Y %H:%M:%S"),
                                        Lat_moy[i],
                                        Lon_moy[i],
                                        Sa_moy_label[f][x][i],
                                    )
                                )
            
            ##################################################################################################-
            
            filelist = glob2.glob("%s/**/*.hac" % path_hac_survey)
            if len(filelist) == 0:
                raise RuntimeError(path_hac_survey + " does not contain HAC files")
            
            list_sounder = au.hac_sounder_descr(filelist[0])
            nb_snd = list_sounder.GetNbSounder()
            
            with open("%s/rapport.adoc" % path_png, "w") as rapport:
            
                rapport.write(("= RAPPORT \n\n"))
                rapport.write(("==== Descriptif des sondeurs \n\n"))
                rapport.write((" nb sondeurs = " + str(nb_snd) + "\n"))
                for isdr in range(nb_snd):
                    sounder = list_sounder.GetSounder(isdr)
                    nb_transducs = sounder.m_numberOfTransducer
                    rapport.write(
                        (
                            "sondeur "
                            + str(isdr)
                            + ":    index: "
                            + str(sounder.m_SounderId)
                            + "   nb trans:"
                            + str(nb_transducs)
                            + "\n"
                        )
                    )
                    for itr in range(nb_transducs):
                        trans = sounder.GetTransducer(itr)
                        for ibeam in range(trans.m_numberOfSoftChannel):
                            softChan = trans.getSoftChannelPolarX(ibeam)
                            rapport.write(
                                (
                                    "   trans "
                                    + str(itr)
                                    + ":    nom: "
                                    + trans.m_transName
                                    + "   freq: "
                                    + str(softChan.m_acousticFrequency / 1000)
                                    + " kHz\n"
                                )
                            )
            
                rapport.write(
                    ("\n==== Etude de la classification multifrequence sur le sondeur EK80\n\n")
                )
            
                #  for x in range (len(nom_numero_radiales[1])):
                #     rapport.write("===== Sv moyen par transducteurs pour la radiale %s\n\n" %(nom_numero_radiales[0][x]))
                #     rapport.write(("image:figure_Sv_moy_%d.png[] \n\n" %(nom_numero_radiales[1][x])))
            
                for x in range(len(nom_numero_radiales[1])):
                    rapport.write("===== Sv pour la radiale %s\n\n" % (nom_numero_radiales[0][x]))
                    rapport.write(("image:figure_Sv_%d.png[] \n\n" % (nom_numero_radiales[1][x])))
            
                rapport.write("===== Resultat de classification\n\n")
                rapport.write("image:figure_clustering.png[] \n\n")
            
                for x in range(nombre_de_categories_sv):
                    rapport.write("===== Affichage du Sa pour le label %d\n\n" % x)
                    rapport.write(("image:map_label_%d.png[] \n\n" % x))
            
            sounder = list_sounder.GetSounder(0)
            np.savetxt(save_Lat_moy,Lat_moy)
            np.savetxt(save_Lon_moy,Lon_moy)
            
    np.savetxt(fpath+'UnitesDecoupageCampagne.txt',unites)
    np.savetxt(fpath+'Profondeur_max.txt',np.array([len_Sv1]))
    np.savetxt(fpath+'Freq_MFR.txt',np.array([freq_MFR]))        
    # f = open(fpath+'Freq_MFR.txt','w')
    # np.savetxt(f,np.array([freq_MFR]))
   
    f = open(fpath+'Depth.pickle','wb')
    pickle.dump(Depth,f)
        




################################################################################
# %% CLUSTERING OF FREQUENCY RESPONSES PER batch 
################################################################################
"""
Clustering of segmentation results per batch at the survey scale, 
based on clusters frequency responses per batch
"""
if recombinaison:
    len_Sv1_float=np.loadtxt(fpath+'Profondeur_max.txt',dtype='double')
    len_Sv1=np.int_(len_Sv1_float)
    indice_profondeur_max = np.max(len_Sv1)
    
    #### Open results per batch files
    for root, subFolders, files in os.walk(fpath):
        for file in files:
            if methode_EI=='pickle':
                if file.endswith("saved_clustering_for_EK80_all.pickle"):
                    with open(root+'/'+file, "rb") as f:
                        (
                            freq_MFR,
                            freqs_moy_transducteur,
                            Sv,
                            Sv_moy_transducteur_x,
                            Sa,
                            Depth,
                            Lat,
                            Lon,
                            Time,
                            nb_transduc,
                            tableau_radiales,
                            nom_numero_radiales,
                        ) = pickle.load(f) 
                    break
            else:
                if file.endswith("1.xsf.nc"):
                    dataset=nc.Dataset(root+'/'+file)
                    Sv=dataset.groups['Sonar'].groups['Gridded_1'].variables['integrated_backscatter'][:]
                    freq_MFR=dataset.groups['Sonar'].groups['Gridded_1'].variables['frequency'][:]
                    freq_MFR=np.sort(freq_MFR)
                    Time=dataset.groups['Sonar'].groups['Gridded_1'].variables['cell_ping_time'][:]
                    Depth=dataset.groups['Sonar'].groups['Gridded_1'].variables['cell_depth'][:]
                    Depth=Depth.T
                    Lat=dataset.groups['Sonar'].groups['Gridded_1'].variables['cell_latitude'][:]
                    Lon=dataset.groups['Sonar'].groups['Gridded_1'].variables['cell_longitude'][:]
                    break
                break
            break
        break
    
    freq_MFR= np.loadtxt(fpath+'Freq_MFR.txt')
    with open(fpath+'Depth.pickle', "rb") as f:
            (
              Depth
            ) = pickle.load(f)
    
    dirlistQ1_ = []
    dirlistQ3_ = []
    dirlistMed = []
    dirlistLabels=[]
    dirlistSa_moy=[]
    dirlistLat_moy=[]
    dirlistLon_moy=[]
    for root, subFolders, files in os.walk(fpath):
        print(root,subFolders,files)
        for file in files:
            print(file)
            if file.endswith("Q1.txt"):
                dirlistQ1_.append(os.path.join(root, file))
            elif file.endswith("Q3.txt"):
                dirlistQ3_.append(os.path.join(root, file))
            elif file.endswith("Med.txt"):
                dirlistMed.append(os.path.join(root, file))
            elif file.endswith("Labels.txt"):
                dirlistLabels.append(os.path.join(root, file))
            elif file.endswith("Sa_moy.txt"):
                dirlistSa_moy.append(os.path.join(root, file))
            elif file.endswith("Lat_moy.txt"):
                dirlistLat_moy.append(os.path.join(root, file))
            elif file.endswith("Lon_moy.txt"):
                dirlistLon_moy.append(os.path.join(root, file))
                
                
    filelistQ1_ = sorted_alphanumeric(dirlistQ1_)
    filelistQ3_ = sorted_alphanumeric(dirlistQ3_)
    filelistMed = sorted_alphanumeric(dirlistMed)
    filelistLabels = sorted_alphanumeric(dirlistLabels)
    filelistSa_moy = sorted_alphanumeric(dirlistSa_moy)
    filelistLat_moy = sorted_alphanumeric(dirlistLat_moy)
    filelistLon_moy = sorted_alphanumeric(dirlistLon_moy)
    
    Lat_tot=[]
    Lon_tot=[]
    for i in range(len(filelistLat_moy)):
        Lat_tot=np.append(Lat_tot,np.loadtxt(filelistLat_moy[i]))
        Lon_tot=np.append(Lon_tot,np.loadtxt(filelistLon_moy[i]))
    
    Q1_tot=np.loadtxt(filelistQ1_[0])
    Q3_tot=np.loadtxt(filelistQ3_[0])
    med_tot=np.loadtxt(filelistMed[0])
    labels_tot=np.loadtxt(filelistLabels[0])
    if labels_tot.shape[1]<indice_profondeur_max:
        blanc = np.zeros((len(labels_tot),indice_profondeur_max-labels_tot.shape[1]))
        blanc.fill(np.nan)
        labels_tot=np.hstack((labels_tot,blanc))
    for i in range(1,len(filelistQ1_)):
        Q1_tot=np.vstack((Q1_tot,np.loadtxt(filelistQ1_[i])))
        Q3_tot=np.vstack((Q3_tot,np.loadtxt(filelistQ3_[i])))
        med_tot=np.vstack((med_tot,np.loadtxt(filelistMed[i])))   
        labels=np.loadtxt(filelistLabels[i])
        if labels.shape[1]<indice_profondeur_max:
            blanc = np.zeros((len(labels),indice_profondeur_max-labels.shape[1]))
            blanc.fill(np.nan)
            labels=np.hstack((labels,blanc))
        labels_tot=np.vstack((labels_tot,labels))
    
    Sa_moy_tot=[]
    for i in range(len(filelistSa_moy)):
        Sa=np.loadtxt(filelistSa_moy[i])
        Sa_moy_tot.append(Sa)
    
    #### Perform LLE+HAC clustering on frequency responses per clusters and batchs
    
    if nombre_composantes_global>len(freq_MFR):
        raise ValueError('Nombre de composantes trop élevé par rapport au nombre de fréquences')
    elif nombre_voisins_global<nombre_composantes_global:
        raise ValueError('Nombre de voisins trop faible par rapport au nombre de composantes')
    else:
        lle = LocallyLinearEmbedding(n_components=nombre_composantes_global, n_neighbors = nombre_voisins_global,method = 'modified', n_jobs = 4,  random_state=0)
        lle.fit(med_tot)
        med_lle = lle.transform(med_tot)
    
    fpath_global = fpath + "global/"
    # If save path does not exist, create it
    if not os.path.exists(fpath_global):
        os.makedirs(fpath_global)
    
    plt.figure()
    sc.dendrogram(sc.linkage(med_lle,method='ward'))
    
    plt.savefig(fpath_global+'Dendogramme_campagne_totale.png')
    
    
    kmeans_sv=AgglomerativeClustering(n_clusters=nombre_de_categories_sv_campEntiere,affinity='euclidean',linkage='ward').fit(med_lle)
    
    couleurs = plt.cm.jet(np.linspace(0,1,nombre_de_categories_sv_campEntiere))
    cmap = colors.ListedColormap(couleurs) 
    
    plt.figure(figsize=(30,15))
    for x in np.unique(kmeans_sv.labels_):
        indice = np.where(kmeans_sv.labels_ == x)
        plt.plot(freq_MFR[:indice_freq_max], np.mean( Q1_tot[indice], axis=0),'-.',color=couleurs[x])
        plt.plot(freq_MFR[:indice_freq_max], np.mean( Q3_tot[indice], axis=0),'-.',color=couleurs[x])
        plt.plot(freq_MFR[:indice_freq_max], np.mean( med_tot[indice], axis=0),'o-',color=couleurs[x],label="Classe"+str(x))
    plt.legend()

    plt.savefig(fpath_global+'ClustersRF_campagne_totale.png')
    
    kmeans_sv.labels_new=kmeans_sv.labels_+nombre_de_categories_sv+1
    labels_tot_new=np.where(labels_tot==nombre_de_categories_sv,np.max(kmeans_sv.labels_new)+1,labels_tot)
    
    unites_float= np.loadtxt(fpath+'UnitesDecoupageCampagne.txt',dtype='double')
    unites=np.int_(unites_float)
    len_Sv0_ent_tot_final=np.cumsum(unites)  
               
    j=0
    for i in range(nombre_de_categories_sv):
        labels_tot_new[:len_Sv0_ent_tot_final[j],:]=np.where(labels_tot_new[:len_Sv0_ent_tot_final[j],:]==i,kmeans_sv.labels_new[i+j],labels_tot_new[:len_Sv0_ent_tot_final[j],:])       
    
    kmeans_sv.labels_new_shape=kmeans_sv.labels_new.reshape((len(len_Sv0_ent_tot_final),nombre_de_categories_sv))     
    for j in range(1,len(len_Sv0_ent_tot_final)):
        for i in range(nombre_de_categories_sv):
            labels_tot_new[len_Sv0_ent_tot_final[j-1]:len_Sv0_ent_tot_final[j],:]=np.where(labels_tot_new[len_Sv0_ent_tot_final[j-1]:len_Sv0_ent_tot_final[j],:]==i,kmeans_sv.labels_new_shape[j,i],labels_tot_new[len_Sv0_ent_tot_final[j-1]:len_Sv0_ent_tot_final[j],:])       
             
    labels_tot_new=labels_tot_new-(nombre_de_categories_sv+1)
          
    couleurs = plt.cm.jet(np.linspace(0,1,nombre_de_categories_sv_campEntiere))
    cmap = colors.ListedColormap(couleurs) 
    
    x=np.arange(0,len_Sv0_ent_tot_final[-1],1)
    #y=np.arange(-Depth[1,0],-Depth[1,(len(Depth[1])-1)]-1,-1)
    y=np.arange(-Depth[0,0],-Depth[0,(len(Depth[0])-1)]-1,-1)
    
    plt.figure(figsize=(30,15))
    plt.xlabel('N*ESU')
    plt.ylabel('profondeur (en m)')
    plt.title(' ')
    plt.pcolormesh(x,y,np.transpose(labels_tot_new), cmap=cmap)
    plt.grid()
    plt.colorbar()
    #plt.show()
    
    plt.savefig(fpath_global+'Echogramme_clustering_campagne_totale.png')
            
    
    Sa_moy_label_tot = []
    Sa_moy_label_log_tot = []
    u=0
    #for f in range(nb_transduc,len(Sa_moy_tot)+1,nb_transduc):
    for f in range(nb_transduc,len(Sa_moy_tot)+1,nb_transduc):
        #print('boucle sur f:',f)
        sa_temp=np.array(Sa_moy_tot[f-nb_transduc:f])
        #print(sa_temp.shape)
        if np.cumsum(unites)[u]<=pas:
            unit0=0
            unit1=unites[0]
        else:
            unit0=np.cumsum(unites)[u-1]
            unit1=np.cumsum(unites)[u]
        # print('unit0',unit0)
        # print('unit1',unit1)
        for i in range(nb_transduc):
            #print('boucle sur i:',i)
            sa_t=sa_temp[i,:,:].T
            #print(sa_t.shape)
            for x in range(nombre_de_categories_sv_campEntiere):
                #print('boucle sur x:',x)
                Sa_label = np.zeros(sa_t.shape)
                Sa_label[:, :] = np.nan
                #print(Sa_label.shape)
                index_lab = np.where(labels_tot_new[unit0:unit1] == x)
                Sa_label[index_lab] = sa_t[index_lab]
                Sa_moy_label_tot.append(np.nansum(Sa_label, axis=1))
                Sa_moy_label_log_tot.append(10 * np.log10(np.nanmean(Sa_label, axis=1)))
        u+=1  

    freq_voulues = []
    freq_log=[]
    freq_lin=[]
    freq_log_temp =  np.logspace(math.log10(freq_MFR[1]),math.log10(freq_MFR[len(freq_MFR)-1]),nbfreqs) 
    freq_lin_temp =  np.linspace(freq_MFR[1],(freq_MFR[len(freq_MFR)-1]),nbfreqs) 


    freq_custom_nearest = []
    for i in range (len(freq_custom)) :
        freq_custom_nearest.append(min(freq_MFR, key=lambda x:abs(x-freq_custom[i])))
    for i in range (len(freq_log_temp)) :
        freq_log.append(min(freq_MFR, key=lambda x:abs(x-freq_log_temp[i])))     
    for i in range(len(freq_lin_temp)) :
        freq_lin.append(min(freq_MFR, key=lambda x:abs(x-freq_lin_temp[i])))
            
    freq_voulues = freq_custom_nearest             
                    
    reindexage=np.arange(0,len(Sa_moy_label_tot),nb_transduc*nombre_de_categories_sv_campEntiere)
    Sa_moy_label_tot_final=Sa_moy_label_tot[0]
    Sa_moy_label_log_tot_final=Sa_moy_label_log_tot[0]
    for j in range(nb_transduc*nombre_de_categories_sv_campEntiere):
        for i in range(len(reindexage)):
            #print(reindexage[i])
            Sa_moy_label_tot_final=np.hstack((Sa_moy_label_tot_final,Sa_moy_label_tot[reindexage[i]]))
            Sa_moy_label_log_tot_final=np.hstack((Sa_moy_label_log_tot_final,Sa_moy_label_log_tot[reindexage[i]]))
        reindexage+=1
    Sa_moy_label_tot_final=Sa_moy_label_tot_final[unites[0]:]
    Sa_moy_label_tot_final=Sa_moy_label_tot_final.reshape((len(freq_voulues),nombre_de_categories_sv_campEntiere,-1))  
    Sa_moy_label_log_tot_final=Sa_moy_label_log_tot_final[unites[0]:]
    Sa_moy_label_log_tot_final=Sa_moy_label_log_tot_final.reshape((len(freq_voulues),nombre_de_categories_sv_campEntiere,-1))  
    
    fpath_global_maps = fpath_global + "maps/"
    # If save path does not exist, create it
    if not os.path.exists(fpath_global_maps):
        os.makedirs(fpath_global_maps)
    
    #les cercles n'apparaissent que pour les valeurs positives (faire coincider figure et legende en n'affichant que les resultats positifs)
    sa_log=np.zeros((nbfreqs,nombre_de_categories_sv_campEntiere,Sa_moy_label_log_tot_final.shape[2]))
    for f in range(len(freq_voulues)):
        for x in range(nombre_de_categories_sv_campEntiere):
            for i in range(Sa_moy_label_log_tot_final.shape[2]):
                if Sa_moy_label_log_tot_final[f][x][i]>0:
                    sa_log[f,x,i]=Sa_moy_label_log_tot_final[f][x][i]+offset
                elif Sa_moy_label_log_tot_final[f][x][i]<=0 and np.isnan(Sa_moy_label_log_tot_final[f][x][i])==False:
                    sa_log[f,x,i]=offset
                    
                    
                 
    plt.rc('legend',fontsize=15)    
    
    ##################################################################################################
    #
    # Création de carte avec la librairie Cartopy : Carte simple , affichage dans les figures.
    # Sa en valeurs naturelles
    #
    ##################################################################################################
    ##################################################################################################
    #
    # Création de carte avec la librairie folium : Carte Openstreetmap, affichage dans un fichier HTML.
    # Sa en Log
    #
    ##################################################################################################
    
    for f in range(len(freq_voulues)):   
        md.affichage_carte_france(
            nombre_de_categories_sv_campEntiere, Lat_tot, Lon_tot, sa_log[f,:,:], extension_zone,'LogSa', fpath_global_maps+'LogSa'+str(np.round(freq_voulues[f]/1000))
        )
            
        md.affichage_carte_france(
            nombre_de_categories_sv_campEntiere, Lat_tot, Lon_tot, Sa_moy_label_tot_final[f,:,:], extension_zone,'Sa', fpath_global_maps+str(np.round(freq_voulues[f]/1000))
        )
    
        md.save_map_html(
            nombre_de_categories_sv_campEntiere, Lat_tot, Lon_tot, Sa_moy_label_log_tot_final[f,:,:]/5, fpath_global_maps+str(np.round(freq_voulues[f]/1000))
        )
    
    if (0):
        nb_radiales=[]
        heured=np.array([])
        heuref=np.array([])
        for u in range(len(unites)):
            if np.size(datedebutini)>1:
                heured=np.append(heured,heuredebutini[indexd]) 
                heuref=np.append(heuref,heurefinini[indexd])
            else:
                heured=np.append(heured,heuredebutini)
                heuref=np.append(heuref,heurefinini)
                
                    
                    
        Sa_jour=np.array([])
        Sa_nuit=np.array([])
        Sa_jour_log=np.array([])
        Sa_nuit_log=np.array([])
        Lat_jour=np.array([])
        Lat_nuit=np.array([])
        Lon_jour=np.array([])
        Lon_nuit=np.array([])
        for f in range(len(freq_voulues)):
            for x in range(nombre_de_categories_sv_campEntiere):
                for u in range(len(unites)):
                    if heured[u]>'04:00:00' and heuref[u]<'21:00:00': #temps UTC+2
                        if u==0:
                            Sa_jour=np.append(Sa_jour,Sa_moy_label_tot_final[f][x][u:np.cumsum(unites[u])[-1]])
                            Sa_jour_log=np.append(Sa_jour_log,Sa_moy_label_log_tot_final[f][x][u:np.cumsum(unites[u])[-1]])
                            Lat_jour=np.append(Lat_jour,Lat_tot[u:np.cumsum(unites[u])[-1]])
                            Lon_jour=np.append(Lon_jour,Lon_tot[u:np.cumsum(unites[u])[-1]])
                        else:
                            Sa_jour=np.append(Sa_jour,Sa_moy_label_tot_final[f][x][np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                            Sa_jour_log=np.append(Sa_jour_log,Sa_moy_label_log_tot_final[f][x][np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                            Lat_jour=np.append(Lat_jour,Lat_tot[np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                            Lon_jour=np.append(Lon_jour,Lon_tot[np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                    else:
                        if u==0:
                            Sa_nuit=np.append(Sa_nuit,Sa_moy_label_tot_final[f][x][u:np.cumsum(unites[u])[-1]])
                            Sa_nuit_log=np.append(Sa_nuit_log,Sa_moy_label_log_tot_final[f][x][u:np.cumsum(unites[u])[-1]])
                            Lat_nuit=np.append(Lat_nuit,Lat_tot[u:np.cumsum(unites[u])[-1]])
                            Lon_nuit=np.append(Lon_nuit,Lon_tot[u:np.cumsum(unites[u])[-1]])
                        else:
                            Sa_nuit=np.append(Sa_nuit,Sa_moy_label_tot_final[f][x][np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                            Sa_nuit_log=np.append(Sa_nuit_log,Sa_moy_label_log_tot_final[f][x][np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                            Lat_nuit=np.append(Lat_nuit,Lat_tot[np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                            Lon_nuit=np.append(Lon_nuit,Lon_tot[np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])           
        Sa_jour=Sa_jour.reshape((nbfreqs,nombre_de_categories_sv_campEntiere,-1))
        Sa_nuit=Sa_nuit.reshape((nbfreqs,nombre_de_categories_sv_campEntiere,-1))
        Sa_jour_log=Sa_jour_log.reshape((nbfreqs,nombre_de_categories_sv_campEntiere,-1))
        Sa_nuit_log=Sa_nuit_log.reshape((nbfreqs,nombre_de_categories_sv_campEntiere,-1))
        ind_lat_j=int(Lat_jour.shape[0]/(nombre_de_categories_sv_campEntiere*nbfreqs))
        ind_lat_n=int(Lat_nuit.shape[0]/(nombre_de_categories_sv_campEntiere*nbfreqs))
        ind_lon_j=int(Lon_jour.shape[0]/(nombre_de_categories_sv_campEntiere*nbfreqs))
        ind_lon_n=int(Lon_nuit.shape[0]/(nombre_de_categories_sv_campEntiere*nbfreqs))
        Lat_jour=Lat_jour[:ind_lat_j]
        Lat_nuit=Lat_nuit[:ind_lat_n]
        Lon_jour=Lon_jour[:ind_lon_j]
        Lon_nuit=Lon_nuit[:ind_lon_n]
        
        #les cercles n'apparaissent que pour les valeurs positives (faire coincider figure et legende en n'affichant que les resultats positifs)
        sa_log_jour=np.zeros((nbfreqs,nombre_de_categories_sv_campEntiere,Sa_jour_log.shape[2]))
        for f in range(len(freq_voulues)):
            for x in range(nombre_de_categories_sv_campEntiere):
                for i in range(Sa_jour_log.shape[2]):
                    if Sa_jour_log[f][x][i]>0:
                        sa_log_jour[f,x,i]=Sa_jour_log[f][x][i]+offset
                    elif Sa_jour_log[f][x][i]<=0 and np.isnan(Sa_jour_log[f][x][i])==False:
                        sa_log_jour[f,x,i]=offset
                        
        #les cercles n'apparaissent que pour les valeurs positives (faire coincider figure et legende en n'affichant que les resultats positifs)
        sa_log_nuit=np.zeros((nbfreqs,nombre_de_categories_sv_campEntiere,Sa_nuit_log.shape[2]))
        for f in range(len(freq_voulues)):
            for x in range(nombre_de_categories_sv_campEntiere):
                for i in range(Sa_nuit_log.shape[2]):
                    if Sa_nuit_log[f][x][i]>0:
                        sa_log_nuit[f,x,i]=Sa_nuit_log[f][x][i]+offset
                    elif Sa_nuit_log[f][x][i]<=0 and np.isnan(Sa_nuit_log[f][x][i])==False:
                        sa_log_nuit[f,x,i]=offset
                        
        ##################################################################################################
        #
        # Création de carte avec la librairie Cartopy : Carte simple , affichage dans les figures.
        # Sa en valeurs naturelles
        #
        ##################################################################################################
        ##################################################################################################
        #
        # Création de carte avec la librairie folium : Carte Openstreetmap, affichage dans un fichier HTML.
        # Sa en Log
        #
        ##################################################################################################
        
        for f in range(len(freq_voulues)):
            #Affichage des cartes de Sa et de log(Sa) par label et par période jour/nuit
            md.affichage_carte_france(
                nombre_de_categories_sv_campEntiere, Lat_jour, Lon_jour, Sa_jour[f,:,:], extension_zone,'Sa', fpath_global_maps+str(np.round(freq_voulues[f]/1000))+'jour'
            )
        
            # md.save_map_html(
            #     nombre_de_categories_sv_campEntiere, Lat_jour, Lon_jour, Sa_jour_log[f,:,:], fpath_global_maps+str(np.round(freq_voulues[f]/1000))+'jour'
            # )
        
            md.affichage_carte_france(
                nombre_de_categories_sv_campEntiere,Lat_jour, Lon_jour, sa_log_jour[f,:,:], extension_zone,'LogSa', fpath_global_maps+'LogSa_jour_corr'+str(np.round(freq_voulues[f]/1000))
            )
            
            if Sa_nuit.shape[-1]!=0:
                md.affichage_carte_france(
                    nombre_de_categories_sv_campEntiere, Lat_nuit, Lon_nuit, Sa_nuit[f,:,:], extension_zone,'Sa', fpath_global_maps+str(np.round(freq_voulues[f]/1000))+'nuit'
                )
            
                # md.save_map_html(
                #     nombre_de_categories_sv_campEntiere, Lat_nuit, Lon_nuit, Sa_nuit_log[f,:,:], fpath_global_maps+str(np.round(freq_voulues[f]/1000))+'nuit'
                # )
                
                md.affichage_carte_france(
                nombre_de_categories_sv_campEntiere,Lat_nuit, Lon_nuit, sa_log_nuit[f,:,:], extension_zone,'LogSa', fpath_global_maps+'LogSa_nuit_corr'+str(np.round(freq_voulues[f]/1000))
                )
            else:
                pass

################################################################################
# %% ECHOGRAMS CORRECTION
################################################################################
from sklearn.ensemble import IsolationForest
import copy

"""
1) Détection de possibles RF aberrantes à partir de la classification finale et 
   récupération des indices des RF en question  
2) Investigation de chaque RF constituant le cluster identifié comme posant problème 
   Selon la forme de la RF et l'échogramme, l'utilisateur décide s'il veut procéder 
   à une phase de correction     
3) Selon les résultats retournés par les différents algorithmes de correction,
   l'utilisateur décide ou non de conserver les corrections
4) La classification finale est refaite à partir des données corrigées
"""

if correction:
    #investigation de valeurs aberrantes à partir de la classification finale
    #critere basé sur le nombre de RF constituant un cluster, si un cluster est 
    #constitué de critere_nb_RF_clusters ou moins, il est repéré (l'utilisateur 
    #confirmera plus tard s'il pense qu'il s'agit effectivement de valeurs aberrantes
    #ou plutôt d'une RF correspondant à des organismes peu observés au cours de la campagne) 
    critere_nb_RF_clusters=35

    num_label_RF_outlier=np.array([],dtype=int)
    for i in range(nombre_de_categories_sv_campEntiere):
        if len(np.where(kmeans_sv.labels_==i)[0])<=critere_nb_RF_clusters:
            for x in np.where(kmeans_sv.labels_==i)[0]:
                num_label_RF_outlier=np.append(num_label_RF_outlier,x)
                
    dirlistCluster=[]
    for root, subFolders, files in os.walk(fpath):
        for file in files:
            if file.endswith("saved_clustering_for_EK80_all.pickle"):
                dirlistCluster.append(os.path.join(root, file))
                
    filelistCluster = sorted_alphanumeric(dirlistCluster)
                
    filelist_indice=np.arange(0,len(med_tot),1)
    filelist_indice=filelist_indice.reshape((len(filelistLabels),nombre_de_categories_sv))  
    #affichage de chaque RF constituant le cluster précédemment repéré comme pouvant contenir des outliers
    for ind in num_label_RF_outlier:
        # labels=np.loadtxt(filelistLabels[np.where(filelist_indice==ind)[0][0]])
        # Q1_=np.loadtxt(filelistQ1_[np.where(filelist_indice==ind)[0][0]])
        # Q3_=np.loadtxt(filelistQ3_[np.where(filelist_indice==ind)[0][0]])
        # med=np.loadtxt(filelistMed[np.where(filelist_indice==ind)[0][0]])
        with open(filelistCluster[np.where(filelist_indice==ind)[0][0]], "rb") as f:
            (
                kmeans,
                labels,
                Q1_,
                Q3_,
                med,
                nombre_de_categories,
                Sv_temp,
                Time_t,
                freq_MFR,
                Depth,
                Lat_t,
                Lon_t,
                nb_transduc,
                nom_numero_radiales,
                methode_clustering,
            ) = pickle.load(f)
        

            
        #investigate transects where RF ressemble outliers
        #plt.ion()
        couleurs = plt.cm.jet(np.linspace(0,1,nombre_de_categories))
        cmap = colors.ListedColormap(couleurs)         
        x=np.arange(0,len(Sv_temp),1)
        y=np.arange(-Depth[1,0],-Depth[1,(len(Depth[1])-1)]-1,-1)
        fig,ax2 = plt.subplots(figsize=(30, 15))
        #image de classification
        plt.subplot(211)
        plt.xlabel('N*ESU')
        plt.ylabel('profondeur (en m)')
        plt.title('Portion de radiale à investiguer pour possibles outliers')
        plt.pcolormesh(x,y,np.transpose(labels), cmap=cmap)
        plt.grid()
        plt.colorbar()
        
        #réponse en fréquence
        plt.subplot(212)
        plt.xlabel('Freq')
        plt.ylabel('Sv dB')
        plt.title('Reponse en frequence par cluster')
        for j in range(nombre_de_categories):
            plt.plot(freq_MFR[:indice_freq_max],Q1_[j],'-.',color=couleurs[j])
            plt.plot(freq_MFR[:indice_freq_max],Q3_[j],'-.',color=couleurs[j])
            plt.plot(freq_MFR[:indice_freq_max],med[j],'o-',color=couleurs[j])
        plt.grid()
        interval = 10 #secondes d'interaction avec la figure données à l'utilisateur
        #pour choisir si une correction est nécessaire
        plt.pause(interval)
        plt.show()
        plt.close()
               
        #après affichage de chaque RF constituant le cluster précédemment repéré comme
        #pouvant contenir des outliers, l'utilisateur confirme ou non au script s'il 
        #faut continuer l'investigation ou s'arrêter car la RF en question correspond 
        #réellement à un organisme observé
        run_corr2=input("do you want to run an outlier detection algorithm ? \n")
        if run_corr2=='y':
            #6 critères de test pour chaque portion:
            num_label_outlier=np.array([])
            for i in range(len(med)):
                if len(np.where(labels==i)[0])<5/1000*len(labels):
                    print("cluster number "+str(i)+" size is very short, outliers suspected")
                    num_label_outlier=np.append(num_label_outlier,i)
                if len(np.where(med[i,:]>-35)[0])!=0:
                    print("intensity of backscattered signal of cluster number "+str(i)+" is very high, outliers suspected")
                    num_label_outlier=np.append(num_label_outlier,i)
                clf = IsolationForest(random_state=0).fit_predict(med[i,:].reshape((len(med[i,:]),1)))
                clf=clf.reshape((len(med[i,:]),1))
                if len(np.where(clf==-1)[0])>10/100*len(med[i,:]):
                    print("high number of values in RF for which number of splittings (based on a randomly selected feature) required to isolate said values in cluster number "+str(i)+" is very low, outliers suspected")
                    num_label_outlier=np.append(num_label_outlier,i)
                span=np.max(med[i,:])-np.min(med[i,:])
                if span>40:
                    print("span of values larger than 40dB, outliers suspected")
                    num_label_outlier=np.append(num_label_outlier,i)
                if np.sqrt((np.cumsum(med[2,:]**2)[0])/len(med[2,:]))>25/100*span:
                    print("high RMS, outliers suspected")
                    num_label_outlier=np.append(num_label_outlier,i)
                if len(np.where((np.abs(med[2,:]-np.mean(med[2,:])))/len(med[2,:])>0.01)[0])>90/100*len(med[i,:]):
                    print("mean distance to the mean very high for some values, outliers suspected")
                    num_label_outlier=np.append(num_label_outlier,i)


            #si une des RF a été identifiée comme valeur aberrante par au moins 
            #num_detect_critere_outlier des criteres ci-dessus alors on génère 
            #un nouveau fichier de classification avec les corrections qui va être 
            #soumis pour vérification à l'utilisateur
            num_detect_critere_outlier=4
            labels_copy=copy.deepcopy(labels)
            for i in range(len(med)):
                with open(filelistCluster[np.where(filelist_indice==ind)[0][0]], "rb") as f:
                    (
                        kmeans,
                        labels,
                        Q1_,
                        Q3_,
                        med,
                        nombre_de_categories,
                        Sv_temp,
                        Time_t,
                        freq_MFR,
                        Depth,
                        Lat_t,
                        Lon_t,
                        nb_transduc,
                        nom_numero_radiales,
                        methode_clustering,
                    ) = pickle.load(f)
                    
  
                H_couche=(Depth[0, 1] - Depth[0, 0])
                temp=ma.masked_object(Sv_temp,-10000000.0)
                Sa_temp=10**(temp/10)*H_couche*1852**2*4*np.pi
                if len(np.where(num_label_outlier==i)[0])>=num_detect_critere_outlier:
                    Sv_temp_bis=np.empty(Sv_temp.shape)
                    Sv_temp_bis[:,:,:]=Sv_temp[:,:,:]
                    Lat_temp_bis=np.empty(Lat_t.shape)
                    Lat_temp_bis[:,:,:]=Lat_t[:,:,:]
                    Lon_temp_bis=np.empty(Lon_t.shape)
                    Lon_temp_bis[:,:,:]=Lon_t[:,:,:]
                    Time_temp_bis=np.empty(Time_t.shape)
                    Time_temp_bis[:,:,:]=Time_t[:,:]
                    Sa_temp_bis=np.empty(Sa_temp.shape)
                    Sa_temp_bis[:,:,:]=Sa_temp[:,:,:]
                    for ind_i in np.where(labels_copy==i)[0]:
                        #print(i,ind_i)
                        for ind_j in np.where(labels_copy==i)[1]:
                            #print(i,ind_j)
                            Sv_temp_bis[ind_i,ind_j,:]=-9999999.9
                            Sa_temp_bis[ind_i,ind_j,:]=-9999999.9
                            Lat_temp_bis[ind_i,ind_j,:]=-9999999.9
                            Lon_temp_bis[ind_i,ind_j,:]=-9999999.9
                            Time_temp_bis[ind_i,ind_j]=-9999999.9

                    Time_temp_bis=au.hac_read_day_time(Time_temp_bis)
                    os.rename(filelistCluster[np.where(filelist_indice==ind)[0][0]],filelistCluster[np.where(filelist_indice==ind)[0][0]][:-7]+'_old'+str(i)+'.pickle')
                    filename_clustering_EK80_corr=filelistCluster[np.where(filelist_indice==ind)[0][0]]       
                    classif.clustering_netcdf(
                    Sv_temp_bis,
                    freq_MFR,
                    nombre_de_categories_sv,
                    Time_temp_bis,
                    Depth,
                    Lat_temp_bis,
                    Lon_temp_bis,
                    nb_transduc,
                    nom_numero_radiales,
                    methode_clustering,
                    nombre_composantes_global,
                    nombre_voisins_global,
                    save=save_data,
                    filename=filename_clustering_EK80_corr,
                    )
            
                    with open(filename_clustering_EK80_corr, "rb") as f_corr:
                        (
                            kmeans_corr,
                            labels_corr,
                            Q1_corr,
                            Q3_corr,
                            med_corr,
                            nombre_de_categories,
                            Sv_temp_bis,
                            Time_temp_bis,
                            freq_MFR,
                            Depth,
                            Lat_temp_bis,
                            Lon_temp_bis,
                            nb_transduc,
                            nom_numero_radiales,
                            methode_clustering,
                        ) = pickle.load(f_corr)

                    #print(i)
            
                    #on affiche les résultats de la classification corrigée
                    # # Affichage de la classification et de la réponse fréquentielle par clusters 
                    x=np.arange(0,len(Sv_temp_bis),1)
                    y=np.arange(-Depth[1,0],-Depth[1,(len(Depth[1])-1)]-1,-1)
                    fig,ax2 = plt.subplots(figsize=(30, 15))
                    #image de classification
                    plt.subplot(211)
                    plt.xlabel('N*ESU')
                    plt.ylabel('profondeur (en m)')
                    plt.title('Portion corrigée des possibles outliers')
                    plt.pcolormesh(x,y,np.transpose(labels_corr), cmap=cmap)
                    plt.grid()
                    plt.colorbar()
                    plt.show()
                    
                    #réponse en fréquence
                    plt.subplot(212)
                    plt.xlabel('Freq')
                    plt.ylabel('Sv dB')
                    plt.title('Reponse en frequence par cluster')
                    for j in range(nombre_de_categories_sv):
                        plt.plot(freq_MFR[:indice_freq_max],Q1_corr[j],'-.',color=couleurs[j])
                        plt.plot(freq_MFR[:indice_freq_max],Q3_corr[j],'-.',color=couleurs[j])
                        plt.plot(freq_MFR[:indice_freq_max],med_corr[j],'o-',color=couleurs[j])
                    plt.grid()
                    plt.pause(interval)
                    plt.show()
                    plt.close()
                    
                    #print(i)
                    
                    #l'utilisateur décide si ces corrections doivent être conservées et appliquées à tous les fichiers
                    #les anciens fichiers sont conservés mais renommés _old.extension
                    save_corr=input("do you want to apply corrections ? \n")
                    if save_corr=='y':
                        os.rename(filelistLabels[np.where(filelist_indice==ind)[0][0]],filelistLabels[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                        np.savetxt(filelistLabels[np.where(filelist_indice==ind)[0][0]],labels_corr)
                        os.rename(filelistQ1_[np.where(filelist_indice==ind)[0][0]],filelistQ1_[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                        np.savetxt(filelistQ1_[np.where(filelist_indice==ind)[0][0]],Q1_corr)
                        os.rename(filelistQ3_[np.where(filelist_indice==ind)[0][0]],filelistQ3_[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                        np.savetxt(filelistQ3_[np.where(filelist_indice==ind)[0][0]],Q3_corr)
                        os.rename(filelistMed[np.where(filelist_indice==ind)[0][0]],filelistMed[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                        np.savetxt(filelistMed[np.where(filelist_indice==ind)[0][0]],med_corr)    
                        fig.savefig(filelistLabels[np.where(filelist_indice==ind)[0][0]][:-25]+'png/Figure_clustering_corr'+str(ind)+'_'+str(ind)+'.png')
  
                        Sa_affiche = []
                        Sv_affiche = []
                        freqs_affichage = []
                        for f in range(len(freq_voulues)):
                            indice = np.where(freq_MFR == freq_voulues[f])
                            Sv_affiche.append(np.transpose(np.squeeze(Sv_temp_bis[:, :, indice])))
                            if methode_EI=='pickle':
                                Sa_moy = np.squeeze(Sa_temp_bis[:, :, f])
                            else:
                                Sa_affiche.append(np.transpose(np.squeeze(Sa_temp_bis[:, :, indice])))
                                Lat_temp=Lat_temp_bis[:,:,f]
                                Lon_temp=Lon_temp_bis[:,:,f]
                                Time_temp=Time_temp_bis[:,f]
                            Time_temp=au.hac_read_day_time(Time_temp)
                            freqs_affichage.append((freq_voulues[f]) / 1000)
                        Sv_affiche = np.array(Sv_affiche)
                        Sa_moy = np.array(Sa_affiche)
                        fig, ax3 = plt.subplots(figsize=(24, 35))
                        dr.DrawMultiEchoGram(
                            Sv_affiche,
                            -Depth,
                            len(freq_voulues),
                            freqs=freqs_affichage,
                            title="Sv sondeur %d kHz",
                        )
                        fig.savefig(filelistLabels[np.where(filelist_indice==ind)[0][0]][:-25]+'png/Figure_Sv_corr'+str(ind)+'_'+str(ind)+'.png')
                        plt.close()
                        
                        # Suppression des valeurs n'ayant pas de sens dans la latitude et la longitude
                        index = np.where((Lon[:, 0] == -200) & (Lat[:, 0] == -100))
                    
                        Lat_temp = np.delete(Lat_temp, (index), axis=0)
                        Lon_temp = np.delete(Lon_temp, (index), axis=0)
                    
                        labels = np.delete(labels, (index), axis=0)
                        Sa_moy = np.delete(Sa_moy, (index), axis=0)
                        a = np.where(Sa_moy == 0)
                        Sa_moy[a] = np.NaN

                    
                        Lat_moy = np.true_divide(Lat_temp.sum(1), (Lat_temp != 0).sum(1))
                        Lon_moy = np.true_divide(Lon_temp.sum(1), (Lon_temp != 0).sum(1))
                    
                        # définition des couches surface et fond agrégées pour relier au chalutage
                        surface_limit = 30
                        indice_surface = int(
                            np.floor((surface_limit - Depth[0, 0]) / H_couche)
                        ) 
                        
                        Sa_moy_label_surface = []
                        Sa_moy_label_fond = []
                        Sa_moy_label = []
                        Sa_moy_label_log = []
                        for f in range(len(freq_voulues)):
                            sa_temp=Sa_moy[f,:,:].T
                            for x in range(nombre_de_categories_sv):
                                Sa_label = np.zeros(sa_temp.shape)
                                Sa_label[:, :] = np.nan
                                index_lab = np.where(labels == x)
                                Sa_label[index_lab] = sa_temp[index_lab]
                                Sa_moy_label_surface.append(np.nansum(Sa_label[:, 0:indice_surface], axis=1))
                                Sa_moy_label_fond.append(np.nansum(Sa_label[:, indice_surface + 1 :], axis=1))
                                Sa_moy_label.append(np.nansum(Sa_label, axis=1))
                                Sa_moy_label_log.append(10 * np.log10(np.nanmean(Sa_label, axis=1)))
                        Sa_moy_label=np.array(Sa_moy_label)
                        Sa_moy_label=Sa_moy_label.reshape((len(freq_voulues),nombre_de_categories_sv,-1))
                        Sa_moy_label_log=np.array(Sa_moy_label_log)
                        Sa_moy_label_log=Sa_moy_label_log.reshape((len(freq_voulues),nombre_de_categories_sv,-1))
                        Sa_moy_label_fond=np.array(Sa_moy_label_fond)
                        Sa_moy_label_fond=Sa_moy_label_fond.reshape((len(freq_voulues),nombre_de_categories_sv,-1))
                        Sa_moy_label_surface=np.array(Sa_moy_label_surface)
                        Sa_moy_label_surface=Sa_moy_label_surface.reshape((len(freq_voulues),nombre_de_categories_sv,-1))                        
            
                        #les cercles n'apparaissent que pour les valeurs positives (faire coincider figure et legende en n'affichant que les resultats positifs)
                        sa_log=np.zeros((nbfreqs,nombre_de_categories_sv,Sa_moy_label_log.shape[2]))
                        for f in range(len(freq_voulues)):
                            for x in range(nombre_de_categories_sv):
                                for i in range(Sa_moy_label_log.shape[2]):
                                    if Sa_moy_label_log[f][x][i]>0:
                                        sa_log[f,x,i]=Sa_moy_label_log[f][x][i]+offset
                                    elif Sa_moy_label_log[f][x][i]<=0 and np.isnan(Sa_moy_label_log[f][x][i])==False:
                                        sa_log[f,x,i]=offset
                                     
                        ##################################################################################################
                        #
                        # Création de carte avec la librairie Cartopy : Carte simple , affichage dans les figures.
                        # Sa en valeurs naturelles
                        #
                        ##################################################################################################
                        ##################################################################################################
                        #
                        # Création de carte avec la librairie folium : Carte Openstreetmap, affichage dans un fichier HTML.
                        # Sa en Log
                        #
                        ##################################################################################################


                        plt.rc('legend',fontsize=15)    

                        for f in range(len(freq_voulues)):
                            if Sv_temp.shape[0]<pas:
                                 save_html_Sa=path_html+'portion_corr'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))
                                 save_fig_Sa=path_png+'portion_corr'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))
                                 save_fig_Salog=path_png+'portion_corr_LogSa'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))
                                 os.rename(filelistSa_moy[np.where(filelist_indice==ind)[0][0]],filelistSa_moy[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                                 save_Sa_moy=path_txt+'portion'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))+'Sa_moy.txt'
                            else:
                                 save_html_Sa=path_html+'portion_corr'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))
                                 save_fig_Sa=path_png+'portion_corr'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))
                                 save_fig_Salog=path_png+'portion_corr_LogSa'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))
                                 os.rename(filelistSa_moy[np.where(filelist_indice==ind)[0][0]],filelistSa_moy[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                                 save_Sa_moy=path_txt+'portion'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))+'Sa_moy.txt'
                            md.affichage_carte_france(nombre_de_categories, Lat_moy, Lon_moy, Sa_moy_label[f,:,:],'Sa', extension_zone, save_fig_Sa)
                            md.affichage_carte_france(nombre_de_categories, Lat_moy, Lon_moy, sa_log[f,:,:], extension_zone,'LogSa', save_fig_Salog)
                            # md.save_map_html(nombre_de_categories, Lat_moy, Lon_moy, Sa_moy_label_log[f,:,:], save_html_Sa)
                            np.savetxt(save_Sa_moy,Sa_moy[f,:,:])
                            
                            ##################################################################################################
                            #
                            # Export de Sa (en valeurs naturelles et log), Lat et Lon dans un fichier CSV
                            #
                            #################################################################################################
                        
                            for x in range(nombre_de_categories_sv):
                                with open(
                                    "%s/Sa_surface_label_%d_freq_%d.csv"
                                    % (path_csv, x, freq_custom_nearest[f]),
                                    "w",
                                    newline="",
                                ) as csvfile:
                                    writer = csv.writer(csvfile, delimiter=" ")
                                    for i in range(len(Lat_moy)):
                                        if Sa_moy_label_surface[f][x][i] > 0:
                                            writer.writerow(
                                                (
                                                    Time_temp[i].strftime("%d/%m/%Y %H:%M:%S"),
                                                    Lat_moy[i],
                                                    Lon_moy[i],
                                                    Sa_moy_label_surface[f][x][i],
                                                )
                                            )
                                            
                                            
                        
                            for x in range(nombre_de_categories_sv):
                                with open(
                                    "%s/Sa_fond_label_%d_freq_%d.csv"
                                    % (path_csv, x, freq_custom_nearest[f]),
                                    "w",
                                    newline="",
                                ) as csvfile:
                                    writer = csv.writer(csvfile, delimiter=" ")
                                    for i in range(len(Lat_moy)):
                                        if Sa_moy_label_fond[f][x][i] > 0:
                                            writer.writerow(
                                                (
                                                    Time_temp[i].strftime("%d/%m/%Y %H:%M:%S"),
                                                    Lat_moy[i],
                                                    Lon_moy[i],
                                                    Sa_moy_label_fond[f][x][i],
                                                )
                                            )
                                            
                        
                            for x in range(nombre_de_categories_sv):
                                with open(
                                    "%s/Sa_label_%d_freq_%d.csv" % (path_csv, x, freq_custom_nearest[f]),
                                    "w",
                                    newline="",
                                ) as csvfile:
                                    writer = csv.writer(csvfile, delimiter=" ")
                                    for i in range(len(Lat_moy)):
                                        if Sa_moy_label[f][x][i] > 0:
                                            writer.writerow(
                                                (
                                                    Time_temp[i].strftime("%d/%m/%Y %H:%M:%S"),
                                                    Lat_moy[i],
                                                    Lon_moy[i],
                                                    Sa_moy_label[f][x][i],
                                                )
                                            )                       

                        os.rename(filelistLat_moy[np.where(filelist_indice==ind)[0][0]],filelistLat_moy[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                        np.savetxt(filelistLat_moy[np.where(filelist_indice==ind)[0][0]],Lat_moy)
                        os.rename(filelistLon_moy[np.where(filelist_indice==ind)[0][0]],filelistLon_moy[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                        np.savetxt(filelistLon_moy[np.where(filelist_indice==ind)[0][0]],Lon_moy)
                        
                    else:
                        pass
                else:
                    print('no outliers found for cluster number '+str(i)+', no corrections needed')
                    save_corr=input('do you still want to apply some corrections on cluster number '+str(i)+' (y/n) ? \n')
                    if save_corr=='y':
                        Sv_temp_bis=np.empty(Sv_temp.shape)
                        Sv_temp_bis[:,:,:]=Sv_temp[:,:,:]
                        H_couche=(Depth[0, 1] - Depth[0, 0])
                        temp=ma.masked_object(Sv_temp,-10000000.0)
                        Sa_temp=10**(temp/10)*H_couche*1852**2*4*np.pi
                        Lat_temp_bis=np.empty(Lat_t.shape)
                        Lat_temp_bis[:,:,:]=Lat_t[:,:,:]
                        Lon_temp_bis=np.empty(Lon_t.shape)
                        Lon_temp_bis[:,:,:]=Lon_t[:,:,:]
                        Time_temp_bis=np.empty(Time_t.shape)
                        Time_temp_bis[:,:,:]=Time_t[:,:]
                        Sa_temp_bis=np.empty(Sa_temp.shape)
                        Sa_temp_bis[:,:,:]=Sa_temp[:,:,:]
                        for ind_i in np.where(labels_copy==i)[0]:
                            for ind_j in np.where(labels_copy==i)[1]:
                                Sv_temp_bis[ind_i,ind_j,:]=-9999999.9
                                Sa_temp_bis[ind_i,ind_j,:]=-9999999.9
                                Lat_temp_bis[ind_i,ind_j,:]=-9999999.9
                                Lon_temp_bis[ind_i,ind_j,:]=-9999999.9
                                Time_temp_bis[ind_i,ind_j]=-9999999.9
                        
                        os.rename(filelistCluster[np.where(filelist_indice==ind)[0][0]],filelistCluster[np.where(filelist_indice==ind)[0][0]][:-7]+'_old'+str(i)+'.pickle')
                        filename_clustering_EK80_corr=filelistCluster[np.where(filelist_indice==ind)[0][0]]          
                        classif.clustering_netcdf(
                        Sv_temp_bis,
                        freq_MFR,
                        nombre_de_categories_sv,
                        Time_temp_bis,
                        Depth,
                        Lat_temp_bis,
                        Lon_temp_bis,
                        nb_transduc,
                        nom_numero_radiales,
                        methode_clustering,
                        nombre_composantes_global,
                        nombre_voisins_global,
                        save=save_data,
                        filename=filename_clustering_EK80_corr,
                        )
                
                        with open(filename_clustering_EK80_corr, "rb") as f_corr:
                            (
                                kmeans_corr,
                                labels_corr,
                                Q1_corr,
                                Q3_corr,
                                med_corr,
                                nombre_de_categories,
                                Sv_temp_bis,
                                Time_temp_bis,
                                freq_MFR,
                                Depth,
                                Lat_temp_bis,
                                Lon_temp_bis,
                                nb_transduc,
                                nom_numero_radiales,
                                methode_clustering,
                            ) = pickle.load(f_corr)                      
        
                
                        #on affiche les résultats de la classification corrigée
                        # # Affichage de la classification et de la réponse fréquentielle par clusters 
                        x=np.arange(0,len(Sv_temp_bis),1)
                        y=np.arange(-Depth[1,0],-Depth[1,(len(Depth[1])-1)]-1,-1)
                        fig,ax2 = plt.subplots(figsize=(30, 15))
                        #image de classification
                        plt.subplot(211)
                        plt.xlabel('N*ESU')
                        plt.ylabel('profondeur (en m)')
                        plt.title('Portion corrigée des possibles outliers')
                        plt.pcolormesh(x,y,np.transpose(labels_corr), cmap=cmap)
                        plt.grid()
                        plt.colorbar()
                        plt.show()
                        
                        #réponse en fréquence
                        plt.subplot(212)
                        plt.xlabel('Freq')
                        plt.ylabel('Sv dB')
                        plt.title('Reponse en frequence par cluster')
                        for j in range(nombre_de_categories_sv):
                            plt.plot(freq_MFR[:indice_freq_max],Q1_corr[j],'-.',color=couleurs[j])
                            plt.plot(freq_MFR[:indice_freq_max],Q3_corr[j],'-.',color=couleurs[j])
                            plt.plot(freq_MFR[:indice_freq_max],med_corr[j],'o-',color=couleurs[j])
                        plt.grid()
                        plt.pause(interval)
                        plt.show()
                        plt.close()
                
                        #l'utilisateur décide si ces corrections doivent être conservées et appliquées à tous les fichiers
                        #les anciens fichiers sont conservés mais renommés _old.extension
                        save_corr=input("do you want to apply corrections ? \n")
                        if save_corr=='y':
                            os.rename(filelistLabels[np.where(filelist_indice==ind)[0][0]],filelistLabels[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                            np.savetxt(filelistLabels[np.where(filelist_indice==ind)[0][0]],labels_corr)
                            os.rename(filelistQ1_[np.where(filelist_indice==ind)[0][0]],filelistQ1_[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                            np.savetxt(filelistQ1_[np.where(filelist_indice==ind)[0][0]],Q1_corr)
                            os.rename(filelistQ3_[np.where(filelist_indice==ind)[0][0]],filelistQ3_[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                            np.savetxt(filelistQ3_[np.where(filelist_indice==ind)[0][0]],Q3_corr)
                            os.rename(filelistMed[np.where(filelist_indice==ind)[0][0]],filelistMed[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                            np.savetxt(filelistMed[np.where(filelist_indice==ind)[0][0]],med_corr)   
                            fig.savefig(filelistLabels[np.where(filelist_indice==ind)[0][0]][:-25]+'png/Figure_clustering_corr'+str(ind)+'_'+str(ind)+'.png')
   
                            Sa_affiche = []
                            Sv_affiche = []
                            freqs_affichage = []
                            for f in range(len(freq_voulues)):
                                indice = np.where(freq_MFR == freq_voulues[f])
                                Sv_affiche.append(np.transpose(np.squeeze(Sv_temp_bis[:, :, indice])))
                                if methode_EI=='pickle':
                                    Sa_moy = np.squeeze(Sa_temp_bis[:, :, f])
                                else:
                                    Sa_affiche.append(np.transpose(np.squeeze(Sa_temp_bis[:, :, indice])))
                                    Lat_temp=Lat_temp_bis[:,:,f]
                                    Lon_temp=Lon_temp_bis[:,:,f]
                                    Time_temp=Time_temp_bis[:,f]
                                Time_temp=au.hac_read_day_time(Time_temp)
                                freqs_affichage.append((freq_voulues[f]) / 1000)
                            Sv_affiche = np.array(Sv_affiche)
                            Sa_affiche = np.array(Sa_affiche)
                            fig, ax3 = plt.subplots(figsize=(24, 35))
                            dr.DrawMultiEchoGram(
                                Sv_affiche,
                                -Depth,
                                len(freq_voulues),
                                freqs=freqs_affichage,
                                title="Sv sondeur %d kHz",
                            )
                            fig.savefig(filelistLabels[np.where(filelist_indice==ind)[0][0]][:-25]+'png/Figure_Sv_corr'+str(ind)+'_'+str(ind)+'.png')
                            plt.close()
                        
                            # Suppression des valeurs n'ayant pas de sens dans la latitude et la longitude
                            index = np.where((Lon[:, 0] == -200) & (Lat[:, 0] == -100))
                        
                            Lat_temp = np.delete(Lat_temp, (index), axis=0)
                            Lon_temp = np.delete(Lon_temp, (index), axis=0)
                        
                            labels = np.delete(labels, (index), axis=0)
                            Sa_moy = np.delete(Sa_moy, (index), axis=0)
                            a = np.where(Sa_moy == 0)
                            Sa_moy[a] = np.NaN

                        
                            Lat_moy = np.true_divide(Lat_temp.sum(1), (Lat_temp != 0).sum(1))
                            Lon_moy = np.true_divide(Lon_temp.sum(1), (Lon_temp != 0).sum(1))
                        
                            # définition des couches surface et fond agrégées pour relier au chalutage
                            surface_limit = 30
                            indice_surface = int(
                                np.floor((surface_limit - Depth[0, 0]) / H_couche)
                            ) 
                            
                            Sa_moy_label_surface = []
                            Sa_moy_label_fond = []
                            Sa_moy_label = []
                            Sa_moy_label_log = []
                            for f in range(len(freq_voulues)):
                                sa_temp=Sa_affiche[f,:,:].T
                                for x in range(nombre_de_categories_sv):
                                    Sa_label = np.zeros(sa_temp.shape)
                                    Sa_label[:, :] = np.nan
                                    index_lab = np.where(labels == x)
                                    Sa_label[index_lab] = sa_temp[index_lab]
                                    Sa_moy_label_surface.append(np.nansum(Sa_label[:, 0:indice_surface], axis=1))
                                    Sa_moy_label_fond.append(np.nansum(Sa_label[:, indice_surface + 1 :], axis=1))
                                    Sa_moy_label.append(np.nansum(Sa_label, axis=1))
                                    Sa_moy_label_log.append(10 * np.log10(np.nanmean(Sa_label, axis=1)))
                            Sa_moy_label=np.array(Sa_moy_label)
                            Sa_moy_label=Sa_moy_label.reshape((len(freq_voulues),nombre_de_categories_sv,-1))
                            Sa_moy_label_log=np.array(Sa_moy_label_log)
                            Sa_moy_label_log=Sa_moy_label_log.reshape((len(freq_voulues),nombre_de_categories_sv,-1))
                            Sa_moy_label_fond=np.array(Sa_moy_label_fond)
                            Sa_moy_label_fond=Sa_moy_label_fond.reshape((len(freq_voulues),nombre_de_categories_sv,-1))
                            Sa_moy_label_surface=np.array(Sa_moy_label_surface)
                            Sa_moy_label_surface=Sa_moy_label_surface.reshape((len(freq_voulues),nombre_de_categories_sv,-1))                        
                
                            #les cercles n'apparaissent que pour les valeurs positives (faire coincider figure et legende en n'affichant que les resultats positifs)
                            sa_log=np.zeros((nbfreqs,nombre_de_categories_sv,Sa_moy_label_log.shape[2]))
                            for f in range(len(freq_voulues)):
                                for x in range(nombre_de_categories_sv):
                                    for i in range(Sa_moy_label_log.shape[2]):
                                        if Sa_moy_label_log[f][x][i]>0:
                                            sa_log[f,x,i]=Sa_moy_label_log[f][x][i]+offset
                                        elif Sa_moy_label_log[f][x][i]<=0 and np.isnan(Sa_moy_label_log[f][x][i])==False:
                                            sa_log[f,x,i]=offset
                                            
                                            
                                            
                            ##################################################################################################
                            #
                            # Création de carte avec la librairie Cartopy : Carte simple , affichage dans les figures.
                            # Sa en valeurs naturelles
                            #
                            ##################################################################################################
                            ##################################################################################################
                            #
                            # Création de carte avec la librairie folium : Carte Openstreetmap, affichage dans un fichier HTML.
                            # Sa en Log
                            #
                            ##################################################################################################


                            plt.rc('legend',fontsize=15)    

                            for f in range(len(freq_voulues)):
                                if Sv_temp.shape[0]<pas:
                                     save_html_Sa=path_html+'portion_corr'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))
                                     save_fig_Sa=path_png+'portion_corr'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))
                                     save_fig_Salog=path_png+'portion_corr_LogSa'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))
                                     os.rename(filelistSa_moy[np.where(filelist_indice==ind)[0][0]],filelistSa_moy[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                                     save_Sa_moy=path_txt+'portion'+str(len(Sv))+'_'+str(np.round(freq_voulues[f]/1000))+'Sa_moy.txt'
                                else:
                                     save_html_Sa=path_html+'portion_corr'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))
                                     save_fig_Sa=path_png+'portion_corr'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))
                                     save_fig_Salog=path_png+'portion_corr_LogSa'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))
                                     os.rename(filelistSa_moy[np.where(filelist_indice==ind)[0][0]],filelistSa_moy[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                                     save_Sa_moy=path_txt+'portion'+str(k+pas)+'_'+str(np.round(freq_voulues[f]/1000))+'Sa_moy.txt'
                                md.affichage_carte_france(nombre_de_categories, Lat_moy, Lon_moy, Sa_moy_label[f,:,:],'Sa', extension_zone, save_fig_Sa)
                                md.affichage_carte_france(nombre_de_categories, Lat_moy, Lon_moy, sa_log[f,:,:],'LogSa', extension_zone, save_fig_Salog)
                                # md.save_map_html(nombre_de_categories, Lat_moy, Lon_moy, Sa_moy_label_log[f,:,:], save_html_Sa)
                                np.savetxt(save_Sa_moy,Sa_affiche[f,:,:])
                                
                                ##################################################################################################
                                #
                                # Export de Sa (en valeurs naturelles et log), Lat et Lon dans un fichier CSV
                                #
                                #################################################################################################
                            
                                for x in range(nombre_de_categories_sv):
                                    with open(
                                        "%s/Sa_surface_label_%d_freq_%d.csv"
                                        % (path_csv, x, freq_custom_nearest[f]),
                                        "w",
                                        newline="",
                                    ) as csvfile:
                                        writer = csv.writer(csvfile, delimiter=" ")
                                        for i in range(len(Lat_moy)):
                                            if Sa_moy_label_surface[f][x][i] > 0:
                                                writer.writerow(
                                                    (
                                                        Time_temp[i].strftime("%d/%m/%Y %H:%M:%S"),
                                                        Lat_moy[i],
                                                        Lon_moy[i],
                                                        Sa_moy_label_surface[f][x][i],
                                                    )
                                                )
                                                
                                                
                            
                                for x in range(nombre_de_categories_sv):
                                    with open(
                                        "%s/Sa_fond_label_%d_freq_%d.csv"
                                        % (path_csv, x, freq_custom_nearest[f]),
                                        "w",
                                        newline="",
                                    ) as csvfile:
                                        writer = csv.writer(csvfile, delimiter=" ")
                                        for i in range(len(Lat_moy)):
                                            if Sa_moy_label_fond[f][x][i] > 0:
                                                writer.writerow(
                                                    (
                                                        Time_temp[i].strftime("%d/%m/%Y %H:%M:%S"),
                                                        Lat_moy[i],
                                                        Lon_moy[i],
                                                        Sa_moy_label_fond[f][x][i],
                                                    )
                                                )
                                                
                            
                                for x in range(nombre_de_categories_sv):
                                    with open(
                                        "%s/Sa_label_%d_freq_%d.csv" % (path_csv, x, freq_custom_nearest[f]),
                                        "w",
                                        newline="",
                                    ) as csvfile:
                                        writer = csv.writer(csvfile, delimiter=" ")
                                        for i in range(len(Lat_moy)):
                                            if Sa_moy_label[f][x][i] > 0:
                                                writer.writerow(
                                                    (
                                                        Time_temp[i].strftime("%d/%m/%Y %H:%M:%S"),
                                                        Lat_moy[i],
                                                        Lon_moy[i],
                                                        Sa_moy_label[f][x][i],
                                                    )
                                                )                       

                            os.rename(filelistLat_moy[np.where(filelist_indice==ind)[0][0]],filelistLat_moy[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                            np.savetxt(filelistLat_moy[np.where(filelist_indice==ind)[0][0]],Lat_moy)
                            os.rename(filelistLon_moy[np.where(filelist_indice==ind)[0][0]],filelistLon_moy[np.where(filelist_indice==ind)[0][0]][:-4]+'_old'+str(i)+'.txt')
                            np.savetxt(filelistLon_moy[np.where(filelist_indice==ind)[0][0]],Lon_moy)

                    else:
                        Sv_temp_bis=Sv_temp
        else:
            pass
    else:
        pass
    


################################################################################
# %% CLUSTERING OF FREQUENCY RESPONSES PER batch AFTER CORRECTION
################################################################################
"""
Recombinaison des portions de la campagne après correction et classification 
de la donnée par regroupement des RF semblables en clusters  
"""
    
if correction & recombinaison:
    
    len_Sv1=np.loadtxt(fpath+'Profondeur_max.txt',dtype='int')
    indice_profondeur_max = np.max(len_Sv1)
    
    for root, subFolders, files in os.walk(fpath):
        for file in files:
            if methode_EI=='pickle':
                if file.endswith("saved_clustering_for_EK80_all.pickle"):
                    with open(root+'/'+file, "rb") as f:
                        (
                            freq_MFR,
                            freqs_moy_transducteur,
                            Sv,
                            Sv_moy_transducteur_x,
                            Sa,
                            Depth,
                            Lat,
                            Lon,
                            Time,
                            nb_transduc,
                            tableau_radiales,
                            nom_numero_radiales,
                        ) = pickle.load(f) 
                    break
            else:
                if file.endswith("1.xsf.nc"):
                    dataset=nc.Dataset(root+'/'+file)
                    Sv=dataset.groups['Sonar'].groups['Gridded_1'].variables['integrated_backscatter'][:]
                    freq_MFR=dataset.groups['Sonar'].groups['Gridded_1'].variables['frequency'][:]
                    freq_MFR=np.sort(freq_MFR)
                    Time=dataset.groups['Sonar'].groups['Gridded_1'].variables['cell_ping_time'][:]
                    Depth=dataset.groups['Sonar'].groups['Gridded_1'].variables['cell_depth'][:]
                    Depth=Depth.T
                    Lat=dataset.groups['Sonar'].groups['Gridded_1'].variables['cell_latitude'][:]
                    Lon=dataset.groups['Sonar'].groups['Gridded_1'].variables['cell_longitude'][:]
                    break
                break
            break
        break
    
    dirlistQ1_ = []
    dirlistQ3_ = []
    dirlistMed = []
    dirlistLabels=[]
    dirlistSa_moy=[]
    dirlistLat_moy=[]
    dirlistLon_moy=[]
    for root, subFolders, files in os.walk(fpath):
        print(root,subFolders,files)
        for file in files:
            print(file)
            if file.endswith("Q1.txt"):
                dirlistQ1_.append(os.path.join(root, file))
            elif file.endswith("Q3.txt"):
                dirlistQ3_.append(os.path.join(root, file))
            elif file.endswith("Med.txt"):
                dirlistMed.append(os.path.join(root, file))
            elif file.endswith("Labels.txt"):
                dirlistLabels.append(os.path.join(root, file))
            elif file.endswith("Sa_moy.txt"):
                dirlistSa_moy.append(os.path.join(root, file))
            elif file.endswith("Lat_moy.txt"):
                dirlistLat_moy.append(os.path.join(root, file))
            elif file.endswith("Lon_moy.txt"):
                dirlistLon_moy.append(os.path.join(root, file))
                
                
    filelistQ1_ = sorted_alphanumeric(dirlistQ1_)
    filelistQ3_ = sorted_alphanumeric(dirlistQ3_)
    filelistMed = sorted_alphanumeric(dirlistMed)
    filelistLabels = sorted_alphanumeric(dirlistLabels)
    filelistSa_moy = sorted_alphanumeric(dirlistSa_moy)
    filelistLat_moy = sorted_alphanumeric(dirlistLat_moy)
    filelistLon_moy = sorted_alphanumeric(dirlistLon_moy)
    
    Q1_tot=np.loadtxt(filelistQ1_[0])
    Q3_tot=np.loadtxt(filelistQ3_[0])
    med_tot=np.loadtxt(filelistMed[0])
    labels_tot=np.loadtxt(filelistLabels[0])
    if labels_tot.shape[1]<indice_profondeur_max:
        blanc = np.zeros((len(labels_tot),indice_profondeur_max-labels_tot.shape[1]))
        blanc.fill(np.nan)
        labels_tot=np.hstack((labels_tot,blanc))
    for i in range(1,len(filelistQ1_)):
        Q1_tot=np.vstack((Q1_tot,np.loadtxt(filelistQ1_[i])))
        Q3_tot=np.vstack((Q3_tot,np.loadtxt(filelistQ3_[i])))
        med_tot=np.vstack((med_tot,np.loadtxt(filelistMed[i])))    
        labels=np.loadtxt(filelistLabels[i])
        if labels.shape[1]<indice_profondeur_max:
            blanc = np.zeros((len(labels),indice_profondeur_max-labels.shape[1]))
            blanc.fill(np.nan)
            labels=np.hstack((labels,blanc))
        labels_tot=np.vstack((labels_tot,labels))
        
    Lat_tot=[]
    Lon_tot=[]
    for i in range(len(filelistLat_moy)):
        Lat_tot=np.append(Lat_tot,np.loadtxt(filelistLat_moy[i]))
        Lon_tot=np.append(Lon_tot,np.loadtxt(filelistLon_moy[i]))
    
    
    Sa_moy_tot=[]
    for i in range(len(filelistSa_moy)):
        Sa=np.loadtxt(filelistSa_moy[i])
        Sa_moy_tot.append(Sa)
        
    if nombre_composantes_global>len(freq_MFR):
        raise ValueError('Nombre de composantes trop élevé par rapport au nombre de fréquences')
    elif nombre_voisins_global<nombre_composantes_global:
        raise ValueError('Nombre de voisins trop faible par rapport au nombre de composantes')
    else:
        lle = LocallyLinearEmbedding(n_components=nombre_composantes_global, n_neighbors = nombre_voisins_global,method = 'modified', n_jobs = 4,  random_state=0)
        lle.fit(med_tot)
        med_lle = lle.transform(med_tot)
    
    plt.figure()
    sc.dendrogram(sc.linkage(med_lle,method='ward'))
    
    plt.savefig(fpath+'Dendogramme_campagne_totale_corr.png')
    
    
    kmeans_sv=AgglomerativeClustering(n_clusters=nombre_de_categories_sv_campEntiere,affinity='euclidean',linkage='ward').fit(med_lle)
    
         
    couleurs = plt.cm.jet(np.linspace(0,1,nombre_de_categories_sv_campEntiere))
    cmap = colors.ListedColormap(couleurs) 
    
    plt.figure(figsize=(30,15))
    for x in np.unique(kmeans_sv.labels_):
        indice = np.where(kmeans_sv.labels_ == x)
        plt.plot(freq_MFR[:indice_freq_max], np.mean( Q1_tot[indice], axis=0),'-.',color=couleurs[x])
        plt.plot(freq_MFR[:indice_freq_max], np.mean( Q3_tot[indice], axis=0),'-.',color=couleurs[x])
        plt.plot(freq_MFR[:indice_freq_max], np.mean( med_tot[indice], axis=0),'o-',color=couleurs[x],label="Classe"+str(x))
    plt.legend()
    
    plt.savefig(fpath+'ClustersRF_campagne_totale_corr.png')
    
    kmeans_sv.labels_new=kmeans_sv.labels_+nombre_de_categories_sv+1
    labels_tot_new=np.where(labels_tot==nombre_de_categories_sv,np.max(kmeans_sv.labels_new)+1,labels_tot)
    
    unites_float= np.loadtxt(fpath+'UnitesDecoupageCampagne.txt',dtype='double')
    unites=np.int_(unites_float)
    len_Sv0_ent_tot_final=np.cumsum(unites)  
               
    j=0
    for i in range(nombre_de_categories_sv):
        labels_tot_new[:len_Sv0_ent_tot_final[j],:]=np.where(labels_tot_new[:len_Sv0_ent_tot_final[j],:]==i,kmeans_sv.labels_new[i+j],labels_tot_new[:len_Sv0_ent_tot_final[j],:])       
    
    kmeans_sv.labels_new_shape=kmeans_sv.labels_new.reshape((len(len_Sv0_ent_tot_final),nombre_de_categories_sv))     
    for j in range(1,len(len_Sv0_ent_tot_final)):
        for i in range(nombre_de_categories_sv):
            labels_tot_new[len_Sv0_ent_tot_final[j-1]:len_Sv0_ent_tot_final[j],:]=np.where(labels_tot_new[len_Sv0_ent_tot_final[j-1]:len_Sv0_ent_tot_final[j],:]==i,kmeans_sv.labels_new_shape[j,i],labels_tot_new[len_Sv0_ent_tot_final[j-1]:len_Sv0_ent_tot_final[j],:])       
             
    labels_tot_new=labels_tot_new-(nombre_de_categories_sv+1)
          
    couleurs = plt.cm.jet(np.linspace(0,1,nombre_de_categories_sv_campEntiere))
    cmap = colors.ListedColormap(couleurs) 
    
    x=np.arange(0,len_Sv0_ent_tot_final[-1],1)
    y=np.arange(-Depth[1,0],-Depth[1,(len(Depth[1])-1)]-1,-1)
    
    plt.figure(figsize=(30,15))
    plt.xlabel('N*ESU')
    plt.ylabel('profondeur (en m)')
    plt.title(' ')
    plt.pcolormesh(x,y,np.transpose(labels_tot_new), cmap=cmap)
    plt.grid()
    plt.colorbar()
    plt.show()
    
    plt.savefig(fpath+'Echogramme_clustering_campagne_totale_corr.png')
    
    
    Sa_moy_label_tot = []
    Sa_moy_label_log_tot = []
    u=0
    for f in range(nb_transduc,len(Sa_moy_tot)+1,nb_transduc):
        #print('boucle sur f:',f)
        sa_temp=np.array(Sa_moy_tot[f-nb_transduc:f])
        #print(sa_temp.shape)
        if np.cumsum(unites)[u]<=pas:
            unit0=0
            unit1=unites[0]
        else:
            unit0=np.cumsum(unites)[u-1]
            unit1=np.cumsum(unites)[u]
        # print('unit0',unit0)
        # print('unit1',unit1)
        for i in range(nb_transduc):
            #print('boucle sur i:',i)
            sa_t=sa_temp[i,:,:].T
            #print(sa_t.shape)
            for x in range(nombre_de_categories_sv_campEntiere):
                #print('boucle sur x:',x)
                Sa_label = np.zeros(sa_t.shape)
                Sa_label[:, :] = np.nan
                #print(Sa_label.shape)
                index_lab = np.where(labels_tot_new[unit0:unit1] == x)
                Sa_label[index_lab] = sa_t[index_lab]
                Sa_moy_label_tot.append(np.nansum(Sa_label, axis=1))
                Sa_moy_label_log_tot.append(10 * np.log10(np.nanmean(Sa_label, axis=1)))
        u+=1
         
          
    reindexage=np.arange(0,len(Sa_moy_label_tot),nb_transduc*nombre_de_categories_sv_campEntiere)
    Sa_moy_label_tot_final=Sa_moy_label_tot[0]
    Sa_moy_label_log_tot_final=Sa_moy_label_log_tot[0]
    for j in range(nb_transduc*nombre_de_categories_sv_campEntiere):
        for i in range(len(reindexage)):
            #print(reindexage[i])
            Sa_moy_label_tot_final=np.hstack((Sa_moy_label_tot_final,Sa_moy_label_tot[reindexage[i]]))
            Sa_moy_label_log_tot_final=np.hstack((Sa_moy_label_log_tot_final,Sa_moy_label_log_tot[reindexage[i]]))
        reindexage+=1
    Sa_moy_label_tot_final=Sa_moy_label_tot_final[unites[0]:]
    Sa_moy_label_tot_final=Sa_moy_label_tot_final.reshape((len(freq_voulues),nombre_de_categories_sv_campEntiere,-1))  
    Sa_moy_label_log_tot_final=Sa_moy_label_log_tot_final[unites[0]:]
    Sa_moy_label_log_tot_final=Sa_moy_label_log_tot_final.reshape((len(freq_voulues),nombre_de_categories_sv_campEntiere,-1))  
    
    #les cercles n'apparaissent que pour les valeurs positives (faire coincider figure et legende en n'affichant que les resultats positifs)
    sa_log=np.zeros((nbfreqs,nombre_de_categories_sv_campEntiere,Sa_moy_label_log_tot_final.shape[2]))
    for f in range(len(freq_voulues)):
        for x in range(nombre_de_categories_sv_campEntiere):
            for i in range(Sa_moy_label_log_tot_final.shape[2]):
                if Sa_moy_label_log_tot_final[f][x][i]>0:
                    sa_log[f,x,i]=Sa_moy_label_log_tot_final[f][x][i]+offset
                elif Sa_moy_label_log_tot_final[f][x][i]<=0 and np.isnan(Sa_moy_label_log_tot_final[f][x][i])==False:
                    sa_log[f,x,i]=offset
                    
                    
                 
    plt.rc('legend',fontsize=15)    
    
    ##################################################################################################
    #
    # Création de carte avec la librairie Cartopy : Carte simple , affichage dans les figures.
    # Sa en valeurs naturelles
    #
    ##################################################################################################
    ##################################################################################################
    #
    # Création de carte avec la librairie folium : Carte Openstreetmap, affichage dans un fichier HTML.
    # Sa en Log
    #
    ##################################################################################################
    
    for f in range(len(freq_voulues)):   
        md.affichage_carte_france(
            nombre_de_categories_sv_campEntiere, Lat_tot, Lon_tot, sa_log[f,:,:], extension_zone,'LogSa', fpath+'LogSa_corr'+str(np.round(freq_voulues[f]/1000))
        )
            
        md.affichage_carte_france(
            nombre_de_categories_sv_campEntiere, Lat_tot, Lon_tot, Sa_moy_label_tot_final[f,:,:], extension_zone,'Sa', fpath+'corr'+str(np.round(freq_voulues[f]/1000))
        )
    
        # md.save_map_html(
        #     nombre_de_categories_sv_campEntiere, Lat_tot, Lon_tot, Sa_moy_label_log_tot_final[f,:,:], fpath+'corr'+str(np.round(freq_voulues[f]/1000))
        # )
    if (0):
        nb_radiales=[]
        compt=0
        heured=np.array([])
        heuref=np.array([])
        for u in range(len(unites)):
            if np.size(datedebutini)>1:
                heured=np.append(heured,heuredebutini[indexd])
                heuref=np.append(heuref,heurefinini[indexd])
            else:
                heured=np.append(heured,heuredebutini)
                heuref=np.append(heuref,heurefinini)
                    
                    
        Sa_jour=np.array([])
        Sa_nuit=np.array([])
        Sa_jour_log=np.array([])
        Sa_nuit_log=np.array([])
        Lat_jour=np.array([])
        Lat_nuit=np.array([])
        Lon_jour=np.array([])
        Lon_nuit=np.array([])
        for f in range(len(freq_voulues)):
            for x in range(nombre_de_categories_sv_campEntiere):
                for u in range(len(unites)):
                    if heured[u]>'04:00:00' and heuref[u]<'21:00:00': #Temps UTC+2
                        if u==0:
                            Sa_jour=np.append(Sa_jour,Sa_moy_label_tot_final[f][x][u:np.cumsum(unites[u])[-1]])
                            Sa_jour_log=np.append(Sa_jour_log,Sa_moy_label_log_tot_final[f][x][u:np.cumsum(unites[u])[-1]])
                            Lat_jour=np.append(Lat_jour,Lat_tot[u:np.cumsum(unites[u])[-1]])
                            Lon_jour=np.append(Lon_jour,Lon_tot[u:np.cumsum(unites[u])[-1]])
                        else:
                            Sa_jour=np.append(Sa_jour,Sa_moy_label_tot_final[f][x][np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                            Sa_jour_log=np.append(Sa_jour_log,Sa_moy_label_log_tot_final[f][x][np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                            Lat_jour=np.append(Lat_jour,Lat_tot[np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                            Lon_jour=np.append(Lon_jour,Lon_tot[np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                    else:
                        if u==0:
                            Sa_nuit=np.append(Sa_nuit,Sa_moy_label_tot_final[f][x][u:np.cumsum(unites[u])[-1]])
                            Sa_nuit_log=np.append(Sa_nuit_log,Sa_moy_label_log_tot_final[f][x][u:np.cumsum(unites[u])[-1]])
                            Lat_nuit=np.append(Lat_nuit,Lat_tot[u:np.cumsum(unites[u])[-1]])
                            Lon_nuit=np.append(Lon_nuit,Lon_tot[u:np.cumsum(unites[u])[-1]])
                        else:
                            Sa_nuit=np.append(Sa_nuit,Sa_moy_label_tot_final[f][x][np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                            Sa_nuit_log=np.append(Sa_nuit_log,Sa_moy_label_log_tot_final[f][x][np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                            Lat_nuit=np.append(Lat_nuit,Lat_tot[np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])
                            Lon_nuit=np.append(Lon_nuit,Lon_tot[np.cumsum(unites[:u])[-1]:np.cumsum(unites[:u+1])[-1]])           
        Sa_jour=Sa_jour.reshape((nbfreqs,nombre_de_categories_sv_campEntiere,-1))
        Sa_nuit=Sa_nuit.reshape((nbfreqs,nombre_de_categories_sv_campEntiere,-1))
        Sa_jour_log=Sa_jour_log.reshape((nbfreqs,nombre_de_categories_sv_campEntiere,-1))
        Sa_nuit_log=Sa_nuit_log.reshape((nbfreqs,nombre_de_categories_sv_campEntiere,-1))
        ind_lat_j=int(Lat_jour.shape[0]/(nombre_de_categories_sv_campEntiere*nbfreqs))
        ind_lat_n=int(Lat_nuit.shape[0]/(nombre_de_categories_sv_campEntiere*nbfreqs))
        ind_lon_j=int(Lon_jour.shape[0]/(nombre_de_categories_sv_campEntiere*nbfreqs))
        ind_lon_n=int(Lon_nuit.shape[0]/(nombre_de_categories_sv_campEntiere*nbfreqs))
        Lat_jour=Lat_jour[:ind_lat_j]
        Lat_nuit=Lat_nuit[:ind_lat_n]
        Lon_jour=Lon_jour[:ind_lon_j]
        Lon_nuit=Lon_nuit[:ind_lon_n]
        
        #les cercles n'apparaissent que pour les valeurs positives (faire coincider figure et legende en n'affichant que les resultats positifs)
        sa_log_jour=np.zeros((nbfreqs,nombre_de_categories_sv_campEntiere,Sa_jour_log.shape[2]))
        for f in range(len(freq_voulues)):
            for x in range(nombre_de_categories_sv_campEntiere):
                for i in range(Sa_jour_log.shape[2]):
                    if Sa_jour_log[f][x][i]>0:
                        sa_log_jour[f,x,i]=Sa_jour_log[f][x][i]+offset
                    elif Sa_jour_log[f][x][i]<=0 and np.isnan(Sa_jour_log[f][x][i])==False:
                        sa_log_jour[f,x,i]=offset
                        
        #les cercles n'apparaissent que pour les valeurs positives (faire coincider figure et legende en n'affichant que les resultats positifs)
        sa_log_nuit=np.zeros((nbfreqs,nombre_de_categories_sv_campEntiere,Sa_nuit_log.shape[2]))
        for f in range(len(freq_voulues)):
            for x in range(nombre_de_categories_sv_campEntiere):
                for i in range(Sa_nuit_log.shape[2]):
                    if Sa_nuit_log[f][x][i]>0:
                        sa_log_nuit[f,x,i]=Sa_nuit_log[f][x][i]+offset
                    elif Sa_nuit_log[f][x][i]<=0 and np.isnan(Sa_nuit_log[f][x][i])==False:
                        sa_log_nuit[f,x,i]=offset
                        
        ##################################################################################################
        #
        # Création de carte avec la librairie Cartopy : Carte simple , affichage dans les figures.
        # Sa en valeurs naturelles
        #
        ##################################################################################################
        ##################################################################################################
        #
        # Création de carte avec la librairie folium : Carte Openstreetmap, affichage dans un fichier HTML.
        # Sa en Log
        #
        ##################################################################################################
        
        for f in range(len(freq_voulues)):
            #Affichage des cartes de Sa et de log(Sa) par label et par période jour/nuit
            md.affichage_carte_france(
                nombre_de_categories_sv_campEntiere, Lat_jour, Lon_jour, Sa_jour[f,:,:], extension_zone,'Sa', fpath+str(np.round(freq_voulues[f]/1000))+'jour_corr'
            )
        
            # md.save_map_html(
            #     nombre_de_categories_sv_campEntiere, Lat_jour, Lon_jour, Sa_jour_log[f,:,:], fpath+str(np.round(freq_voulues[f]/1000))+'jour_corr'
            # )
            
            md.affichage_carte_france(
                nombre_de_categories_sv_campEntiere,Lat_jour, Lon_jour, sa_log_jour[f,:,:], extension_zone,'LogSa', fpath+'LogSa_jour_corr'+str(np.round(freq_voulues[f]/1000))
            )
        
            if Sa_nuit.shape[-1]!=0:
                md.affichage_carte_france(
                    nombre_de_categories_sv_campEntiere, Lat_nuit, Lon_nuit, Sa_nuit[f,:,:], extension_zone,'Sa', fpath+str(np.round(freq_voulues[f]/1000))+'nuit_corr'
                )
            
                # md.save_map_html(
                #     nombre_de_categories_sv_campEntiere, Lat_nuit, Lon_nuit, Sa_nuit_log[f,:,:], fpath+str(np.round(freq_voulues[f]/1000))+'nuit_corr'
                # )
                
                md.affichage_carte_france(
                nombre_de_categories_sv_campEntiere,Lat_nuit, Lon_nuit, sa_log_nuit[f,:,:], extension_zone,'LogSa', fpath+'LogSa_nuit_corr'+str(np.round(freq_voulues[f]/1000))
                )
            else:
                pass
    else:
        pass
    
        
        
        
        
        
    
    
    
