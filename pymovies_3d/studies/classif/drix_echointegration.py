# -*- coding: utf-8 -*-
"""
Created on Tue Feb 28 11:27:23 2023

@author: mcambrel
"""
import netCDF4 as nc
import pandas as pd
import pymovies_3d.core.echointegration.ei_survey as ei
import os
import numpy as np

# %% 1. Mettre en forme le 'jackpot'
# Fonction pour convertir une date au format "AAAA.MM.JJ" en "JJ/MM/AAAA"
def convert_date(date):
    new_date = date.split('.')
    new_date = new_date[2] + '/' + new_date[1] + '/' + new_date[0]
    return new_date

datedebut = []
end_dates = []
heuredebut = []
end_times = []
pref='C:/Users/lberger/Desktop/DELMOGES/'
# Lire le fichier CSV
path_evt=pref+'Casino/suiviDrixDELMOGES_echotypage_tot.csv'
df = pd.read_csv(path_evt, sep=',')

# Borner le fichier si besoin
# df = df.loc[df['Date'] < '2023.02.12']
df = df.loc[df['Date'] > '2023.02.14'] # juste le parcours 2

# Filtrer les lignes où la colonne "Opération" est égale à "DEBRAD" ou "FINRAD"
debrad_reprad = df[(df['Opération'] == "DEBRAD") | (df['Opération'] == "REPRAD")]
finrad_stoprad = df[(df['Opération'] == "FINRAD") | (df['Opération'] == "STOPRAD")]
# réénitialiser les indexs et renommer les transects
debrad_reprad.reset_index(inplace=True)
finrad_stoprad.reset_index(inplace=True)
strate = debrad_reprad['Strate'].str.replace('\s+', '_')

# resultat = []
# for valeur in set(debrad_reprad['Strate']):
#     if valeur not in set(finrad_stoprad['Strate']):
#         resultat.append(valeur)
# print(resultat)   
     
# Dates et les heures de début et de fin de radiale
for index, row in debrad_reprad.iterrows():
    datedebut.append(convert_date(row['Date']))
    heuredebut.append(row['Heure'])

for index, row in finrad_stoprad.iterrows():
    end_dates.append(convert_date(row['Date']))
    end_times.append(row['Heure'])

# %% 2. Lancer l'echointegration

#Données  -60db
# path_config = pref + 'config_60'
path_config =pref+'/config/'
thresholds = [-80]
path_config = path_config + "/" + str(thresholds[0])
# path_hac_survey_drix = 'E:/Delmoges/Drix/EK80/DELMOGES/CW/HAC/SBES_MBES/DELMOGES/'
# Path to HAC or netCDF data files:
path_hac_survey=pref+'HAC/Parcours_2/'
# Save path
path_save=pref+'/echointegration/'

ind_decoup = len(debrad_reprad)
for indexd in range(ind_decoup):    
    ei.ei_survey_transects_netcdf_cpp(
        path_hac_survey,
        path_config,
        path_save,
        datedebut[indexd],
        end_dates[indexd],
        heuredebut[indexd],
        end_times[indexd],
        strate[indexd]
    )