
import datetime
from pathlib import Path
from time import strftime
import time
import pandas as pd
import os

from tqdm import tqdm
from pymovies_3d.core.echointegration import ei_survey as ei

#init paths 

# Define paths 
def format_time(value):
        return  datetime.datetime.fromtimestamp(value).strftime('%Y-%m-%d %H:%M:%S')



class EIParameters():

    def __init__(self,year:int,threshold:int,path_hac_survey:Path,path_config_base:Path,output_path:Path,acoustic_sequences_file:Path):
        self.path_config = path_config_base / str(threshold)
        # Path to HAC or netCDF data files:
        self.path_hac_survey=path_hac_survey
        # Save path
        self.path_save = output_path
        # If save path does not exist, create it
        if not os.path.exists(self.path_save):
            os.makedirs(self.path_save)
        self.acoustic_sequences_file= acoustic_sequences_file
        self.year = year
        self.threshold=threshold

    def print(self):
        print(f"Using config : {self.path_config.as_uri()}")
        print(f"Using hac directory : {self.path_hac_survey.as_uri()}")
        print(f"Using radials file : {self.acoustic_sequences_file.as_uri()}")
        print(f"Using output directory : {self.path_save.as_uri()}")
     
    def validate(self):
        if not self.path_save.exists():
             raise FileNotFoundError(f"file {self.path_save.as_uri()} not found") 
        if not self.path_config.exists():
             raise FileNotFoundError(f"file {self.path_config.as_uri()} not found") 
        if not self.path_hac_survey.exists():
             raise FileNotFoundError(f"file {self.path_hac_survey.as_uri()} not found") 
        if not self.acoustic_sequences_file.exists():
             raise FileNotFoundError(f"file {self.acoustic_sequences_file.as_uri()} not found") 
        

def process(parameters:EIParameters):



    #read mantot
    df = pd.read_csv(parameters.acoustic_sequences_file)
    
    process_start=time.time()
    print(f"starting EI processing at threshold {parameters.threshold} {format_time(process_start)}")
    for index,values in tqdm(df.iterrows(),total=df.shape[0], desc=f'Processing acoustic sequence'):
        name =values["ID"]
        datetime_start = pd.to_datetime(values["datestart"])
        datetime_end =  pd.to_datetime(values["dateend"])
        start_time = datetime_start.strftime("%H:%M:%S")
        start_date = datetime_start.strftime('%d/%m/%Y')
        end_time = datetime_end.strftime("%H:%M:%S")
        end_date = datetime_end.strftime('%d/%m/%Y')
        ei.ei_survey_transects_netcdf_cpp(path_hac_survey=parameters.path_hac_survey.as_posix(),
                                        path_config=parameters.path_config.as_posix(),path_save=parameters.path_save.as_posix(),
                                        date_start=start_date,
                                        date_end=end_date,
                                        time_start=start_time,
                                        time_end=end_time,
                                        name_transects=name,skip_existing=True)

    end_process = time.time()
    print(f"finished EI processing {format_time(end_process)} (started at {format_time(process_start)})")
    print(f"time elapsed {end_process-process_start}")  


