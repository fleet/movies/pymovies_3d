# -*- coding: utf-8 -*-
"""
Created on Thu Apr 23 16:47:52 2020

@author: guillaume
"""


import cartopy.crs as ccrs
import cartopy.feature as cfeature
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER

import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.ticker as mticker
import matplotlib.font_manager as fm

import numpy as np

import folium


from mpl_toolkits.axes_grid1.anchored_artists import AnchoredSizeBar


def carte_france(extension_zone,ech,figsize=(35,20)):
    fig = plt.figure(figsize=figsize)
    #remove pylint warning, seems a false detection
    #pylint:disable=abstract-class-instantiated
    ax = fig.add_subplot(1, 1, 1, projection=ccrs.PlateCarree())
    #ax.set_extent([-3.05, -2.45, 47, 47.4])
    ax.set_extent(extension_zone)
    Grider = ax.gridlines(draw_labels=True)
    Grider.xformatter = LONGITUDE_FORMATTER
    Grider.yformatter = LATITUDE_FORMATTER
    Grider.xlabel_style = {'size': 15,'weight': 'bold'}
    Grider.ylabel_style = {'size': 15,'weight': 'bold'}
    Grider.xlabels_top  = False
    Grider.ylabels_right  = False
    fontprops = fm.FontProperties(size=18)
    Grider.xlocator = mticker.MaxNLocator(2)
    Grider.ylocator = mticker.MaxNLocator(2)

    ax.add_feature(cfeature.OCEAN.with_scale("50m"))
    ax.add_feature(cfeature.RIVERS.with_scale("50m"))
    ax.add_feature(cfeature.BORDERS.with_scale("50m"), linestyle=":")
    if ech=='Sa':
        ax.set_title('$S_A$ en dB re 1($m^{2} nmi^{-2}$)',size=15)
    else:
        ax.set_title('log($S_A$)',size=15)
    ax.legend(fontsize=15)
    scalebar = AnchoredSizeBar(ax.transData,
                           0.016, '1 MN', 'lower center', 
                           pad=0.1,
                           color='white',
                           frameon=False,
                           size_vertical=0.001,
                           fontproperties=fontprops)

    ax.add_artist(scalebar)
    return ax

#pylint:disable=no-member
def affichage_carte_france(
    nombre_de_categories, Lat_moy, Lon_moy, Sa_moy, extension_zone, ech, path=None
):
    couleurs = plt.cm.jet(np.linspace(0, 1, nombre_de_categories))
    for x in range(nombre_de_categories):
        
        ax = carte_france(extension_zone,ech)
        l=ax.scatter(Lon_moy, Lat_moy, s=Sa_moy[x], color=couleurs[x])
        legend=ax.legend(*l.legend_elements("sizes", num='auto',color=couleurs[x]),labelspacing=2)

        #ax.scatter(Lon_moy, Lat_moy, s=Sa_moy[x], color=couleurs[x])
        if path is not None:
            plt.savefig("%s_map_label_%d.png" % (path, x))
        plt.close()



#pylint:disable=consider-using-enumerate
#pylint:disable=no-member
def save_map_html(nombre_de_categories, Lat_moy, Lon_moy, Sa_moy, path=None):
    couleurs = plt.cm.jet(np.linspace(0, 1, nombre_de_categories))
    for x in range(nombre_de_categories):
        couleur_hexa = colors.to_hex(couleurs[x], keep_alpha=True)

        map_osm = folium.Map(location=[Lat_moy[0], Lon_moy[0]])
        for i in range(len(Lat_moy)):

            folium.CircleMarker(
                [Lat_moy[i], Lon_moy[i]],
                radius=(Sa_moy[x][i]),
                color=couleur_hexa,
                fill=True,
                fill_opacity=1,
            ).add_to(map_osm)
        # Ajouter le chemin  dans map_osm.save sinon la page html s'enregistre dans le dossier contenant ei_survey

        if path is not None:
            map_osm.save("%s_carte_%d.html" % (path, x))
        else:
            map_osm.save("carte_%d.html" % (x))
