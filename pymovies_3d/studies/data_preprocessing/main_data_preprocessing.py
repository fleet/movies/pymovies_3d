########################################################################################################################
# Initialize paths and data
from configparser import NoOptionError
import glob
import json
import pickle
import os
import shutil
from collections import namedtuple
from typing import NamedTuple,Type,Dict, Iterable, List,Tuple

from pathlib import Path
from enum import Enum
import warnings

import pandas as pd
import pytz as tz
import xarray as xr
from tqdm import tqdm

from pymovies_3d.core.survey.common import display
from pymovies_3d.core.survey.common.labels import ClassesPelgas2023
from pymovies_3d.core.survey.man_tot import BadAssumption
from pymovies_3d.core.survey import man_tot 
from pymovies_3d.core.survey.preprocessing import ei_file
from pymovies_3d.core.survey.preprocessing import polygon as polygondriver
from pymovies_3d.core.survey.preprocessing.polygon import MLayer,MPolygon
from pymovies_3d.core.survey.preprocessing.file_manager import get_sequence_from_dir, get_radial_from_ei_name
from pymovies_3d.studies.data_preprocessing.model import Esu, MantotEsu, Radial
from pymovies_3d.studies.data_preprocessing.preprocess_parameters import PreprocessingParameters
from pymovies_3d.core.util.report import Report
from pymovies_3d.studies.data_preprocessing.polygon_label_computer import extract_polygon_label,StatCounter, Context as PolygonContext

     
class ProcessingStep(Enum):
    STEP_DATA_ORGANIZATION = 0
    STEP_LABEL_COMPUTATION = 1
    STEP_DATAMODEL_CREATION = 2

LABELLED_LAYER = "LABELLED_layer.csv"

# ORGANISE DATA DIRECTORIES
DataInput = namedtuple("DataInput", ["screenshot", "ei"])

EI_Input = namedtuple("EI_Input", ["ei_ref_60", "ei_60_path", "ei_80", "ei_80_path"])

class Context(NamedTuple):
    report:Report
    parameter:PreprocessingParameters


def extract_esu_from_screenshot(filename: str):
    """
    Extract ESU from screenshot filename
    :return start_esu,end_esu datetimes.
    """

    filename = Path(filename).name
    # we guess ESU time limits from screenshots
    time = filename.split("38000_")[1]  # get 1_20230502_045845_20230502_050508.png
    time = time.replace(".png", "")
    num, date_start, time_start, date_end, time_end = time.split("_")
    start_esu = pd.to_datetime(f"{date_start}_{time_start}", format="%Y%m%d_%H%M%S")
    end_esu = pd.to_datetime(f"{date_end}_{time_end}", format="%Y%m%d_%H%M%S")
    return start_esu, end_esu


def match_radials(screenshot_seq: Dict[str,List[str]], ei_radials: Iterable[str], mantot_seq:Iterable[str], report : Report):
    """
    Take two acoustic sequence lists, one from ei files and the other one from screen shot
    and make a match between the two of them
    Due to jackpot file error some sub radials (like RL-a1) can be defined in screenshot names
    but belong to the same master radial in the ei list (like RL-a)
    This function return the list of unmatched screenshot (ie no ei file matching),
    unmatched ei (ie no screenshot) and the list of ei master radials
    """
    #list of screenshot having no radials in mantot
    unmatched_screenshots = [name for name in screenshot_seq.keys() if name not in mantot_seq]
    unmatched_screenshots.sort()
    if len(unmatched_screenshots) > 0:
        report.warn(
            f"missing acoustic sequence for the screenshot files : {unmatched_screenshots} ",
            channel="Missing acoustic sequence",
        )
        for k in unmatched_screenshots:
            report.warn(
                f"acoustic sequence {k} missing for screenshot files : {screenshot_seq[k]} ",
                channel="Missing acoustic sequence details",
            )


    unmatched_screenshots = [name for name in mantot_seq if name not in screenshot_seq]
    unmatched_screenshots.sort()
    if len(unmatched_screenshots) > 0:
        report.warn(
            f"missing screenshot files for the following radials : {unmatched_screenshots} ",
            channel="Missing acoustic sequence",
        )

    #list of ei having no radial in mantot
    if len(ei_radials) >0:
        ei_radial_unmatched = [name for name in ei_radials if name not in mantot_seq]
        ei_radial_unmatched.sort()
        if len(ei_radial_unmatched) > 0:
            report.warn(
                f"missing screenshot files for the following echo integration files : {ei_radial_unmatched} ",
                channel="Missing acoustic sequence",
            )
        ei_radial_unmatched = [name for name in mantot_seq if name not in ei_radials]
        ei_radial_unmatched.sort()
        if len(ei_radial_unmatched) > 0:
            report.warn(
                f"missing ei files for the following radials : {ei_radial_unmatched} ",
                channel="Missing acoustic sequence",
            )
        valid_radials = [name for name in mantot_seq if name in ei_radials]
        valid_radials.sort()
        invalid_radials = [name for name in mantot_seq if name not in valid_radials]
        invalid_radials.sort()
        if len(invalid_radials)>0:
            report.warn(
                f"Missing acoustic sequence in ei or screen shots {invalid_radials}",
                channel="Missing acoustic sequence"
            )
    else:
        valid_radials =  mantot_seq
    return valid_radials


def prepare_ei_files(context:Context) -> List[EI_Input]:
    """Parse echo integration files and get a list of matching files (typipcally 60 and 80 ei files)"""
    # check ei data input, ensure that all files in -60 dir has one in -80
    # make a match between ei at -60 and ei at -80
    ei_60 = glob.glob(str(Path(context.parameter.ei_path.echointegration_files_60) / "*.nc"))
    ei_60 = {Path(f).name: f for f in ei_60}
    ei_80 = glob.glob(str(Path(context.parameter.ei_path.echointegration_files_80) / "*.nc"))
    ei_80 = {Path(f).name: f for f in ei_80}

    # we match based on filenames, we need to cut file names to ensure because last part of file name
    ei_60_dict = {
        k.split("PELGAS")[0]: (k, v) for k, v in ei_60.items()
    }  # use dictionnary to match name and real file
    ei_80_dict = {
        k.split("PELGAS")[0]: (k, v) for k, v in ei_80.items()
    }  # use dictionnary to match name and real file

    # merge two dictionnary
    missing_ei_files = [k for k in ei_60_dict.keys() if k not in ei_80_dict.keys()]
    if len(missing_ei_files) > 0:
        raise FileNotFoundError(
            f"File {missing_ei_files} does not have matching file at -80  "
        )

    # naming of the echointegration files is
    link_60_80 = [
        EI_Input(
            ei_ref_60=ei_60_dict[k][0],
            ei_60_path=ei_60_dict[k][1],
            ei_80=ei_80_dict[k][0],
            ei_80_path=ei_80_dict[k][1],
        )
        for k in ei_60_dict.keys()
    ]
    return link_60_80


def organize_data_input(context:Context):
    
    #load radials definition
    acoustic_sequences = pd.read_csv(context.parameter.sequence_file)  
    acoustic_sequences = acoustic_sequences.set_index("ID")       
    acoustic_sequences = acoustic_sequences.sort_index()  
    
    # read mantot
    esu_data = man_tot.read_raw(context.parameter.input_paths.mantot_file,year=context.parameter.year)

    esu_sequence, esu_start,esu_end = man_tot.extract_esu(esu_data,report=context.report)

    #check if all radials are in the mantot file    
    missing_seq = [name for name in acoustic_sequences.index if name not in esu_sequence]    
    if len(missing_seq) > 0:    
        context.report.error(f"Missing acoustic sequence in mantot file {missing_seq}",channel="Missing acoustic sequence",radial_name="None")
    missing_esu_radials = [name for name in esu_sequence if name not in acoustic_sequences.index]    
    if len(missing_esu_radials) > 0:
        context.report.error(f"Missing acoustic sequence in mantot file {missing_esu_radials}",channel="Missing acoustic sequence",radial_name="None")

    #exit if we have missing radials
    #if len(missing_seq) > 0 or len(missing_esu_radials) > 0:
    #    context.report.print()
    #    raise ValueError("Missing acoustic sequence in mantot file")
    
    #copy everything in a dict structure, this will be a dictionnary of list of MantotEsu
    mantot_esu :Dict[str,List[MantotEsu]] = {}
    for radial, start,end in zip(esu_sequence,esu_start,esu_end):
        if radial not in mantot_esu:
            mantot_esu[radial] = []
        mantot_esu[radial].append(MantotEsu(start_esu=start,end_esu=end))

    # read all polygons
    polygon_directory=Path(context.parameter.input_paths.polygon_path)
    frequency_filter :str= "38kHz"
    ei_polygons:List[MPolygon]=[]

    #TODO should we check for polygons in 120kHz ?

    for file in tqdm(glob.glob(str(polygon_directory / f"*{frequency_filter}*.csv")),desc="Parsing polygons files"):
        try:
            p=polygondriver.read_one_polygon(Path(file))
            ei_polygons.append( polygondriver.read_one_polygon(Path(file)))
        except Exception as polygon_error:
            context.report.error(channel="Polygon parsing error",radial_name = "None",msg =f"{Path(file).as_uri()} skipped parsing error :  , {polygon_error}")

    #polygondriver.check(ei_polygons)
    ei_inputs: List[EI_Input] = [] 
    if context.parameter.is_using_echointegrated_files():
        ei_inputs = prepare_ei_files(context=context)

    data_view = []


    seq_from_mantot = acoustic_sequences.index.to_list()

    # get transect lines
    screenshot_seq = get_sequence_from_dir(
        input_dir=str(context.parameter.input_paths.screen_shots), file_pattern="*ES38*.png"
    )

    # Assume we could have several ei file per radial
    echointegration_radials = {get_radial_from_ei_name(v.ei_ref_60): v for v in ei_inputs}

    # intersect two list
    valid_radials = match_radials(
        screenshot_seq=screenshot_seq,
        ei_radials=echointegration_radials.keys(),
        mantot_seq=seq_from_mantot,
        report = context.report
    )

    # move file matching radials having both screen shots and echo integration files
    # clear previous  ?
    if os.path.exists(context.parameter.workdir_paths.sequence_workdir):
        shutil.rmtree(str(context.parameter.workdir_paths.sequence_workdir))
    context.parameter.workdir_paths.sequence_workdir.mkdir(parents=True, exist_ok=False)
    # warnings = []
    for master_radial in tqdm(valid_radials, desc="acoustic_sequence"):
        radial_dir = context.parameter.workdir_paths.sequence_workdir / master_radial
        radial_dir.mkdir()
        #if needed copy EI files
        ei_60_radial = None
        ei_80_radial = None
        if context.parameter.is_using_echointegrated_files():
            # copy ei files there should be only one per radial
            ei_60_dir = radial_dir / "60"
            ei_60_dir.mkdir()

            ei_60_radial = ei_60_dir / Path(
                echointegration_radials[master_radial].ei_ref_60
            )
            shutil.copy(echointegration_radials[master_radial].ei_60_path, ei_60_radial)

            ei_80_dir = radial_dir / "80"
            ei_80_dir.mkdir()
            ei_80_radial = ei_80_dir / Path(echointegration_radials[master_radial].ei_80)
            shutil.copy(echointegration_radials[master_radial].ei_80_path, ei_80_radial)
        radial_data = Radial(directory = radial_dir,
            ei_reference_60=ei_60_radial, ei_80=ei_80_radial, name=master_radial
        )
        data_view.append(radial_data)
        # copy screenshot files, there could be several per radial
        # (including one per esu and taking into account for subradial)
        screen_shots_list = glob.glob(
            str(context.parameter.input_paths.screen_shots / f"{master_radial}_*_*ES38*.png")
        )
        for esu_def in tqdm(mantot_esu[master_radial], desc="esu"):

            # create an ESU directory name

            start_esu = esu_def.start_esu
            end_esu = esu_def.end_esu
            esu_dir_name = f"ESU_{start_esu.strftime('%Y%m%d_%H%M%S')}_{end_esu.strftime('%Y%m%d_%H%M%S')}"
            esu_dir = radial_dir / esu_dir_name

            if not esu_dir.exists():
                esu_dir.mkdir()
                # save filtered mantot
                # extract mantot data
                filtered_mantot = man_tot.time_filter(
                    esu_data, esu_time_start=start_esu, esu_time_stop=end_esu
                )
                mantot_filtered_file = esu_dir / "mantot_extract.csv"
                filtered_mantot.to_csv(mantot_filtered_file)
                mantot_filtered_file_nc = esu_dir / "mantot_extract.nc"
                filtered_mantot.to_csv(mantot_filtered_file_nc)

                # find intersecting polygons
                esu_polygons = polygondriver.filter(
                    polygons=ei_polygons, start_time=start_esu, end_time=end_esu
                )

                # copy polygon files to directory
                polygons_files = []
                for p in esu_polygons:
                    dst = esu_dir / f"polygon_{p.filename.name}"
                    shutil.copy(p.filename, dst)
                    polygons_files.append(dst)

                esu = Esu(
                    start_esu=start_esu,
                    end_esu=end_esu,
                    directory=esu_dir,
                    mantot_file=mantot_filtered_file,
                    polygon_files=polygons_files,
                )
                radial_data.esu.append(esu)

            # else:
            #     warn_str= f"ESU {esu_dir_name} has multiple screenshots directory is ({esu_dir})"
            #     warnings.append(warn_str)

            # find and copy screenshot in subdirectory
            processed_list = []
            for f in screen_shots_list:
                filename = Path(f).name
                start_esu_screenshot, end_esu_screenshot = extract_esu_from_screenshot(filename=Path(f).name)
                #filter to keep only screenshot matching this esu definition
                #we keep only where mean date
                mid_point_timestamp = (end_esu_screenshot - start_esu_screenshot) / 2
                mid_point = start_esu_screenshot + mid_point_timestamp
                if mid_point > start_esu and mid_point < end_esu:
                    dst = esu_dir / Path(f).name
                    shutil.copy(f, dst)
                    #remove from the list
                    processed_list.append(f)

            screen_shots_list = [f for f in screen_shots_list if f not in processed_list]
        #check if some screen shot are still not copied
        if len(screen_shots_list) > 0:
            for f in screen_shots_list:
                context.report.warn(msg=f"Radial {master_radial} screenshot {Path(f).as_uri()} have no ESU associated  ",channel='Screenshot without ESU')
    
    for r in data_view:
        r.remove_root(context.parameter.workdir_paths.sequence_workdir)    
    with open(str(context.parameter.workdir_paths.data_model_path), "wb") as f:
        pickle.dump(data_view, f)

def __make_esu_snapshot(ei_60:xr.Dataset,ei_80:xr.Dataset,radial:Radial,esu:Esu,report:Report):

    # read dataset and filter to match esu
    esu_dataset = ei_file.filter(
        dataset=ei_60, start_time=esu.start_esu, end_time=esu.end_esu
    )
    if len(esu_dataset.time) == 0:
        msg = f"Error while processing ESU {esu.directory.as_uri()} , ei file times does not match any part of the ESU "
        report.error(
            msg=msg,
            channel="Bad EI timestamp ",
            radial_name=radial.name,
        )
        raise ValueError(msg)
    
    # save esu_dataset without any classes informations
    overview_file = Path(esu.directory) / "ei_60.png"
    display.save_figure(
        esu_dataset,
        filename=overview_file,
        frequencies=[38000.0],
        show_classes=False,
    )

    esu80_dataset = ei_file.filter(
        dataset=ei_80, start_time=esu.start_esu, end_time=esu.end_esu
    )
    overview_file = Path(esu.directory) / "ei_80.png"
    display.save_figure(
        esu80_dataset,
        filename=overview_file,
        frequencies=[38000.0],
        show_classes=False,
    )

def process_sequence_step_two(radial: Radial,context:Context)-> Path:
    """
    For the given radial process all

    :param radial:
    :return: the radial final dataset path
    """
    # create a subpart of ei integration file

    # read dataset
    ei_60 = ei_file.read_dataset(radial.ei_reference_60)
    # save seafloor information to apply it again after processing
    ei_60_bottom_mask = ei_60["classes"] == ClassesPelgas2023.SEAFLOOR.value
    # ei_60_seasurface =

    ei_80 = ei_file.read_dataset(radial.ei_80)
    radial_directory = radial.directory
    temp_counter = 0

    # process esu
    for esu in (pbar := tqdm(radial.esu)):
        start_esu = esu.start_esu
        stop_esu = esu.end_esu
        pbar.set_description(f"Processing {esu.directory.as_uri()}")

        try:
            __make_esu_snapshot(ei_60,ei_80,radial,esu,context.report)
            ei_polygons = polygondriver.read_polygon(esu.polygon_files)

            # process and integrate layer classes
            layers_label = pd.read_csv(esu.directory / LABELLED_LAYER)
            layers_label["datestart"] = pd.to_datetime(layers_label["datestart"]).dt.tz_localize(tz.utc)
            layers_label["dateend"] = pd.to_datetime(layers_label["dateend"]).dt.tz_localize(tz.utc)

            # get polygons for this ESU
            # CPO not sure it is necessary, polygons match already the ESU time
            esu_polygons = polygondriver.filter(
                polygons=ei_polygons, start_time=start_esu, end_time=stop_esu
            )

            for index, row in layers_label.iterrows():
                layer_start_time = row["datestart"]
                layer_end_time = row["dateend"]

                layer_depthstart = row["depthstart"]
                layer_depthend = row["depthend"]

                label_enum = ClassesPelgas2023.from_name(row["label"])
                if label_enum is None:
                    context.report.warn(
                        msg=f"ESU {esu.directory.as_uri()} "
                            f"layer [{row['datestart']}-{row['dateend']} {row['depthstart']}-{row['depthend']}] "
                            f"has an unknown label {row['label']}, mapping to unknown",
                        channel="Missing Layer label",
                        radial_name=radial.name
                    )
                    label_enum = ClassesPelgas2023.UNKNOWN #if no label map per default 
                # use where
                ei_60["classes"] = ei_60["classes"].where(
                    ~(
                        (ei_60["time"] >= layer_start_time)
                        & (ei_60["time"] <= layer_end_time)
                        & (ei_60["depth"] >= layer_depthstart)
                        & (ei_60["depth"] <= layer_depthend)
                    ),
                    other=label_enum.value,
                )
                        # # compute ei layer information

      
            
            # if we have polygon, apply their classes to the dataclasses matrix, we need to do it pixel per pixel
            # retrieve index of dataset
            begin_time: pd.DatetimeIndex = start_esu.tz_localize(tz.utc)
            end_time = stop_esu.tz_localize(tz.utc)
            time_values = ei_60["time"].loc[
                (ei_60["time"] >= begin_time) & (ei_60["time"] <= end_time)
            ]

            for p in esu_polygons:
                polygon_start_time = p.start_time.tz_localize(tz.utc)
                polygon_end_time = p.end_time.tz_localize(tz.utc)
                min_depth, max_depth = polygondriver.get_min_max_depth(polygon=p)

                # refine the list of point that can intersect the polygon
                polygon_label = ClassesPelgas2023.from_name(p.data_class)
                if polygon_label is None:
                    context.report.error(
                                            msg=f"ESU {esu.directory.as_uri()} "
                                                f"polygon {p.filename.name} "
                                                f"has an unknown label {p.data_class}",
                                            channel="Missing polygon label",
                                            radial_name=radial.name
                                        )
                    continue

                p_time_values = time_values.loc[
                    (time_values["time"] >= polygon_start_time)
                    & (time_values["time"] < polygon_end_time)
                ]
                p_depth_values = ei_60["depth"].loc[
                    (ei_60["depth"] >= min_depth) & (ei_60["depth"] <= max_depth)
                ]

                # loop on point subset
                for point_time in p_time_values.values:
                    for point_depth in p_depth_values:
                        if polygondriver.contains(
                            polygon=p, time_value=point_time, depth_value=point_depth
                        ):
                            ei_60["classes"].loc[
                                dict(time=point_time, depth=point_depth)
                            ] = polygon_label.value

            # re apply bottom mask
            ei_60["classes"] =  ei_60["classes"].where(
                ~ei_60_bottom_mask, other=ClassesPelgas2023.SEAFLOOR.value
            )

            # save an intermediate image / per ESU of the classes
            esu_60_disp = ei_file.filter(
                dataset=ei_60, start_time=start_esu, end_time=stop_esu
            )
            display.save_figure(
                esu_60_disp,
                filename=Path(esu.directory)
                / f"dev_esu_classes_{len(esu_polygons)}_polygons.png",
                frequencies=[38000.0],
                show_classes=True,
            )
        except Exception as e:
            context.report.error(
                msg=f"Error while processing ESU {esu.directory.as_uri()} {e}",
                channel="Exception channel",
                radial_name=radial.name
            )
#            import traceback
#            print(traceback.format_exc())


    # check for data from EI files outside of ESU min and max date to add some warnings
    start_dates = []
    end_dates = []
    for esu in radial.esu:
        l_time = esu.start_esu.tz_localize(tz.utc)
        start_dates.append(l_time)
        l_time = esu.end_esu.tz_localize(tz.utc)
        end_dates.append(l_time)
    min_date = min(start_dates)
    max_date = max(end_dates)
    before_start_date = ei_60["time"].loc[(ei_60["time"] < min_date)]
    after_start_date = ei_60["time"].loc[(ei_60["time"] > max_date)]
    if len(before_start_date) > 0:
        context.report.warn(
            f"Radial {radial_directory.as_uri()}, "
            f"EI file has {len(before_start_date)} pings before start of ESUs {min_date}",
            channel="EI exceed data",
        )
    if len(after_start_date) > 1: #EI does not stop exactly at the stop date, we add a tolerance of 1 ping
        context.report.warn(
            f"Radial {radial_directory.as_uri()}, "
            f"EI file has {len(after_start_date)} pings  after start of ESUs {max_date}",
            channel="EI exceed data",
        )

    temp_counter += 1
    display.save_figure(
        ei_60,
        filename=Path(radial_directory) / "dev_esu_38_with_classes.png",
        frequencies=[38000.0],
        show_classes=True,
    )

    #split data to match all esu start and end time and save it
    filtered_ei_60_dataset  = ei_file.filter(
        dataset=ei_60, start_time=min_date, end_time=max_date
    )
    filtered_ei_80_dataset = ei_file.filter(
        dataset=ei_80, start_time=min_date, end_time=max_date
    )


    # create an ouput datamodel with data at -80 and -60 and associated classes


    bs_60 = filtered_ei_60_dataset["integrated_bs"].rename('bs_60')
    final_dataset = filtered_ei_80_dataset.assign(bs_60=bs_60)
    final_dataset = final_dataset.rename({"integrated_bs":"bs_80"})
    final_dataset['classes']=ei_60['classes']
    #recast time as nupmpy time to allow netcdf backup
    warnings.filterwarnings('ignore',category=UserWarning)
    time = final_dataset.time.astype(dtype='datetime64[ms]')
    warnings.filterwarnings('default',category=UserWarning)
    final_dataset = final_dataset.assign_coords(time=time)
    if len(time) > 0:
        display.save_figure(
            final_dataset,
            filename=Path(radial_directory) / "final_ei_dataset.png",
            frequencies=[38000.0],
            show_classes=True,
        )
    dataset_path=Path(radial_directory) / f"{radial.name}_dataset.nc"
    final_dataset.to_netcdf(path =dataset_path )
    return dataset_path


   

def print_statistics(data_view: List[Radial]):
    esu_with_one_polygons = {}
    esu_with_several_polygons = {}
    for radial in data_view:
        for esu in radial.esu:
            if len(esu.polygon_files) > 1:
                esu_with_several_polygons[esu.directory] = [
                    p for p in esu.polygon_files
                ]
            if len(esu.polygon_files) == 1:
                esu_with_one_polygons[esu.directory] = [p for p in esu.polygon_files]

    print("ESU with one polygon")
    for k, items in esu_with_one_polygons.items():
        print(f"{k.as_uri()}")
    print("ESU with two or more polygons")
    for k, items in esu_with_several_polygons.items():
        print(f"{k.as_uri()}")

def __export_layers_label(layers:pd.DataFrame,radial:Radial, esu:Esu,context:Context):
    """
    Export layer to a new file format or make error if the layer is not valid 
    """
    #duplicate layer dataframe
    layers = layers.copy()
    #add a label column
    layers["label"] = None
    report:Report = context.report
    for index, row in layers.iterrows():
        # get all class values for the current layer, classes values initially contains polygons classes and layer classes
        #but polygons classes values are supposed to be removed from the layer
        layer_labels = [
            col.name
            for col in ClassesPelgas2023
            if col.name in row and row[col.name] > 0
        ]
        if len(layer_labels) > 1 :
            report.error(channel="Bad Layer",msg=f"Esu {esu.directory.as_uri()} Layer {index} has two many classes values {layer_labels}",radial_name=radial.name)
            #TODO use the best one ? 
            layers.loc[index,"label"] = None
        elif len(layer_labels) == 0:
            #No data at all mark as background
            layers.loc[index,"label"] = 'D11'
        else:
            layers.loc[index,"label"] =layer_labels[0]
    #export layers as a new dataframe with only selected columns
    layers = layers[["datestart","dateend","depthstart","depthend","label"]]
    layers.to_csv(esu.directory / LABELLED_LAYER)
    

def get_polygon_duplicates(polygons_files:List[Path]) -> Tuple[List[MPolygon],List[MPolygon]]:
    """Check for duplicate polygons in the list of polygons and return a list of uniques and duplicates polygons"""
    duplicates = []
    polygons = [polygondriver.read_one_polygon(p) for p in polygons_files]

    for p in polygons:
        if p.filename in duplicates:
            continue
        for p2 in polygons:
            if p.filename != p2.filename and p.is_similar(p2):
                duplicates.append(p2.filename)   

    unique_polygons:List[MPolygon] = [p for p in polygons if p.filename not in duplicates]   
    duplicated_polygons:List[MPolygon] = [p for p in polygons if p.filename in duplicates]   
    return unique_polygons,duplicated_polygons



def process_sequence_step_one(sequence:Radial,context: Context):
    """
    First step of dataprocessing
    parse polygon and mantot files to guess the data label of each polygon
    export result to export files to allow manual correction

    """
    report = context.report
    stat= StatCounter()
    for esu in sequence.esu:
        polygons:List[Path] = esu.polygon_files

        try:
            tot_layers = man_tot.read_esu(esu.mantot_file)
            if esu.mantot_file ==Path('D:/PELGAS2022/workdir/radials/R01_LC-a3/ESU_20220429_095408_20220429_100010/mantot_extract.csv' ):
                print("debug")
            if len(polygons) > 0:
                #save layers to a new file model
                #copy tot_layer to a file name LABELLED_LAYER
                tot_layers:pd.DataFrame = extract_polygon_label(polygon_files=polygons,
                                                    tot_layers= tot_layers,report=report,
                                                    context=PolygonContext(esu=esu,radial_name=sequence.name),
                                                    stat=stat,
                                                    write_result=True
                                                    ,)

            
            __export_layers_label(tot_layers,sequence,esu,context) 
    
        except BadAssumption as e:
            report.error(msg=f"Error while processing ESU {esu.directory.as_uri()} : {e}",channel="Bad Assumption",radial_name=sequence.name)    


def remove_duplicates(data_view:List[Radial],report) -> List[Radial]:
    #remove duplicates polygons 
    for sequence in (pbar := tqdm(data_view)):
        for esu in sequence.esu:
            unique_polygons,duplicated_polygons = get_polygon_duplicates(esu.polygon_files)
            if len(duplicated_polygons) > 0:
                report.warn(msg=f"Radial {sequence.name} ESU {esu.directory.as_uri()} has duplicated polygons {[p.filename for p in duplicated_polygons]}",channel="Duplicated polygons")
                esu.polygon_files = [p.filename for p in unique_polygons]
                #rename polygon_files
                for p in duplicated_polygons:
                    os.rename(p.filename, p.filename.with_suffix(".csv.duplicated"))
    
    return data_view   
                

def load_datamodel(context:Context,load_labelled_polygon:bool )->List[Radial]:
    #load data model 
    with open(str(context.parameter.workdir_paths.data_model_path), "rb") as f:
        data_view = pickle.load(f)
 
    #remap root path to the current working directory
    for r in data_view:
        r.set_root_path(context.parameter.workdir_paths.sequence_workdir)

    # rebuild polygons data (Bug in previous version of the code erase the polygon_files attribute)
    for r in data_view:
        for esu in r.esu:
            polygon_pattern = "polygon_*.csv"
            if load_labelled_polygon:
                polygon_pattern = "LABELLED_polygon*.csv"
            esu.polygon_files = [esu.directory / Path(p) for p in glob.glob(dir_fd=esu.directory,pathname=polygon_pattern)]
      
    # sort on radial name
    data_view.sort(key=lambda r: r.name)
    # sort esu internally
    for radial in data_view:
        radial.esu.sort(key=lambda esu: esu.directory)

    return data_view

   
def run_preprocessing(parameter:PreprocessingParameters, step:ProcessingStep, report_filename:str):
    report = Report()

    parameter.validate()

    # create a context
    context = Context(report=report, parameter=parameter)

    if step.value == ProcessingStep.STEP_DATA_ORGANIZATION.value:
        print(f"Running {step}")

        # sort and organize data file to match expectation and order them 
        organize_data_input(context=context)
        
        
        #remove duplicates polygons 

        #load data model to clean up polygons 
        data_view = load_datamodel(context=context,load_labelled_polygon=False)
        print("Remove duplicated polygons")
        data_view = remove_duplicates(data_view,report) 

        #save back datamofle
        with open(str(context.parameter.workdir_paths.data_model_path), "wb") as f:
            pickle.dump(data_view, f)

        report.print(file=context.parameter.workdir_paths.reports_dir / report_filename)
        return
    
    data_view = load_datamodel(context=context,load_labelled_polygon= step.value == ProcessingStep.STEP_DATAMODEL_CREATION.value)

    #filter to process only one seq    
    # filter_dataview = []
    # for seq in data_view:
    #     if "R16_MC-b" in seq.name:
    #         filter_dataview.append(seq)
    #         print(seq.name)
    
    # data_view = filter_dataview 

    # count esu
    esu_count = 0
    acoustic_sequence = {}
    for sequence in (pbar := tqdm(data_view)):
        sequence_directory = sequence.directory
        pbar.set_description(desc=f"Processing acoustic sequence {sequence_directory.as_uri()}")
        esu_count += len(sequence.esu)
        if step.value == ProcessingStep.STEP_LABEL_COMPUTATION.value:
            process_sequence_step_one(sequence=sequence,context=context)

        # once manual processing is done 
        if step.value == ProcessingStep.STEP_DATAMODEL_CREATION.value:
            if not context.parameter.is_using_echointegrated_files():
                raise ValueError("Cannot run datamodel without echo integrated files, consider to rerun all processes with use_ei_files setted in parameter")
            radial_dataset=process_sequence_step_two(radial=sequence,context=context)
            acoustic_sequence[sequence.name]=radial_dataset
    
            #keep radial that where correctly processed
            acoustic_sequence = {name:str(path) for name,path in acoustic_sequence.items() if name not in report.radial_errors}


            #save all radial and their datamodel having no errors
            with open(context.parameter.workdir_paths.reports_dir / "radial_list.json","w",encoding="utf-8") as f:
                json.dump(acoustic_sequence,f)


        #print report
    report.print(file=context.parameter.workdir_paths.reports_dir / report_filename)
