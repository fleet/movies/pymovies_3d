""" Run echo integration on files"""

from email import header
from pathlib import Path

import pandas as pd
import pymovies_3d.core.survey.preprocessing.man_tot as man_tot
from pymovies_3d.core.util.report import Report

class Parameters:
    main_dir = Path("D:/PELGAS23/")
    workdir = main_dir / "workdir"
    mantot_file = main_dir / "echotyage_manuel" / "Export"/ "mantot38.csv"



def run(    param : Parameters):
    """Run ei on all files"""
    report = Report()

    #parse mantot files
    esu_data =  man_tot.read_raw(param.mantot_file)

    radials= man_tot.extract_radials(esu_data,report=report)
    radials.to_csv(r"d:\man_tot_radials.csv")
    

    df = pd.read_csv(r"d:\jackpot_radial.csv",names=["date_start","date_stop","radial_name"],sep=";")
    #convert types
    df["date_start"]=pd.to_datetime(df['date_start'],dayfirst=True)
    df["date_stop"]=pd.to_datetime(df['date_stop'],dayfirst=True)
    df["radial_name"]=df["radial_name"].str.replace("_PEL23","")
    df=df.set_index("radial_name")
    df =df.sort_index()
    df.to_csv(r"d:\jackpot_sorted.csv")
    print(df)

if __name__ == "__main__":
    run(param=Parameters())