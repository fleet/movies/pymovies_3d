


from dataclasses import dataclass, field
from pathlib import Path
from typing import Any, Dict, Iterable, List,Tuple, Optional

import pandas as pd


def remap_path(old_path: Path, old_root: Path, new_root: Path) -> Path:
    relative_path = old_path.relative_to(old_root)
    new_path = new_root.joinpath(relative_path)
    return new_path



@dataclass
class Esu:
    start_esu: pd.DatetimeIndex
    end_esu: pd.DatetimeIndex

    directory: Path
    mantot_file: Path
    polygon_files: List[Path]

@dataclass
class MantotEsu:
    start_esu: pd.DatetimeIndex
    end_esu: pd.DatetimeIndex


@dataclass
class Radial:
    ei_reference_60: Optional[Path]
    ei_80: Optional[Path]
    name: str
    directory:Path
    esu: List[Esu] = field(default_factory=list)

    def set_root_path(self, root: Path):
        """Set root directory for all paths"""
        def reroot(value:Optional[Path]) -> Optional[Path]:
            """root path, taking None into account"""
            if value is not None:
                return root.joinpath(value)
            return None
            

        self.ei_reference_60 = reroot(self.ei_reference_60)
        self.ei_80 = reroot(self.ei_80)
        self.directory = reroot(self.directory)
        for e in self.esu:
            e.directory = reroot(e.directory)
            e.mantot_file = reroot(e.mantot_file)
            e.polygon_files = []
            for p in e.polygon_files:
                e.polygon_files.append(reroot(p))

                    
    def remove_root(self, root:Path):
        """Remap paths to new root directory"""
        self.directory = self.directory.relative_to(root)
        if self.ei_reference_60 is not None:
            self.ei_reference_60 = self.ei_reference_60.relative_to(root)
        if self.ei_80 is not None:
            self.ei_80 = self.ei_80.relative_to(root)
        for e in self.esu:
            e.directory = e.directory.relative_to(root)
            e.mantot_file = e.mantot_file.relative_to(root)
            e.polygon_files = [p.relative_to(root) for p in e.polygon_files]
