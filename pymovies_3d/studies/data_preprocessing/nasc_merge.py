"""Merge NASC"""

from re import S
import pandas as pd
import matplotlib.pyplot as plt


df  = pd.read_csv(r"D:\WGFast\new_weight\nasc_values.csv",sep=";")
df['radial'] = df['radial'].str.split('_').str[0]


summed_values = df.groupby('radial').mean()

#summed_values.to_csv(r"D:\WGFast\nasc_values_summed.csv")
summed_values = summed_values.reset_index()
summed_values['percent'] = ((summed_values['nasc_predicted'] - summed_values['nasc_expert']) / summed_values['nasc_expert']) * 100

summed_values.plot(x='radial', y=['nasc_expert', 'nasc_predicted'], kind='bar', figsize=(15, 5))

summed_values.plot(x='radial', y=['percent'], kind='line', figsize=(15, 2),grid=True)
df = summed_values




one_value = df.mean(numeric_only=True)


plt.show(block = True)

