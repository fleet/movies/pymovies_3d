

from calendar import c
from logging import Filterer
from operator import inv
import re
import shutil
from collections import namedtuple
from dataclasses import dataclass, field
from pathlib import Path
from stat import FILE_ATTRIBUTE_READONLY
from typing import Any, Dict, Iterable, List,Tuple
import math
from bs4 import PageElement
import numpy as np
import pandas as pd
import pytz as tz
from tomlkit import boolean
from wcwidth import list_versions
import xarray as xr
from tqdm import tqdm
import pickle
import dataclasses
import itertools

from pymovies_3d.core.survey.common import display
from pymovies_3d.core.survey.common.labels import ClassesPelgas2023
from pymovies_3d.core.survey.man_tot import BadAssumption
from pymovies_3d.core.survey import man_tot
from pymovies_3d.core.survey.preprocessing import ei_file
from pymovies_3d.core.survey.preprocessing import polygon as polygondriver
from pymovies_3d.core.survey.preprocessing.polygon import MLayer,MPolygon

from pymovies_3d.core.util import report
from pymovies_3d.core.util.report import Report



from pymovies_3d.studies.data_preprocessing.model import Esu, MantotEsu, Radial
DEBUG = False # For Pelgas 2023 only where we have label associated with polygons

@dataclasses.dataclass
class Context:
    esu:Esu
    radial_name:str



class StatCounter:
    _valid_counter = 0
    _invalid_counter =0

    def add(self,valid:int,invalid:int) -> None:
        self._valid_counter += valid
        self._invalid_counter += invalid

    def __str__(self) -> str:
        return f"Valid: {self._valid_counter}, Invalid: {self._invalid_counter}, percentage valid: {self._valid_counter/(self._valid_counter+self._invalid_counter)*100:.2f}%"

def check_polygon(polygon:MPolygon,expected_label:str,report:Report,context:Context):
    if not DEBUG:
        return
    if polygon.data_class != expected_label:
            report.error(msg=f"ESU {context.esu.directory.as_uri()}, Label found {expected_label} does not match polygon {polygon.data_class} in  {polygon.filename}",channel="Polygon Label Error",radial_name=context.radial_name)

def _remove_layer_label(condition:pd.Series,layers:pd.DataFrame,label:str) -> pd.DataFrame:
    layers.loc[condition, label] = 0
    return layers


def _remove_deep_polygons(polygons:List[MPolygon],tot_layers:pd.DataFrame,report:Report,context:Context) -> List[MPolygon]:
      #ignore polygons that are strictly below the max depth of the mantot file
    man_tot_max_depth = tot_layers.depthend.max()
    filtered_polygons  = [p for p in polygons if p.min_depth < man_tot_max_depth]
    if len(polygons) - len(filtered_polygons) >0:
        report.info(f"ESU {context.esu.directory.as_uri()}, remove {len(polygons) - len(filtered_polygons)} polygons below the max depth of the mantot",channel="Polygon ignored")
    return filtered_polygons

def _match_unique_label(polygons:List[MPolygon],label_list:List[str],tot_layers:pd.DataFrame,report:Report,context:Context,write_result:bool) -> Tuple[List[MPolygon],pd.DataFrame]:
    """Check if all polygon match one of the label of the intersecting layers
    return the list of polygon that has been matched and the updated layers
    """
    # check if the sum of all polygon SA match one of the label of the intersecting layers
    # max polygon depth
    if len(polygons) == 0:
        raise ValueError("No polygon to process")

    max_depth,min_depth = max([p.max_depth for p in polygons]),min([p.min_depth for p in polygons])
    intersecting_layers = tot_layers[(tot_layers.depthend >= min_depth) & (tot_layers.depthstart <=  max_depth)  ]
    #check if the sum of all polygon SA match one of the label of the intersecting layers
    sa_per_label:Dict[str,float] =  {n:0 for n in label_list}
    for label in label_list:
        sa_per_label[label]=intersecting_layers[label].sum()


    #clone layers so we keep reference untouched
    tot_layers = tot_layers.copy()

    matched_polygons = []

    #check if the sum of all polygon SA match one of the label of the intersecting layers
    total_sa = 0
    for polygon in polygons:
        total_sa +=polygon.sa

    def process(sa_per_label:Dict[str,float]):
        found = False
        label = None
        for k,v in sa_per_label.items():
            if v > 0 and math.isclose(v, total_sa,rel_tol=1e-4):
                #found the label for all polygons
                for polygon in polygons:
                    check_polygon(polygon=polygon,expected_label=k,report=report,context=context)
                    if write_result:
                            # save polygon with its label
                            polygon.data_class = k
                            #polygon new file name with label
                            polygon_dir = polygon.filename.parent
                            filename = polygon_dir / f"LABELLED_{polygon.filename.stem}{polygon.filename.suffix}"
                            polygon.to_csv(filename)
                #all polygon labels have been found
                matched_polygons.extend(polygons)
                label = k
                found = True

                break
        return found,label

    match_found,label= process(sa_per_label)
    if match_found:
         #remove sa values from layers where depth match the polygon min and max depth
        condition: pd.Series[bool] = (tot_layers.depthend >= min_depth) & (tot_layers.depthstart <= max_depth)
        _remove_layer_label(condition=condition,layers=tot_layers,label=label)
    else:
        #check if label match one of the layer, this could happen in the polygon drawn overlap a layer with the same label but has no data in this overlap
        layer_iter =intersecting_layers.iterrows()
        for index,lay in layer_iter:
            for label in label_list:
                sa_per_label[label]=lay[label]
            match_found,label = process(sa_per_label=sa_per_label)
            if match_found:
                tot_layers.loc[index,label]=0
                break

    return matched_polygons,tot_layers

def get_combinations(polygons:List[MPolygon]) -> List[List[MPolygon]]:
    """
    Return all combinations of polygons
    """
    n = len(polygons)
    if n == 0:
        return []
    combinations = [polygons.copy()] #initial state with the combination of all polygons
    if n == 1:
        return combinations
    # create all combinations of polygons from single polygon to n-1 combinations
    for i in range(1,n):
        c = list(itertools.combinations(polygons,i))
        for v in c:
            if len(v) == 1:
                combinations.extend([list(v)])
            else:
                combinations.extend([list(v)])

    return combinations

def extract_polygon_label(polygon_files:List[Path], tot_layers:pd.DataFrame, report: Report,context:Context,stat:StatCounter,write_result:bool) -> pd.DataFrame:
    """
    Compute polygon label from the mantot file limitated to ESU definition,
    write the result in the polygon file if write_result is True
    """

    polygons = polygondriver.read_polygon(polygon_files)

    #create a list of label that are in the mantot file
    label_list = list(set([n.name for n in ClassesPelgas2023]) & set(tot_layers.columns))

    #Clean polygon list
    polygons = _remove_deep_polygons(polygons=polygons,tot_layers=tot_layers,report=report,context=context)

    if len(polygons) == 0:
        return tot_layers


    polygons_without_label = [p for p in polygons]

    #create all combination of polygons from remaining polygons
    do_search = True
    while do_search:
        #if we have too many polygons, the combination algorithm will be too slow
        if len(polygons_without_label) > 15:
            report.error(msg=f"ESU {context.esu.directory.as_uri()}, too many polygons to process, aborting",channel="Too many polygons",radial_name=context.radial_name)
            break
        combinations = get_combinations(polygons_without_label)
        found_one = False #tell if we found a combination that match all polygons
        index = 0
        for polygon_combination in combinations:
            index += 1
            polygon_matched,filtered_layers = _match_unique_label(polygons=polygon_combination,label_list=label_list,tot_layers=tot_layers,report=report,context=context,write_result=write_result)
            if len(polygon_matched) > 0:
                #we found a combation that match all polygons

                #remove polygons from the list polygon_without_label
                polygons_without_label = [p for p in polygons_without_label if p not in polygon_matched]
                tot_layers = filtered_layers
                found_one = True
                #we need to break the loop and recompute a new combination with the remaining polygons
                break
        do_search = found_one #we continue to search if we found at least one combination that match


    #check and report errors if there is still some polygons that have not been matched

    if len(polygons_without_label) > 0:
        for polygon in polygons_without_label:
            if polygon.data_class is not None:
                    # polygon new file name with label
                    polygon_dir = polygon.filename.parent
                    filename = polygon_dir / f"LABELLED_{polygon.filename.stem}{polygon.filename.suffix}"
                    polygon.to_csv(filename)
            else:
                report.error(msg=f"ESU {context.esu.directory.as_uri()}, no label found for polygon {polygon.filename} (hints = {polygon.data_class},sa = {polygon.sa})",channel="Missing Polygon",radial_name=context.radial_name)

    stat.add(invalid=len(polygons_without_label),valid=len(polygons)-len(polygons_without_label))

    return tot_layers

if __name__ ==  "__main__":

    # VALIDATION and Test based on pelgas 2023 data, where we know the label associated with the polygons
    report=Report()
    stat= StatCounter()
    with open(str(Path("D:/PELGAS23/workdir/radials/data_with_polygon.pkl")), "rb") as f:
        dataview = pickle.load(f)
        for radial in dataview:

            for esu in radial.esu:
                # if radial.name == "R13_CM-a1":  # looks like merging issues
                #     continue
                # if radial.name == "R09_CL-a":  # Error on radial ESU_20230507_141352_20230507_142002, one polygon is D8 but no D8 in mantot
                #     continue
                # if radial.name == "R10_LC-b": #polygon label as D13 but nothing like this in mantot, in fact no polygon on screenshot
                #     continue
                # if radial.name == "R12_CM-a":  #polygon label as D13 but nothing like this in mantot, in fact no polygon on screenshot
                #     continue
                # if radial.name == "R13_CM-a2":  #ESU_20230511_094418_20230511_094959 , probablement polygons remplacé par couche \
                #                                 #R13_CM-a2/ESU_20230511_095544_20230511_100128 mauvaise copie de polygones
                #     continue
                # if radial.name in ["R20_CM-a1","R21_MC-a","R21_MC-b","R22_MC-a","R22_ML-a1","R25_LM-a3"]: #polygon label as D13 but nothing like this in mantot and found exactly another label
                #     continue

                # if radial.name == "R14_LC-b": #label D9 but no D9 in mantot
                #     continue
                # if radial.name == "R16_ML-a2": #one polygon file is missing 'number 2'
                #     continue
                # if radial.name == "R11_CM-c": #bad polygon label
                #     continue
                # if radial.name == "R22_MC-b": #polygon label as D13 but nothing like this in mantot, D14 instead
                #     continue
                # if radial.name == "R16_ML-a1":
                #     continue
                # if (radial.name == "R15_CM-b"  and esu.directory.name == "ESU_20230514_121630_20230514_122227"):  #polygon file 1 should have been removed
                #     continue
                # if (radial.name == "R17_LM-a1"  and esu.directory.name == "ESU_20230516_141033_20230516_141625"):  #polygon file 1 should have been removed
                #     continue
                # ... to be continued, but likely error in the data rather than in the algorithm
                polygons = esu.polygon_files
                tot_layers = man_tot.read_esu(esu.esu_mantot)
                try:
                    extract_polygon_label(polygon_files=polygons, tot_layers = tot_layers,report=report,context=Context(esu=esu,radial_name=radial.name),stat=stat,write_result=False)
                except BadAssumption as e:
                    report.error(msg=f"Error while processing ESU {esu.directory.as_uri()} : {e}",channel="Bad Assumption",radial_name=radial.name)

    report.print()
    print(stat)
