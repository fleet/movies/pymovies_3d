from argparse import ArgumentError
from pathlib import Path


from dataclasses import dataclass,fields

from cycler import V
from twine import metadata

def validate_single_path(value:Path):
    
    if isinstance(value, Path) and not value.exists()  :  # Vérifie si l'attribut est de type Path
        raise FileNotFoundError(f"File not found {value}")    

def validate_paths(dataclass_instance):
    # Fonction pour vérifier l'existence des fichiers pour les attributs de type Path
    for field in fields(dataclass_instance):
        value = getattr(dataclass_instance, field.name)
        if value is None:
            print(f"Null value : {field.name}")
        else: 
            validate_single_path(value=value)
    

@dataclass
class ValidationInputPathParameters():
    """Class containing file parameters for data validation """
    screen_shots:Path
    mantot_file:Path
    polygon_path:Path
        
    # Fonction pour vérifier l'existence des fichiers pour les attributs de type Path
    def validate(self):
        validate_paths(self)
    

@dataclass
class ValidationWorkdirPaths():
    reports_dir:Path
    sequence_workdir:Path
    data_model_path:Path
    metadata_database:Path
    
    def make_dir(self):
        
        #create subdir if not exists
        self.sequence_workdir.mkdir(parents=True,exist_ok=True)
        self.reports_dir.mkdir(parents=True,exist_ok=True)

    def validate(self):

        validate_single_path(self.reports_dir)
        validate_single_path(self.sequence_workdir)
        validate_single_path(self.data_model_path.parent)
    
@dataclass
class EchoIntegrationPaths():
    """Parameters for EI files"""
    echointegration_files_60:Path
    echointegration_files_80:Path
    
    def validate(self):
        validate_paths(self)

def make_as_usual(year:int, root_dir:Path):
    """Compute path as usal, ie as data are stored on datarwork"""
    if year < 2000:
        raise ArgumentError(argument="year",message="year should be between 2000 and 2030")
    main_dir = root_dir / Path(f"PELGAS{year}")
    validation_dir  = main_dir / "validation" 
    ei_dir = main_dir /  "Acoustique/echointegration"

    return ValidationInputPathParameters(
        screen_shots= main_dir / "Acoustique/Echotypage/screenshots",
        mantot_file = main_dir / r"EvaluationBiomasse\Donnees\Acoustique\mantot38.csv",
        polygon_path = main_dir / "Acoustique/Echotypage/polygons/"
        
    ),ei_dir / f"PELGAS_{year}_acoustic_sequence.csv",  ValidationWorkdirPaths(
        reports_dir = validation_dir  / "report",
        sequence_workdir = validation_dir / "sequences",
        data_model_path = validation_dir / "data_model.pkl",
        metadata_database = validation_dir / "metadata_database.pkl"
    ), EchoIntegrationPaths(
        echointegration_files_60=ei_dir / "-60",
        echointegration_files_80=ei_dir / "-80",
    )


class PreprocessingParameters():
    """Base class for preprocessing parameters"""

    def __init__(self,
                 year:int,
                 input_paths :ValidationInputPathParameters,
                 sequence_file :Path,
                 workdir_path :ValidationWorkdirPaths,
                 ei_path :EchoIntegrationPaths,
                 use_ei_files=True) -> None:
        self.year  = year,
        self.input_paths: ValidationInputPathParameters=input_paths
        self.sequence_file:Path=sequence_file
        self.workdir_paths:ValidationWorkdirPaths=workdir_path
        self.ei_path: EchoIntegrationPaths = ei_path
        self.use_ei_files: bool = use_ei_files


    def set_use_echointegrated_files(self,use:bool):
        self.use_ei_files = use

    def is_using_echointegrated_files(self):
        return self.use_ei_files
    

    def __str__(self):
        return str(vars(self))
        
        
    def validate(self):
        self.input_paths.validate()
        if not self.sequence_file.exists():
            raise FileNotFoundError(f"File not found {self.sequence_file}")    
        if self.use_ei_files:
            self.ei_path.validate()
        self.workdir_paths.validate()

        
        