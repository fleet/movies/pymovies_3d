# -*- coding: utf-8 -*-
"""
    Created on Wed Feb  5 11:52:44 2020
    @author: nlebouff
    
    Lit un hac ou un dossier de RUN, effectue la localisation des lignes de niveaux autour de l'écho de fond
    Calcule l'entropie liée à la forme de l'écho de fond pour chaque ping et la compare à un seuil pour déterminer
    s'il y a présence d'herbier
    
    Enregistre les images des échogrammes avec les détections d'herbier, ainsi que l'entropie calculée ping à ping
    Enregistre des fichiers csv par chunk, avec les colonnes Long/Lat/herbier/entropie/heure/minute/seconde + ~profondeur + vitesse + lignes de niveaux
    Enregistre des fichiers hac avec le fond corrigé adapté aux cas d'herbiers
    
    Renseigner en début de code :
        le numéro de run ou le nom du fichier à traiter (choisir le bloc à activer L39)
        les chemins des dossiers des fichiers et de sauvegarde
        le chemin de la configuration M3D 

"""

# imports
#########
from pyMovies import *

from drawing import DrawEchoGram
import pymovies_3d.core.hac_util.hac_util as util

import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import datetime
import csv
from scipy import ndimage
from scipy.signal import medfilt

# fichier de config et fichier hac + paramètres
###############################################
if 1:  # traitement d'un RUN
    chemin_ini = "F:\\algues\Morbihan13\EK60_Morbihan_2013"  # dossier contenant les dossiers de RUN
    # chemin_ini="F:\\algues\Marhalio19\MARHALIO2\HAC_herbiers" # dossier contenant les dossiers de RUN
    run = str(1)  # numéro de run à traiter
    chemin_sav = "F:\\algues\Morbihan13\Resultats7\\"  # dossier de sauvegarde des csv et échogrammes
    # chemin_sav="F:\\algues\Marhalio19\Resultats\Lancieux19_2\\" # dossier de sauvegarde des csv et échogrammes
    chemin_config = ".\config_det\\"  # dossier de config M3D

    num_run = "000" + run
    num_run = num_run[-3:]
    name_sav = "RUN" + num_run
    FileName = chemin_ini + "\\RUN" + num_run + "\\"
    CheminNewHac = chemin_ini + "\\HAC_herbiers\\RUN" + num_run + "\\"

    if not os.path.isdir(FileName):
        print("le dossier n existe pas")
        sys.exit()

else:  # traitement d'un hac
    chemin_ini = "F:\data\marhalio-leg2\SBES_MBES\MARHALIO2\\RUN004\\"  # dossier contenant le hac
    # chemin_ini="F:\data\marhalio-leg2\EK60_Morbihan_2013\\"
    chemin_sav = (
        "F:\\algues\\Lancieux19"  # dossier de sauvegarde des csv et échogrammes
    )
    chemin_config = ".\config_det\\"  # dossier de config M3D
    # Hac_name="MARHALIO2_002_20190906_110459.hac"
    # Hac_name="MARHALIO2_002_20190906_063033.hac"
    Hac_name = "MARHALIO2_004_20190909_105236.hac"
    # Hac_name="det_ES120-7C_ZOSTERA_006_20130903_083948.hac"

    name_sav = Hac_name[-4:]
    FileName = chemin_ini + "\\" + Hac_name
    CheminNewHac = chemin_ini + "\\HAC_herbiers\\"

    if not os.path.isfile(FileName):
        print("le fichier n existe pas")
        sys.exit()


# goto = datetime.datetime(yyyy, m, d,h,mm,ss,tzinfo=datetime.timezone.utc).timestamp()
# goto = datetime.datetime(2019, 9, 6,8,38,23,tzinfo=datetime.timezone.utc).timestamp()
# goto = datetime.datetime(2019, 9, 6,11,15,50,tzinfo=datetime.timezone.utc).timestamp()
# goto = datetime.datetime(2013, 9, 4,6,42,49,tzinfo=datetime.timezone.utc).timestamp()
# goto = datetime.datetime(2013, 9, 3,8,57,51,tzinfo=datetime.timezone.utc).timestamp()
# goto = datetime.datetime(2013, 9, 3,8,57,20,tzinfo=datetime.timezone.utc).timestamp()
# goto = datetime.datetime(2019,9,11,13,11,33,tzinfo=datetime.timezone.utc).timestamp()
goto = -1

niv = np.flipud([-10, -15, -20, -25, -30, -35, -40, -45, -50])
iniv_ref = 5
n_niv = np.size(niv, 0)

seuil_entropy = 8

Rec_sv_dev = True

# ouverture hac et config
#########################

if not os.path.isdir(chemin_sav + "\\echogrammes"):
    os.makedirs(chemin_sav + "\\echogrammes", exist_ok=True)
    os.makedirs(chemin_sav + "\\csv_herbiers", exist_ok=True)
if not os.path.isdir(CheminNewHac):
    os.makedirs(CheminNewHac, exist_ok=True)


moLoadConfig(chemin_config)
moOpenHac(FileName)

# presentation sondeurs
#######################
list_sounder = util.hac_sounder_descr()
# choix sondeur à traiter
index_sondeur = 0
index_trans = 0

if index_sondeur >= list_sounder.GetNbSounder():
    print("index sondeur hors limite")
    sys.exit()
sounder = list_sounder.GetSounder(index_sondeur)
if index_trans >= sounder.m_numberOfTransducer:
    print("index transducteur hors limite")
    sys.exit()
transducer = sounder.GetTransducer(index_trans)

dec_tir = int(4 * np.ceil(transducer.m_pulseDuration / transducer.m_timeSampleInterval))
delta = transducer.m_beamsSamplesSpacing


ParameterDef = moLoadReaderParameter()
ParameterDef.m_ChunckDef.m_chunkNumberOfPingFan = 2000
taille_chunk = ParameterDef.m_ChunckDef.m_chunkNumberOfPingFan
moSaveReaderParameter(ParameterDef)

# impose taille mémoire = taille chunk pour enregistrement du hac
ParameterKernel = moLoadKernelParameter()
ParameterKernel.m_nbPingFanMax = taille_chunk
ParameterKernel.m_bAutoLengthEnable = False
moSaveKernelParameter(ParameterKernel)


# goto éventuel
#################
if goto > -1:
    hac_goto(goto)

# lecture et traitement
######################
FileStatus = moGetFileStatus()

ilec = 0
iping = -1

cor1 = np.full(0, np.nan)
cor2 = np.full(0, np.nan)
imax_prox = np.full(0, np.nan)
prof_max = np.full(0, np.nan)
ir_chx = np.full(0, np.nan)
entropy = np.full(0, np.nan)
ir_quant = np.full(0, np.nan)
ir_niv = np.full((n_niv, 0), np.nan)
ir_niv2 = np.full((n_niv, 0), np.nan)
std_ir15 = np.full(0, np.nan)
pil = np.full(0, np.nan)
ih_ref = np.full(0, np.nan)
d_buffer = np.array([0])
d_ref_quant = np.full(0, np.nan)
isep = np.full(0, 0)
ctr = np.full(0, 0)
dif_ei = np.full(0, 0)
iping_h = -1
index_pg = np.full(ParameterDef.m_ChunckDef.m_chunkNumberOfPingFan, np.nan)
pingId = -1

data_csv = np.full((0, 20), np.nan)

# while not FileStatus.m_StreamClosed and ilec<1:
while not FileStatus.m_StreamClosed:
    moReadChunk()
    ilec = ilec + 1
    print("ping " + str(iping + 1))
    iping0 = iping + 1


    # remove pings from memory that were read in previous chunk (end of file)
    MX = moGetPingFan(0)
    while MX.m_computePingFan.m_pingId <= pingId:
        moRemovePing(MX.m_computePingFan.m_pingId)
        MX = moGetPingFan(0)
    
    nb_pings = moGetNumberOfPingFan()

    for index in range(max(0, nb_pings - taille_chunk), nb_pings):
        MX = moGetPingFan(index)
        SounderDesc = MX.m_pSounder
        if (SounderDesc.m_SounderId == sounder.m_SounderId) & (MX.m_computePingFan.m_pingId > pingId):
            pingId = MX.m_computePingFan.m_pingId
            polarMat = MX.GetPolarMatrix(index_trans)
            sv_data_ini = (np.array(polarMat.m_Amplitude) / 100.0).reshape(-1)
            # lissage sur 4 échantillons et 2 pings
            sv_data_smooth = np.correlate(sv_data_ini, np.ones(4) / 4, "same")
            # sv_data_smooth=sv_data_ini #temp
            iping = iping + 1
            index_pg[iping - iping0] = index
            if iping == 0:
                sv_data_prec = sv_data_smooth
            sv_data = (sv_data_smooth + sv_data_prec) / 2
            sv_data_prec = sv_data_smooth
            sv_data = sv_data_smooth  # temp

            cor1 = np.append(cor1, np.nan)
            cor2 = np.append(cor2, np.nan)
            imax_prox = np.append(imax_prox, np.nan)
            prof_max = np.append(prof_max, np.nan)
            ir_chx = np.append(ir_chx, np.nan)
            std_ir15 = np.append(std_ir15, np.nan)
            pil = np.append(pil, np.nan)
            entropy = np.append(entropy, np.nan)
            ih_ref = np.append(ih_ref, np.nan)
            ir_quant = np.append(ir_quant, np.nan)
            d_ref_quant = np.append(d_ref_quant, np.nan)
            ir_niv = np.append(ir_niv, np.full((n_niv, 1), np.nan), axis=1)
            ir_niv2 = np.append(ir_niv2, np.full((n_niv, 1), np.nan), axis=1)

            if iping == 0:
                uu = np.asarray(np.where(sv_data < -30))  # indices des éléments <-30
                dec_tir = max(
                    [dec_tir, uu[0, 0] + 1]
                )  # longueur du tir (si sous-évaluée)
                nb_ech = np.size(sv_data, axis=0)

            if (iping == iping0) & Rec_sv_dev:
                sv_mat = np.full((1, nb_ech), np.nan)
                lat = np.full(0, np.nan)
                long = np.full(0, np.nan)

            if (iping > iping0) & Rec_sv_dev:
                sv_mat = np.append(sv_mat, np.full((1, nb_ech), np.nan), axis=0)
                lat = np.append(
                    lat, MX.beam_data[index_trans].navigationPosition.m_lattitudeDeg
                )
                long = np.append(
                    long, MX.beam_data[index_trans].navigationPosition.m_longitudeDeg
                )

            # correction de pilonnement en fonction de l'attitude
            cor1[iping] = MX.beam_data[
                0
            ].navigationAttitude.sensorHeaveMeter - 0 * np.tan(
                MX.beam_data[0].navigationAttitude.pitchRad
            )
            cor2[iping] = np.cos(MX.beam_data[0].navigationAttitude.pitchRad) * np.cos(
                MX.beam_data[0].navigationAttitude.rollRad
            )

            ##########################################
            # detection de fond par lignes de niveaux #
            ##########################################

            # niveau maximum de sv sur le ping, et index de sample correspondant
            max_sv = max(sv_data[dec_tir:])
            imax = dec_tir + np.argmax(sv_data[dec_tir:])

            # initialisation du niveau maximum de sv dans le voisinage
            # de la profondeur détectée au ping précédent
            imax_prox[iping] = imax
            max_sv_prox = max_sv

            # examen du maximum
            if max_sv < -35:  # pas de fond, niveau maximum trop faible
                if iping > 0:  # on garde les valeurs du ping précédent
                    imax_prox[iping] = imax_prox[iping - 1]
                    std_ir15[iping] = std_ir15[iping - 1]
                    # if ~isnan(ir_chx[iping-1]) :
                    #     std_ir15[iping]=std_ir15[iping-1]
                else:
                    imax_prox[iping] = np.nan
            else:  # on dispose d'un fond crédible
                if (
                    iping > 0
                ):  # on teste si le maximum de l'écho de fond déterminé est plausible (ce pourrait être un écho fort de la colonne d'eau)
                    if (
                        abs(
                            (imax_prox[iping] + cor1[iping] / delta) * cor2[iping]
                            - (imax_prox[iping - 1] + cor1[iping - 1] / delta)
                            * cor2[iping - 1]
                        )
                        > 3 * std_ir15[iping - 1]
                    ):  # on compare la profondeur du maximum du ping à celle du ping précédent.
                        # Si le résultat est supérieur à 3 fois l'écart-type observé,
                        # on cherche un candidat de maximum de fond plus
                        # crédible à proximité de la détection précédente

                        # iim=int(np.floor(((imx[ip-1]+cor1[ip-1]/delta)*cor2[ip-1]/cor2[ip]-cor1[ip]/delta))) # index d'échantillon de même profondeur que le maximum du fond du ping précédent
                        # #nuu=max(1,int(np.ceil((3*std_ir15[iping-1])/delta)))
                        # nuu=20
                        # iech1=int(max(dec_tir,iim-nuu))
                        # iech2=iech1+2*nuu
                        # sv_nat=10**(sv_data[iech1:iech2+1]/10)
                        # imax_t=int(np.sum(sv_nat*np.arange(iech1,iech2+1))/np.sum(sv_nat))
                        # # max_sv_prox=max(sv_data[iech1:iech2+1]) # recherche du max du ping dans un voisinage du max du ping précédent, large de 3x l'écart-type (et après le tir)
                        # # imax_t=iech1+np.min(np.argmax(sv_data[iech1:iech2+1])) # extraction de l'index d'échantillon correspondant
                        # if sv_data[imax_t]>-35 : #maximum plus crédible (de niveau suffisant)
                        #     imx[ip]=imax_t

                        iim = int(
                            np.floor(
                                (
                                    (imax_prox[iping - 1] + cor1[iping - 1] / delta)
                                    * cor2[iping - 1]
                                    / cor2[iping]
                                    - cor1[iping] / delta
                                )
                            )
                        )  # index d'échantillon de même profondeur que le maximum du fond du ping précédent
                        nuu = max(1, int(np.ceil((3 * std_ir15[iping - 1]) / delta)))
                        nuu = 20
                        iech1 = int(max(dec_tir, iim - nuu))
                        iech2 = iech1 + 2 * nuu
                        sv_nat = 10 ** (sv_data[iech1 : iech2 + 1] / 10)
                        imax_t = int(
                            np.sum(sv_nat * np.arange(iech1, iech2 + 1))
                            / np.sum(sv_nat)
                        )
                        max_sv_prox = sv_data[imax_t]
                        # max_sv_prox=max(sv_data[iech1:iech2+1]) # recherche du max du ping dans un voisinage du max du ping précédent, large de 3x l'écart-type (et après le tir)
                        # imax_t=iech1+np.min(np.argmax(sv_data[iech1:iech2+1])) # extraction de l'index d'échantillon correspondant
                        if (
                            max_sv_prox > -35
                        ):  # maximum plus crédible (de niveau suffisant)
                            imax_prox[iping] = imax_t

            # lignes de niveau
            for iniv in range(0, n_niv):
                vec_inf = (
                    sv_data < niv[iniv]
                )  # données du ping inférieures au niveau considéré
                vec_inf2 = ndimage.binary_erosion(
                    vec_inf, np.full(4, True)
                )  # on érode le résultat de 4 échantillons, pour ne garder que les séquences inférieures au niveau considéré sur au moins 4 échantillons successifs
                ind_niv = np.asarray(
                    np.where(vec_inf2[0 : int(imax_prox[iping])])
                )  # on trouve les échantillons respectant ce critère entre le tir et le maximum du fond
                if (
                    ind_niv.size == 0
                ):  # pas d'échantillon candidat, pas de passage sous la ligne de niveau
                    ir_niv[iniv, iping] = dec_tir
                else:
                    ir_niv[iniv, iping] = np.min(
                        [np.max(ind_niv) + 1, imax_prox[iping]]
                    )  # l'échantillon (+1) le plus proche du maximum du fond correspond à la ligne de niveau recherchée

                # ligne de niveau équivalente, mais après le maximum de
                # l'écho de fond
                ind_niv = np.asarray(np.where(vec_inf2[int(imax_prox[iping]) :]))
                if not ind_niv.size == 0:
                    ir_niv2[iniv, iping] = imax_prox[iping] + np.max(
                        [np.min(ind_niv) - 2, 0]
                    )
                else:
                    ir_niv2[iniv, iping] = nb_ech

            # on considère la ligne de niveau de référence comme
            # détection initiale du fond.
            ir_chx[iping] = ir_niv[iniv_ref, iping]  ##ECRASE APRES

            if 0:  # détection mixte
                # on regarde l'espacement des lignes de niveaux
                # suivantes (s'éloignant du fond)
                dec = ir_niv[1 : iniv_ref + 2, iping] - ir_niv[0 : iniv_ref + 1, iping]
                # on repère les espacements supérieurs à 2
                # échantillons, ou croissant par rapport à la
                # différence des deux lignes précédentes (plus loin du
                # fond)
                ind_plateau = np.asarray(
                    np.where(
                        (dec[0 : iniv_ref - 1] > 2)
                        | (dec[0 : iniv_ref - 1] > dec[1:iniv_ref])
                    )
                )
                # backstep éventuel des lignes de niveau pour la
                # détection de fond:
                if (
                    ind_plateau.size == 0
                ):  # toutes les lignes sont très proches (<1 ech) et se resserrent continuement en s'éloignant du fond (front montant)
                    ir_chx[iping] = ir_niv[
                        0, iping
                    ]  # on prend alors la ligne de niveau la plus faible (la moins profonde)
                else:
                    ir_chx[iping] = ir_niv[
                        np.max(ind_plateau), iping
                    ]  # on prend la première ligne de niveau précédente, présentant
                    # une distance de plus de 2 échantillons avec celle d'avant , ou un écart croissant (détection de début de front montant)

            prof_max[iping] = (imax_prox[iping] * delta + cor1[iping]) * cor2[iping]

            # calcul de l'écart-type de la ligne du niveau max de
            # l'écho de fond sur les derniers pings
            if iping > 1:
                u = prof_max[max(2, iping - 14) : iping]
                u = u[~np.isnan(u)]
                std_ir15[iping] = np.sqrt(np.var(u))
            else:
                std_ir15[iping] = 2 * delta

            # calcul pilonnement (positif vers le haut)
            pil[iping] = (imax_prox[iping] + cor1[iping] / delta) * cor2[
                iping
            ] - imax_prox[iping]

            ##################
            # cuisine algues #
            ##################
            # entropie
            echo_fd = 80 + np.maximum(
                -79, sv_data_ini[int(ir_niv[0, iping]) : int(ir_niv2[2, iping])]
            )
            p = echo_fd / (10 * np.log10(np.sum(10 ** (echo_fd / 10))))
            entropy[iping] = np.sum(-p * np.log(p))
            crit_entropy = entropy[iping] > seuil_entropy

            # echo intégration cumulée
            echo_fd2 = sv_data_ini[int(ir_niv[0, iping]) : int(ir_niv2[0, iping] + 1)]
            cdf_echo = 10 * np.log10(
                np.correlate(
                    10 ** (echo_fd2 / 10),
                    np.ones(max(1, int(ir_niv2[0, iping] - ir_niv[0, iping]))),
                    "full",
                )
            )
            dBmax = np.max(cdf_echo)
            # indquant=np.min(np.where(cdf_echo>dBmax-0.22)) #ligne des 95%
            indquant = np.min(np.where(cdf_echo > dBmax - 0.13))  # ligne des 97%
            ir_quant[iping] = int(ir_niv[0, iping]) + indquant

            # trouve la limite sup du max du niveau cumulé sur 6 ech et teste si le fond est visible
            echo_fd3 = sv_data_ini[int(ir_niv[5, iping]) : int(ir_niv2[5, iping]) + 6]
            vv = np.correlate(10 ** (echo_fd3 / 10), np.full(6, 1), "full")
            ctr = np.append(ctr, np.max(vv) / sum(10 ** (echo_fd3 / 10)))
            isep = np.append(isep, ir_niv[5, iping] + np.argmax(vv) - 6)

            ir_chx[iping] = isep[iping]
            # ir_chx[iping]=ir_niv[7,iping]

            data_csv = np.append(data_csv, np.full((1, 20), np.nan), axis=0)
            data_csv[-1, 0] = MX.beam_data[
                index_trans
            ].navigationPosition.m_longitudeDeg
            data_csv[-1, 1] = MX.beam_data[
                index_trans
            ].navigationPosition.m_lattitudeDeg
            timeping = MX.m_meanTime.m_TimeCpu + MX.m_meanTime.m_TimeFraction / 10000
            date_ping = util.hac_read_day_time(timeping)
            data_csv[-1, 2] = str(date_ping.hour)
            data_csv[-1, 3] = str(date_ping.minute)
            data_csv[-1, 4] = str(date_ping.second)
            data_csv[-1, 5] = str(date_ping.microsecond)
            titre = ("Long", "Lat", "heure", "minutes", "secondes", "microsecondes")
            data_csv[-1, 6] = crit_entropy
            titre = titre + ("Herbier",)
            data_csv[-1, 7] = entropy[iping]
            titre = titre + ("Entropy",)
            data_csv[-1, 8] = ir_quant[iping]
            titre = titre + ("97%",)
            data_csv[-1, 9] = ir_niv[0, iping]  # ligne niveau 50dB
            titre = titre + ("50dB",)
            data_csv[-1, 10] = ir_niv[2, iping]  # ligne niveau 40dB
            titre = titre + ("40dB",)
            data_csv[-1, 11] = ir_niv[5, iping]  # ligne niveau 25dB
            titre = titre + ("25dB",)
            data_csv[-1, 12] = ir_niv2[0, iping]  # ligne niveau 50dB
            titre = titre + ("50dB dessous",)
            data_csv[-1, 13] = ir_niv2[2, iping]  # ligne niveau 40dB
            titre = titre + ("40dB dessous",)
            data_csv[-1, 14] = ir_niv2[5, iping]  # ligne niveau 25dB
            titre = titre + ("25dB dessous",)
            data_csv[-1, 15] = ir_quant[iping] * delta  # profondeur , sera écrasé par cuisine herbier
            titre = titre + ("~profondeur (m)",)
            data_csv[-1, 16] = (
                MX.beam_data[0].navigationAttribute.m_speedMeter * 3600 / 1852
            )  # vitesse knt
            titre = titre + ("vitesse knt",)
            data_csv[-1, 17] = timeping  # time
            titre = titre + ("time POSIX",)
            data_csv[-1, 18] = ir_quant[iping] #  sera écrasé par cuisine herbier
            titre = titre + ("profondeur index",)
            data_csv[-1, 19] = delta
            titre = titre + ("delta (m)",)

            if Rec_sv_dev:
                sv_mat[iping - iping0, :] = sv_data.reshape(1, sv_data_ini.size)

            d_ref_quant[iping] = np.nan
            if not crit_entropy:
                ih_ref[iping] = iping
                iping_h = iping_h + 1
                if d_buffer.size < 20:
                    d_buffer = np.append(d_buffer, ir_quant[iping] - ir_chx[iping])
                else:
                    d_buffer[np.mod(iping_h, 20)] = ir_quant[iping] - ir_chx[iping]
            elif iping > 0:
                ih_ref[iping] = ih_ref[iping - 1]
                d_ref_quant[iping] = np.quantile(d_buffer, 0.5)
            else:
                ih_ref[iping] = np.nan
                d_ref_quant[iping] = np.quantile(d_buffer, 0.5)

    # fond herbiers
    ##############

    ir_quant_smooth = np.correlate(ir_quant, np.ones(3) / 3, "same")
    # ir_quant_smooth=medfilt(ir_quant,5)

    fd_herbier=np.where(np.logical_and(entropy[iping0:iping+1]>seuil_entropy,ctr[iping0:iping+1]<0.8),ir_quant_smooth[iping0:iping+1]-d_ref_quant[iping0:iping+1],ir_niv[5,iping0:iping+1])
    fd_herbier=np.where(np.logical_and(entropy[iping0:iping+1]>seuil_entropy,ctr[iping0:iping+1]>=0.8),isep[iping0:iping+1],fd_herbier)
    #fd_herbier=np.where(entropy[iping0:iping+1]>seuil_entropy*1.5,ir_quant[iping0:iping+1]-d_ref_smooth,fd_herbier)
    fd_herbier=np.where(entropy[iping0:iping+1]>seuil_entropy*1.5,ir_quant_smooth[iping0:iping+1]-d_ref_quant[iping0:iping+1],fd_herbier)
    fd_herbier=np.where(ctr[iping0:iping+1]>=0.8,isep[iping0:iping+1],fd_herbier)
    fd_herbier=medfilt(fd_herbier,5)


    # modification des données de fond du hac
    ########################################
    for iip in range(0, iping - iping0 + 1):
        MX = moGetPingFan(int(index_pg[iip]))
        if not (np.isnan(fd_herbier[iip])):
            MX.beam_data[index_trans].m_bottomRange = (fd_herbier[iip] - 1) * delta
            MX.m_maxRangeWasFound = True
            MX.beam_data[index_trans].m_bottomWasFound = True
        else:
            MX.beam_data[index_trans].m_bottomWasFound = False
            
    # modification des données de fond du csv
    ########################################
    data_csv[iping0:iping+1, 15] = (fd_herbier - 1) * delta  # profondeur
    data_csv[iping0:iping+1, 18] = fd_herbier
    
    
    # écriture HAC
    ##############
    MX = moGetPingFan(0)
    timestart = MX.m_meanTime.m_TimeCpu + MX.m_meanTime.m_TimeFraction / 10000
    date_start = util.hac_read_day_time(timestart)
    hhs = "0" + str(date_start.hour)
    mms = "0" + str(date_start.minute)
    sss = "0" + str(date_start.second)
    moStartWrite(
        CheminNewHac, "det_" + hhs[-2:] + "_" + mms[-2:] + "_" + sss[-2:] + "_"
    )
    moStopWrite()

    # Affichage éventuel
    ####################
    if Rec_sv_dev:
        step = 400
        plt.figure(1, figsize=(30, 15))
        plt.clf()
        plt.tight_layout()
        ax1 = plt.subplot(2, 1, 1)
        ax1.imshow(sv_mat.T, vmin=-80, vmax=10, aspect="auto", cmap="jet")
        fd2h = np.where(
            entropy[iping0 : iping + 1] > seuil_entropy,
            ir_niv[5, iping0 : iping + 1],
            np.nan,
        )
        plt.plot(fd2h, "w*", markersize=8)
        plt.plot(ir_quant[iping0 : iping + 1], "c")
        plt.plot(fd_herbier, "k")
        plt.ylabel("samples , delta= " + str(np.round(delta * 1000) / 10) + " cm")
        ax2 = plt.subplot(2, 1, 2)
        plt.plot(entropy[iping0:iping])
        plt.grid()
        for iplot in range(0, int(np.ceil((iping - iping0 + 1) / step))):
            ip1 = iplot * step
            ip2 = ip1 + step - 1
            fd_moy = np.median(ir_chx[iping0 + ip1 : iping0 + ip2])
            plt.axes(ax1)
            plt.ylim(fd_moy + 100, fd_moy - 100)
            plt.xlim(ip1, ip2)
            plt.axes(ax2)
            plt.xlim(ip1, ip2)
            MX = moGetPingFan(ip1)
            timestart = MX.m_meanTime.m_TimeCpu + MX.m_meanTime.m_TimeFraction / 10000
            date_start = util.hac_read_day_time(timestart)
            hhs = "0" + str(date_start.hour)
            mms = "0" + str(date_start.minute)
            sss = "0" + str(date_start.second)
            plt.savefig(
                chemin_sav
                + "\\echogrammes\\"
                + "test_herbiers_"
                + name_sav
                + "_"
                + hhs[-2:]
                + "_"
                + mms[-2:]
                + "_"
                + sss[-2:]
                + ".png"
            )
            # plt.colorbar(ax)
            # plt.plot(ir_chx.T)
            # plt.plot(ir_niv.T)
            # plt.plot(ir_niv2.T)
            # plt.plot(imax_prox)

# écriture csv
#############
if data_csv.size > 0:
    with open(
        chemin_sav + "\\csv_herbiers\\" + "test_herbiers_" + name_sav + ".csv",
        "w",
        newline="",
    ) as csvfile:
        writer = csv.writer(csvfile, delimiter=",")
        writer.writerow(titre)
        writer.writerows(data_csv)
