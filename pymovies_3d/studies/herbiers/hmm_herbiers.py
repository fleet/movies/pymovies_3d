# -*- coding: utf-8 -*-
"""
Created on Mon Apr 27 14:57:11 2020

@author: nlebouff
"""
import numpy as np
import matplotlib.pyplot as plt


def hmm_herbiers(sv_mat, pil, ir_chx, ir_niv, ir_niv2, ir_quant, ip1, ip2):
    """
    pistage markovien
    """
    # observations
    npings = ip2 - ip1 + 1
    ie1 = int(min(ir_niv[0, ip1 : ip2 + 1]))
    ie2 = int(max(ir_niv2[0, ip1 : ip2 + 1]))
    nech = int(ie2 - ie1 + 1)
    nfd = 6  # epaisseur fond
    B = np.full((npings, nech), np.nan)
    for ip in range(ip1, ip2 + 1):
        b = np.correlate(10 ** (sv_mat[ip, ie1 : ie2 + 1] / 10), np.ones(nfd), "same")
        ib1 = int(ir_niv[0, ip] - ie1)
        ib2 = int(ir_niv2[0, ip] - ie1)  # dernier pas pris en compte
        # b=b/np.sum(b[ib1:ib2])
        b = (10 * np.log10(b) + 35) / 10
        # b[0:ib1]=-1 #masquage
        # b[ib2:]=-1 #masquage
        B[ip - ip1, :] = b

    # plt.figure(3)
    # plt.clf()
    # plt.imshow(B.T, vmin=0, vmax=0.1, aspect='auto')
    # plt.colorbar()

    # etat initial
    fd = np.full(npings, int(0))
    # fd[0]=56
    fd[0] = ir_chx[ip1] - ie1 + 2
    alpha = np.full(nech, float(0))
    alpha[fd[0]] = 1

    # proba de transition
    Dmax = 5
    fact = 1
    A = np.full((nech, nech), float(0))
    for ii in range(0, nech):
        jj = np.arange(0, nech)
        a = np.abs(ii - jj)
        a = np.where(a <= Dmax, np.cos(np.pi / 2 * a / Dmax) ** fact, 0)
        A[ii, :] = a.T

    # plt.figure(4)
    # plt.clf()
    # plt.imshow(10*np.log10(Vit.T), aspect='auto')

    # Viterbi
    Vit = np.full((npings, nech), 0)
    # Alpha=np.full((npings,nech),np.nan)
    alpha_p = alpha
    for ip in range(ip1 + 1, ip2 + 1):
        for ii in range(0, nech):
            trans = (
                alpha
                * A[:, max(0, min(nech - 1, ii + int(pil[ip] - pil[max(0, ip - 1)])))]
            )
            Vit[ip - ip1, ii] = int(np.argmax(trans))
            alpha_p[ii] = trans[Vit[ip - ip1, ii]] * B[ip - ip1, ii]
        alpha = alpha_p / np.max(alpha_p)
    #     Alpha[ip-ip1,:]=alpha

    # plt.figure(6)
    # plt.clf()
    # plt.imshow(Alpha.T)

    # gagnant
    fd[-1] = np.argmax(alpha)
    # fd[-1]=28
    for ip in range(ip2 - 1, ip1, -1):
        fd[ip - ip1] = Vit[ip - ip1 + 1, fd[ip - ip1 + 1]]

    # plt.figure(2)
    # plt.clf()
    # plt.imshow(sv_mat.T, vmin=-80, vmax=10, aspect='auto',cmap='jet')
    # plt.plot(range(ip1,ip2+1),ie1+fd,'w')
    # plt.plot(ir_quant)

    return ie1 + fd
