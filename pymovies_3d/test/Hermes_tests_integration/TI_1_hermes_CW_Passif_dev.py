# -*- coding: utf-8 -*-
"""
Created on Mon Sep 18 15:11:25 2023

@author: nlebouff


Effectue des tests unitaires Hermes

    test unitaire CW Passif:
           - effectue la détection de cibles simples sur les fichiers hac du répertoire Ref_data
           - charge les résultats de références data_ref obtenus à l'aide d'une version validée (TS_unit_test_create_result)
           - compare les TScomp, TSrange, TSangles et TSpositions pour chaque transducteur
           - si des résultats différents sont obtenus un message "KO" est indiqué, "OK" sinon
           - si tous les résultats sont concordants, le .pickle et le dossier de résultats sont supprimés, sinon ils sont conservés pour analyse
"""
import os
import shutil
import pickle
import datetime
import pytz
from pathlib import Path
from pyMovies import pyMovies as mv
import numpy as np

##########################################################################
# test d'intérgation analysant l'acquisition de la configuration CW_Passif
##########################################################################
print("****** Test Integration Hermes CW_passif")
test_all=True

# chemins
chemin_config = './Config_Lecture_M3D/'
#chemin_hac='//134.246.149.203/Hermes_Tests_Integration/Data/HAC/'
chemin_hac='V:\Data\HAC'
chemin_raw='U:\TestsIntegration'

#paramètres config hermes
rec=0.5
range_record=20 # -1: si asservi au fond
bottom_tol=20 #m, tolerance d'enregistrement au fond
T_us=256
pulseShape=1 # 1:CW
actif=0
freq_min=200*1000
freq_max=200*1000
freq=200*1000
Cpx=0 # 0: Sv 
phase=1 # 1: souscription angles
tsd_depth=2 # tsd z - waterlevel
Power=90
Gain=26
SaCorr=0
cel=1494.6
absorp=60.4*10000
BottomMin=8
BottomMax=400
BottomDet=False

# teste fichier hac créé
test=True
print("checking fichier hac créé")
run=os.listdir(chemin_hac)
if len(run)>0:
    if len(os.listdir(chemin_hac + '/' + run[-1]))==0:
        print("****** KO: pas de record hac")
        test=False
else:
    print("****** KO: pas de dossier RUN")
    test=False
test_all= (test & test_all)

# teste fichier raw créé
test_all= (test & test_all)
test=True
print("checking fichier raw créé")
raw=os.listdir(chemin_raw)
if not(len(raw)>0):
    print("****** KO: pas de fichier raw")
    test=False
    
#lecture fichier
mv.moLoadConfig(chemin_config)
mv.moOpenHac(chemin_hac)
mv.moReadChunk()

# teste ping rate (2Hz)
test_all= (test & test_all)
test=True
print("checking ping rate")
if (mv.moGetNumberOfPingFan()>3):
     MX = mv.moGetPingFan(3)
#     t0=MX.m_meanTime.m_TimeCpu+MX.m_meanTime.m_TimeFraction/10000
#     MX = mv.moGetPingFan(1)
#     t1=MX.m_meanTime.m_TimeCpu+MX.m_meanTime.m_TimeFraction/10000
#     test= test & (t1-t0>rec*0.9) & (t1-t0<rec*1.1)
     test= test & (MX.m_pSounder.m_pingInterval==rec)
     if not test:
         print("****** KO: mauvais ping rate")
else:
    print("****** KO: pas de ping")
    test=False


# teste range record
test_all= (test & test_all)
test=True
print("checking range record")
list_sounder = mv.moGetSounderDefinition()
sounder = list_sounder.GetSounder(0)
transducer = sounder.GetTransducer(0)
softChan = transducer.getSoftChannelPolarX(0)
delta = transducer.m_beamsSamplesSpacing
polarMat = MX.GetPolarMatrix(0)
val=np.array(polarMat.m_Amplitude)
range_hac=np.max(np.where(val>-30000))*delta
if range_record>0:
    test= test & (range_hac>range_record*0.9) & (range_hac<range_record*1.1)
elif MX.beam_data[0].m_bottomWasFound:
    range_bottom=MX.beam_data[0].m_bottomRange+bottom_tol
    test= test & (range_hac>range_bottom*0.9) & (range_hac<range_bottom*1.1)
else:
    test= False
    print("****** KO: pas de detection de fond")
if not test: print("****** KO: mauvais range record")

# teste det fond
test_all= (test & test_all)
test=True
print("checking bottom detection")
test= test & (softChan.m_bottomDetectionMaxDepth==BottomMax)
test= test & (softChan.m_bottomDetectionMinDepth==BottomMin)
test= test & (MX.beam_data[0].m_bottomWasFound==BottomDet) #vaut ce que ça vaut (ping 0)
if not test: print("****** KO: mauvais reglage bottom window")

# teste type acquisition (Sv/Cpx, angles)
test_all= (test & test_all)
test=True
print("checking data type (Sv/Cpx)")
dec=sounder.m_soundVelocity*transducer.m_pulseDuration/2/(delta*1000000)
test= (dec*Cpx>=5*Cpx) & (dec*(1-Cpx)<5)
if not test: print("****** KO: mauvais data type (Sv/Cpx)")
test_all= (test & test_all)
test=True
print("checking angles souscription")
test= (len(polarMat.m_AlongAngle)>phase)
if not test: print("****** KO: mauvais phase souscription")


# teste config acoustique (T, passif, CW)
test_all= (test & test_all)
test=True
print("checking pulse length")
test= (transducer.m_pulseDuration==T_us)
if not test: print("****** KO: mauvais pulse length")
test_all= (test & test_all)
test=True
print("checking CW/FM")
test= (transducer.m_pulseShape==pulseShape)
if not test: print("****** KO: mauvais pulse type (CW/FM)")
test_all= (test & test_all)
test=True
print("checking actif/passif")
if actif:
    test= (polarMat.m_Amplitude[3][0]/100>20) 
else:
    test= (polarMat.m_Amplitude[3][0]/100<-20)
if not test: print("****** KO: mauvais pulse mode (actif/passif)")
test_all= (test & test_all)
test=True
print("checking freq")
test= test & (softChan.m_acousticFrequency==freq) 
test= test & (softChan.m_startFrequency==freq_min) 
test= test & (softChan.m_endFrequency==freq_max) 
if not test: print("****** KO: mauvaise frequence")
test_all= (test & test_all)
test=True
print("checking power")
test= (softChan.m_transmissionPower==Power)
if not test: print("****** KO: mauvais Power")
test_all= (test & test_all)
test=True
if pulseShape==1: #only for CW
    print("checking calibration")
    test= (softChan.m_beamGain==Gain)
    if not test: print("****** KO: mauvais gain")
    test_all= (test & test_all)
    test=True
    test= (softChan.m_beamSACorrection==SaCorr)
    if not test: print("****** KO: mauvais SaCorr")

#teste installation tsd
print("checking transducer depth")
test_all= (test & test_all)
test=True
test= (transducer.m_transDepthMeter==tsd_depth)
if not test: print("****** KO: mauvaise transducer depth")
print("checking environment")
test_all= (test & test_all)
test=True
test= (sounder.m_soundVelocity>cel*0.98) & (sounder.m_soundVelocity<cel*1.02)
if not test: print("****** KO: mauvaise celerite")
test_all= (test & test_all)
test=True
test= (softChan.m_absorptionCoef>absorp*0.98) & (softChan.m_absorptionCoef<absorp*1.02)
if not test: print("****** KO: mauvaise absorption")


if test_all:
    print(" ")
    print("==> OK Test Integration Hermes CW_passif")
    mv.moOpenHac(chemin_hac + 'ini') # lache le hac ouvert (pas trouve autrement)
    shutil.rmtree(chemin_hac)
    os.mkdir(chemin_hac)
    shutil.rmtree(chemin_raw)
    os.mkdir(chemin_raw)
else:
    print(" ")
    print("****** KO Test Integration Hermes CW_passif")