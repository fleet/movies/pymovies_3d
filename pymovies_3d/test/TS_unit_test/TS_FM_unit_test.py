# -*- coding: utf-8 -*-
"""
Created on Fri Jan  8 16:24:21 2021

@author: nlebouff

Effectue des tests unitaires de modules Movies3D

    test unitaire FM EK80:
           - effectue la détection de cibles simples sur les fichiers hac du répertoire Ref_data
           - charge les résultats de références data_ref obtenus à l'aide d'une version validée (TS_unit_test_create_result)
           - compare les TScomp, TSrange, TSangles et TSpositions pour chaque transducteur
           - si des résultats différents sont obtenus un message "KO" est indiqué, "OK" sinon
           - si tous les résultats sont concordants, le .pickle et le dossier de résultats sont supprimés, sinon ils sont conservés pour analyse
"""
import os
import shutil
from pymovies_3d.core.TS_analysis.samplecomputeTS import sample_compute_TS
import pickle
import datetime
import pytz
from pathlib import Path

def test_FM_EK80():
    #########################
    # test unitaire FM EK80
    ##########################
    package_directory = os.path.dirname(os.path.abspath(__file__))
    
    test_dir = Path(package_directory).joinpath("Ref_data_FM")
    print(test_dir)
    chemin_ini = str(test_dir)
    chemin_config = test_dir.joinpath("TS_FM_M3D_config_15cm")
    chemin_save = test_dir.joinpath("./Vtest_FM_results")
    utc_tz = pytz.utc
    date = datetime.datetime.now(utc_tz)
    chemin_save = os.path.abspath("./" + date.strftime("%Y-%m-%d-%Hh%M") + "_results")
    data_ref = test_dir.joinpath("Calib_FM_Ref_results_TS.pickle")
    
    # process data
    nameTest = "Calib_FM_" + date.strftime("%Y-%m-%d-%Hh%M")
    sample_compute_TS(str(chemin_ini), str(chemin_config), str(chemin_save), nameTransect=nameTest)
    
    # load results and reference
    [
        timeTargetRef,
        TSrangeRef,
        TScompRef,
        TSucompRef,
        TSalongRef,
        TSathwartRef,
        TSpositionRef,
        TSpositionGPSRef,
        TSlabelRef,
        TSfreqRef,
        name_sdr_tsdRef,
    ] = pickle.load(open(data_ref, "rb"))
    [
        timeTarget,
        TSrange,
        TScomp,
        TSucomp,
        TSalong,
        TSathwart,
        TSposition,
        TSpositionGPS,
        TSlabel,
        TSfreq,
        name_sdr_tsd,
    ] = pickle.load(open(chemin_save + "/" + nameTest + "_results_TS.pickle", "rb"))
    
    Test_TS_OK = True
    
    # compare results to reference
    print(" ")
    print(" ")
    print(" ******Resultats TS FM*******")
    for isdr in range(len(name_sdr_tsdRef)):
        for itsd in range(len(name_sdr_tsdRef[isdr])):
            TScomp_test = TScompRef[isdr][itsd] == TScomp[isdr][itsd]
            if not TScomp_test.all:
                print(
                    "KO: Echec test TScomp, sondeur "
                    + str(isdr)
                    + " transducteur "
                    + str(itsd)
                )
                Test_TS_OK = False
            else:
                print(
                    "OK: Succes test TScomp, sondeur "
                    + str(isdr)
                    + " transducteur "
                    + str(itsd)
                )
            TSalong_test = TSalongRef[isdr][itsd] == TSalong[isdr][itsd]
            if not TSalong_test.all:
                print(
                    "KO: Echec test TSalong, sondeur "
                    + str(isdr)
                    + " transducteur "
                    + str(itsd)
                )
                Test_TS_OK = False
            else:
                print(
                    "OK: Succes test TSalong, sondeur "
                    + str(isdr)
                    + " transducteur "
                    + str(itsd)
                )
            TSathwart_test = TSathwartRef[isdr][itsd] == TSathwart[isdr][itsd]
            if not TSathwart_test.all:
                print(
                    "KO: Echec test TSathwart, sondeur "
                    + str(isdr)
                    + " transducteur "
                    + str(itsd)
                )
                Test_TS_OK = False
            else:
                print(
                    "OK: Succes test TSathwart, sondeur "
                    + str(isdr)
                    + " transducteur "
                    + str(itsd)
                )
            TSrange_test = TSrangeRef[isdr][itsd] == TSrange[isdr][itsd]
            if not TSrange_test.all:
                print(
                    "KO: Echec test TSrange, sondeur "
                    + str(isdr)
                    + " transducteur "
                    + str(itsd)
                )
                Test_TS_OK = False
            else:
                print(
                    "OK: Succes test TSrange, sondeur "
                    + str(isdr)
                    + " transducteur "
                    + str(itsd)
                )
            TSpositionGPS_test = TSpositionGPSRef[isdr][itsd] == TSpositionGPS[isdr][itsd]
            if not TSpositionGPS_test.all:
                print(
                    "KO: Echec test TSpositionGPS, sondeur "
                    + str(isdr)
                    + " transducteur "
                    + str(itsd)
                )
                Test_TS_OK = False
            else:
                print(
                    "OK: Succes test TSpositionGPS, sondeur "
                    + str(isdr)
                    + " transducteur "
                    + str(itsd)
                )
            TSucomp_test = TSucompRef[isdr][itsd] == TSucomp[isdr][itsd]
            if not TSucomp_test.all:
                print(
                    "KO: Echec test TSucomp, sondeur "
                    + str(isdr)
                    + " transducteur "
                    + str(itsd)
                )
                Test_TS_OK = False
            else:
                print(
                    "OK: Succes test TSucomp, sondeur "
                    + str(isdr)
                    + " transducteur "
                    + str(itsd)
                )
            TScomp_test = TScompRef[isdr][itsd] == TScomp[isdr][itsd]
            if not TScomp_test.all:
                print(
                    "KO: Echec test TScomp, sondeur "
                    + str(isdr)
                    + " transducteur "
                    + str(itsd)
                )
                Test_TS_OK = False
            else:
                print(
                    "OK: Succes test TScomp, sondeur "
                    + str(isdr)
                    + " transducteur "
                    + str(itsd)
                )
            TSfreq_test = TSfreqRef[isdr][itsd] == TSfreq[isdr][itsd]
            if not TSfreq_test.all:
                print(
                    "KO: Echec test TSfreq, sondeur "
                    + str(isdr)
                    + " transducteur "
                    + str(itsd)
                )
                Test_TS_OK = False
            else:
                print(
                    "OK: Succes test TSfreq, sondeur "
                    + str(isdr)
                    + " transducteur "
                    + str(itsd)
                )
        assert Test_TS_OK
        if Test_TS_OK:
            shutil.rmtree(chemin_save)
