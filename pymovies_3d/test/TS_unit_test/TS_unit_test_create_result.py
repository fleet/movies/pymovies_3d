# -*- coding: utf-8 -*-
"""
Created on Fri Jan  8 16:24:21 2021

@author: nlebouff
"""

import os
from pymovies_3d.core.TS_analysis.samplecomputeTS import sample_compute_TS

# test CW EK80
chemin_ini = os.path.abspath("./Ref_data/")
chemin_config = os.path.abspath("./Ref_data/TS_det_config/")
chemin_save = os.path.abspath("./Ref_data/")

print("Running sample_compute_TS")
sample_compute_TS(
    chemin_ini, chemin_config, chemin_save, nameTransect="Calib_CW_Ref"
)

# test FM EK80
chemin_ini = os.path.abspath("./Ref_data_FM/")
chemin_config = os.path.abspath("./Ref_data_FM/TS_FM_M3D_config_15cm/")
chemin_save = os.path.abspath("./Ref_data_FM/")

print("Running sample_compute_TS for FM")
sample_compute_TS(
    chemin_ini, chemin_config, chemin_save, nameTransect="Calib_FM_Ref"
)
