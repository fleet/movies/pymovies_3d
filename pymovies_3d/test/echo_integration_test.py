from glob import glob
from pathlib import Path
from pyMovies import pyMovies as mv
import tempfile as tmp

from pymovies_3d.core.echointegration import ei_survey as ei
import pandas as pd



def test_ei():
    #load test dir
    output_dir = Path(tmp.mkdtemp())
    test_dir = Path("pymovies_3d/test/TS_unit_test/Ref_data/")
    config_dir = Path("pymovies_3d/test/TS_unit_test/Ref_data/TS_ei_config")
    #test_dir = Path("Ref_data/")
    file_name = test_dir.joinpath("ESTECH22_012_20220109_132006.hac").absolute()
    assert file_name.exists(), f"File {file_name} does not exist"
    file = mv.moOpenHac(str(file_name))
    FileStatus = mv.moGetFileStatus()
        

    datetime_start = pd.to_datetime("2022/01/09 13:20:06")
    datetime_end =   pd.to_datetime("2022/01/09 13:20:33")
    start_time = datetime_start.strftime("%H:%M:%S")
    start_date = datetime_start.strftime('%d/%m/%Y')
    end_time = datetime_end.strftime("%H:%M:%S")
    end_date = datetime_end.strftime('%d/%m/%Y')
    ei.ei_survey_transects_netcdf_cpp(path_hac_survey=test_dir.absolute().as_posix(),
                                    path_config=config_dir.as_posix(),path_save=output_dir.as_posix(),
                                    date_start=start_date,
                                    date_end=end_date,
                                    time_start=start_time,
                                    time_end=end_time,
                                    name_transects="TRANSECT",skip_existing=True)
    

    #TODO check if file exists
    pattern =  output_dir.absolute().as_posix() + '/TRANSECT*.nc'
    file_list: list[str] = glob(pattern)
    assert len(file_list) >0 
