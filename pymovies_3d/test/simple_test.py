import os
from pathlib import Path
from pyMovies import pyMovies as mv


def test_import_movies3D():
    #very sample test opening a hac file
    test_dir = Path("pymovies_3d/test/TS_unit_test/Ref_data/")
    #test_dir = Path("Ref_data/")
    file_name = test_dir.joinpath("ESTECH22_012_20220109_132006.hac").absolute()
    assert file_name.exists(), f"File {file_name} does not exist"
    file = mv.moOpenHac(str(file_name))
    FileStatus = mv.moGetFileStatus()
    # check that FileStatus exist and is not None
    assert FileStatus is not None
    assert not FileStatus.m_StreamClosed
