import os
import pytest
import shutil

from pathlib import Path
from pymovies_3d.core.survey.preprocessing.polygon import write_polygon,read_one_polygon

def test_write_polygon(tmp_path):
    # Create a temporary file path
    filename = tmp_path / "test_polygon.csv"

    polygon_A = Path(__file__).parent / Path("polygon_A.csv")
    filename = polygon_A.parent / "test_polygon.csv"    

    assert polygon_A.exists()

    polygon = read_one_polygon(polygon_A)


    # Write the polygon to the temporary file
    write_polygon(polygon, filename)

    # Assert that the file exists
    assert filename.exists()

    # Assert that the file is not empty
    assert filename.stat().st_size > 0

    # Read the contents of the file
    polygon_written = read_one_polygon(filename)
    
    # Assert that the written polygon is equal to the original polygon
    assert polygon_written == polygon

    try:
        os.remove(filename)
        #remove temporary directory
        shutil.rmtree(tmp_path) 
    except: 
        print(f"File {filename} not found, skipping deletion of file")
    
  
