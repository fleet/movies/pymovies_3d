#!/bin/bash

#install mamba if not already done

conda activate base

conda install mamba -c conda-forge

mamba env remove --name movies_env

mamba env create -f requirements_linux.yml --name movies_env

#set LD_LIBRARY_PATH when activating environment, it seems not done by mamba
cp env_vars.sh $CONDA_PREFIX/envs/movies_env/etc/conda/activate.d
