REM install mamba if not already done
call conda install mamba -c conda-forge -y


REM echo force remove directory %CONDA_PREFIX%\envs\movies_env
REM call rmdir /S /Q %CONDA_PREFIX%\envs\movies_env

REM echo mamba env create -f requirements.yml --name movies_env
call conda env remove -n movies_env
call mamba env create -f requirements.yml --name movies_env


