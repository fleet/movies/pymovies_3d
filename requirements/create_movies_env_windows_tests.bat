REM install mamba if not already done
call conda install mamba -c conda-forge -y


call conda env remove -n movies_env
echo force remove directory %CONDA_PREFIX%\envs\movies_env
call rmdir /S /Q %CONDA_PREFIX%\envs\movies_env

REM echo mamba env create -f requirements.yml --name movies_env
call mamba env create -f requirements_test.yml --name movies_env


