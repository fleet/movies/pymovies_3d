from distutils.core import setup

setup(
    name='Movies3D python scripts',
    version='0.1.0',
    author='L.Berger',
    author_email='laurent.berger@ifremer.fr',
    packages=['pymovies_3d'],
    url='http://pypi.python.org/pypi/TowelStuff/',
    license='LICENSE',
    description='Movies3D python scripts',
    long_description=open('README.adoc').read(),
)